@extends('layouts.admin')
@section('content')
<div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <select class="selectpicker" name="period" data-style="btn-default btn-sm">
                        <option value="today" selected>اليوم</option>
                        <!--<option value="yesterday">الامس</option>
                        <option value="last_7d">أخر 7 ايام</option>-->
                    </select>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib"> لوحة التحكم </span>
                </h1>
           </div>
           <div class="row gutter-xs">
            <div class="col-md-6 col-lg-3 col-lg-push-0">
              <div class="card bg-primary">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-primary-inverse circle sq-48">
                        <span class="icon icon-user"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">{{$client}}</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-push-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-success-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">طلبات التغليف</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">{{ $packing }}</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-3">
              <div class="card bg-info">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-info-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">طلبات الشراء</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">{{$item1}}</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-0">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-danger-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">خدمات اضافية</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">{{$image}}</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row gutter-xs">
            <h4> طلبات التغليف</h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>كود الشحنة</th>
                                <th> عدد المنتجات	</th>
                                <th>رقم العميل</th>
                                <th>تاريخ الطلب</th>
                                <th>نوع التغليف </th>
                                <th>عنوان الشحن</th>
                                <th>شركة الشحن</th>
                                <th>خدمات اضافية</th>
                                <th> حالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packings as $packing)
                                <tr>
                                    <td>{{ $packing->code_parcel }}</td>
                                    <td>{{ $packing->Nbpackage }}</td>
                                    <td><a href="{{ url('control/clients',$packing->getUser->policia)}}" target="_blank">{{ $packing->getUser->policia }}</a></td>
                                    <td>{{ $packing->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        @if($packing->modepacking =="withpack")
                                            بتغليف المصنع
                                        @else
                                             ازالة التغليف
                                        @endif
                                    </td>
                                    <td style="text-align: left;">
                                        {{ $packing->getAddress->adress }} <br/>
                                        {{ $packing->getAddress->province }} |  {{ $packing->getAddress->city }}<br/>
                                        {{ $packing->getAddress->phone }} <br/>
                                        {{ $packing->getAddress->getcountry->countriename }} <br/>
                                    </td>
                                    <td>{{ $packing->getCompany->companiename }}</td>
                                    <td>
                                        @foreach ($packing->getchild as $child)
                                            {{ $child->getService->service_name }} <br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        <span class="label label-warning"> قيد التجهيز </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> انتظار الشحن</h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>الشحنة</th>
                                <th> العدد	</th>
                                <th>رقم العميل</th>
                                <th>تاريخ الطلب</th>
                                <th>نوع التغليف </th>
                                <th>عنوان الشحن</th>
                                <th>شركة الشحن</th>
                                <th> الوزن / الأبعاد </th>
                                <th>خدمات اضافية</th>
                                <th> المجموع</th>
                                <th> حالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($waiting as $packing)
                                <tr class="{{ $packing->id }}">
                                    <td>{{ $packing->code_parcel }}</td>
                                    <td>{{ $packing->Nbpackage }}</td>
                                    <td><a href="{{ url('control/clients',$packing->getUser->policia)}}" target="_blank">{{ $packing->getUser->policia }}</a></td>
                                    <td>{{ $packing->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        @if($packing->modepacking =="withpack")
                                            بتغليف المصنع
                                        @else
                                             ازالة التغليف
                                        @endif
                                    </td>
                                    <td style="text-align: left;">
                                        {{ $packing->getAddress->adress }} <br/>
                                        {{ $packing->getAddress->province }} |  {{ $packing->getAddress->city }}<br/>
                                        {{ $packing->getAddress->phone }} <br/>
                                        {{ $packing->getAddress->getcountry->countriename }} <br/>
                                    </td>
                                    <td class="company_name">{{ $packing->getCompany->companiename }}</td>
                                    <td>
                                        {{ $packing->weight}} كيلو <br/>
                                        {{ $packing->length}} * {{ $packing->width}} * {{ $packing->height}} سم <br/>
                                    </td>
                                    <td>
                                        @foreach ($packing->getchild as $child)
                                            {{ $child->getService->service_name }} <br/>
                                            {{ $child->getService->service_price }}$ <br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $packing->pricetotal }}$
                                    </td>
                                    <td>
                                        <span class="label label-warning"> انتظار الشحن </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> خدمات اضافية </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th> رقم الطلب</th>
                                <th> كود الشحنة</th>
                                <th>رقم العميل</th>
                                <th>تاريخ الطلب  </th>
                                <th>التكلفة</th>
                                <th>وصف الخدمة</th>
                                <th> حالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($services as $service)
                                <tr>
                                    <td>{{ $service->code_service }}</td>
                                    <td>{{ $service->getPship->codeparcel }}</td>
                                    <td><a href="{{ url('control/clients',$service->getuser->policia)}}" target="_blank">{{ $service->getuser->policia }}</a></td>
                                    <td>{{ $service->updated_at->format('Y-m-d') }}</td>
                                    <td> {{ $service->getPservice->service_price }}$</td>
                                    <td>
                                        {{ $service->information}} <br/>
                                        <b style="color:red"> {{ $service->getPservice->service_name }}</b>
                                    </td>
                                    <td>
                                        @if($service->status==0)
                                            <span class='label label-warning'> قيد التنفيد </span>
                                        @elseif($service->status==1)
                                            <span class='label label-info'> تم التنفيد </span>
                                        @elseif($service->status==2)
                                            <span class='label label-success'> تم الدفع </span>
                                        @elseif($service->status==3)
                                          <span class='label label-primary'> ملغي </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> خدمات ارجاع </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th> رقم الطلب </th>
                                <th> كود الشحنة </th>
                                <th>رقم العميل</th>
                                <th>تاربخ الطلب </th>
                                <th> عنوان البائع </th>
                                <th>سبب الارجاع </th>
                                <th> التكلفة </th>
                                <th> الحالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($parcels as $parcel)
                                <tr>
                                    <td>{{$parcel->GetReturn->code_return}}</td>
                                    <td>{{ $parcel->codeparcel}}</td>
                                    <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
                                    <td>{{ $parcel->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                        {{ $parcel->GetReturn->seller_address }}
                                    </td>
                                    <td> {{ $parcel->GetReturn->return_info }}</td>
                                    <td>
                                        @if($parcel->GetReturn->status!=0)
                                            {{ $parcel->GetReturn->pricetopay }}$
                                        @else
                                            <b>غير محددة </b>
                                        @endif
                                    </td>
                                    <td>
                                        @if($parcel->GetReturn->status==0)
                                            <span class="label label-danger"> قيد المراجعة </span>
                                        @elseif($parcel->GetReturn->status==1)
                                            <span class="label label-info"> انتظار الدفع</span>
                                        @elseif($parcel->GetReturn->status==2)
                                            <span class="label label-warning"> انتظار الشحن </span>
                                        @elseif($parcel->GetReturn->status==3)
                                            <span class="label label-success"> تم الشحن </span>
                                        @endif
                                    </td>
                                </tr>
                           @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> طلبات شراء جديدة </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>كود الطلب</th>
                                <th> رقم العميل</th>
                                <th>تاريخ الطلب </th>
                                <th>عدد المنتجات </th>
                                <th>السعر الاجمالي</th>
                                <th>حالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders_new as $order)
                                <tr>
                                    <td>{{ $order->code_order }}</td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $order->count_product }}</td>
                                    <td>{{ $order->price_total }} {{ $order->currency }}</td>
                                    <td><span class='label label-warning'> قيد المراجعة </span></td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> طلبات  قيد الشراء </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>كود الطلب</th>
                                <th> رقم العميل</th>
                                <th>تاريخ الطلب </th>
                                <th>عدد المنتجات </th>
                                <th>السعر الاجمالي</th>
                                <th>حالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order_process as $order)
                                <tr>
                                    <td>{{ $order->code_order }}</td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $order->count_product }}</td>
                                    <td>{{ $order->price_total }}{{ $order->currency }}</td>
                                    <td><span class='label label-success'> قيد التنفيد </span></td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4> جديد طلبات الشحن المباشر </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>كود الطلب</th>
                                <th> عدد الشحنات	</th>
                                <th>رقم العميل</th>
                                <th>تاريخ الطلب</th>
                                <th>مكان الاستلام</th>
                                <th> السعر </th>
                                <th> الحالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($new_direct as $order)
                                <tr>
                                    <td> {{ $order->code_order }} </td>
                                    <td> {{ $order->getchild->count() }} </td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td> {{ $order->created_at->format('Y-m-d') }} </td>
                                    <td>
                                        {{ $order->getcollect->firstname }}  {{ $order->getcollect->lastname }} <br/>
                                        {{ $order->getcollect->province }} {{ $order->getcollect->city }} <br/>
                                        {{ $order->getcollect->zipcode }} <br/>
                                        {{ $order->getcollect->address }} <br/>
                                        {{ $order->getcollect->phone }} <br/>
                                        {{ $order->getcollect->ship_country }} <br/>
                                    </td>
                                    <td>  {{ $order->pricetopay }}$ </td>
                                    <td>
                                        @if($order->status == 1)
                                            <span class='label label-success'> انتظار الاستلام </span>
                                        @elseif($order->status == 2)
                                            <span class='label label-info'> تم الاستلام </span>
                                        @elseif($order->status == 3)
                                            <span class='label label-info'> وصلت الشحنة </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4>   طلبات الشحن المباشر المستلمة</h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>كود الطلب</th>
                                <th> عدد الشحنات	</th>
                                <th>رقم العميل</th>
                                <th>تاريخ الطلب</th>
                                <th>مكان الاستلام</th>
                                <th> السعر </th>
                                <th> الحالة </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rec_direct as $order)
                                <tr class="{{ $order->id }}">
                                    <td> {{ $order->code_order }} </td>
                                    <td> {{ $order->getchild->count() }} </td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td> {{ $order->created_at->format('Y-m-d') }} </td>
                                    <td>
                                        {{ $order->getcollect->firstname }}  {{ $order->getcollect->lastname }} <br/>
                                        {{ $order->getcollect->province }} {{ $order->getcollect->city }} <br/>
                                        {{ $order->getcollect->zipcode }} <br/>
                                        {{ $order->getcollect->address }} <br/>
                                        {{ $order->getcollect->phone }} <br/>
                                        {{ $order->getcollect->ship_country }} <br/>
                                    </td>
                                    <td>
                                        @if($order->company_url)
                                            <a href="{{ $order->company_url }}" target="_blank"> URL </a> <br/>
                                            {{ $order->tracking_number }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>  {{ $order->pricetopay }}$ </td>
                                    <td>
                                        @if($order->status == 1)
                                            <span class='label label-success'> انتظار الاستلام </span>
                                        @elseif($order->status == 2)
                                            <span class='label label-info'> تم الاستلام </span>
                                        @elseif($order->status == 3)
                                            <span class='label label-info'> وصلت الشحنة </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
            <h4>  العملاء المسجلين في اخر 7 ايام </h4>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables2" class="table table-middle nowrap">
                        <thead>
                            <tr>
                                <th>رقم العميل</th>
                                <th>اسم العميل</th>
                                <th>الأيميل</th>
                                <th>الدولة</th>
                                <th>المدينة</th>
                                <th>الهاتف</th>
                                <th>عضو مند </th>
                                <th>الحالة</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clients as $client )
                                <tr id="{{ $client->id }}">
                                    <td class="maw-320">
                                        <span class="truncate">
                                            {{ $client->policia }}
                                        </span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate"> {{ $client->firstname }} {{ $client->lastname }}</span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate"> {{ $client->email }}</span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate">{{ $client->getcountry->countriename }}</span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate"> {{ $client->city }}</span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate"> {{ $client->phone }}</span>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate"> {{ $client->created_at->format('Y-m-d') }}</span>
                                    </td>
                                    <td>
                                        @if($client->status == 0 )
                                            <span class="label label-info label-pill">غير مفعل</span>
                                        @elseif($client->status == 1)
                                            <span class="label label-success label-pill">نشيط</span>
                                        @else
                                            <span class="label label-primary label-pill">موقف</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end-->
          </div>
        </div>
    </div>
 </div>
@endsection
