@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> المعاملات المالية </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
        <div class="row" style="margin-top: 20px;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                        <div id="demo-dynamic-tables-2_filter" class="dataTables_filter">
                            <label><input type="search" id="search" class="form-control input-sm searchtransaction" placeholder="البحث عن معاملة" aria-controls="demo-dynamic-tables-2"></label>
                        </div>
                    </div>
                </div>
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
					<table id="demo-dynamic-tables+2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>اسم العميل</th>
						  	<th>الأيميل</th>
                            <th>رقم المعاملة</th>
                            <th>مبلغ المعاملة</th>
                            <th>مبلغ معاد</th>
                            <th>تاريخ المعاملة</th>
                            <th>عدد الطلبات </th>
                            <th>وسيلة الدفع  </th>
						  	<th></th>
                        </tr>
                      </thead>
                      <tbody class="">
                            @foreach ($transaction as  $transact )
                                <tr>
                                    <td>{{ $transact->id }}</td>
                                    <td>{{ $transact->getUser->firstname }} {{ $transact->getUser->lastname }}</td>
                                    <td>{{ $transact->getUser->email }}</td>
                                    <td>{{ $transact->transaction_code }}</td>
                                    <td>{{ $transact->transaction_amount }}{{ $transact->currency }}</td>
                                    <td>{{ $transact->transaction_refund }}{{ $transact->currency }}</td>
                                    <td>{{ $transact->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $transact->transaction_orders }}</td>
                                    <td>{{ $transact->transaction_payment }}</td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                          <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                            <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                          </button>
                                          <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" class="btn-link showOrders " id="{{ $transact->id }}"> مشاهدة  التفاصيل </a></li>
                                                <li><a href="#" class="btn-link refundtoclient" id="{{ $transact->id }}"> اعادة مبلغ  </a></li>
                                          </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
