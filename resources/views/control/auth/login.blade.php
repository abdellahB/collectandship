@extends('layouts.login')

@section('content')
    <div class="login">
      <div class="login-body">
        <span class="text-center login-brand" style="width:100%">
            <h3 style="font-weight: bold; margin-bottom: 40px;">  تسجيل الدخول </h3>
        </span>
        <div class="login-form">
            <form method="POST" action="{{route('control.auth.login')}}" data-toggle="validator">
                <div class="form-group">
                    <label for="email">البريد الالكتروني</label>
                    <input id="email" class="form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="من فصلك ادخل بريدك الالكتروني" required>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="password">كلمة المرور</label>
                    <input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="كلمة المرور يجب ان تكون 6 حروف أو أكثر " data-msg-required="من فضلك ادخل كلمة المرور" required>
                </div>
                <div class="form-group">
                    <label class="custom-control custom-control-primary custom-checkbox">
                        <input class="custom-control-input" type="checkbox" checked="checked">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-label">تذكرني على الجهاز </span>
                    </label>
                    <span aria-hidden="true"> · </span>
                    <a href="">نسيت كلمة المرور ؟</a>
                </div>
                <style>
                    .g-recaptcha div {
                        width: auto !important;
                    }
                </style>
                <div class="form-group">
                    <div class="col-12 text-center">
                        {!! app('captcha')->display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit" name="submit">تسحيل الدخول</button>
            </form>
        </div>
      </div>
    </div>
@endsection
