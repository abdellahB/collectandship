@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> قيد التنفيد </h2>
			</div>
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table id="demo-dynamic-tables-2" class="table table-middle">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
							<th>كود الطلب</th>
                            <th> رقم العميل</th>
                            <th>اسم العميل</th>
                            <th>الأيميل</th>
							<th>تاريخ الطلب </th>
							<th>عدد المنتجات </th>
							<th>السعر الاجمالي</th>
						    <th>حالة </th>
						    <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($Orders as $order)
                                <tr>
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAll[]" type="checkbox" value="">
                                              <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>{{ $order->code_order }}</td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td>{{ $order->getuser->firstname }} {{ $order->getuser->lastname }}</td>
                                    <td>{{ $order->getuser->email }}</td>
                                    <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $order->count_product }}</td>
                                    <td>{{ $order->price_total }}{{ $order->currency }}</td>
                                    <td><span class='label label-success'> قيد التنفيد </span></td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" class="btn-link fastbuyetat_ok" id="{{ $order->id }}"> مراجعة المنتجات </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
