@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> تعديل معلومات المستخدم </h2>
                <a href="{{url('control/users')}}" class="btn btn-success pull-right"> كـل المستخدمين  </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/users/edit',$admin->id)}}" enctype="multipart/form-data">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="default prepend-icon" >الاسم الاول</label>
                    <input id="firstname" class="form-control" type="text" name="firstname" value="{{ $admin->firstname }}" tabindex="2" required="true">
                </div>
                <div class="form-group">
                    <label class="default prepend-icon" >الاسم الثاني</label>
                       <input id="lastname" class="form-control" type="lastname"  value="{{ $admin->lastname }}" name="lastname" tabindex="2" required="true">
               </div>
               <div class="form-group">
                    <label class="default prepend-icon" >البريد الالكتروني</label>
                   <input id="email" class="form-control" type="email" value="{{ $admin->email }}" name="email" tabindex="2" required="true">
               </div>
                <div class="form-group">
                    <label class="default prepend-icon" >اختر الرتبة</label>
                    <select name="role" class="form-control" tabindex="1" required="true">
                        <option value="">اختر الرتبة</option>
                        <option value="admin" @if($admin->role == 'admin') selected @endif>مدير عام</option>
                        <option value="employers" @if($admin->role == 'employers') selected @endif>موظف</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="default prepend-icon" >كلمة المرور</label>
                    <input id="password" class="form-control" type="password" name="password">
                </div>
               <div class="form-group">
                   <label class="default prepend-icon" >صورة شخصية</label>
                   <input type="file" name="image"  class="form-control">
               </div>
                @csrf
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block"> تعديل المعلومات  </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
