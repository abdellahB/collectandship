@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> وسائل الدفع </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form method="POST" action="{{ url('control/modepay')}}" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-6">
                        <h4> بايبال </h4>
                        <div class="form-group">
                            <label class="default prepend-icon" >  client_id رقم العميل </label>
                            <input type="hidden" name="id" value="{{ $general->id }}"/>
                            <input  class="form-control" type="text" name="paypal_client_id" value="{{ $general->paypal_client_id }}" required="true">
                        </div>
                        <div class="form-group">
                             <label class="default prepend-icon" > الرمز السري</label>
                             <input class="form-control" type="text" name="paypal_secret" value="{{ $general->paypal_secret }}" required="true">
                        </div>
                        @csrf
                        <div class="form-group">
                            <label class="default prepend-icon" > حالة الوسيلة </label>
                            <select class="form-control" name="paypal_mode" required="true">
                                <option value="sandbox" @if($general->paypal_mode == "sandbox") selected @endif> sandbox </option>
                                <option value="live" @if($general->paypal_mode == "live") selected @endif> Live </option>
                            </select>
                       </div>
                    </div>
                    <div class="col-lg-6">
                        <h4> Paytabs </h4>
                        <div class="form-group">
                            <label class="default prepend-icon" >ايميل الحساب </label>
                            <input class="form-control" type="text" name="paytabs_merchant_email" value="{{ $general->paytabs_merchant_email }}" required="true">
                        </div>
                        <div class="form-group">
                            <label class="default prepend-icon" >الرمز السري </label>
                            <input class="form-control" type="text" name="paytabs_secret_key"  value="{{ $general->paytabs_secret_key }}" required="true">
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-info btn-block">تحديث المعلومات</button>
                        </div>
                    </div>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection
