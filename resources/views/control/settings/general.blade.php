@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">معلومات الموقع</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form method="POST" action="{{ url('control/general')}}" enctype="multipart/form-data">
                <ul class="nav nav-tabs nav-addcat" role="tablist">
                    <li role="presentation" class="active"><a href="#ar-lang" aria-controls="ar-lang" role="tab" data-toggle="tab">العربية </a></li>
                    <li role="presentation"><a href="#en-lang" aria-controls="en-lang" role="tab" data-toggle="tab">الأنجليزية</a></li>
                </ul>
                <div class="tab-content">
                    <!--en-longe-->
                    <div role="tabpanel" class="tab-pane active" id="ar-lang">
                        <br/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="default prepend-icon" >اسم الموقع</label>
                                    <input type="hidden" name="id" value="{{ $general->id }}"/>
                                    <input type="hidden" name="logo" value="{{ $general->sitelogo }}"/>
                                    <input id="sitename" class="form-control" type="text" name="sitename" placeholder="اسم الموقع" value="{{ $general->sitename }}" required="true">
                                </div>
                                <div class="form-group">
                                     <label class="default prepend-icon" >فيس بوك</label>
                                     <input id="facebook" class="form-control" type="text" name="facebook" placeholder="فيس بوك" value="{{ $general->facebook }}" required="true">
                                </div>
                                @csrf
                                <div class="form-group">
                                    <label class="default prepend-icon" >انستغرام</label>
                                    <input id="instagram" class="form-control" type="text" name="instagram" placeholder="انستغرام" value="{{ $general->instagram }}" required="true">
                               </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >رقم الهاتف</label>
                                    <input id="phone" class="form-control" type="text" name="phone" placeholder="رقم الهاتف" value="{{ $general->phone }}" required="true">
                                </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >اوقات العمل</label>
                                    <input id="working" class="form-control" type="text" name="working" placeholder="اوقات العمل" value="{{ $general->working }}" required="true">
                               </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" > عنوان على الخريطة iframe </label>
                                    <input id="maps_iframe" class="form-control" type="text" name="maps_iframe" placeholder="عنوان على الخريطة iframe" value="{{ $general->maps_iframe }}" required="true">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                     <label class="default prepend-icon" >شعار الموقع</label>
                                     <input id="sitelogo" class="form-control" type="file" name="sitelogo" tabindex="5">
                                </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >تويتر</label>
                                    <input id="twitter" class="form-control" type="text" name="twitter" placeholder="تويتر" value="{{ $general->twitter }}" required="true">
                                </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >الدولة</label>
                                    <input id="countrie" class="form-control" type="text" name="countrie" placeholder="الدولة" value="{{ $general->countrie }}" required="true">
                               </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >العنوان</label>
                                    <input id="address" class="form-control" type="text" name="address" placeholder="العنوان" value="{{ $general->address }}" required="true">
                               </div>
                               <div class="form-group">
                                    <label class="default prepend-icon" >سعر الشحن المباشر</label>
                                    <input class="form-control" type="text" name="taux_directship" placeholder="عمولة الشراء" value="{{ $general->taux_directship }}" required="true">
                                </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >البريد الالكتروني</label>
                                    <input id="contact_email" class="form-control" type="email" name="contact_email" placeholder="البريد الالكتروني" value="{{ $general->contact_email }}" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="default prepend-icon" >وصف الموقع</label>
                                    <textarea name="description" placeholder="وصف الموقع" class="form-control" rows="5">
                                        {{ $general->description }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-info btn-block">تحديث المعلومات</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="en-lang">
                        <br/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="default prepend-icon" >اسم الموقع</label>
                                    <input id="sitename" class="form-control" type="text" name="sitename_en" placeholder="اسم الموقع" value="{{ $general->sitename_en }}" required="true">
                                </div>
                                <div class="form-group">
                                    <label class="default prepend-icon" >الدولة</label>
                                    <input id="countrie_en" class="form-control" type="text" name="countrie_en" placeholder="الدولة" value="{{ $general->countrie_en }}" required="true">
                               </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="default prepend-icon" >العنوان</label>
                                    <input id="address" class="form-control" type="text" name="address_en" placeholder="العنوان" value="{{ $general->address_en }}" required="true">
                               </div>
                               <div class="form-group">
                                    <label class="default prepend-icon" >اوقات العمل</label>
                                    <input id="working_en" class="form-control" type="text" name="working_en" placeholder="اوقات العمل" value="{{ $general->working_en }}" required="true">
                               </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="default prepend-icon" >وصف الموقع</label>
                                    <textarea name="description_en" placeholder="وصف الموقع" class="form-control" rows="5">
                                        {{ $general->description_en }}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection
