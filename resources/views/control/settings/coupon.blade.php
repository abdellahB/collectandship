@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">كوبون تخفيض </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
			<div class="col-lg-12">
				<div class="col-lg-2 form-group">
					<input type="text" class="form-control couponccode" id="couponccode" readonly name="couponccode"placeholder="كوبو التخفيض"/>
					<button type="button" class="btn btn-success" onClick="generating(5)" style="position:absolute;top:0px;left:0;"> انشاء</button>
				</div>
				<div class="col-lg-2 form-group">
					<input type="number" class="form-control couponprice" name="couponprice" placeholder="القيمة او النسبة"/>
				</div>
				<div class="col-lg-1 form-group">
					<select name="coupontype" id="coupontype" class="form-control coupontype" style="width: 70px;">
						<option value="">اختر</option>
						<option value="pourcent">نسبة</option>
						<!--<option value="fixed">مبلغ</option>-->
					</select>
				</div>
				<div class="col-lg-2 form-group">
					<input type="number" class="form-control couponutilisation" name="couponutilisation" placeholder="عدد الاستخدامات"/>
				</div>
				<div class="col-lg-2 form-group">
					<div class="input-with-icon">
					  <input class="form-control datetostart" type="text" data-date-format="yyyy-mm-dd" data-provide="datepicker" placeholder="تاربخ البدء">
					  <span class="icon icon-calendar input-icon"></span>
					</div>
				</div>
				<div class="col-lg-2 form-group">
					<div class="input-with-icon">
					  <input class="form-control datetoend" type="text" data-date-format="yyyy-mm-dd" data-provide="datepicker" placeholder="تاريخ الانتهاء">
					  <span class="icon icon-calendar input-icon"></span>
					</div>
				</div>
				<div class="col-lg-1 form-group">
					<input type="submit" class="btn btn-success form-control addcoupons" name="submit" value="اضافة"/>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
						<thead>
							<tr>
							  <th>كوبون التخفيض</th>
							  <th>القيمة او النسبة</th>
							  <th>العدد المطلوب</th>
							  <th>العدد المستخدم</th>
							  <th>تاريخ البدء </th>
							  <th>تاريخ الانتهاء</th>
							   <th>الحالة</th>
							  <th></th>
							</tr>
						</thead>
                        <tbody>
                            @foreach ($coupons as $coupon)
                            <tr>
                              <td>{{ $coupon->discountcode}}</td>
                              <td>{{ $coupon->amount}}%</td>
                              <td>{{ $coupon->numberperson}}</td>
                              <td>{{ $coupon->numberutiliser}}</td>
                              <td>{{ $coupon->startdate}}</td>
                              <td>{{ $coupon->enddate}}</td>
                              <td>
                                  <span>
                                      @if($coupon->status == 0)
                                        <p class="label label-success">  نشيط </p>
                                      @elseif($coupon->status == -1)
                                         <p class="label label-danger"> غير نشيط </p>
                                      @endif
                                  </span>
                              </td>
                              <td>
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                        <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if($coupon->status == 0)
                                                <li>
                                                    <a href="javascript:void(0)" id="{{ $coupon->id}}"  class="dropdown-item operDiscount 1"> ايقاف  </a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="javascript:void(0)" id="{{ $coupon->id}}"  class="dropdown-item operDiscount 2"> تنشيط </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                              </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
	</div>
</div>
<script>
	function generating(length) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		document.getElementById("couponccode").value = result.toUpperCase();
	}
</script>
@endsection
