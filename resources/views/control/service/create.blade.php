@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> اضافة خدمة جديدة </h2>
                <a href="{{url('control/services')}}" class="btn btn-success pull-right"> كـل الحدمات  </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/services/create')}}" enctype="multipart/form-data">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label> نوع الخدمة </label>
                    <select  name="type_service" class="form-control" required >
                        <option value="packing">خدمات التجميع </option>
                        <option value="product"> خدمات على المنتجات</option>
                    </select>
                </div>
                <div class="form-group">
                    <label> اسم الخدمة</label>
                    <input type="text" name="service_name" placeholder="اسم الخدمة" id="" class="form-control" required >
                </div>
                <div class="form-group">
                    <label> اسم الخدمة بالانجليزية</label>
                    <input type="text" name="service_name_en" placeholder="اسم الخدمة بالانجليزية " id="" class="form-control" required >
                </div>
                <div class="form-group">
                    <label> سعر الخدمة  </label>
                    <input type="number" name="service_price" placeholder=" سعر الخدمة " class="form-control" required>
                </div>
                <div class="form-group">
                    <label> وصف الخدمة  </label>
                    <textarea name="service_des" placeholder="وصف الخدمة" rows="4" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label> وصف الخدمة بالانجليزية  </label>
                    <textarea name="service_des_en" placeholder="وصف الخدمة بالانجليزية " rows="4" class="form-control" required></textarea>
                </div>
                @csrf
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">اضافة الخدمة </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
