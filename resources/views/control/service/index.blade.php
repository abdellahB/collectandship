@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
       		 <h2 class="page-header"> كل الخدمات
            </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <a href="{{url('control/services/create')}}" class="btn btn-success pull-right">اضافة خدمة اضافية </a>
          <div class="row">
            <div class="col-xs-12">
			<div class="panel">
                <div class="panel-body">
                  <div class="tableresponsive">
                    <table id="" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>نوع الخدمة </th>
                          <th> سعر الخدمة  </th>
                          <th> الوصف </th>
                          <th>تاريخ الاضافة</th>
                          <th>الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($services as $service)
                                <tr>
                                    <td>{{ $service->service_name}}</td>
                                    <td>{{ $service->service_price}}$</td>
                                    <td>{{ $service->service_des}}</td>
                                    <td>{{ $service->created_at->format("Y-m-d")}}</td>
                                    <td>
                                        @if($service->status == 0)
                                            <span class='label label-success'> متاح </span>
                                        @else
                                             <span class='label label-primary'> موقف  </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                              @if($service->status == 0)
                                                <li><a href="javascript:void()" id="{{ $service->id }}" class="btn-link actionService 1"> ايقاف الخدمة </a> </li>
                                              @else
                                                <li><a href="javascript:void()" id="{{ $service->id }}" class="btn-link actionService 2"> عرض الخدمة  </a></li>
                                              @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
