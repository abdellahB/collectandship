<div class="row">
    @foreach($service->GetData as $data)
        @if($data->type == 'photo')
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="#">
                        <img src="{{ url('upload/ship')}}/{{ $data->imagename }}" alt="Lights" style="width:100%">
                    </a>
                </div>
            </div>
        @else
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{ url('upload/ship')}}/{{ $data->imagename }}"></iframe>
            </div>
        @endif
    @endforeach
</div>
