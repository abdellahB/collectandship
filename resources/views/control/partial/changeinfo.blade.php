<form class="trackings" role="form">
    <div class="form-group">
        <label for="company_url"> شركة الشحن </label>
        <input type="hidden" id="packing_id" name="packing_id" value="{{ $packing->id }}">
        <select class="form-control mode_shipping" name="mode_shipping" required="true">
            @foreach ($companies as $companie)
                <option value="{{ $companie->id }}" @if($companie->id == $packing->companie_id) selected @endif>
                    {{ $companie->companiename }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="tracking_info"> رقم التتبع  </label>
        <input type="text" class="form-control tracking_info" required="true" name="tracking_info" id="tracking_info" placeholder="رقم التتبع"/>
    </div>
</form>
