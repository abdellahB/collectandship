<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Shipping information
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form role="form">
                    <div class="form-group">
                        <label>Date Shipping (mm/dd/yy)</label>
                        <div class="input-with-icon">
                        <input class="form-control" type="text" name="dateship" data-provide="datepicker" data-date-today-highlight="true">
                        <span class="icon icon-calendar input-icon"></span>
                        </div>
                        <input type="hidden" name="packing_id" class="packing_id" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="companiesname"> Company </label>
                        <input type="text" class="form-control" name="companiesname" id="companiesname" placeholder="company name"/>
                    </div>
                    <div class="form-group">
                        <label for="demo-select2-2" class="form-label">tracking</label>
                        <select type="text" id="demo-select2-3" name="tracking[]" class="demo-select2-3 form-control" multiple="multiple"/>
                        </select>
                    </div>
                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary addtracking">
                    Save changes
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalQuestion" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">اغلاق</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    الاسئلة الشائعة
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form">
                    <ul class="nav nav-tabs nav-addcat" role="tablist">
                        <li role="presentation" class="active"><a href="#en-lang" aria-controls="en-lang" role="tab" data-toggle="tab">الأنجليزية</a></li>
                        <li role="presentation"><a href="#ar-lang" aria-controls="ar-lang" role="tab" data-toggle="tab">العربية </a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="en-lang">
                            <br/>
                            <div class="form-group">
                                <label class="default prepend-icon" >السؤال بالانجليزية</label>
                                <input class="form-control questions_en" type="text" name="questions_en" tabindex="2" required="true">
                            </div>
                            <div class="form-group">
                                <label>الجواب بالانجليزية</label>
                                <textarea class="form-control answer_en" rows="3" name="answer_en" required="true"></textarea>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="ar-lang">
                            <br/>
                            <div class="form-group">
                                <label class="default prepend-icon" >السؤال بالعربية</label>
                                <input class="form-control questions_ar" type="text" name="questions_ar" tabindex="2" required="true">
                           </div>
                           <div class="form-group">
                                <label>الجواب بالعربية</label>
                                <textarea class="form-control answer_ar" rows="3" name="answer_ar" required="true"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveQuestion">
                   اضافة الاسئلة
                </button>
            </div>
        </div>
    </div>
</div>

<!-- new users -->
<div class="modal fade" id="myModalUsers" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">اغلاق</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   اضافة المستخدمين
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form data-toggle="validator" class="addusers" role="form" method="POST" action="" enctype="multipart/form-data">
					<div class="col-lg-6">
						<div class="form-group">
							<label class="default prepend-icon" >الاسم الاول</label>
							<input id="firstname" class="form-control" type="text" name="firstname" tabindex="2" required="true">
						</div>
						<div class="form-group">
							<label class="default prepend-icon" >اختر الرتبة</label>
							<select name="role" class="form-control" tabindex="1" required="true">
								<option value="">اختر الرتبة</option>
								<option value="admin">مدير عام</option>
								<option value="employers">موظف</option>
							</select>
						</div>
						<div class="form-group">
							 <label class="default prepend-icon" >كلمة المرور</label>
								<input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="من فضلك ادخل كلمة المرور" required="true">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							 <label class="default prepend-icon" >الاسم الثاني</label>
								<input id="lastname" class="form-control" type="lastname" name="lastname" tabindex="2" required="true">
						</div>
						<div class="form-group">
							 <label class="default prepend-icon" >البريد الالكتروني</label>
							<input id="email" class="form-control" type="email" name="email" tabindex="2" required="true">
						</div>
						<div class="form-group">
							<label class="default prepend-icon" >صورة شخصية</label>
							<input type="file" name="image"  class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button type="submit" name="submit" class="btn btn-primary btn-block addusers">اضافة المستخدم</button>
					</div>
				</form>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form role="form" class="sendemailtouser">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        ارسال رسالة للعميل
                    </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">موضوع الرسالة </label>
                        <input type="text" class="form-control" name="subject" id="subject" required placeholder="موضوع الرسالة"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">محتوى الرسالة  </label>
                        <textarea type="text" class="form-control" rows="4" required name="message" id="message" placeholder="محتوى الرسالة"></textarea>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-block sendemailtouser" value="ارسال">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" value="" class="user_id" name="user_id">
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>


<div class="modal fade" id="myModalNewsLetter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form role="form" class="sendnewletter">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        ارسال رسالة لكل العملاء
                    </h4>
                </div>
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message_lang"> لغة الرسالة </label>
						<select class="form-control" name="message_lang" required="true">
							<option value="ar">العربية</option>
							<option value="en">الانجليزية</option>
						</select>
                    </div>
                    <div class="form-group">
                        <label for="subject_text">موضوع الرسالة </label>
                        <input type="text" class="form-control" required name="subject_text" id="subject_text" placeholder="موضوع الرسالة"/>
                    </div>
                    <div class="form-group">
                        <label for="message_text">محتوى الرسالة  </label>
                        <textarea class="form-control" rows="5" name="message_text" id="message_text" placeholder="محتوى الرسالة"></textarea>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-block sendnewletter" value="ارسال">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>


<div id="classModalOrderWafirha" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width: 83%;margin-right:16%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
          </button>
          <h4 class="modal-title" id="classModalLabel">
             منتجات الطلب
          </h4>
        </div>
        <div class="modal-body">
          <table id="demo-dynamic-tables-2" class="table table-middle">
            <thead>
                <th> الطلب </th>
                <th style="width:20%"> المنتج </th>
                <th> الرابط </th>
                <th>الكمية</th>
                <th>السعر</th>
                <th>الضريبة</th>
                <th>الشحن د</th>
                <th>العمولة</th>
                <th>مجموع</th>
                <th>ملاحظة</th>
                <th>الحالة</th>
                <th></th>
            </thead>
            <tbody class="show_data_wafirha_prd">

            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">
            اغلاق الصفحة
          </button>
        </div>
      </div>
    </div>
</div>

<!-- Edit product --->
<div id="myModalEditProduct" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row getproduct_info">

                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block update_fastbuyf">  تحديث معلومات المنتج </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalTracking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="companyurl"> رابط التتبع </label>
                        <input type="hidden" id="productid" name="productid" value="">
                        <input type="hidden" id="orderId" name="orderId" value="">
                        <input type="text" class="form-control" name="companyurl" id="companyurl" placeholder="رابط التتبع"/>
                    </div>
                    <div class="form-group">
                        <label for="trackinginfo"> رقم التتبع  </label>
                        <input type="text" class="form-control" name="trackinginfo" id="trackinginfo" placeholder="رقم التتبع"/>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            اغلاق الصفحة
                </button>
                <button type="button" class="btn btn-primary submittrackinginfo">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalShowTracking" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="result_resp" style="text-align: center; padding-top: 10px; color: green; font-size: 15px;"></div>
        <div class="modal-body showData">

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">
                        اغلاق الصفحة
            </button>
            <button type="button" class="btn btn-primary updatetrackinginfo">
               تحديث بيانات الشحن
            </button>
        </div>
      </div>
    </div>
</div>

<div id="myModalShowProduct" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> الشحنة </th>
                        <th scope="col">كود الشحنة </th>
                        <th scope="col">كود المنتج</th>
                        <th scope="col">الشحنة</th>
                        <th scope="col">الوزن / الأبعاد</th>
                        <th scope="col">الوصف</th>
                    </tr>
                </thead>
                <tbody class="getlist_s">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="myModalPacking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات تعليف الشحنة
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="formPack">
                    <div class="form-group">
                        <label> وحدة القياس</label>
						<select class="form-control" name="ship_unity" required="true">
							<option value="kg">Kg</option>
							<option value="lbg">Lbs</option>
						</select>
                    </div>
                    <div class="form-group">
                        <label for="weight"> وزن الشحنة </label>
                        <input type="hidden" id="packign_id" name="packign_id" value="">
                        <input type="number" class="form-control" name="weight" required="true" id="weight" placeholder="وزن الشحنة"/>
                    </div>
                    <div class="form-group">
                        <label for="length"> الطول  </label>
                        <input type="number" class="form-control" name="length" required="true" id="length" placeholder="الطول"/>
                    </div>
                    <div class="form-group">
                        <label for="width"> العرض  </label>
                        <input type="number" class="form-control" name="width" required="true" id="width" placeholder="العرض"/>
                    </div>
                    <div class="form-group">
                        <label for="height"> الارتفاع  </label>
                        <input type="number" class="form-control" name="height" required="true" id="height" placeholder="الارتفاع"/>
                    </div>
                    <div class="form-group">
                        <label for="trackinginfo"> صور الشحنة  </label>
                        <input type="file" class="form-control" id="fileToUpload" name="images" required="true" multiple id="images"/>
                    </div>
                    @csrf
                    <div class="form-group">
                        <button type="button" name="submit" class="btn btn-primary btn-block packFinish">
                            حفط المعلومات
                        </button>
                        <img class="loading_icons" style="height:60px;width:60px;margin-left: auto; text-align: center;margin-right:auto;display:none;" src="{{asset('assets/user/img/loading.gif')}}"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalPackingTracking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body getforms">

            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_tracking_pack">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProductImage" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="images" role="form">
                    <div class="form-group">
                        <label> الخدمة المطلوبة </label>
						<select class="form-control service_s" required ="true" name="service_s" required="true">
                            <option value="1"> خدمة اخرى </option>
                            <option value="2">صور اضافية </option>
                            <option value="3">رفع فيديو</option>
                        </select>
                        <input type="hidden" id="product_id" name="product_id" value="">
                        <input type="hidden" id="service_id" name="service_id" value="">
                    </div>
                    <div class="form-group photo">
                        <label for="company_url"> صور اضافية </label>
                        <input type="file" class="form-control" name="more_images" id="more_images" multiple />
                    </div>
                    <div class="form-group video">
                        <label for="company_url"> رفع فيديو </label>
                        <input type="file" class="form-control" name="more_video" id="more_video"/>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_product_img">
                    تنفيد الطلب
                 </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalProductReturn_price" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="price_return" role="form">
                    <div class="form-group">
                        <label for="price_topay"> مصاريف الشحن </label>
                        <input type="number" placeholder="مصاريف الشحن" class="form-control" name="price_topay" id="price_topay"/>
                        <input type="hidden" class="return_id" id="return_id" name="return_id" value="">
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_product_return">
                    اضافة سعر الشحن
                 </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProductReturn_Track" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <div class="track_return" role="form">
                    <div class="form-group">
                        <label for="company_urls"> شركة الشحن  </label>
                        <input type="text" placeholder="شركة الشحن" class="form-control" name="company_urls" id="company_urls"/>
                        <input type="hidden" class="return_id" id="return_id" name="return_id" value="">
                    </div>
                    <div class="form-group">
                        <label for="company_track"> رقم التتبع </label>
                        <input type="text" placeholder="رقم التتبع" class="form-control" name="company_track" id="company_track"/>
                    </div>
                </div>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_track_return">
                   اضافة رقم التتيع
                 </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalUserPolicia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="resp_error"></div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="userpolicia"> رقم حساب العميل </label>
                    <input type="text" placeholder="رقم حساب العميل" class="form-control userpolicia" name="userpolicia" id="userpolicia"/>
                    <input type="hidden" class="shippingid" id="shippingid" name="shippingid" value="">
                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block update_shipping"> ربط الشحنة  </button>
            </div>
        </div>
    </div>
</div>


<div id="myModalShowDetail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> اسم الخدمة  </th>
                        <th scope="col">رقم الطلب  </th>
                        <th scope="col">سعر الطلب </th>
                        <th scope="col">كود المعاملة</th>
                        <th scope="col">التأمين </th>
                        <th scope="col">التاريخ </th>
                        <th scope="col">الحالة</th>
                    </tr>
                </thead>
                <tbody class="getlist_details">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div id="myModalShowDirectShip" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>

                        <th scope="col"> # </th>
                        <th scope="col"> رقم الطلب </th>
                        <th scope="col">وزن الشحنة </th>
                        <th scope="col">ابعاد الشحنة </th>
                        <th scope="col">محتوى الشحنة</th>
                        <th scope="col">قيمة الشحنة</th>
                    </tr>
                </thead>
                <tbody class="getlist_directShip">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div class="modal fade" id="myModalDirectShipTracking" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    معلومات الشحن
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="tracking_direct" role="form">
                    <div class="form-group">
                        <label for="company_url"> شركة الشحن </label>
                        <input type="hidden" id="directship_id" name="directship_id" value="">
                        <input type="text" class="form-control company_url" required="true" name="company_url" id="company_url" />
                    </div>
                    <div class="form-group">
                        <label for="tracking_number_direct"> رقم التتبع  </label>
                        <input type="text" class="form-control" required="true" name="tracking_number_direct" id="tracking_number_direct" placeholder="رقم التتبع"/>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_tracking_direct">
                    حفط المعلومات
                </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalAddStore" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="resp_msg"></div>
                <div class="form-group">
                    <label for="catname"> اسم التصنيف بالعربي  </label>
                    <input type="text" placeholder="اسم التصنيف بالعربي" class="form-control catname" name="catname" id="catname"/>
                </div>
                <div class="form-group">
                    <label for="catname_en"> اسم التصنيف بالانجليزي </label>
                    <input type="text" placeholder=" اسم التصنيف بالانجليزي " class="form-control catname_en" name="catname_en" id="catname_en"/>
                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block addcategory"> اضافة التصنيف  </button>
            </div>
        </div>
    </div>
</div>


<div id="myModalRefund" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group">
                    <label for="refunded"> المبلغ الدي تم ارجاعه للعميل  </label>
                    <input type="hidden" class="transact_id" name="transact_id" value=""/>
                    <input type="number" placeholder="المبلغ الدي تم ارجاعه للعميل" class="form-control refunded" name="refunded" id="refunded"/>
                </div>
            </div>
            <div class="modal-footer" style="padding: 7px;">
                <button type="button" class="btn btn-success btn-block done_refund"> ارجاع المبلع للعميل  </button>
            </div>
        </div>
    </div>
</div>


<div id="myModalShowShipment" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col">كود الشحنة </th>
                        <th scope="col">تفاصيل الشحنة </th>
                        <th scope="col">الوزن / الأبعاد</th>
                        <th scope="col">الوصف</th>
                    </tr>
                </thead>
                <tbody class="getlist_shipment">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<div id="myModalShowService" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist_service">

        </div>
      </div>
    </div>
</div>
