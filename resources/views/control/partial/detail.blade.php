@foreach ($transaction as $transact )
    @foreach ($transact->getDetails as $detail)
        <tr id="{{ $transact->id }}">
            <td class="maw-320">
                @if($detail->service_name == "purchase")
                    خدمة الشراء
                @elseif($detail->service_name == "shipping")
                    خدمة الشحن
                @elseif($detail->service_name == "prd_service")
                    خدمة مشتريات
                @elseif($detail->service_name == "return_prd")
                    خدمة ارجاع المنتج
                @elseif($detail->service_name == "ship_direct")
                    الشحن المباشر
                @endif
            </td>
            <td class="maw-320">
                {{ $detail->code_order }}
            </td>
            <td class="maw-320">
                {{ $detail->order_price }}{{ $detail->currency }}
            </td>
            <td class="maw-320">
                {{ $transact->transaction_code }}
            </td>
            <td class="maw-320">
                {{ $detail->assurance }}
            </td>
            <td class="maw-320">
                {{ $transact->created_at->format('Y-m-d') }}
            </td>
            <td class="maw-320">
                @if($detail->status == 1)
                    <span class='label label-success'> مدفوع </span>
                @endif
            </td>
        </tr>
    @endforeach
@endforeach
