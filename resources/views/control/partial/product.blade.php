@foreach ($order->getOrders as $prd)
    <tr class="">
        <td> {{$order->code_order}} </td>
        <td class="product maw-320">
            {{$prd->product_name}}
        </td>
        <td class="product maw-320">
            <a href="{{$prd->product_url}}" target="_blank" style="color:blue"> الرابط</a>
        </td>
        <td> {{$prd->product_qty}} </td>
        <td> {{$prd->product_price}} {{ $prd->currency}}</td>
        <td> {{$prd->product_tax}}{{ $prd->currency}}</td>
        <td> {{$prd->product_shipping}} {{ $prd->currency}}</td>
        <td> {{$prd->product_taux}} {{ $prd->currency}}</td>
        <td> {{$prd->total_price}}{{ $prd->currency}}</td>
        <td><b onclick="show_hide_row('hidden_row_{{$prd->id}}');"> تفاصيل </b> </td>
        <td>
            @if($prd->status==0)
                <span class="label label-warning"> للمراجعة </span>
            @elseif($prd->status==1)
                <span class="label label-danger"> انتظار الدفع </span>
            @elseif($prd->status==2)
                <span class="label label-info"> قيد التنفيد </span>
            @elseif($prd->status==3)
                <span class="label label-success"> تم الشراء </span>
            @elseif($prd->status==4)
                <span class="label label-success"> مكتمل </span>
            @elseif($prd->status==10)
                <span class="label label-primary"> ملغي </span>
            @endif
        </td>
        <td>
            <div class="btn-group pull-right dropdown">
            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
            </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    @if($prd->status < 2)
                    <li><a href="#" class="btn-link Edit_product" id="{{$prd->id}}"> تحرير </a></li>
                    @endif
                    @if($prd->status==2)
                        <li><a href="#"  class="btn-link addtracking {{$prd->order_id}}" id="{{$prd->id}}"> تم الشراء </a></li>
                    @elseif($prd->status==3)
                        <li><a href="#"  class="btn-link show_tracking" id="{{$prd->id}}"> معلومات الشحن </a></li>
                        <li><a href="#"  class="btn-link prd_arrive {{$prd->order_id}}" id="{{$prd->id}}"> تم الاستلام </a></li>
                    @elseif($prd->status==4)
                        <li><a href="#"  class="btn-link show_tracking" id="{{$prd->id}}"> معلومات الشحن </a></li>
                    @endif
                </ul>
            </div>
        </td>
    </tr>
    <tr id="hidden_row_{{$prd->id}}" class="hidden_row" style="display:none;">
        <td colspan=15> {{$prd->product_des}} </td>
    </tr>
@endforeach
