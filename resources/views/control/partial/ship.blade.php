<tr>
    <td>
        <span class="icon-with-child m-r">
            @foreach ($ship->GetImage as $image)
                <a href="javascript:voide()" id="fancybox-manual-{{$ship->id}}">
                    <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                </a>
                @break
            @endforeach
        </span>
        <script>
            $(document).ready(function() {
                $("#fancybox-manual-{{$ship->id}}").click(function() {
                    $.fancybox.open([
                        @foreach ($ship->GetImage as $image)
                            {
                               href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                title : 'My title'
                            },
                        @endforeach
                    ]
                    );
                });
            });
        </script>
    </td>
    <td>
        <b> كود الشحنة  </b>   :{{ $ship->codeparcel}} <br/>
        <b> ناريخ الوصول </b>  : {{ $ship->date_arrival}} <br/>
        <b> المخزن      </b>   : {{ $ship->stockcode }} <br/>
        <b> دولة الشحن </b>    : @if($ship->shipcountry=="usa") <b class="ware">امريكا </b> @elseif($ship->shipcountry=="sa") <b class="ware">السعودية</b> @else  <b class="ware">الامارات </b> @endif
    </td>
    <td>
        <b>الوزن</b>     : {{ $ship->weight}} كيلو <br/>
        <b>الأبعاد</b>  : {{ $ship->lenght }} * {{ $ship->width }} * {{ $ship->height }} سم <br/>
        <b>شركة الشحن </b>   : {{ $ship->company}} <br/>
        <b>رقم التتبع</b>      : {{ $ship->tracking}} <br/>
    </td>
    <td>
        <b> {{ $ship->sellername }}</b> <br/>
        {!! $ship->description !!}
    </td>
</tr>
