@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> إضافة متجر جديد </h2>
                <a href="{{url('control/stores')}}" class="btn btn-success pull-right"> كـل المتاجر  </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/stores/create')}}" enctype="multipart/form-data">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label> تصنيف المتجر  </label>
                    <select  name="categorie_id" class="form-control" required >
                        @if(count($categories)>0)
                            @foreach ($categories as $categorie)
                                <option value="{{ $categorie->id}}">{{ $categorie->catname}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label> اسم المتجر </label>
                    <input type="text" name="storename" placeholder="اسم المتجر" id="" class="form-control" required >
                </div>
                <div class="form-group">
                    <label> اسم المتجر بالانجليزي </label>
                    <input type="text" name="storename_en" placeholder="اسم المتجر بالانجليزي" id="" class="form-control" required >
                </div>
                <div class="form-group">
                    <label> رابط المتجر  </label>
                    <input type="text" name="store_url" placeholder=" رابط المتجر" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> شعار المتجر  </label>
                    <input type="file" name="store_logo" placeholder="شعار المتجر" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> وصف المتجر  </label>
                    <textarea name="store_desc" placeholder="وصف المتجر" rows="4" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label> وصف المتجر بالانجليزية  </label>
                    <textarea name="store_desc_en" placeholder="وصف المتجر بالانجليزية " rows="4" class="form-control" required></textarea>
                </div>
                @csrf
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">اضافة المتجر </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
