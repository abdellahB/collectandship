@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">دليل مواقع التسوق</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
        <div class="row" style="margin-top: 20px;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" data-toggle="modal" data-target="#myModalAddStore" class="btn btn-success">
                               اضافة تضنيف جديد
                            </a>
                            <a href="{{ url('control/stores/create')}}" class="btn btn-info">
                                اضافة متجر جديد
                            </a>
                        </div>
                    </div>
                </div>
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
					<table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                  <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                                  <span class="custom-control-indicator"></span>
                                </label>
                            </th>
						  	<th>شعار المتجر</th>
                            <th>اسم المتجر </th>
                            <th> تصنيف المتجر</th>
                            <th>رابط المتجر</th>
						  	<th>وصف المتجر </th>
                            <th>تاريخ النشر </th>
                            <th>الحالة</th>
						  	<th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($stores as $store)
                               <tr>
                                   <td></td>
                                    <td>
                                        <span class="icon-with-child m-r">
                                            <img class="circle" width="36" height="36" src="{{asset('upload/stores/'.$store->store_logo)}}"/>
                                        </span>
                                    </td>
                                    <td>
                                        {{ $store->storename }}
                                    </td>
                                    <td>
                                        {{ $store->getCategory->catname }}
                                    </td>
                                    <td>
                                        {{ $store->store_url }}
                                    </td>
                                    <td class="maw-320" style="white-space: pre-line;">
                                        {{ $store->store_desc }}
                                    </td>
                                    <td>
                                        {{ $store->created_at->format('Y-m-d') }}
                                    </td>
                                    <td>
                                        @if($store->status == 0 )
                                            <span class="label label-success label-pill">نشيط </span>
                                        @else
                                            <span class="label label-danger label-pill">موقف</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($store->status == 0 )
                                                    <li><a href="#" class="btn-link" id=""> ايقاف المتجر </a></li>
                                                @else
                                                    <li><a href="#" class="btn-link" id=""> نشر المتجر </a></li>
                                                @endif
                                            </ul>
                                          </div>
                                    </td>
                               </tr>
                            @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
