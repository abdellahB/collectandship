@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
       		 <h2 class="page-header"> قائمة الاسعار </h2>
        		<a href="{{url('control/company')}}" class="btn btn-success pull-right">كل شركات الشحن </a>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
			        <div class="panel">
                <div class="panel-body">
                  <div class="tableresponsive">
                    <table id="" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th> شعار الشركة </th>
                          <th> اسم الشركة</th>
                          <th> الدولة </th>
                          <th> الوزن </th>
                          <th> السعر </th>
                          <th>  </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($companies AS $comp)
                          <tr class="{{$comp->id}}">
                              <td><img style="height:40px;width: 40px;" src="{{asset('upload/company')}}/{{$comp->getcomp->companylogo}}"/></td>
                              <td>{{$comp->getcomp->companiename}}</td>
                              <td>{{$comp->getcomp->getcountry->countriename}}</td>
                              <td>{{$comp->weightcompany}} كيلو</td>
                              <td><input type="text" style="width:60%" required="required" name="getprice" class="form-control getprice" value="{{$comp->pricecompany}}"></td>
                              <td><button id="updateprices" value="{{$comp->id}}" class="btn-success updateprices">تحديث</button></td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
