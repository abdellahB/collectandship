@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> إضافة شركة شحن </h2>
                <a href="{{url('control/company')}}" class="btn btn-success pull-right"> كـل الشركات </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/company/store')}}" enctype="multipart/form-data">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>دولة الشحن </label>
                    <select class="form-control" name="ship_country" required="true">
                        <option value="usa">أمريكا</option>
                        <option value="sa">السعودية</option>
                        <option value="uae">الامارات</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>اسم الشركة </label>
                    <input type="text" name="companiename" placeholder="اسم الشركة" id="companiename" class="form-control" required >
                </div>
                <div class="form-group">
                    <label>اسم الشركة بالانجليزية</label>
                    <input type="text" name="companiename_en" placeholder="اسم الشركة بالانجليزية" id="companiename_en" class="form-control" required >
                </div>
                <div class="form-group">
                    <label>رابط التتبع</label>
                    <input type="text" name="company_url" placeholder="رابط التتبع" id="company_url" class="form-control" required >
                </div>
            </div>
            <div class="col-lg-6">
                <!--<div class="form-group">
                    <label>اختر الدولة</label>
                    <select class="form-control" name="countrie_id">
                        <option value="">اختر الدولة</option>
                        @foreach ($countries as $countrie)
                            <option value="{{$countrie->id}}">{{$countrie->countriename}}</option>
                         @endforeach
                    </select>
                </div>-->
                <div class="form-group">
                    <label> المدة الزمنية </label>
                    <input type="text" name="datedelevier" placeholder="المدة الزمنية" class="form-control" required >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
                <div class="form-group">
                    <label> المدة الزمنية بالانجليزية </label>
                    <input type="text" name="datedelevier_en" placeholder="المدة الزمنية بالانجليزية" class="form-control" required >
                </div>
                <div class="form-group">
                    <label> شعار الشركة </label>
                    <input type="file" name="companylogo"  class="form-control" required>
                </div>
                <div class="form-group">
                    <label> ملف الاسعار </label>
                    <input type="file" name="companyprice"  class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">اضافة الشركة </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
