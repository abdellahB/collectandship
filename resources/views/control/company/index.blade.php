@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
       		 <h2 class="page-header"> شحنات الشحن </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <label>دولة المصدر </label>
                    <select class="form-control ship_country" name="ship_country" required="true">
                        <option value="usa">أمريكا</option>
                        <option value="sa">السعودية</option>
                        <option value="uae">الامارات</option>
                    </select>
                </div>
                <div class="col-lg-4">
                    <label>دولة الشحن </label>
                    <select name="country" class="form-control country">
                        <option value="">اختر الدولة</option>
                        @foreach($countries as $countrie)
                        <option value="{{$countrie->id}}">{{$countrie->countriename}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-4">
                    <a href="{{url('control/company/create')}}" class="btn btn-success pull-right">اضافة شركة</a>
                </div>
            </div>
          <div class="row">
            <div class="col-xs-12">
			<div class="panel">
                <div class="panel-body">
                  <div class="tableresponsive">
                    <table id="" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>شعار الشركة</th>
                          <th>اسم الشركة </th>
                          <th> رابط التتبع </th>
                          <th>المدة الزمنية</th>
                          <th>تاريخ الاضافة</th>
                          <th>الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="show_result">



                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
