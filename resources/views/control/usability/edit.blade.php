@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> تعديل معلومات   </h2>
                <a href="{{url('control/usability')}}" class="btn btn-success pull-right"> كـل المعلومات   </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/usability/edit',$data->id)}}" enctype="multipart/form-data">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label> اسم الصفحة  </label>
                    <select type="text" name="page_name" class="form-control" required >
                        <option value="account" @if($data->page_name == "account") selected @endif> حساب العميل </option>
                        <option value="product" @if($data->page_name == "product") selected @endif>  شحنات جديده </option>
                        <option value="processing" @if($data->page_name == "processing") selected @endif>   شحنات قيد التجهيز </option>
                        <option value="payment" @if($data->page_name == "payment") selected @endif>  جاهزة للدفع و الشحن </option>
                        <option value="prod_service" @if($data->page_name == "prod_service") selected @endif>  خدمات اضافية </option>
                        <option value="inprocess" @if($data->page_name == "inprocess") selected @endif> خدمات قيد التنفيد </option>
                        <option value="return" @if($data->page_name == "return") selected @endif> طلب ارجاع المنتج </option>
                        <option value="waitreturn" @if($data->page_name == "waitreturn") selected @endif>  قيد التجهيز و الدفع</option>
                        <option value="purchasecreate" @if($data->page_name == "purchasecreate") selected @endif>   طلب شراء </option>
                        <option value="purchasewaiting" @if($data->page_name == "purchasewaiting") selected @endif> انتظار المراجعة </option>
                        <option value="purchasepayment" @if($data->page_name == "purchasepayment") selected @endif>  جاهز للدفع و التنفيد</option>
                        <option value="purchasecompleted" @if($data->page_name == "purchasecompleted") selected @endif>  تم التنفيد </option>
                        <option value="transaction" @if($data->page_name == "transaction") selected @endif> الدفعات المالية </option>
                        <option value="profile" @if($data->page_name == "profile") selected @endif>  ادارة الحساب </option>
                        <option value="address" @if($data->page_name == "address") selected @endif> دفتر عناوين الشحن الخاصة بي </option>
                        <option value="directship" @if($data->page_name == "directship") selected @endif>  الشحن المباشر </option>
                        <option value="shipement" @if($data->page_name == "shipement") selected @endif> طلبات الشحن المباشر </option>
                        <option value="stores" @if($data->page_name == "stores") selected @endif>  دليل مواقع التسوق</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>مكان الظهور  </label>
                    <select type="text" name="position" class="form-control" required >
                        <option value="top" @if($data->position == "top") selected @endif> في الاعلى </option>
                        <option value="bottom" @if($data->position == "bottom") selected @endif> في الاسفل </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> المحتوى  </label>
                    <textarea name="message" placeholder="المحتوى" rows="4" class="form-control content" required>
                        {!! $data->message !!}
                    </textarea>
                </div>
                <div class="form-group">
                    <label> المحتوى بالانجليزية   </label>
                    <textarea name="message_en" placeholder="المحتوى بالانجليزية " rows="4" class="form-control content" required>
                        {!! $data->message_en !!}
                    </textarea>
                </div>
                @csrf
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block"> تعديل المعلومات </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
