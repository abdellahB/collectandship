@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> معلومات توضيحية </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
        <div class="row" style="margin-top: 20px;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ url('control/usability/create')}}" class="btn btn-info">
                                اضافة معلومات جديدة
                            </a>
                        </div>
                    </div>
                </div>
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
					<table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                  <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                                  <span class="custom-control-indicator"></span>
                                </label>
                            </th>
						  	<th> الصفحة </th>
                            <th>مكان الظهور</th>
                            <th> محتوى النص</th>
                            <th>  النص الانجليزي</th>
                            <th>تاريخ الاضافة </th>
                            <th>الحالة</th>
						  	<th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($data as $page)
                              <tr>
                                    <td></td>
                                    <td>{{ $page->page_name }}</td>
                                    <td>{{ $page->position }}</td>
                                    <td class="maw-320" style="white-space: pre-line;">
                                        {!! $page->message !!}
                                    </td>
                                    <td class="maw-320" style="white-space: pre-line;">
                                        {!! $page->message_en !!}
                                    </td>
                                    <td>{{ $page->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        @if($page->status == 0)
                                            <span class="label label-success"> منشور </span>
                                        @else
                                            <span class="label label-primary"> غير منشور </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($page->status == 0 )
                                                    <li><a href="#" class="btn-link pageaction 1" id="{{ $page->id }}"> ايقاف النشر </a></li>
                                                @else
                                                    <li><a href="#" class="btn-link pageaction 2" id="{{ $page->id }}"> نشر النص </a></li>
                                                @endif
                                                <li><a href="{{url('control/usability/edit',$page->id)}}" class="btn-link"> تعديل النص  </a></li>
                                            </ul>
                                          </div>
                                    </td>
                              </tr>
                            @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
