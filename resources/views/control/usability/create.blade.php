@extends('layouts.admin')
@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> إضافة معلومات  جديدة </h2>
                <a href="{{url('control/usability')}}" class="btn btn-success pull-right"> كـل المعلومات   </a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br/>
        <div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <form data-toggle="validator" role="form" method="POST" action="{{url('control/usability/create')}}" enctype="multipart/form-data">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label> اسم الصفحة  </label>
                    <select type="text" name="page_name" class="form-control" required >
                        <option value="account"> حساب العميل </option>
                        <option value="product">  شحنات جديده </option>
                        <option value="processing">   شحنات قيد التجهيز </option>
                        <option value="payment">  جاهزة للدفع و الشحن </option>
                        <option value="prod_service">  خدمات اضافية </option>
                        <option value="inprocess"> خدمات قيد التنفيد </option>
                        <option value="return"> طلب ارجاع المنتج </option>
                        <option value="waitreturn">  قيد التجهيز و الدفع</option>
                        <option value="purchasecreate">   طلب شراء </option>
                        <option value="purchasewaiting"> انتظار المراجعة </option>
                        <option value="purchasepayment">  جاهز للدفع و التنفيد</option>
                        <option value="purchasecompleted">  تم التنفيد </option>
                        <option value="transaction"> الدفعات المالية </option>
                        <option value="profile">  ادارة الحساب </option>
                        <option value="address"> دفتر عناوين الشحن الخاصة بي </option>
                        <option value="directship">  الشحن المباشر </option>
                        <option value="shipement"> طلبات الشحن المباشر </option>
                        <option value="stores">  دليل مواقع التسوق</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>مكان الظهور  </label>
                    <select type="text" name="position" class="form-control" required >
                        <option value="top"> في الاعلى </option>
                        <option value="bottom"> في الاسفل </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> المحتوى  </label>
                    <textarea name="message" placeholder="المحتوى" rows="4" class="form-control content" required></textarea>
                </div>
                <div class="form-group">
                    <label> المحتوى بالانجليزية   </label>
                    <textarea name="message_en" placeholder="المحتوى بالانجليزية " rows="4" class="form-control content" required></textarea>
                </div>
                @csrf
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 text-center"></div>
                <div class="col-lg-8 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-block"> نشر المعلومات </button>
                </div>
                <div class="col-lg-2 text-center"></div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
