@extends('layouts.admin')
@section('content')
<div class="layout-content" >
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">  تحديث عنوان الشحن  </h2>
			</div>
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a href="{{ url('control/address') }}" class="btn btn-success"> عناوين الشحن </a>
            <form data-toggle="validator" role="form" method="POST" action="{{ url('control/address/edit')}}">
				<div class="col-lg-6">
					<!--<div class="form-group">
						<label>Full Name</label>
                        <input type="text" name="fullname" required value="{{$address->fullname}}" class="form-control"/>
					</div>-->
					<div class="form-group">
						<label>العنوان الاول</label>
						<input type="text" name="Adressline" required value="{{$address->Adressline}}" class="form-control"/>
					</div>
					<!--<div class="form-group">
						<label>Address line1 </label>
						<input type="text" name="Adresslinetwo" value="{{$address->Adresslinetwo}}" required class="form-control"/>
					</div>-->
					<div class="form-group">
						<label>المدينة</label>
						<input type="text" name="city" value="{{$address->city}}" class="form-control" required/>
                    </div>
                    <div class="form-group">
						<label>رقم الهاتف</label>
						<input type="text" name="mobile"  value="{{$address->mobile}}" class="form-control" required/>
                    </div>
                    <input type="hidden" name="id" value="{{$address->id}}" class="form-control" required/>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>المحافظة</label>
						<input type="text" name="state"  value="{{$address->state}}" class="form-control" required />
					</div>
					<div class="form-group">
						<label>الرمز البريدي</label>
						<input type="text" name="zipcode"  value="{{$address->zipcode}}" class="form-control" required />
					</div>
                    @csrf
					<div class="form-group">
						<label>الدولة</label>
                        <select class="form-control" name="country" required="true">
							<option value="usa" @if($address->country ==="usa") selected @endif>أمريكا</option>
							<option value="sa"  @if($address->country ==="sa") selected @endif>السعودية</option>
							<option value="uae" @if($address->country ==="uae") selected @endif>الامارات</option>
						</select>
					</div>
				</div>
				<hr></hr>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"> تحديث العنوان </button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
