@extends('layouts.admin')
@section('content')
<div class="layout-content" >
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">  عناوين الشحن</h2>
			</div>
		</div>
		<div class="row gutter-xs">
            @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <a href="{{ url('control/address') }}" class="btn btn-success"> عناوين الشحن </a>
            <form data-toggle="validator" role="form" method="POST" action="{{ url('control/address/create')}}">
				<div class="col-lg-6">
                    @csrf
					<!--<div class="form-group">
						<label>Full Name</label>
						<input type="text" name="fullname" required class="form-control"/>
					</div>-->
					<div class="form-group">
						<label>العنوان الاول</label>
						<input type="text" name="Adressline" required class="form-control"/>
					</div>
					<!--<div class="form-group">
						<label>Address line1 </label>
						<input type="text" name="Adresslinetwo" required class="form-control"/>
					</div>-->
					<div class="form-group">
						<label>المدينة</label>
						<input type="text" name="city" class="form-control" required/>
                    </div>
                    <div class="form-group">
						<label>رقم الهاتف</label>
						<input type="text" name="mobile"  class="form-control" required/>
                    </div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>المحافظة</label>
						<input type="text" name="state"  class="form-control" required />
					</div>
					<div class="form-group">
						<label>الرمز البريدي</label>
						<input type="text" name="zipcode"  class="form-control" required />
					</div>
                    <div class="form-group">
						<label>الدولة</label>
                        <select class="form-control" name="country" required="true">
							<option value="usa">أمريكا</option>
							<option value="sa">السعودية</option>
							<option value="uae">الامارات</option>
						</select>
                    </div>
				</div>
				<hr></hr>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"> اضافة العنوان </button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
