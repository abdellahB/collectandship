@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> خدمات مشتريات </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th> رقم الطلب</th>
                          <th> كود الشحنة</th>
                          <th>رقم العميل</th>
                          <th>تاريخ الطلب  </th>
                          <th>التكلفة</th>
                          <th>وصف الخدمة</th>
                          <th> حالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($services as $service)
                                <tr>
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAllyoushop[]" type="checkbox" value="">
                                            <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>{{ $service->code_service }}</td>
                                    <td>{{ $service->getPship->codeparcel }}</td>
                                    <td><a href="{{ url('control/clients',$service->getuser->policia)}}" target="_blank">{{ $service->getuser->policia }}</a></td>
                                    <td>{{ $service->updated_at->format('Y-m-d') }}</td>
                                    <td> {{ $service->getPservice->service_price }}$</td>
                                    <td>
                                        {{ $service->information}} <br/>
                                        <b style="color:red"> {{ $service->getPservice->service_name }} <br/>
                                            <a href="javascript:void()" class="viewService" id="{{$service->id}}" style="color:blue">
                                                مشاهدة
                                            </a>
                                        </b>
                                    </td>
                                    <td>
                                        @if($service->status==0)
                                            <span class='label label-warning'> قيد التنفيد </span>
                                        @elseif($service->status==1)
                                            <span class='label label-info'> تم التنفيد </span>
                                        @elseif($service->status==2)
                                            <span class='label label-success'> تم الدفع </span>
                                        @elseif($service->status==3)
                                          <span class='label label-primary'> ملغي </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($service->status==0)
                                                    <li><a href="#" id="{{ $service->getPship->id }}" class="btn-link addimages {{ $service->id }}"> تنفيد الخدمة  </a></li>
                                                @endif
                                                <li><a href="javascript:void()" id="{{ $service->getPship->id }}" class="btn-link shipdetail"> تفاصيل الشحنة  </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
