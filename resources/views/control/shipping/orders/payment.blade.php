@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> انتظار الدفع </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th> الشحنة</th>
                          <th>  المنتجات	</th>
                          <th>رقم العميل</th>
                          <th>اسم العميل</th>
                          <th>الأيميل</th>
                          <th>التاريخ</th>
                          <th>نوع التغليف </th>
                          <th>عنوان الشحن</th>
                          <th>شركة الشحن</th>
                          <th> الخدمات</th>
                          <th> حالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($packings as $packing)
                                <tr>
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                        <input class="custom-control-input" name="selected[]" type="checkbox" value="">
                                            <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>{{ $packing->code_parcel }}</td>
                                    <td>{{ $packing->Nbpackage }}</td>
                                    <td><a href="{{ url('control/clients',$packing->getUser->policia)}}" target="_blank">{{ $packing->getUser->policia }}</a></td>
                                    <td>{{ $packing->getUser->firstname }} {{ $packing->getuser->lastname }}</td>
                                    <td>{{ $packing->getUser->email }}</td>
                                    <td>{{ $packing->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        @if($packing->modepacking =="withpack")
                                            بتغليف المصنع
                                        @else
                                             ازالة التغليف
                                        @endif
                                    </td>
                                    <td style="text-align: left;">
                                        {{ $packing->getAddress->adress }} <br/>
                                        {{ $packing->getAddress->province }} |  {{ $packing->getAddress->city }}<br/>
                                        {{ $packing->getAddress->phone }} <br/>
                                        {{ $packing->getAddress->getcountry->countriename }} <br/>
                                    </td>
                                    <td>{{ $packing->getCompany->companiename }}</td>
                                    <td>
                                        @foreach ($packing->getchild as $child)
                                            {{ $child->getService->service_name }} <br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        <span class="label label-info"> انتظار الدفع </span>
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                          <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                            <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                          </button>
                                          <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="javascript:void()" id="{{ $packing->id }}" class="btn-link getlist">مشاهدة المنتجات </a>
                                            </li>
                                          </ul>
                                        </div>
                                      </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
