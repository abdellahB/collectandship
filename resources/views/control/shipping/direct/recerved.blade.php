@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> تم الاستلام  </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>كود الطلب</th>
                          <th> عدد الشحنات	</th>
                          <th>رقم العميل</th>
                          <th>اسم العميل</th>
                          <th>الأيميل</th>
                          <th>تاريخ الطلب</th>
                          <th>مكان الاستلام</th>
                          <th> شركة الشحن </th>
                          <th> السعر </th>
                          <th> الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($orders as $order)
                                <tr class="{{ $order->id }}">
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAllyoushop[]" type="checkbox" value="">
                                            <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td> {{ $order->code_order }} </td>
                                    <td> {{ $order->getchild->count() }} </td>
                                    <td><a href="{{ url('control/clients',$order->getUser->policia)}}" target="_blank">{{ $order->getUser->policia }}</a></td>
                                    <td>{{ $order->getUser->firstname }} {{ $order->getUser->lastname }}</td>
                                    <td>{{ $order->getUser->email }}</td>
                                    <td> {{ $order->created_at->format('Y-m-d') }} </td>
                                    <td>
                                        {{ $order->getcollect->firstname }}  {{ $order->getcollect->lastname }} <br/>
                                        {{ $order->getcollect->province }} {{ $order->getcollect->city }} <br/>
                                        {{ $order->getcollect->zipcode }} <br/>
                                        {{ $order->getcollect->address }} <br/>
                                        {{ $order->getcollect->phone }} <br/>
                                        {{ $order->getcollect->ship_country }} <br/>
                                    </td>
                                    <td>
                                        @if($order->company_url)
                                            <a href="{{ $order->company_url }}" target="_blank"> URL </a> <br/>
                                            {{ $order->tracking_number }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>  {{ $order->pricetopay }}$ </td>
                                    <td>
                                        @if($order->status == 1)
                                            <span class='label label-success'> انتظار الاستلام </span>
                                        @elseif($order->status == 2)
                                            <span class='label label-info'> تم الاستلام </span>
                                        @elseif($order->status == 3)
                                            <span class='label label-info'> وصلت الشحنة </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" id="{{ $order->id }}" class="btn-link detail_ship"> تفاصيل الطلب </a></li>
                                                <li><a href="#" id="{{ $order->id }}" class="btn-link ship_recerved"> وصلت الشحنة </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
