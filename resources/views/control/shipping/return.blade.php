@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> طلب ارجاع المنتج </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>  الطلب </th>
                          <th>  الشحنة </th>
                          <th>رقم العميل</th>
                          <th>اسم العميل</th>
                          <th>الأيميل</th>
                          <th>تاربخ  </th>
                          <th>  التتبع </th>
                          <th>  البائع </th>
                          <th>سبب الارجاع </th>
                          <th> التكلفة </th>
                          <th> الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                           @foreach ($parcels as $parcel)
                                <tr>
                                    <td>{{$parcel->GetReturn->code_return}}</td>
                                    <td>{{ $parcel->codeparcel}}</td>
                                    <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
                                    <td>{{ $parcel->getUser->firstname }} {{ $parcel->getuser->lastname }}</td>
                                    <td>{{ $parcel->getUser->email }}</td>
                                    <td>{{ $parcel->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                        @if($parcel->GetReturn->status==3)
                                            <a href="{{$parcel->GetReturn->tracking_url}}" target="_black"> {{$parcel->GetReturn->tracking_code}} </a>
                                        @else
                                            --
                                        @endif
                                    </td>
                                    <td>
                                        {{ $parcel->GetReturn->seller_address }}
                                    </td>
                                    <td> {{ $parcel->GetReturn->return_info }}</td>
                                    <td>
                                        @if($parcel->GetReturn->status!=0)
                                            {{ $parcel->GetReturn->pricetopay }}$
                                        @else
                                            <b>غير محددة </b>
                                        @endif
                                    </td>
                                    <td>
                                        @if($parcel->GetReturn->status==0)
                                            <span class="label label-danger"> قيد المراجعة </span>
                                        @elseif($parcel->GetReturn->status==1)
                                            <span class="label label-info"> انتظار الدفع</span>
                                        @elseif($parcel->GetReturn->status==2)
                                            <span class="label label-warning"> انتظار الشحن </span>
                                        @elseif($parcel->GetReturn->status==3)
                                            <span class="label label-success"> تم الشحن </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($parcel->GetReturn->status==0)
                                                    <li><a href="javascript:void()" id="{{ $parcel->GetReturn->id }}" class="btn-link ship_status 1"> مصاريف الشحن </a></li>
                                                @elseif($parcel->GetReturn->status==2)
                                                    <li><a href="javascript:void()" id="{{ $parcel->GetReturn->id }}" class="btn-link ship_status 2"> تم الشحن </a></li>
                                                @endif
                                                <li><a href="javascript:void()" id="{{ $parcel->id }}" class="btn-link shipdetail"> تفاصيل الشحنة  </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                           @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
