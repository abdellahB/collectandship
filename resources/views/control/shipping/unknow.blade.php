@extends('layouts.admin')
@section('content')
<style>
    .resp_error{
        padding: 10px;
        text-align: center;
        color: red;
        font-size: 14px;
    }
</style>
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> شحنات بدون هوية </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>الصورة</th>
                          <th>رقم الشحنة</th>
                          <th> العميل</th>
                          <th> الاسم الكامل</th>
                          <th> تاريخ الوصول </th>
                          <th>شركة الشحن </th>
                          <th>التتبع </th>
                          <th>المخزن </th>
                          <th>الدولة </th>
                          <th> الحالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="">
                        @foreach ($parcels as $parcel)
                        <tr class="">
                            <td>
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" name="checkAllyoushop[]" type="checkbox" value="">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td data-order="Jessica Brown">
                                <span class="icon-with-child m-r">
                                    @foreach ($parcel->GetImage as $image)
                                        <img class="circle" width="40" height="40" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                        @break
                                    @endforeach
                                </span>
                            </td>
                            <td>{{ $parcel->codeparcel }}</td>
                            <td>غير معروف</td>
                            <td>{{ $parcel->fullname }}</td>
                            <td>{{ $parcel->date_arrival }}</td>
                            <td>{{ $parcel->company }}</td>
                            <td>{{ $parcel->tracking }}</td>
                            <td>{{ $parcel->stockcode }}</td>
                            <td>{{ $parcel->shipcountry }}</td>
                            <td data-order="1">
                                <span class="label label-info label-pill"> بدون هوية </span>
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                    <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                        <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#" id="{{ $parcel->id }}" class="btn-link inknow_parcel 1"> ربط الشحنة  </a></li>
                                        <li><a href="#" id="{{ $parcel->id }}" class="btn-link inknow_parcel 2"> حدف الشحنة  </a></li>
                                        <li><a href="javascript:void()" id="{{ $parcel->id }}" class="btn-link shipdetail"> تفاصيل الشحنة  </a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
