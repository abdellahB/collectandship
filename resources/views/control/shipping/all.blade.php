@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header"> كل الشحنات </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                  <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>
                            <label class="custom-control custom-control-primary custom-checkbox">
                              <input id="checkAll" class="custom-control-input checkAll" type="checkbox">
                              <span class="custom-control-indicator"></span>
                            </label>
                          </th>
                          <th>الصورة</th>
                          <th> كود الشحنة</th>
                          <th>رقم العميل</th>
                          <th>اسم العميل</th>
                          <th>الأيميل</th>
                          <th>تارخ الوصول </th>
                          <th>المخزن </th>
                          <th>الدولة </th>
                          <th>شركة الشحن </th>
                          <th> التتبع </th>
                          <th> حالة </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($parcels as $parcel)
                                <tr class="{{ $parcel->id}}">
                                    <td>
                                        <label class="custom-control custom-control-primary custom-checkbox">
                                            <input class="custom-control-input" name="checkAllyoushop[]" type="checkbox" value="">
                                            <span class="custom-control-indicator"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <span class="icon-with-child m-r">
                                            @foreach ($parcel->GetImage as $image)
                                                <img class="circle" width="40" height="40" src="{{ asset('upload/ship') }}/{{ $image->imagename}}" alt="">
                                                @break
                                            @endforeach
                                        </span>
                                    </td>
                                    <td>{{ $parcel->codeparcel }}</td>
                                    <td><a href="{{ url('control/clients',$parcel->getuser->policia)}}" target="_blank">{{ $parcel->getuser->policia }}</a></td>
                                    <td>{{ $parcel->getuser->firstname }} {{ $parcel->getuser->lastname }}</td>
                                    <td>{{ $parcel->getuser->email }}</td>
                                    <td>{{ $parcel->date_arrival }}</td>
                                    <td>{{ $parcel->stockcode }}</td>
                                    <td>
                                        @if($parcel->shipcountry == 'usa')
                                            امريكا
                                        @elseif($parcel->shipcountry == 'sa')
                                            السعودية
                                        @else
                                            الامارات
                                        @endif
                                    </td>
                                    <td>{{ $parcel->company }}</td>
                                    <td>{{ $parcel->tracking }}</td>
                                    <td>
                                        @if($parcel->status == 0)
                                            <span class='label label-success'> منشورة </span>
                                        @elseif($parcel->status == 5)
                                            <span class='label label-info'> مسترجعة </span>
                                        @elseif($parcel->status == 10)
                                            <span class='label label-danger'> ملغية </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group pull-right dropdown">
                                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                              <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if($parcel->status == 0)
                                                    <li><a href="#" id="{{ $parcel->id}}" class="btn-link removeparcel 10"> ازلة الشحنة </a></li>
                                                @elseif($parcel->status == 10)
                                                    <li><a href="#" id="{{ $parcel->id}}" class="btn-link removeparcel 0"> استعادة الشحنة </a></li>
                                                @endif
                                                <li><a href="javascript:void()" id="{{ $parcel->id }}" class="btn-link shipdetail"> تفاصيل الشحنة  </a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
