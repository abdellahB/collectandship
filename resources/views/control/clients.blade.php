@extends('layouts.admin')
@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">حسابات العملاء</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
        <div class="row" style="margin-top: 20px;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" data-toggle="modal" data-target="#myModalNewsLetter" class="btn btn-success">رسالة جماعية</a>
                        </div>
                        <div class="col-sm-6">
                        <div id="demo-dynamic-tables-2_filter" class="dataTables_filter">
                            <label><input type="search" id="search" class="form-control input-sm searchclient" placeholder="البحث عن عميل" aria-controls="demo-dynamic-tables-2"></label>
                        </div>
                    </div>
                </div>
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
					<table id="demo-dynamic-tables+2" class="table table-middle nowrap">
                      <thead>
                        <tr>
						  	<th>الصورة</th>
                            <th>رقم العميل</th>
                            <th>اسم العميل</th>
						  	<th>الأيميل</th>
                            <th>الدولة</th>
                            <th>المدينة</th>
							<th>الهاتف</th>
                            <th>عضو مند </th>
							<th>الحالة</th>
						  	<th></th>
                        </tr>
                      </thead>
                      <tbody class="resp_results">
                        @foreach ($clients as $client )
                        <tr id="{{ $client->id }}">
                            <td data-order="Jessica Brown">
                                <span class="icon-with-child m-r">
                                    <img class="circle" width="36" height="36" src="@if($client->thumbnail != "") {{asset('assets/user/img/'.$client->thumbnail)}} @else {{asset('assets/admin/img/0180441436.jpg')}} @endif" alt="{{$client->firstname}}"/>
                                </span>
							</td>
                            <td class="maw-320">
                                <span class="truncate">
                                    {{ $client->policia }}
                                </span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->firstname }} {{ $client->lastname }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->email }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate">{{ $client->getcountry->countriename }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->city }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->phone }}</span>
                            </td>
                            <td class="maw-320">
                                <span class="truncate"> {{ $client->created_at->format('Y-m-d') }}</span>
                            </td>
							<td>
                                @if($client->status == 0 )
                                    <span class="label label-info label-pill">غير مفعل</span>
                                @elseif($client->status == 1)
                                    <span class="label label-success label-pill">نشيط</span>
                                @else
                                    <span class="label label-primary label-pill">موقف</span>
                                @endif
                            </td>
                          <td>
                            <div class="btn-group pull-right dropdown">
                              <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right">
                                    @if($client->status == 0 || $client->status == 2)
                                        <li><a href="#" class="btn-link operaccount 1" id="{{ $client->id }}">تفعيل الحساب</a></li>
                                    @elseif($client->status == 1)
                                        <li><a href="#" class="btn-link operaccount 2" id="{{ $client->id }}">توقيف الحساب</a></li>
                                    @endif
                                    <li><a href="#" class="btn-link sendEmail " id="{{ $client->id }}"> ارسال رسالة </a></li>
                                    <li><a href="{{ url('control/clienttransact',$client->id) }}" class="btn-link " id="{{ $client->id }}"> المعاملات المالية </a></li>
                              </ul>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
                @if(count($clients)>1)
                <div class="box-body">
                    {{ $clients->links() }}
                </div>
                @endif
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection
