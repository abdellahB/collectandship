@extends('layouts.admin')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title "> All pages ... </h3>
                </div>
            <div>
        </div>
    </div>
    <div class="mcontent">
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
            <div class="m-alert__icon">
                <i class="flaticon-exclamation m--font-brand"></i>
            </div>
            <div class="m-alert__text">
                All pages ...
            </div>
            <div class="m-alert__text pull-right">
                <a href="{{ url('control/pages/create')}}" class="btn btn-success"> Create </a>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="row gutter-xs">
                  <div class="col-lg-12">
                    <table id="" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                          <th>Page name english</th>
                          <th>Page name chinese</th>
                          <th>Position </th>
                          <th>Operations</th>
                        </tr>
                      </thead>
                      <tbody>
                            @foreach ($pages as $page)
                              <tr>
                                <td>{{ $page->title_en}}</td>
                                <td>{{ $page->title_zh}}</td>
                                <td>{{ $page->position}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                            <i class="la la-cog"></i> Action
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="javascript:void(0)" class="dropdown-item"> Edit page </a>
                                        </div>
                                    </div>
                                </td>
                              </tr>
                            @endforeach
                      </tbody>
                    </table>

                  </div>
                </div>
            </div>
        </div>
    </div><!--end mcontent-->
</div>
@endsection
