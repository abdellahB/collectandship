@extends('layouts.admin')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                    <h3 class="m-subheader__title "> Import Shipement </h3>
                </div>
            <div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="mcontent">
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
            <div class="m-alert__icon">
                <i class="flaticon-exclamation m--font-brand"></i>
            </div>
            <div class="m-alert__text">
                
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <form class="row">
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label for="packingid"> Packing ID </label>
                            <input type="text" class="form-control m-input" id="packingid" aria-describedby="packingid" placeholder=" Packing ID ...">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="usercode">User code </label>
                            <input type="text" class="form-control m-input importpolicier txt-auto" id="usercode" aria-describedby="usercode" placeholder=" User Code ...">
                            <input type="hidden" name="user_id" id="user_id" value="" >
                        </div>
                        <div class="form-group">
                            <label>Full Name </label>
                            <input type="text" name="fullname" placeholder="Full Name" class="form-control txt-auto" required readonly>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="weight">Weight</label>
                            <input type="number" class="form-control m-input" id="weight" aria-describedby="weight" placeholder="Weight">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Image Browser</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1"> Tracking Number </label>
                            <input type="text" class="form-control m-input" id="trackingnumber" aria-describedby="trackingnumber" placeholder="Tracking Number ...">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="datearrival"> Date Arrival </label>
                            <input type="date" class="form-control m-input" id="datearrival" aria-describedby="datearrival" placeholder="Date Arrival">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="country"> Country </label>
                            <input type="text" class="form-control m-input" id="country" aria-describedby="country" placeholder="country" readonly>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="dimensions">Dimensions</label>
                            <input type="text" class="form-control m-input" id="dimensions" aria-describedby="dimensions" placeholder="Dimensions">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="price">Price</label>
                            <input type="number" class="form-control m-input" id="price" aria-describedby="price" placeholder="price">
                        </div>
                        
                    </div>
                    <div class="m-form__actions col-lg-12" style="margin-top:30px;">
                        <button type="button" class="btn btn-primary btn-block addshippemt"> Add Shipement</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
