@extends('layouts.admin')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                    <h3 class="m-subheader__title ">  </h3>
                </div>
            <div>
        </div>
    </div>
<!-- END: Subheader -->
<div class="mcontent">
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
            <div class="m-alert__icon">
                <i class="flaticon-exclamation m--font-brand"></i>
            </div>
            <div class="m-alert__text">
                Create page
            </div>
            <div class="m-alert__text pull-right">
                <a href="{{ url('control/pages/allpages')}}" class="btn btn-info"> All pages </a>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Create page
                        </h3>

                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">English </a></li>
                    <li><a data-toggle="tab" href="#menu1"> Chinese </a></li>
                </ul>
                @if ($message = Session::get('success'))
                    <div class="alert-success success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <form method="POST" action="{{ url('control/pages/create') }}">
                    <div class="tab-content row">
                        <div id="home" class="tab-pane fade in active show col-lg-12">
                            <div class="form-group">
                                <label for="title_en"> Title Page English </label>
                                <input type="text" class="form-control m-input" name="title_en" id="title_en" aria-describedby="title_en" placeholder="Title Page English">
                            </div>
                            <div class="form-group">
                                <label for="position"> Page position </label>
                                <select type="text" class="form-control m-input" name="position">
                                    <option value="1"> Position 1</option>
                                    <option value="2"> Position 2</option>
                                    <option value="3"> Position 3</option>
                                    <option value="4"> Position 4</option>
                                    <option value="5"> Position 5</option>
                                </select>
                                {{ csrf_field() }}
                            </div>
                            <div class="form-group">
                                <label for="Content_en"> Content English</label>
                                <textarea rows="10" class="form-control" name="Content_en" id="Content_en" aria-describedby="Content_en" placeholder="about & condition  Content"> </textarea>
                            </div>
                            <div class="m-form__actions col-lg-12" style="margin-top:30px;">
                                <button type="submit" class="btn btn-primary btn-block saveaboutchange"> Save Change </button>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade col-lg-12">
                            <div class="form-group">
                                <label for="title_zh"> Title Page Chinese </label>
                                <input type="text" class="form-control m-input" id="title_zh" name="title_zh" aria-describedby="title_zh" placeholder="Title Page Chinese">
                            </div>
                            <div class="form-group">
                                <label for="Content_zh"> Content Chinese </label>
                                <textarea rows="20" class="form-control" id="Content_zh" name="Content_zh" aria-describedby="Content_zh" placeholder="about & condition Content"> </textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
