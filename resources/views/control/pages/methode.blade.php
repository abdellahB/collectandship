@extends('layouts.admin')

@section('content')
<div class="layout-content page">
    <div class="layout-content-body">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> وسائل الشحن و الدفع المعتمدة
                </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row gutter-xs">
        <form data-toggle="validator" role="form" method="POST" action="{{ url('control/methode')}}" enctype="multipart/form-data">
            <div class="col-lg-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-addcat" role="tablist">
                    <li role="presentation" class="active"><a href="#en-lang" aria-controls="en-lang" role="tab" data-toggle="tab">الأنجليزية</a></li>
                    <li role="presentation"><a href="#ar-lang" aria-controls="ar-lang" role="tab" data-toggle="tab">العربية </a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--en-longe-->
                    <div role="tabpanel" class="tab-pane active" id="en-lang">
                        <br/>
                        <div class="form-group">
                            <textarea  name="content_en" id="content_en" row="4" class="form-control content">
                                {{ $methode->content_en }}
                            </textarea>
                        </div>
                    </div>
                    <!-- ar-longe -->
                    <div role="tabpanel" class="tab-pane" id="ar-lang">
                        <br/>
                        <div class="form-group">
                            <textarea  name="content_ar" id="content_ar" row="4" class="txtR form-control content" >
                                {{ $methode->content_ar }}
                            </textarea>
                        </div>
                    </div>
                </div><!-- end Tab panes -->
                <div class="col-lg-12">
                    <div class="col-lg-2 text-center">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="idmethode" value="{{ $methode->id }}">
                    </div>
                    <div class="col-lg-8 text-center">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">تحديث المعلومات </button>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
