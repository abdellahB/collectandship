@extends('layouts.admin')

@section('content')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">  الأسئلة الشائعة </h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row gutter-xs">
		<a href="#" data-toggle="modal" data-target="#myModalQuestion" class="btn btn-success addQuestion">اضافة سؤال</a>
		<div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-middle">
                      <thead>
                        <tr>
                            <th>السؤال بالعربية</th>
                            <th> الجواب </th>
                            <th> السؤال بالانجليزي</th>
                            <th> الجواب </th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($questions as $question)
                            <tr id="1">
                                <td>{{ $question->questions_ar }}</td>
                                <td class="maw-320">{{ $question->answer_ar }}</td>
                                <td>{{ $question->questions_en }}</td>
                                <td class="maw-320">{{ $question->answer_en }}</td>
                                <td>
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                            <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ url ('control/faq',$question->id) }}" id="" class="btn-link delete_faq"> حدف </button></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
               <div class="text-center">  {{ $questions->links() }} </div>
              </div>
            </div>
		</div>
	</div>
</div>
@endsection
