@extends('layouts.app')

@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.whyus') }}
            </h1>
        </div>
    </div>
    <section class="section pb-5">
        <div class="container-lg py-3">
            <p class="section-description pb-4">
                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                    {!! $about->content_ar !!}
                @else
                    {!! $about->content_en !!}
                @endif
            </p>
        </div>
    </section>
</div>
@endsection
