<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center" style="display: block;">
                <b class="signup_title">  {{ __('home.signup_title') }}</b>
                <b class="rest_title" style="display:none"> {{ __('home.rest_title') }}</b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <!-- Begin # Login Form -->
                <form id="login-form" method="POST" action="#" class="form-bg signin">
                    <div class="loginerreur text-center" style="display:none">
                        <span class='label label-warning col-md-12'>
                            {{ __('home.loginerror') }}
                        </span>
                    </div>
                    <div class="logincaptcha text-center" style="display:none;padding-top: 8px; font-weight: bold; color: red;">
                        <span class='label label-warning col-md-12'>
                            {{ __('home.logincaptcha') }}
                        </span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">{{ __('home.email') }}<samp class="asterisk">*</samp></span> </label>
                            <input name="email" class="form-control login_username" type="email" placeholder="{{ __('home.email') }}" required="true">
                        </div>
                        <div class="form-group">
                            <label for="password">{{ __('home.password') }}<samp class="asterisk">*</samp></span> </label>
                            <input name="password" class="form-control login_password" type="password" placeholder="{{ __('home.password') }}" required="true">
                        </div>
                        <input type='hidden' class="position" id='position' value='0'/>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" checked>  {{ __('home.rememberme') }}
                            </label>
                        </div>
                        <style>
                            .g-recaptcha div {
                                width: auto !important;
                            }
                        </style>
                        <div class="row form-group">
                            <div class="col-12 text-center">
                                {!! app('captcha')->display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-secondary btn-lg btn-block rounded-lg signin">{{ __('home.login') }}</button>
                        </div>
                        <div>
                            <button id="login_lost_btn" type="button" class="btn btn-link">{{ __('home.forgetpassword') }}</button>
                            <!--<button id="login_register_btn" type="button" class="btn btn-link">{{ __('home.register') }}</button>-->
                        </div>
                    </div>
                </form>
                <!-- End # Login Form -->

                <!-- Begin | Lost Password Form -->
                <form id="lost-form" style="display:none;">
                    <div class="modal-body">
                        <div class="loginerreur resetmsg text-center" style="display:none;">
                            {{ __('home.rest_msg') }}
                        </div>
                        <div class="form-group tohide">
                            <label for="email">{{ __('home.email') }}<samp class="asterisk">*</samp></span> </label>
                            <input id="lost_email" class="form-control" type="email" placeholder="{{ __('home.email') }}" required="true"/>
                        </div>
                    </div>
                    <div class="modal-footer tohide">
                        <div>
                            <button type="button" class="btn btn-secondary btn-lg btn-block rounded-lg restpassword">{{ __('home.resetpassword') }}</button>
                        </div>
                        <div>
                            <button id="lost_login_btn" type="button" class="btn btn-link">{{ __('home.login') }}</button>
                        </div>
                    </div>
                </form>
                <form id="register-form" style="display:none;" method="POST" action="" class="form-bg gradient-bg p-4  registerform" _lpchecked="1">
                    <div class="registererreur text-center"></div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="firstname" required="true" placeholder="{{ __('home.firstname') }}" class="form-control shadow-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center; cursor: auto;">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="lastname" required="true" placeholder="{{ __('home.lastname') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="email" name="email" required="true" placeholder="{{ __('home.email') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="password" name="password" required="true" placeholder="{{ __('home.password') }}" class="form-control shadow-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAABKRJREFUWAnNl0tsVGUUxzvTTlslZUaCloZHY6BRFkp9sDBuqgINpaBp02dIDImwKDG6ICQ8jBYlhg0rxUBYEALTpulMgBlqOqHRDSikJkZdGG0CRqAGUuwDovQ1/s7NPTffnTu3zMxGvuT2vP7n8Z3vu+dOi4r+5xUoJH8sFquamZmpTqfTVeIfCARGQ6HQH83NzaP5xsu5gL6+vuVzc3NdJN1Kkhd8Ev1MMYni4uJjra2tt3wwLvUjCxgYGFg8Pj7+MV5dPOUub3/hX0zHIpFId0NDw6Q/jO4tZOzv76+Znp6+AOb5TBw7/YduWC2Hr4J/IhOD/GswGHy7vb39tyw2S+VbAC1/ZXZ29hKoiOE8RrIvaPE5WvyjoS8CX8sRvYPufYpZYtjGS0pKNoD/wdA5bNYCCLaMYMMEWq5IEn8ZDof3P6ql9pF9jp8cma6bFLGeIv5ShdISZUzKzqPIVnISp3l20caTJsaPtwvc3dPTIx06ziZkkyvY0FnoW5l+ng7guAWnpAI5w4MkP6yy0GQy+dTU1JToGm19sqKi4kBjY+PftmwRYn1ErEOq4+i2tLW1DagsNGgKNv+p6tj595nJxUbyOIF38AwipoSfnJyMqZ9SfD8jxlWV5+fnu5VX6iqgt7d3NcFeUiN0n8FbLEOoGkwdgY90dnbu7OjoeE94jG9wd1aZePRp5AOqw+9VMM+qLNRVABXKkLEWzn8S/FtbdAhnuVQE7LdVafBPq04pMYawO0OJ+6XHZkFcBQA0J1xKgyhlB0EChEWGX8RulsgjvOjEBu+5V+icWOSoFawuVwEordluG28oSCmXSs55SGSCHiXhmDzC25ghMHGbdwhJr6sAdpnyQl0FYIyoEX5CeYOuNHg/NhvGiUUxVgfV2VUAxjtqgPecp9oKoE4sNnbX9HcVgMH8nD5nAoWnKM/5ZmKyySRdq3pCmDncR4DxOwVC64eHh0OGLOcur1Vey46xUZ3IcVl5oa4OlJaWXgQwJwZyhUdGRjqE14VtSnk/mokhxnawiwUvsZmsX5u+rgKamprGMDoA5sKhRCLxpDowSpsJ8vpCj2AUPzg4uIiNfKIyNMkH6Z4hF3k+RgTYz6vVAEiKq2bsniZIC0nTtvMVMwBzoBT9tKkTHp8Ak1V8dTrOE+NgJs7VATESTH5WnVAgfHUqlXK6oHpJEI1G9zEZH/Du16leqHyS0UXBNKmeOMf5NvyislJPB8RAFz4g8IuwofLy8k319fUP1EEouw7L7mC3kUTO1nn3sb02MTFxFpsz87FfJuaH4pu5fF+reDz+DEfxkI44Q0ScSbyOpDGe1RqMBN08o+ha0L0JdeKi/6msrGwj98uZMeon1AGaSj+elr9LwK9IkO33n8cN7Hl2vp1N3PcYbUXOBbDz9bwV1/wCmXoS3+B128OPD/l2LLg8l9APXVlZKZfzfDY7ehlQv0PPQDez6zW5JJdYOXdAwHK2dGIv7GH4YtHJIvEOvvunLCHPPzl3QOLKTkl0hPbKaDUvlTU988xtwfMqQBPQ3m/4mf0yBVlDCSr/CRW0CipAMnGzb9XU1NSRvIX7kSgo++Pg9B8wltxxbHKPZgAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center; cursor: auto;">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="text" name="address" required="true" placeholder="{{ __('home.address') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <select name="countrie_id" required="true" class="form-control shadow-control">
                                <option value="">{{ __('home.countrie') }}</option>
                                @if(count($countries)>0)
                                    @foreach ($countries as $countrie)
                                        <option value="{{ $countrie->id }}">  {{ $countrie->countriename }} </option>
                                    @endforeach
                                @endif
                            </select>                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" required="true" name="city" placeholder="{{ __('home.city') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" required="true" name="zipcode" placeholder="{{ __('home.zipcode') }}" class="form-control shadow-control">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" required="true" name="phone" placeholder="{{ __('home.phone') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input shadow-control" id="customCheck" name="terms&condition">
                                <label class="custom-control-label" for="customCheck">{{ __('home.terms&condition') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer row">
                        <div class="col-5 m-auto">
                            <button type="submit" class="btn btn-secondary btn-lg btn-block rounded-lg registerform"> {{ __('home.register') }} </button>
                        </div>
                        <div class="col-7 m-auto">
                            <a href="javascript:void()" id="register_login_btn" class="custom-control customcontrol">{{ __('home.login') }}</a>
                            <a href="javascript:void()" id="register_lost_btn"  class="custom-control customcontrol">{{ __('home.forgetpassword') }}</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>
<!---pooop up -->
<div class="modal fade" id="myModalSignSuccess" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--<div class="modal-header" align="center">
                {{ __('home.signup_response') }}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>-->
            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <div id="div-login-msg" class="text-center">
                    <div class="message text-center">
                        <!--<h3> {{ __('home.signup_resp_1') }} </h3>-->
                        <blockquote class="blockquote-reverse" style="padding-top:20px;font-weight:bold;">
                            <!--<p>{{ __('home.signup_resp_2') }}</p>-->
                            <p style="font-size: 17px;">{{ __('home.signup_resp_3') }}</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>
<div class="modal fade" id="myModalResetPassword" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content resetmypasswords" method="POST">
            <div class="modal-header text-center" style="display: block;">
                <b> {{ __('home.changepassword_title')}} </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="loginerreur text-center" id="reset_errors"></div>
                <div class="text-center" id="reset_success" style="display:none;color: green; font-weight: bold;text-align: center;">
                    {{ __('home.rest_success')}}
                </div>
                <div class="form-group tohide">
                    <label for="password">{{ __('home.password') }}<samp class="asterisk">*</samp></span> </label>
                    <input  type="password" name="password" class="form-control" placeholder="{{ __('home.password') }}" required="true">
                    <input type="hidden" name="user_id" id="user_id" />
                </div>
                <div class="form-group tohide">
                    <label for="password_confirmation">{{ __('home.comfirmpassword') }}<samp class="asterisk">*</samp></span> </label>
                    <input type="password" name="password_confirmation" class="form-control"  placeholder="{{ __('home.comfirmpassword') }}" required="true">
                </div>
            </div>
            <div class="modal-footer tohide">
                <button type="button" class="btn btn-secondary  btn-block resetmypassword">
                    {{ __('home.changepassword')}}
                </button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        if(window.location.href.indexOf("directship") > -1) {
            $('.position').val("");
            $('.position').val(1);
        }
    });
</script>
