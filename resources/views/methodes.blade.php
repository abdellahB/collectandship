@extends('layouts.app')

@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.shiptopya') }}
            </h1>
        </div>
    </div>
    <section class="section pb-5">
        <div class="container-lg py-3">
            <p class="section-description pb-4" style="text-transform: lowercase;">
                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                    {!! $methode->content_ar !!}
                @else
                    {!! $methode->content_en !!}
                @endif
            </p>
        </div>
    </section>
</div>
@endsection
