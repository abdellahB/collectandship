@extends('layouts.app')

@section('content')
<div id="body">
    <div id="land-page" class="shadow mb-4">
        <div class="container-lg py-5">
            <div class="row py-5 px-lg-1 px-5">
                <div class="col-md-6 col-sm-12 col-12">
                    <div id="land-text" class="text-justify"><span class="big-word"> {{ __('home.title_sous') }}</span> {{ __('home.title') }}</div>
                    <a href="#signup" class="btn btn-third btn-lg px-5 my-5"> {{ __('home.title_btn') }}</a>
                </div>
                <div class="col-md-6 col-sm-12 col-12">
                    <div id="colis-logo">
                        <img src="{{asset('assets/front/images/colis.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center mb-4">{{ __('home.service') }}</h1>
            <p>{{ __('home.service_sous') }}</p>
        </div>
    </div>
    <div id="warehouse" class="section">
        <div class="container-lg py-3">
            <h3 class="primary-head text-center mb-4">{{ __('home.warehouse_how') }}</h3>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="etape-item">
                        <div class="etape-inner">
                            <div class="etape-num-label">
                                <div class="etape-label">{{ __('home.warehouse_step') }}</div>
                                <div class="etape-num">1</div>
                            </div>
                        <div class="etape-icon"><img src="{{asset('assets/front/images/signup.png')}}"></div>
                        </div>
                    </div>
                    <div class="etape-caption">
                        <p>{{ __('home.warehouse_step1') }}</p>
                        <p>{{ __('home.warehouse_step2') }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="etape-item">
                        <div class="etape-inner">
                            <div class="etape-num-label">
                                <div class="etape-label">{{ __('home.warehouse_step') }}</div>
                                <div class="etape-num">2</div>
                            </div>
                        <div class="etape-icon"><img src="{{asset('assets/front/images/shopping.png')}}"></div>
                        </div>
                    </div>
                    <div class="etape-caption">
                        <p>{{ __('home.warehouse_step3') }}</p>
                        <p>{{ __('home.warehouse_step4') }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="etape-item">
                        <div class="etape-inner">
                            <div class="etape-num-label">
                                <div class="etape-label">{{ __('home.warehouse_step') }}</div>
                                <div class="etape-num">3</div>
                            </div>
                        <div class="etape-icon"><img src="{{asset('assets/front/images/delivring.png')}}"></div>
                        </div>
                    </div>
                    <div class="etape-caption">
                        <p>{{ __('home.warehouse_step5') }}</p>
                        <p>{{ __('home.warehouse_step6') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="signup" class="section">
        <div class="container-lg py-3">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-12">
                    <form class="form-bg gradient-bg p-4 rounded-lg registerforms">
                        <h3 class="white-head mb-5">{{ __('home.signup_warehouse') }}</h3>
                        <div class="row">
                            <div id="reg_errors" class="text-white text-center"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12 col-sm-12 col-12">
                                <input type="text" required="true" name="firstname" placeholder="{{ __('home.firstname') }}" class="form-control shadow-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12 col-sm-12 col-12">
                                <input type="text" required="true" name="lastname" placeholder="{{ __('home.lastname') }}" class="form-control shadow-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <input type="email" required="true" name="email" placeholder="{{ __('home.email') }}" class="form-control shadow-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <input type="password" required="true" name="password" placeholder="{{ __('home.password') }}" class="form-control shadow-control">
                                <small class="text-white"> {{ __('home.msg_password') }} </small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <input type="password" required="true" name="password_confirmation" placeholder="{{ __('home.comfirmpassword') }}" class="form-control shadow-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12 col-sm-12 col-12">
                                <select name="countrie_id" required="true" class="form-control shadow-control">
                                    <option value="">{{ __('home.countrie') }}</option>
                                    @if(count($countries)>0)
                                        @foreach ($countries as $countrie)
                                            <option value="{{ $countrie->id }}">
                                                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                    {{ $countrie->countriename_ar }}
                                                @else
                                                    {{ $countrie->countriename }}
                                                @endif
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" checked class="custom-control-input shadow-control" id="customCheck" name="terms&condition">
                                    <label class="custom-control-label"  for="customCheck">{{ __('home.terms&condition') }}</label>
                                </div>
                            </div>
                        </div>
                        <style>
                            .g-recaptcha div {
                                width: auto !important;
                            }
                        </style>
                        <div class="row form-group">
                            <div class="col-12 text-center">
                                {!! app('captcha')->display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 m-auto">
                                @if (!Auth::check())
                                    <button type="submit" class="btn btn-secondary btn-lg btn-block rounded-lg registerform"> {{ __('home.signup') }} </button>
                                @else
                                    <a href="{{url('account')}}" class="btn btn-secondary btn-lg btn-block rounded-lg"> {{ __('home.account') }} </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-12 col-12 mt-md-0 mt-sm-5">
                    <h3 class="secondary-head text-shadow">{{ __('home.title_buy') }}</h3>
                    <h3 class="secondary-head text-shadow">{{ __('home.title_buy1') }}</h3>
                    <div id="image-slider" class="mt-5">
                        <div id="image-slider-inner">
                            <div class="image-slider-item">
                                <img src="{{asset('assets/front/images/amazonsa.jpg')}}">
                            </div>
                            <div class="image-slider-control right"></div>
                            <div class="image-slider-control left"></div>
                        </div>
                        <div id="image-slider-container">
                            <div class="row">
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/o-company.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/carrefour.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/jarjir.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/danube.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/wadi.png')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/samad.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/pand.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/arabian-oud.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/daraah.png')}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="image-slider-item">
                                        <img src="{{asset('assets/front/images/extra.png')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5 text-center">
                                <a href="{{ url('stores')}}" class="btn btn-secondary"> {{ __('home.allstore') }} </a>
                            </div>
                        </div>
                        <script type="text/javascript">
                            var current = 0;
                            $(document).on('click', '.image-slider-control', function() {
                                var right = $(this).hasClass('right');
                                var total = $("#image-slider-container").find('.image-slider-item').length;

                                if( right )
                                    current--;
                                else
                                    current++;

                                if( current < 0 )
                                    current = total - 1;

                                if( current >= total )
                                    current = 0;

                                var current_wrap = $("#image-slider-container").find('.image-slider-item').eq(current);
                                var holder_wrap = $("#image-slider-inner").find('.image-slider-item');

                                var current_img = current_wrap.html();
                                var holder_img = holder_wrap.html();

                                current_wrap.html(holder_img);
                                holder_wrap.html(current_img);
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container-lg py-3">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="row">
                        <div class="col company-col"><img src="{{asset('assets/front/images/saudi-barid.png')}}"></div>
                        <div class="col company-col"><img src="{{asset('assets/front/images/aramex.png')}}"></div>
                        <div class="col company-col"><img src="{{asset('assets/front/images/sms-ex.png')}}"></div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="row">
                        <div class="col company-col"><img src="{{asset('assets/front/images/dhl.png')}}"></div>
                        <div class="col company-col"><img src="{{asset('assets/front/images/fedex.png')}}"></div>
                        <div class="col company-col"><img src="{{asset('assets/front/images/ups.png')}}"></div>
                        <div class="col company-col"><img src="{{asset('assets/front/images/tnt.png')}}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
