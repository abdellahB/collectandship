<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>لوحة التحكم </title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@naksoid">
    <meta name="twitter:creator" content="@naksoid">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/application.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" >
    {!! NoCaptcha::renderJs('ar') !!}
  </head>
  <body class="layout layout-header-fixed">


    @yield('content')

  <script src="{{asset('assets/admin/js/vendor.js')}}"></script>
  <script src="{{asset('assets/admin/js/elephant.js')}}"></script>
  <script src="{{ asset('assets/admin/js/application.js')}}"></script>
  <script src="{{ asset('assets/admin/js/demo.js') }}"></script>
  <script src="{{ asset('assets/admin/js/jquery-ui-1.10.3.custom.min.js')}}"></script>
  <script src="{{ asset('assets/admin/js/javascript.js')}}"></script>
  </body>
</html>
