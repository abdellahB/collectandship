<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>لوحة التحكم</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@naksoid">
    <meta name="twitter:creator" content="@naksoid">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('assets/admin/css/application.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery-ui-1.10.3.custom.min.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" >
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.css" integrity="sha512-n4zeiQQn2mF+WmlJPAI8KFYroekSgfduwHgU19cweh8xaxgfW1s+NL8njPqj8tyB+asq2kYh5beRCJhGZUR+mw==" crossorigin="anonymous" />
    <style type="text/css" media="print">
		@page
		{
			size:  auto;   /* auto is the initial value */
			margin: 0mm;  /* this affects the margin in the printer settings */
		}

		html
		{
			background-color: #FFFFFF;
			margin: 0px;  /* this affects the margin on the html before sending to printer */
		}

		body
		{
			margin: 10mm 15mm 10mm 15mm; /* margin you want for the content */
		}
    </style>

  </head>
<body class="layout layout-header-fixed">
<div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="{{url('control')}}">
            <img style="width: 120px; height: 40px; margin-top: -10px;" src="{{asset('assets/front/images/white-logo.png')}}">
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="@if(Auth::guard('admin')->user()->image != '-') {{asset('assets/admin/img/'.Auth::guard('admin')->user()->image)}} @else {{asset('assets/admin/img/0180441436.jpg')}} @endif" alt="{{Auth::guard('admin')->user()->username}}"/>
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center"> مرحبا .</h4>
              </li>
              <!--<li class="dropdown">
                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon-with-child hidden-xs">
                    <span class="icon icon-bell-o icon-lg"></span>
                    <span class="badge badge-danger badge-above right">0</span>
                  </span>
                  <span class="visible-xs-block">
                    <span class="icon icon-bell icon-lg icon-fw"></span>
                    <span class="badge badge-danger pull-right">0</span>
                    الاشعارات
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                  <div class="dropdown-header">
                    <a class="dropdown-link" href="#">جعل الكل كمقروء</a>
                    <h5 class="dropdown-heading">جديد الاشعارات</h5>
                  </div>
                  <div class="dropdown-body">
                    <div class="custom-scrollable-area" style="position: relative; overflow: hidden; width: 100%; height: 100%;"><div class="list-group list-group-divided custom-scrollbar" style="overflow: hidden; width: 100%; height: 100%;">
                      <!--<a class="list-group-item" href="#">
                        <div class="notification">
                          <div class="notification-media">
                            <span class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>
                          </div>
                          <div class="notification-content">
                            <small class="notification-timestamp">35 min</small>
                            <h5 class="notification-heading">Update Status</h5>
                            <p class="notification-text">
                              <small class="truncate">Failed to get available update data. To ensure the proper functioning of your application, update now.</small>
                            </p>
                          </div>
                        </div>
                      </a>-
                    </div><div class="custom-scrollbar-gripper" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 4px;"></div><div class="custom-scrollbar-track" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 4px;"></div></div>
                  </div>
                  <div class="dropdown-footer">
                    <a class="dropdown-btn" href="#">مشاهدة الكل </a>
                  </div>
                </div>
              </li>-->
              <!--<li class="dropdown">
                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
                  <span class="icon-with-child hidden-xs">
                    <span class="icon icon-envelope-o icon-lg"></span>
                    <span class="badge badge-danger badge-above right">0</span>
                  </span>
                  <span class="visible-xs-block">
                    <span class="icon icon-envelope icon-lg icon-fw"></span>
                    <span class="badge badge-danger pull-right">0</span>
                    الرسائل
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                  <div class="dropdown-header">
                    <a class="dropdown-link" href="{{url('control/messenger')}}">رسالة جديدة</a>
                    <h5 class="dropdown-heading">جديد الرسائل</h5>
                  </div>
                  <div class="dropdown-body">
                    <div class="list-group list-group-divided custom-scrollbar">

                    </div>
                  </div>
                  <div class="dropdown-footer">
                    <a class="dropdown-btn" href="#">مشاهدة الكل</a>
                  </div>
                </div>
              </li>-->
              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                  <img class=" img-circle" width="36" height="36" src="@if(Auth::guard('admin')->user()->image != '-') {{asset('assets/img/'.Auth::guard('admin')->user()->image)}} @else {{asset('assets/admin/img/0180441436.jpg')}} @endif" alt="{{Auth::guard('admin')->user()->username}}">
                  {{Auth::guard('admin')->user()->username}}
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li class="divider"></li>
                  <!--<li><a href="{{url('control/profile')}}" id="viewprofile" data-id="">الملف الشخصي</a></li>-->
                  <li><a href="{{url('control/logout')}}">تسجيل الخروج</a></li>
                </ul>
              </li>
              <!--<li class="visible-xs-block">
                <a href="{{url('control/profile')}}">
                  <span class="icon icon-user icon-lg icon-fw"></span>
                  الملف الشخصي
                </a>
              </li>-->
              <li class="visible-xs-block">
                <a href="{{url('control/logout')}}">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                 تسجيل الخروج
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
</div>
<div class="layout-main">
    <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">
                <li class="sidenav-search hidden-md hidden-lg">
                  <form class="sidenav-form" action="/">
                    <div class="form-group form-group-sm">
                      <div class="input-with-icon">
                        <input class="form-control" type="text" placeholder="Search…">
                        <span class="icon icon-search input-icon"></span>
                      </div>
                    </div>
                  </form>
                </li>
                <li class="sidenav-heading">القائمة</li>
                <li class="sidenav-item">
                  <a href="{{url('/control')}}">
                    <span class="sidenav-label">لوحة التحكم </span>
                  </a>
                </li>
                @if(Auth::guard('admin')->user()->role == "admin" || Auth::guard('admin')->user()->role == "employers")
                <li class="sidenav-heading"> خدمات التجميع والشحن</li>
                <li class="sidenav-item">
                    <a href="{{url('/control/import')}}">
                      <span class="sidenav-label"> رفع شحنة </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/unknow')}}">
                      <span class="sidenav-label"> شحنات مجهولة </span>
                    </a>
                </li>
                <li class="sidenav-item">
                  <a href="{{url('/control/all')}}">
                  <span class="badge badge-success">{{ $parcel }}</span>
                    <span class="sidenav-label">شحنات معروفة</span>
                  </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/moreimage')}}">
                    <span class="badge badge-success">{{ $image }}</span>
                      <span class="sidenav-label">خدمات اضافية</span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/return')}}">
                    <span class="badge badge-success">{{ $return }}</span>
                      <span class="sidenav-label">طلبات ارجاع</span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/new')}}">
                       <span class="badge badge-success">{{ $packing }}</span>
                      <span class="sidenav-label"> طلبات التغليف </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/payment')}}">
                      <span class="badge badge-success">{{ $packing1 }}</span>
                      <span class="sidenav-label"> انتظار الدفع  </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/waiting')}}">
                      <span class="badge badge-success">{{ $packing2 }}</span>
                      <span class="sidenav-label"> انتظار الشحن  </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/shipped')}}">
                      <span class="badge badge-success">{{ $packing3 }}</span>
                      <span class="sidenav-label"> ارشيف الشحنات </span>
                    </a>
                </li>
                <li class="sidenav-heading"> المتسوق الشخصي</li>
                <li class="sidenav-item">
                    <a href="{{url('/control/purchase/new')}}">
                    <span class="badge badge-success">{{$item1}}</span>
                      <span class="sidenav-label"> طلبات جديدة </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/purchase/waiting')}}">
                      <span class="badge badge-success">{{$item2}}</span>
                      <span class="sidenav-label"> انتظار الدفع  </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/purchase/proccessing')}}">
                      <span class="badge badge-success">{{$item3}}</span>
                      <span class="sidenav-label"> قيد الشراء  </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/purchase/purchase')}}">
                      <span class="badge badge-success">{{$item4}}</span>
                      <span class="sidenav-label"> تم الشراء  </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/purchase/completed')}}">
                      <span class="badge badge-success">{{$item5}}</span>
                      <span class="sidenav-label"> طلبات مكتملة  </span>
                    </a>
                </li>
                <li class="sidenav-heading"> الشحن المباشر  </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/direct/new')}}">
                    <span class="badge badge-success"> {{ $order1 }}</span>
                      <span class="sidenav-label"> طلبات جديدة </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/direct/recerved')}}">
                      <span class="badge badge-success"> {{ $order2 }}</span>
                      <span class="sidenav-label"> تم الاستلام </span>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/shipping/direct/shipped')}}">
                      <span class="badge badge-success"> {{ $order3 }}</span>
                      <span class="sidenav-label"> وصلت المركز  </span>
                    </a>
                </li>
                <li class="sidenav-heading"> العملاء </li>
                <li class="sidenav-item">
                  <a href="{{url('/control/clients')}}">
                    <span class="sidenav-label">قائمة العملاء </span>
                  </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{url('/control/stores')}}">
                        <span class="sidenav-label"> دليل مواقع التسوق</span>
                    </a>
                </li>
                @endif
                @if(Auth::guard('admin')->user()->role == "admin")
                <li class="sidenav-heading"></li>
                <li class="sidenav-item has-subnav">
                    <a href="#" aria-haspopup="true">
                      <span class="sidenav-icon icon icon-wpforms"></span>
                      <span class="sidenav-label"> الصفحات</span>
                    </a>
                    <ul class="sidenav-subnav collapse">
                      <li><a href="{{ url('/control/terms') }}">اتفافية الاستخدام </a></li>
                      <li><a href="{{ url('/control/privacy') }}">سياسة الخصوصية </a></li>
                      <li><a href="{{ url('/control/faq') }}">الاسئلة الشائعة</a></li>
                      <li><a href="{{ url('/control/about') }}">حول الموقع </a></li>
                      <li><a href="{{ url('/control/methode') }}"> وسائل الدفع/الشحن </a></li>
                    </ul>
                </li>
                <li class="sidenav-item has-subnav">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-list"></span>
                    <span class="sidenav-label">الاعدادات</span>
                  </a>
                  <ul class="sidenav-subnav collapse">
                    <li><a href="{{url('/control/transaction')}}">المعاملات المالية</a></li>
                    <li><a href="{{ url('/control/general') }}"> معلومات عامة</a></li>
                    <li><a href="{{ url('/control/users') }}"> المستخدمين</a></li>
                    <li><a href="{{ url('/control/company') }}"> شركات الشحن </a></li>
                    <li><a href="{{ url('/control/address') }}"> عناوين التسوق  </a></li>
                    <li><a href="{{ url('/control/services') }}">  خدمات التجهيز  </a></li>
                    <li><a href="{{ url('/control/modepay') }}">  وسائل الدفع   </a></li>
                    <li><a href="{{ url('/control/usability') }}">  معلومات توضحية   </a></li>
                  </ul>
                </li>
                @endif
                <li class="sidenav-heading"></li>
                <li class="sidenav-heading"></li>
              </ul>
            </nav>
          </div>
        </div>
    </div>

    @yield('content')

  </div>
  @include('control.partial.modal')

  <script src="{{asset('assets/admin/js/vendor.js')}}"></script>
  <script src="{{asset('assets/admin/js/elephant.js')}}"></script>
  <script src="{{ asset('assets/admin/js/application.js')}}"></script>
  <script src="{{ asset('assets/admin/js/demo.js') }}"></script>
  <script src="{{ asset('assets/admin/js/jquery-ui-1.10.3.custom.min.js')}}"></script>
  <script src="{{ asset('assets/admin/js/javascript.js')}}"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pobo3e0004h6lj73j1jcn8hp1m4pdzanf7nlpkap43i7r6el"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.js" integrity="sha512-NeIRO2UIUAi8hOoecRAxOR7bKLarifYdzZ9e8XZ5RtCRhty4MflgqkdPTq3oP4FC17GpBRVkUdy/Goe4rjNudw==" crossorigin="anonymous"></script><script>
      tinymce.init({
          selector:'.content',
          language: 'ar',
          height: 250,
          theme: 'modern',
          plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
          ],
          toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
              { title: 'Test template 1', content: 'Test 1' },
              { title: 'Test template 2', content: 'Test 2' }
          ],
          directionality : 'rtl',
          language_url : '{{ url('langs/ar.js') }}',

      });
  </script>
    <script type="text/javascript">
        function show_hide_row(row){
            $("#"+row).toggle();
        }
    </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.5.0/jQuery.print.min.js"></script>
  <!--date picker-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
  <script>
      $(document).ready(function(){
          $(".sidenav li").click(function(){
              var id = $(this).attr("id");
              $('#' + id).siblings().find(".active").removeClass("active");
              $('#' + id).addClass("active");
              localStorage.setItem("selectedolditem", id);
          });
          var selectedolditem = localStorage.getItem('selectedolditem');
          if (selectedolditem != null) {
              $('#' + selectedolditem).siblings().find(".active").removeClass("active");
              $('#' + selectedolditem).addClass("active");
          }
          $('.mewlist li').on("click",function(){
              var idnav = $(this).attr("id");
              $('#' + idnav).siblings().find(".activetabs").removeClass("activetabs");
              $('#' + idnav).addClass("activetabs");
              location.reload();
              localStorage.setItem("selectedactivetabs", idnav);
          });
          var selectedactivetabs = localStorage.getItem('selectedactivetabs');
          if (selectedactivetabs != null) {
              $('#' + selectedactivetabs).siblings().find(".activetabs").removeClass("activetabs");
              $('#' + selectedactivetabs).addClass("activetabs");
          }
          $('.new_datepicker').datepicker({
              setDate: new Date(),
              format: 'yyyy-mm-dd',
              todayHighlight: true,
              autoclose: true
          });
          $(".datetopickup").datepicker({
              setDate: new Date(),
              format: 'yyyy-mm-dd',
              todayHighlight: true,
              autoclose: true
          });
      });
  </script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <img class="loading" style="height: 90px; position: fixed; top: 50%; right: 50%; display: none;" src="{{asset('assets/user/img/loading.gif')}}"/>
  </body>
</html>
