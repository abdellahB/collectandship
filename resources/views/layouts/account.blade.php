<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="baseurl" content="{{ LaravelLocalization::getCurrentLocale() }}"/>
    <meta name="url" content="{{url('/')}}/{{ LaravelLocalization::getCurrentLocale() }}"/>
    <title>
        @if( LaravelLocalization::getCurrentLocale() == 'ar')
            {{ $setting->sitename }} | {{ __('home.account') }}
        @else
            {{ $setting->sitename_en }} | {{ __('home.account') }}
        @endif
    </title>
    @if( LaravelLocalization::getCurrentLocale() == 'ar')
        <link rel="stylesheet" href="{{asset('css/user/rtl.css')}}">
        <!--<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap-rtl.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/user/css/rtl.css')}}">-->
    @else
        <link rel="stylesheet" href="{{asset('css/user/ltr.css')}}">
        <!--<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/ltr.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/user/css/ltr.css')}}">-->
    @endif
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/fa/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/sweetalert.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/user/css/jquery.fancybox.css')}}"/>
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <style>
        .flashy {
            bottom: 80% !important;
        }
    </style>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M2SNHDK');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M2SNHDK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="global">
        <div id="header" class="shadow">
            <div class="container-fluid py-2">
                <div class="row px-lg-5">
                    <div class="col-md-2 col-12">
                        <div class="header-logo">
                            <a href="{{ url('/account')}}">
                                <img src="{{asset('assets/front/images/site-logo.png')}}">
                            </a>
                            <button id="collapse-menu" class="navbar-toggler d-md-none d-block" type="button">
                                <span class="fa fa-bars"></span>
                            </button>
                            <div class="float-left d-sm-block d-md-none cartmobile">
                                <a href="{{ url('checkout')}}">
                                    <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                                    <span class="badge badge-pill badge-danger">
                                        {{ Cart::instance('collectandship')->content()->count() }}
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $("#collapse-menu").on('click', function() {
                            $("#collapsed-menu").toggleClass('show');
                            if( $("#collapsed-menu").hasClass('show') )
                                $('body').addClass('menu-shown');
                            else
                                $('body').removeClass('menu-shown');
                        });
                    </script>
                    <div id="collapsed-menu" class="col-md-10 d-md-block d-none">
                        <div class="clearfix">
                            <div id="top-nav" class="float-left">
                                <ul class="nav">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li  class="nav-item">
                                            <a class="nav-link" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                <span class="nav-icon">
                                                    <img src="@if($localeCode=="ar" ){{asset('assets/front/images/saudia-flag.png')}} @else {{asset('assets/front/images/usa-flag.png')}} @endif">
                                                </span>
                                                <span> {{ $properties['native'] }} </span>
                                            </a>
                                        </li>
                                    @endforeach
                                    <li class="dropdown">
                                        <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                        {{ __('account.currency') }} ( {{ Session::get('currency') }} )
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="{{ url('currency','$') }}"> {{ __('account.dollar') }}</a>
                                          <a class="dropdown-item" href="{{ url('currency','ريال') }}"> {{ __('account.sar') }}</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div id="main-nav" class="float-left">
                                <ul class="nav d-none d-lg-flex">
                                    <li class="nav-item md">
                                        <a href="{{ url('checkout')}}" class="btn btn-info" >
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                            {{ __('home.cart') }}
                                            <span class="badge badge-pill badge-danger">
                                                {{ Cart::instance('collectandship')->content()->count() }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/account')}}">
                                            <span>{{ __('home.account') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/logout')}}">
                                            <span> {{ __('home.logout') }} </span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav d-sm-block d-md-none">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/account')}}">
                                            <span>{{ __('home.home') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/product')}}">
                                            <span>{{ __('account.menu_2') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('processing')}}">
                                            <span>{{ __('account.menu_3') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('payment')}}">
                                            <span>{{ __('account.menu_4') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('archive_prod')}}">
                                            <span>{{ __('account.menu_5') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('prod_service')}}">
                                            <span> {{ __('account.menu_7') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('inprocess')}}">
                                            <span>{{ __('account.menu_8') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('archive')}}">
                                            <span>{{ __('account.menu_9') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('return')}}">
                                            <span>{{ __('account.menu_11') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('waitreturn')}}">
                                            <span>  {{ __('account.menu_12') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('archive_return')}}">
                                            <span>{{ __('account.menu_13') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('purchase/create')}}">
                                            <span> {{ __('account.menu_15') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('purchase/waiting')}}">
                                            <span> {{ __('account.menu_16') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('purchase/payment')}}">
                                            <span> {{ __('account.menu_17') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('purchase/completed')}}">
                                            <span> {{ __('account.menu_18') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('archive_purchase')}}">
                                            <span>{{ __('account.menu_19') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('directship')}}">
                                            <span> {{ __('home.directship') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('shipement')}}">
                                            <span> {{ __('account.menu_21') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('ship_archive')}}"> <!--shipement-->
                                            <span>{{ __('account.menu_22') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('profile')}}">
                                            <span> {{ __('account.menu_23') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('address')}}">
                                            <span> {{ __('account.menu_24') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('transaction')}}">
                                            <span> {{ __('account.menu_24_4') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/logout')}}">
                                            <span> {{ __('home.logout') }} </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="body">
            <div id="account">
                <div class="container-fluid pb-3">
                    <div class="row">
                        <div class="col-md-2 col-sm-12 col-12 d-none d-sm-block">
                            <div class="shadow py-3" id="main-nav">
                                <ul class="nav vertical">
                                    <li class="nav-item" id="1">
                                        <a class="nav-link active_nav" href="{{url('/account')}}">
                                            <span>{{ __('home.home') }}</span>
                                        </a>
                                    </li>
                                    <hr/>
                                    <li class="sidenav-heading"> {{ __('account.menu_1') }} </li>
                                    <li class="nav-item" id="2">
                                        <a class="nav-link" href="{{url('/product')}}">
                                            <span>{{ __('account.menu_2') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="3">
                                        <a class="nav-link" href="{{url('processing')}}">
                                            <span>{{ __('account.menu_3') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="4">
                                        <a class="nav-link" href="{{url('payment')}}">
                                            <span>{{ __('account.menu_4') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="5">
                                        <a class="nav-link" href="{{url('archive_prod')}}">
                                            <span>{{ __('account.menu_5') }} </span>
                                        </a>
                                    </li>
                                    <hr/>
                                    <li class="sidenav-heading">{{ __('account.menu_6') }}</li>
                                    <li class="nav-item" id="6">
                                        <a class="nav-link" href="{{url('prod_service')}}">
                                            <span> {{ __('account.menu_7') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="7">
                                        <a class="nav-link" href="{{url('inprocess')}}">
                                            <span>{{ __('account.menu_8') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="8">
                                        <a class="nav-link" href="{{url('archive')}}">
                                            <span>{{ __('account.menu_9') }}  </span>
                                        </a>
                                    </li>
                                    <hr/>
                                    <li class="sidenav-heading">{{ __('account.menu_10') }}</li>
                                    <li class="nav-item" id="9">
                                        <a class="nav-link" href="{{url('return')}}">
                                            <span>{{ __('account.menu_11') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="10">
                                        <a class="nav-link" href="{{url('waitreturn')}}">
                                            <span>  {{ __('account.menu_12') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="11">
                                        <a class="nav-link" href="{{url('archive_return')}}">
                                            <span>{{ __('account.menu_13') }}  </span>
                                        </a>
                                    </li>
                                    <hr/>
                                    <li class="sidenav-heading">{{ __('account.menu_14') }}</li>
                                    <li class="nav-item" id="12">
                                        <a class="nav-link" href="{{url('purchase/create')}}">
                                            <span> {{ __('account.menu_15') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="13">
                                        <a class="nav-link" href="{{url('purchase/waiting')}}">
                                            <span> {{ __('account.menu_16') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="14">
                                        <a class="nav-link" href="{{url('purchase/payment')}}">
                                            <span> {{ __('account.menu_17') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="15">
                                        <a class="nav-link" href="{{url('purchase/completed')}}">
                                            <span> {{ __('account.menu_18') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="16">
                                        <a class="nav-link" href="{{url('archive_purchase')}}">
                                            <span>{{ __('account.menu_19') }}  </span>
                                        </a>
                                    </li>
                                    <hr>
                                    <li class="sidenav-heading">{{ __('account.menu_20') }}</li>
                                    <li class="nav-item" id="17">
                                        <a class="nav-link" href="{{ url('directship')}}">
                                            <span> {{ __('home.directship') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="17_1">
                                        <a class="nav-link" href="{{ url('shipement')}}">
                                            <span> {{ __('account.menu_21') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="18">
                                        <a class="nav-link" href="{{ url('ship_archive')}}"> <!--shipement-->
                                            <span>{{ __('account.menu_22') }}  </span>
                                        </a>
                                    </li>
                                    <hr>
                                    <li class="nav-item" id="19">
                                        <a class="nav-link" href="{{url('profile')}}">
                                            <span> {{ __('account.menu_23') }}  </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="20">
                                        <a class="nav-link" href="{{url('address')}}">
                                            <span> {{ __('account.menu_24') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item" id="21">
                                        <a class="nav-link" href="{{url('transaction')}}">
                                            <span> {{ __('account.menu_24_4') }} </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-10 col-sm-12 col-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div id="top-footer" class="secondary-bg @if( LaravelLocalization::getCurrentLocale() == 'ar') text-md-right @else text-md-left @endif text-center">
                <div class="container-lg py-4">
                    <div class="row">
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5">
                            <h4 class="white-head">{{ __('home.modepay') }}</h4>
                            <div id="payment-methods" class="mx-md-0 mx-auto">
                                <div class="row text-center pt-2">
                                <div class="col-4"><img src="{{asset('assets/front/images/amex.png')}}" width="50px" height="34px"></div>
                                <div class="col-4"><img src="{{asset('assets/front/images/visa.png')}}" width="50px" height="34px"></div>
                                <div class="col-4"><img src="{{asset('assets/front/images/master-card.png')}}" width="50px" height="34px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5 text-center">
                        <a href=""><img src="{{asset('assets/front/images/white-logo.png')}}" height="50px"></a>
                            <h4 class="white-head">{{ __('home.contactus') }}</h4>
                            <div id="social-links" class="row text-center pt-2">
                                <div class="col-3"><a class="social-icon" href="http://collectandship.com/"><span class="fal fa-globe"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->instagram}}"><span class="fab fa-instagram"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->twitter}}"><span class="fab fa-twitter"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->facebook}}"><span class="fab fa-facebook-square"></span></a></div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5">
                            <ul class="list-links">
                                <li><a class="white-color" href="{{ url('/faq')}}">{{ __('home.faq') }}</a></li>
                                <!--<li><a class="white-color" href="{{ url('/contact')}}">{{ __('home.questions') }}</a></li>-->
                                <li><a class="white-color" href="{{ url('/privacy')}}">{{ __('home.privacy') }}</a></li>
                                <li><a class="white-color" href="{{ url('/terms')}}">{{ __('home.terms') }}</a></li>
                                <li><a class="white-color" href="{{ url('/ship-pay-method')}}">{{ __('home.shiptopya') }}</a></li>
                                <!--<li><a class="white-color" href="{{ url('/directship')}}">{{ __('home.directship') }}</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="copy-footer" class="primary-bg text-center">
                <div class="clearfix">
                    <div class="float-right">
                        <a id="top-btn" class="white-color" href="#top"><span class="far fa-angle-up"></span></a>
                    </div>
                    <script type="text/javascript">
                        $(document).on('click', '#top-btn', function() {
                            $('html, body').animate({scrollTop : 0},800);
                            return false;
                        });
                        $(document).ready(function() {
                            $(".vertical li").click(function(){
                                var id = $(this).attr("id");
                                $('#' + id).siblings().find(".active_nav").removeClass("active_nav");
                                $('#' + id).addClass("active_nav");
                                localStorage.setItem("selectedolditem", id);
                            });
                            var selectedolditem = localStorage.getItem('selectedolditem');
                            if (selectedolditem != null) {
                                $('#' + selectedolditem).siblings().find(".active_nav").removeClass("active_nav");
                                $('#' + selectedolditem).addClass("active_nav");
                            }
                        });
                    </script>
                    <span class="copy-text">  {{ __('home.copyright') }} &copy;</span>
                </div>
            </div>
        </div>
    </div>
	<script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/front/js/sweetalert.min.js')}}" defer></script>
    <script src="{{asset('assets/user/js/jquery.fancybox.js')}}"></script>
    <script src="{{asset('assets/front/js/script.js?v=1')}}"></script>
    <script src="{{asset('assets/user/js/script.js?v=1')}}"></script>

    <img class="loading" style="height: 90px; position: fixed; top: 50%; right: 50%; display: none;" src="{{asset('assets/user/img/loading.gif')}}"/>
    @include('flashy::message')
    @include('account.partial.modal')
</body>
</html>
