<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <title>@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->sitename }}@else{{ $setting->sitename_en }}@endif</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="baseurl" content="{{ LaravelLocalization::getCurrentLocale() }}"/>
    <meta name="url" content="{{url('/')}}/{{ LaravelLocalization::getCurrentLocale() }}"/>
    <meta name="title" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->sitename }}@else{{ $setting->sitename_en }}@endif">
    <meta name="description" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->description }}@else{{ $setting->description_en }}@endif">
    <meta name=”robots” content="index, follow">
    <meta name="language" content="{{ LaravelLocalization::getCurrentLocaleName() }}">
    <link rel="alternate" href="{{url('/')}}/ar" hreflang="ar" />
    <link rel="alternate" href="{{url('/')}}/en" hreflang="en" />
    <link href="{{ Request::url() }}" rel="canonical">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{url('/')}}/{{ LaravelLocalization::getCurrentLocale() }}">
    <meta property="og:title" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->sitename }}@else{{ $setting->sitename_en }}@endif">
    <meta property="og:description" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->description }}@else{{ $setting->description_en }}@endif">
    <meta property="og:image" content="">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{url('/')}}/{{ LaravelLocalization::getCurrentLocale() }}">
    <meta property="twitter:title" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->sitename }}@else{{ $setting->sitename_en }}@endif">
    <meta property="twitter:description" content="@if( LaravelLocalization::getCurrentLocale() == 'ar'){{ $setting->description }}@else{{ $setting->description_en }}@endif">
    <meta property="twitter:image" content="">
    @if( LaravelLocalization::getCurrentLocale() == 'ar')
    <link rel="stylesheet" href="{{ asset('css/rtl.css') }}">
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">-->
    @else
    <link rel="stylesheet" href="{{ asset('css/ltr.css') }}">
    <!--<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/ltr.css')}}">-->
    @endif
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/fa/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/sweetalert.min.css')}}">
    <script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    {!! NoCaptcha::renderJs(LaravelLocalization::getCurrentLocale()) !!}
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M2SNHDK');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M2SNHDK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="global">
        <div id="header" class="shadow">
            <div class="container-lg py-3">
                <div class="row px-lg-1 px-5">
                    <div class="col-md-2 col-12">
                        <div class="header-logo">
                            <a href="{{ url('/')}}">
                                <img src="{{asset('assets/front/images/site-logo.png')}}">
                            </a>
                            <button id="collapse-menu" class="navbar-toggler d-md-none d-block" type="button">
                                <span class="fa fa-bars"></span>
                            </button>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $("#collapse-menu").on('click', function() {
                            $("#collapsed-menu").toggleClass('show');
                            if( $("#collapsed-menu").hasClass('show') )
                                $('body').addClass('menu-shown');
                            else
                                $('body').removeClass('menu-shown');
                        });
                    </script>
                    <div id="collapsed-menu" class="col-md-10 d-md-block d-none">
                        <div class="clearfix">
                            <div id="top-nav" class="float-left">
                                <ul class="nav">
                                    @if (!Auth::check())
                                    <li class="nav-item-btn form-inline">
                                        <a class="btn btn-secondary btn-sm border-0" href="#" data-toggle="modal" data-target="#myModalSign"><span>
                                            {{ __('home.register') }}
                                        </span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#" data-toggle="modal" data-target="#loginmodal"><span class="nav-icon">
                                            <img src="{{asset('assets/front/images/user-icon.png')}}"></span> <span>
                                                {{ __('home.login') }}
                                            </span>
                                        </a>
                                    </li>
                                    @else
                                        <li class="nav-item-btn form-inline">
                                            <a class="nav-link" href="{{ url('account')}}" ><span> {{ __('home.account') }} </span></a>
                                        </li>
                                        <li class="nav-item-btn form-inline">
                                            <a class="nav-link" href="{{url('/logout')}}">
                                                <span> {{ __('home.logout') }} </span>
                                            </a>
                                        </li>
                                    @endif
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li  class="nav-item">
                                            <a class="nav-link" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                <span class="nav-icon">
                                                    <img src="@if($localeCode=="ar" ){{asset('assets/front/images/saudia-flag.png')}} @else {{asset('assets/front/images/usa-flag.png')}} @endif">
                                                </span>
                                                <span> {{ $properties['native'] }} </span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="clearfix">
                            <div id="main-nav" class="float-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/')}}"><span class="nav-icon-lg">
                                            <img src="{{asset('assets/front/images/home-icon.png')}}"></span> <span>
                                                {{ __('home.home') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('about')}}"><span class="nav-icon-lg">
                                            <img src="{{asset('assets/front/images/question-icon.png')}}"></span> <span>
                                                {{ __('home.whyus') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#warehouse"><span class="nav-icon-lg">
                                            <img src="{{asset('assets/front/images/tabs-icon.png')}}"></span> <span>
                                                {{ __('home.warehouse') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('pricing')}}"><span class="nav-icon-lg">
                                            <img src="{{asset('assets/front/images/calc-price.png')}}"></span> <span>
                                                {{ __('home.calculator') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('contact')}}"><span class="nav-icon-lg">
                                            <img src="{{asset('assets/front/images/contact-icon.png')}}"></span>
                                            <span>
                                                {{ __('home.contact') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('stores')}}">
                                          <span>
                                           {{ __('home.directory') }}
                                          </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        <div id="footer">
            <div id="top-footer" class="secondary-bg @if( LaravelLocalization::getCurrentLocale() == 'ar') text-md-right @else text-md-left @endif text-center">
                <div class="container-lg py-4">
                    <div class="row">
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5">
                            <h4 class="white-head">{{ __('home.modepay') }}</h4>
                            <div id="payment-methods" class="mx-md-0 mx-auto">
                                <div class="row text-center pt-2">
                                <div class="col-4"><img src="{{asset('assets/front/images/amex.png')}}" width="50px" height="34px"></div>
                                <div class="col-4"><img src="{{asset('assets/front/images/visa.png')}}" width="50px" height="34px"></div>
                                <div class="col-4"><img src="{{asset('assets/front/images/master-card.png')}}" width="50px" height="34px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5 text-center">
                        <a href=""><img src="{{asset('assets/front/images/white-logo.png')}}" height="50px"></a>
                            <h4 class="white-head">{{ __('home.contactus') }}</h4>
                            <div id="social-links" class="row text-center pt-2">
                                <div class="col-3"><a class="social-icon" href="http://collectandship.com/" target="_blank"><span class="fal fa-globe"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->instagram}}" target="_blank"><span class="fab fa-instagram"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->twitter}}" target="_blank"><span class="fab fa-twitter"></span></a></div>
                                <div class="col-3"><a class="social-icon" href="{{ $setting->facebook}}" target="_blank"><span class="fab fa-facebook-square"></span></a></div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12 mb-5">
                            <ul class="list-links">
                                <li><a class="white-color" href="{{ url('/')}}">{{ __('home.home') }}</a></li>
                                <li><a class="white-color" href="{{ url('/faq')}}">{{ __('home.faq') }}</a></li>
                                <!--<li><a class="white-color" href="{{ url('/contact')}}">{{ __('home.questions') }}</a></li>-->
                                <li><a class="white-color" href="{{ url('/privacy')}}">{{ __('home.privacy') }}</a></li>
                                <li><a class="white-color" href="{{ url('/terms')}}">{{ __('home.terms') }}</a></li>
                                <li><a class="white-color" href="{{ url('/ship-pay-method')}}">{{ __('home.shiptopya') }}</a></li>
                                <!--<li><a class="white-color" href="{{ url('/directship')}}">{{ __('home.directship') }}</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="copy-footer" class="primary-bg text-center">
                <div class="clearfix">
                    <div class="float-right">
                        <a id="top-btn" class="white-color" href="#top"><span class="far fa-angle-up"></span></a>
                    </div>
                    <script type="text/javascript">
                        $(document).on('click', '#top-btn', function() {
                            $('html, body').animate({scrollTop : 0},800);
                            return false;
                        });
                    </script>
                    <span class="copy-text"> {{ __('home.copyright') }} &copy;</span>
                </div>
            </div>
        </div>
    </div>
    @if(!empty(Session::get('activated')) && Session::get('activated') == 1)
    <script>$(function() { $('#ModalVerify').modal('show'); });</script>
    @endif
    @if(!empty(Session::get('resetpassword')) && Session::get('resetpassword') != 0)
    <script>
        $(function() {
            $("#user_id").val({{ Session::get('resetpassword') }});
            $('#myModalResetPassword').modal('show');
        });
    </script>
    @endif
	<script src="{{asset('assets/front/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/front/js/sweetalert.min.js')}}" defer></script>
    <script src="{{asset('assets/front/js/script.js')}}"></script>

    @include('auth.login')
    @include('auth.register')
</body>
</html>
