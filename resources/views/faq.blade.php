@extends('layouts.app')
@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.faq') }}
            </h1>
        </div>
    </div>
    <section class="section pb-5">
        <div class="container-lg py-3">
            <div id="accordion">

                @foreach ($questions as $question)
                    <div class="card">
                        <div class="card-header" id="heading{{$question->id}}">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$question->id}}" aria-expanded="true" aria-controls="collapse{{$question->id}}">
                                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                    {{$question->questions_ar}}
                                @else
                                    {{$question->questions_en}}
                                @endif
                            </button>
                        </h5>
                        </div>
                        <div id="collapse{{$question->id}}" class="collapse" aria-labelledby="heading{{$question->id}}" data-parent="#accordion">
                        <div class="card-body">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {{$question->answer_ar}}
                            @else
                                {{$question->answer_en}}
                            @endif
                        </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
@endsection
