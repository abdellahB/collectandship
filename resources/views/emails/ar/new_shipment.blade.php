<!DOCTYPE html>
<html lang="ar">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title></title>
  </head>
  <body>
    <p><span style="color: rgb(36, 39, 41); font-family: &quot;Sofia Pro&quot;, HelveticaNeueBold, HelveticaNeue-Bold, &quot;Helvetica Neue Bold&quot;, HelveticaBold, Helvetica-Bold, &quot;Helvetica Bold&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(243, 243, 246); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span><br>
    </span></span></p>
    <!-- Web Font / @font-face : END -->
    <center id="main_content" style="width: 100%; background: #ffffff;">
      <table style="width: 92%; margin: 0 auto;">
        <tbody>
          <tr style="height: 36px;">
            <td style="padding: 20px 20px 10px; text-align: center; border-width: 1px 1px 0px; border-image: initial; height: 36px; border-color: #eeeeee #eeeeee initial #eeeeee; border-style: solid solid initial solid;"

              bgcolor="#f4f4f4"><img src="http://collectandship.com/assets/front/images/site-logo.png"

                alt="" height="91" width="234"></td>
          </tr>
          <tr style="height: 19px;">
            <td style="direction: rtl; text-align: right; padding: 20px 20px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #666666; height: 19px;"

              bgcolor="#ffffff"><strong> عزيزي <span style="color: #183e77;">
                {{ $fullname }}
                </span></strong></td>
          </tr>
          <tr style="height: 28px;">
            <td class="txtContent" style="direction: rtl; padding: 5px 20px 10px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 28px;"

              bgcolor="#ffffff">تم استلام شحنة جديدة في مستودعكم، التفاصيل
              بالأسفل </td>
          </tr>
          <tr style="height: 20px;">
            <td style="border: 1px solid #eeeeee; direction: rtl; padding: 20px; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #183e77; height: 20px;"

              bgcolor="#F2F3F4">
              <h3 style="margin: 0; padding-bottom: 0px; font-size: 18px;"><strong>
                  تفاصيل الشحنة
                </strong></h3>
            </td>
          </tr>
          <tr style="height: 148px;">
            <td style="direction: rtl; text-align: right; padding: 10px 20px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #666666; border-bottom: 1px solid #eeeeee; height: 148px;"

              bgcolor="#ffffff">
              <table style="margin: 0; width: 100%;" role="presentation" cellspacing="0"

                cellpadding="0" border="0" align="center">
                <tbody>
                  <tr class="order-list__item ">
                    <td class="order-list__item__cell" style="direction: rtl; text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; padding-top: 10px;">
                      <table style="margin: 0; width: 100%; table-layout: initial !important;">
                        <tbody>
                          <tr>
                            <td class="order-list__product-description-cell" style="direction: rtl; text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666;">
                                <span class="order-list__item-title" style="font-weight: normal; font-size: 14px;">
                                    <strong> رقم الشحنة :</strong>
                                    {{ $body['codeparcel'] }} <br/>
                                    <strong> شركة الشحن : </strong>
                                    {{ $body['company'] }} <br/>
                                    <strong> رقم التتبع :</strong>
                                    {{ $body['tracking'] }} <br/>
                                    <strong> تاريخ الاستلام :</strong>
                                    {{ $body['date_arrival'] }} <br/>
                                    <strong> محتويات الشحنة : </strong>
                                    {!! $body['description'] !!} <br/>
                                </span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr style="height: 58px;">
            <td class="txtContent" style="direction: rtl; padding: 10px 20px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 58px;" bgcolor="#ffffff">
                الرجاء الدخول لحسابكم لمعاينة جميع محتويات
              المستودع الخاص بكم، يمكنكم من خلال صفحة المستودع أيضا اختيار
              الأصناف التي ترغبون بشحنها
            </td>
          </tr>
          <tr style="height: 58px;">
            <td class="txtContent" style="direction: rtl; padding: 10px 20px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 58px;" bgcolor="#ffffff">شكرًا لتعاملكم معنا ونأمل أن نحظى دائمًا بثقتكم<br>
              <a style="text-decoration:none;" href="https://www.collectandship.com"></a>collectandship.com</td>
          </tr>
          <tr style="height: 101px;">
            <td style="height: 101px;" bgcolor="#ffffff">
              <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td style="text-align: center; direction: rtl; padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; background: #f4f4f4;">إذا
                      كان لديك أي استفسار فلا تتردد في التواصل معنا عبر بريدنا
                      الإلكتروني support@collectandship.com</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr style="height: 52px;">
            <td style="padding: 10px 20px; width: 100%; font-size: 12px; font-family: Arial, Helvetica, sans-serif; line-height: 18px; text-align: center; color: #888888; height: 52px;">
                CollectAndShip <br>
              <span class="mobile-link--footer" style="font-size: 12px;">
                مبنى 8794 |جدة 22442-3382|  المملكة العربية السعودية
              </span><br>
              <span class="mobile-link--footer" style="font-size: 12px;">
                هاتف:966126217229
                </span>
            </td>
          </tr>
          <tr style="height: 17px;">
            <td style="padding: 10px 20px; width: 100%; font-size: 12px; font-family: Arial, Helvetica, sans-serif; line-height: 18px; text-align: center; color: #888888; height: 17px;">جميع
              الحقوق محفوظة لمؤسسة التسليم العاجل التجارية © 2020-2022</td>
          </tr>
        </tbody>
      </table>
    </center>
  </body>
</html>
