<!DOCTYPE html>
<!-- saved from url=(0051)http://aljassarem.com/email/New_Account_Arabic.html -->
<html dir="rtl" lang="ar"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
  </head>
  <body>
    <table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 394px;">
            <table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: rtl !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 78px;" align="center">
                    <img src="http://collectandship.com/assets/front/images/site-logo.png" style="width: 201px; height: 78px;"></td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif; text-align: center;">&nbsp;</td>
                </tr>
                <tr style="background-color: #183e77;">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 20px;">
                    <table class="devicewidth" width="648" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; font-family: Arial, Helvetica, sans-serif;">
                            عزيزي
                            {{ $fullname }},</span></td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">هذا
                              تأكيد بأن مستودعك في collectandship.com جاهز للإستخدام.</span></td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                            <td style="font-family: Arial, Helvetica, sans-serif;">
                                <span style="font-size: 14px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                                علما ان عناوينك لدينا جاهزة لكي تستخدمها في التسوق من مواقع الانترنت وهي كالتالي:
                                </span>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                    <p></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @if(count($address)>0)
        @foreach ($address as $address)
            <tr style="font-family: Arial, Helvetica, sans-serif; text-align: left;">
                <td style="font-family: Arial, Helvetica, sans-serif; height: 59px;">
                <p>
                    <strong>
                        @if($address->country == 'usa')
                            عنوانك الامريكي
                        @elseif($address->country == 'sa')
                           عنوانك السعودي
                        @else
                           عنوانك الاماراتي
                        @endif
                    </strong>
                </p>
                <p>
                    Full name:  <b> {{ $fullname }}</b> <br/>
                    Adress line 1 : <b> {{ $address->Adressline }}</b> <br/>
                    Line 2 : <b> {{ $resp->policia }} </b><br/>
                    City : <b>{{ $address->city }}</b><br/>
                    State: <b>{{ $address->state }}</b><br/>
                    Post/Zipe code : <b>{{ $address->zipcode }}</b><br/>
                    Country:
                    <b>
                        @if($address->country == 'usa')
                            USA
                        @elseif($address->country == 'sa')
                            Saudi Arabia
                        @else
                            United Arab Emirates
                        @endif
                    </b><br/>
                    Mobile: <b>{{ $address->mobile }}</b><br/>
                </p>
            </td>
          </tr>
          <br/>
        @endforeach
        @endif
        <tr style="height: 13px;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 13px;">
            <br/>
            <p>شكرًا لتعاملكم معنا ونأمل أن نحظى دائمًا بثقتكم.<span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;"></span></p>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 48px;"><span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;"><a href="https://collectandship.com/">collectandship.com</a><br>
              <br>
            </span></td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; height: 20px;" height="10">---------------------------------------</td>
        </tr>
      </tbody>
    </table>
<table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: rtl !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">جميع الحقوق محفوظة لمؤسسة التسليم العاجل التجارية 2022-2020</span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">مبنى 8794</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">جدة 22442-3382</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">المملكة العربية السعودية</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">هاتف: 966126217229</span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: rtl !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">البريد الإلكتروني:<span>&nbsp;</span><a target="_blank" href="http://aljassarem.com/email/support@collectandship.com" rel="noopener">support@collectandship.com</a></span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #fff; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;"></span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="font-family: Arial, Helvetica, sans-serif;" height="10"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p></p>
</body></html>
