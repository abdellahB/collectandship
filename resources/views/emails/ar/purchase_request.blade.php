<!DOCTYPE html>
<html dir="rtl" lang="ar-sa">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="generator" content="openElement (1.57.9)">
    <link id="openElement" rel="stylesheet" type="text/css" href="WEFiles/Css/v02/openElement.css?v=50491134000">
    <link id="OETemplate1" rel="stylesheet" type="text/css" href="Templates/Base.css?v=50491134000">
    <link id="OEBase" rel="stylesheet" type="text/css" href="index.css?v=50491134000">
    <!--[if lte IE 7]>
  <link rel="stylesheet" type="text/css" href="WEFiles/Css/ie7.css?v=50491134000" />  <![endif]-->
    <script type="text/javascript">
   var WEInfoPage = {"PHPVersion":"phpOK","OEVersion":"1-57-9","PagePath":"index","Culture":"DEFAULT","LanguageCode":"AR-AE","RelativePath":"","RenderMode":"Export","PageAssociatePath":"index","EditorTexts":null};
  </script>
    <script type="text/javascript" src="WEFiles/Client/jQuery/1.10.2.js?v=50491134000"></script>
    <script type="text/javascript" src="WEFiles/Client/jQuery/migrate.js?v=50491134000"></script>
    <script type="text/javascript" src="WEFiles/Client/Common/oe.min.js?v=50491134000"></script>
  </head>
  <body class="" data-gl="{&quot;KeywordsHomeNotInherits&quot;:false}">
    <form id="XForm" method="post" action="#" style="height: 5px;"></form>
    <div id="XBody" class="BaseDiv RBoth OEPageXbody OESK_XBody_Default" style="z-index:1000">
      <div class="OESZ OESZ_DivContent OESZG_XBody">
        <div class="OESZ OESZ_XBodyContent OESZG_XBody OECT OECT_Content OECTAbs"></div>
        <div class="OESZ OESZ_XBodyFooter OESZG_XBody OECT OECT_Footer OECTAbs"></div>
      </div>
    </div>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: rtl !important"
      width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif">
        <tr style="font-family: Arial, Helvetica, sans-serif">
          <td style="font-family: Arial, Helvetica, sans-serif">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: rtl !important"
              width="648" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif">
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" align="center"><img
                      src="http://collectandship.com/assets/front/images/site-logo.png"
                      style="width: 201px; height: 78px;"></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="4"
                    bgcolor="#183e77"> <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="20">
                    <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="font-size: 14px;color: #595959;font-family: Arial, Helvetica, sans-serif">
                      عزيزي   {{ $fullname }} </span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif">
                        <br/>
                        طلب المتسوق الشخصي الخاص بكم جاهز للدفع . الرجاء تسجيل الدخول لحسابكم ودفع المبلغ المستحق بالوسيلة التي تناسبكم
                    </span>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="15">
                    <br/>
                    <a href="https://collectandship.com" target="_blank"> https://collectandship.com </a>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif">
                    <span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif">
                        <br/>
                        بعد التأكد من عملية الدفع، سنقوم بتنفيذ الطلب لدى موقع البائع بأسرع ما يمكن.
                       </span>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="15">
                    <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">شكرًا
                      لتعاملكم معنا ونأمل أن نحظى دائمًا بثقتكم.</span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif"><a
                        href="https://collectandship.com">collectandship.com</a></span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="10">
                    <br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: rtl !important"
      width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif">
        <tr style="font-family: Arial, Helvetica, sans-serif">
          <td style="font-family: Arial, Helvetica, sans-serif">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: rtl !important"
              width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
              align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif">
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">جميع
                      الحقوق محفوظة لمؤسسة التسليم العاجل التجارية 2022-2020</span></td>
                </tr>
                <tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px;"
                    ;="">مبنى 8794</td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px">جدة
                    22442-3382</td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px">المملكة
                    العربية السعودية</td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif">
                    <span style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">هاتف:
                      966126217229 </span> </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">البريد
                      الإلكتروني: <a target="_blank" href="support@collectandship.com">support@collectandship.com</a></span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #fff;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif"></span><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="10">
                    <br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
