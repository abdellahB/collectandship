<!DOCTYPE html>
<html dir="ltr" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
  </head>
  <body>
    <table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 394px;">
            <table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 78px;" align="center">
                    <img src="http://collectandship.com/assets/front/images/site-logo.png" style="width: 201px; height: 78px;"></td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif; text-align: center;">&nbsp;</td>
                </tr>
                <tr style="background-color: #183e77;">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 20px;">
                    <table class="devicewidth" width="648" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; font-family: Arial, Helvetica, sans-serif;">
                            Hey
                            {{ $content->firstname }} {{ $content->lastname }}
                            </span>
                            </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;">
                            <span style="font-size: 14px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                                To verify your account, please click on the button below:
                              </span>
                            </td>
                        </tr>
                        <tr>
                          <td bgcolor="#ffffff" align="left">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 60px 30px;">
                                          <table border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                  <td align="center" style="border-radius: 3px;" bgcolor="#183e77">
                                                      <a href="{{ url('/verify') }}/{{ $content->verifyToken }}" target="_blank" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #183e77; display: inline-block;">Verify Account</a>
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                        </tr> <!-- COPY -->
                        <tr>
                          <td style="font-family: Arial, Helvetica, sans-serif;">
                              <p style="font-size: 14px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                                If that doesn't work, copy and paste the following link into your browser:
                            </p>
                          </td>
                        </tr> <!-- COPY -->
                        <tr>
                            <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                                <p style="margin: 0;">
                                    <a href="{{ url('/verify') }}/{{ $content->verifyToken }}" target="_blank" style="color: #FFA73B;">
                                      {{ url('/verify') }}/{{ $content->verifyToken }}
                                    </a>
                                </p>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                    <p></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="height: 13px;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 13px;">
            <p>If you have any questions, just reply to this email - we are always happy to help.)
              <span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;"></span></p>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; height: 20px;" height="10">---------------------------------------</td>
        </tr>
      </tbody>
    </table>
<table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">
    rights reserverd for Express Delivery Foundation for
    Trading 2020
</span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> Bldg. 8794 </td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> 22442-Jeddah
    3382 </td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
    Saudi Arabia
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">Phone: 966126217229</span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
<table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
<tbody style="font-family: Arial, Helvetica, sans-serif;">
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">E-mail:<span>&nbsp;</span><a target="_blank" href="#" rel="noopener">support@collectandship.com</a></span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
</tbody>
</table>
</td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #fff; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;"></span></td>
</tr>
<tr style="font-family: Arial, Helvetica, sans-serif;">
<td style="font-family: Arial, Helvetica, sans-serif;" height="10"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p></p>
</body></html>
