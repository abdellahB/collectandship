<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title></title>
  </head>
  <body>
    <p><span style="color: rgb(36, 39, 41); font-family: &quot;Sofia Pro&quot;, HelveticaNeueBold, HelveticaNeue-Bold, &quot;Helvetica Neue Bold&quot;, HelveticaBold, Helvetica-Bold, &quot;Helvetica Bold&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(243, 243, 246); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"><span><br>
        </span></span></p>
    <p>&nbsp;</p>
    <p></p>
    <!-- Web Font / @font-face : BEGIN -->
    <p></p>
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <p></p>
    <!-- [if mso]>
		<style>			* {
				font-family: sans-serif !important;			}
		</style>	<![endif]-->
    <p></p>
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <p></p>
    <!-- [if !mso]><!-->
    <p></p>
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <p></p>
    <!--<![endif]-->
    <p></p>
    <!-- Web Font / @font-face : END -->
    <center id="main_content" style="width: 100%; background: #ffffff;">
      <table style="width: 92%; margin: 0 auto;">
        <tbody>
          <tr style="height: 36px;">
            <td style="padding: 20px 20px 10px; text-align: center; border-width: 1px 1px 0px; border-image: initial; height: 36px; border-color: #eeeeee #eeeeee initial #eeeeee; border-style: solid solid initial solid;"

              bgcolor="#f4f4f4"><img src="http://collectandship.com/assets/front/images/site-logo.png"

                alt="" height="91" width="234"></td>
          </tr>
          <tr style="height: 19px;">
            <td style="direction: ltr; text-align:left;; padding: 20px 20px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #666666; height: 19px;"

              bgcolor="#ffffff"><strong>Hey <span style="color: #183e77;">
                {{ $fullname }}
                </span></strong></td>
          </tr>
          <tr style="height: 28px;">
            <td class="txtContent" style="direction: ltr; padding: 5px 20px 10px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 28px;" bgcolor="#ffffff">
                A new shipment has been received in your warehouse, details below
            </td>
          </tr>
          <tr style="height: 20px;">
            <td style="border: 1px solid #eeeeee; direction: ltr; padding: 20px; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #183e77; height: 20px;"

              bgcolor="#F2F3F4">
              <h3 style="margin: 0; padding-bottom: 0px; font-size: 18px;"><strong>
                Shipment details
                </strong></h3>
            </td>
          </tr>
          <tr style="height: 148px;">
            <td style="direction: ltr; text-align:left;; padding: 10px 20px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; color: #666666; border-bottom: 1px solid #eeeeee; height: 148px;"

              bgcolor="#ffffff">
              <table style="margin: 0; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                  <tr class="order-list__item ">
                    <td class="order-list__item__cell" style="direction: ltr; text-align:left;; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; padding-top: 10px;">
                      <table style="margin: 0; width: 100%; table-layout: initial !important;">
                        <tbody>
                          <tr>
                            <td class="order-list__product-description-cell" style="direction: ltr; text-align:left;; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666;">
                                <span class="order-list__item-title" style="font-weight: normal; font-size: 14px;">
                                    <strong> Shipment :</strong>
                                    {{ $body['codeparcel'] }} <br/>
                                    <strong> Courier Name : </strong>
                                    {{ $body['company'] }} <br/>
                                    <strong> Tracking :</strong>
                                    {{ $body['tracking'] }} <br/>
                                    <strong> Date Received :</strong>
                                    {{ $body['date_arrival'] }} <br/>
                                    <strong> Package content : </strong>
                                    {!! $body['description'] !!} <br/>
                                </span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr style="height: 58px;">
            <td class="txtContent" style="direction: ltr; padding: 10px 20px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 58px;" bgcolor="#ffffff">
              Please log in to your account to view all contents of your warehouse. Through the warehouse page, you can also choose the items that you want to ship.
            </td>
          </tr>
          <tr style="height: 58px;">
            <td class="txtContent" style="direction: ltr; padding: 10px 20px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666; height: 58px;"

              bgcolor="#ffffff">Thank you for dealing with us and we hope to always earn your trust
              <br>
              <a style="text-decoration: none;" href="https://www.collectandship.com"></a>collectandship.com</td>
          </tr>
          <tr style="height: 101px;">
            <td style="height: 101px;" bgcolor="#ffffff">
              <table role="presentation" width="100%" cellspacing="0" cellpadding="0"

                border="0">
                <tbody>
                  <tr>
                    <td style="text-align: center; direction: ltr; padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; background: #f4f4f4;">
                        If you have any questions, please feel free to contact us via our email
                        support@collectandship.com</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr style="height: 52px;">
            <td style="padding: 10px 20px; width: 100%; font-size: 12px; font-family: Arial, Helvetica, sans-serif; line-height: 18px; text-align: center; color: #888888; height: 52px;">Collect
              And Ship<br>
              <span class="mobile-link--footer" style="font-size: 12px;">Bldg
                8794, Jeddah 22442, Saudi Arabia</span><br>
              <span class="mobile-link--footer" style="font-size: 12px;">Phone:
                966126217229</span></td>
          </tr>
          <tr style="height: 17px;">
            <td style="padding: 10px 20px; width: 100%; font-size: 12px; font-family: Arial, Helvetica, sans-serif; line-height: 18px; text-align: center; color: #888888; height: 17px;">
                rights reserverd for Express Delivery Foundation for
                Trading 2020</td>
          </tr>
        </tbody>
      </table>
    </center>
  </body>
</html>
