<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body>
    <table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 394px;">
            <table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 78px;" align="center">
                    <img src="http://collectandship.com/assets/front/images/site-logo.png" style="width: 201px; height: 78px;"></td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif; text-align: center;">&nbsp;</td>
                </tr>
                <tr style="background-color: #183e77;">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
                  </td>
                </tr>
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 20px;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
                      Dear {{ $fullname }}
                      </span><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"></span></p>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 80px;">
                    <p>Your shipment # {{$body['code_parcel']}} was shipped via {{ $body->getCompany->companiename}}. The
                      tracking numbers and link are: <strong><a href="{{ $body->getTracking->company_url }}" target="_blank" rel="noopener">{{ $body->getTracking->tracking_info }}&nbsp;</a></strong></p>
                    <p><br>
                      You can also visit the following link to track the status
                      of your order and view its details:</p>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; background-color: #f6f6f6; height: 102px;">
                    <table class="devicewidth" style="width: 558px; height: 50px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" align="center">
                      <tbody>
                        <tr style="height: 10px;">
                          <td style="background-color: white; width: 600px; height: 10px;">
                            <table style="width: 696.5px; border-collapse: collapse;" border="0">
                              <tbody>
                                <tr>
                                  <td style="width: 248px;"><br>
                                  </td>
                                    <td style="background-color: white; width: 165px; height: 20px;">
                                        <h3 style="font-size: 18px; margin: 0px; padding-bottom: 0px; font-family: Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">
                                        <a href="https://collectandship.com" style="background-color: #183e77; font-family: Arial,Helvetica,sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold; color: #183e77; padding: 15px 30px;" class="m_5129719892172671372button-a" target="_blank" rel="noopener">
                                            <span style="color: #ffffff; font-size: 14px;">ORDER DETAILS</span>
                                        </a>
                                        </h3> <br/>
                                    </td>
                                  <td style="background-color: white; width: 233px;"><br>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                          <td style="width: 10.4px; height: 10px;"><br>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <h3 style="font-size: 18px; margin: 0px; padding-bottom: 0px; font-family: Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;"><span></span></h3>
                    <h3 style="text-align: center;"> Shipment Summary </h3>
                  </td>
                </tr>
                <tr style="height: 83px;">
                  <td style="background-color: white; height: 83px;">
                    <table style="width: 100%; border-spacing: 0!important; border-collapse: collapse!important; table-layout: fixed!important; margin: 0 auto;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                        <tr>
                          <td>
                              <strong> Pack N°          : </strong> {{ $body['code_parcel'] }} <br/>
                              <strong> Date of shipment : </strong> {{ $body['updated_at'] }} <br/>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <span style="font-size: 14px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                    </span></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 124px;" height="15">
            <table style="border-collapse: collapse; width: 100%;" border="0">
              <tbody>
                <tr>
                  <td style="width: 50%;"><strong>Shipping Address:<br>
                    </strong></td>
                </tr>
                <tr>
                  <td style="width: 50%;">
                    {{ $body->getAddress->adress }} <br/>
                    {{ $body->getAddress->province }} |  {{ $body->getAddress->city }}<br/>
                    {{ $body->getAddress->phone }} <br/>
                    {{ $body->getAddress->getcountry->countriename }} <br/>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 59px;">
            <p>Please allow some time for the shipment status to be updated at the shipping company website.</p>
          </td>
        </tr>
        <tr style="height: 13px;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 13px;">
            <p>Thank you for giving us the opportunity to serve you. We hope to always earn your trust.</p>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 48px;">
            <span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;">
                <a href="https://collectandship.com/">collectandship.com</a><br><br>
                </span>
            </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; height: 20px;" height="10">---------------------------------------</td>
        </tr>
      </tbody>
    </table>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif;">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                    <table class="devicewidth" style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                            <table class="devicewidth" style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: rgb(89, 89, 89); line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                      All rights reserved for
                                                      Express Delivery
                                                      Foundation for Trading
                                                      2020-2022</span></td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);">
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                        <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);">
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Bldg.8794</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Jeddah 22442-3382</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Saudi Arabia</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Phone:
                                    966126217229</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">Customer Support email:<span>&nbsp;</span><a target="_blank" href="http://aljassarem.com/email/support@collectandship.com" rel="noopener">support@collectandship.com</a></span></td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #fff; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;"></span><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif;" height="10"><br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <p></p>
</body></html>
