<!DOCTYPE html>
<!-- saved from url=(0052)http://aljassarem.com/email/New_Account_English.html -->
<html dir="ltr" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title></title>
  </head>
  <body>
    <table id="backgroundTable" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 394px;">
            <table class="devicewidth" style="font-family: Arial,Helvetica,sans-serif; border-collapse: collapse; direction: ltr !important; width: 648px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 78px;" align="center"><img src="http://collectandship.com/assets/front/images/site-logo.png" style="width: 201px; height: 78px;"></td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif; text-align: center;">&nbsp;</td>
                </tr>
                <tr style="background-color: #183e77;">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <p><span style="color: #595959; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr>
                  <td style="font-family: Arial, Helvetica, sans-serif;"><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif; height: 20px;">
                    <table class="devicewidth" width="648" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; font-family: Arial, Helvetica, sans-serif;">Dear
                            {{ $fullname }},
                            </span></td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;">
                            This is a confirmation that your account at collectandship.com is ready to use.
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="font-family: Arial, Helvetica, sans-serif;" height="15"><br>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                            <td style="font-family: Arial, Helvetica, sans-serif;">
                             Please also note that your addresses can be immediately used for shopping from the online shops, as follows:
                            </td>
                        </tr>
                      </tbody>
                    </table>
                    <p></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @if(count($address)>0)
        @foreach ($address as $address)
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 124px;" height="15">
            <p>
                <strong>
                    @if($address->country == 'usa')
                        Your US address
                    @elseif($address->country == 'sa')
                        Your Saudi address
                    @else
                        Your Emirates address
                    @endif
                </strong>
            </p>
            <p>
                Full name:  <b> {{ $fullname}}</b> <br/>
                Adress line 1 : <b> {{ $address->Adressline }}</b> <br/>
                Line 2 : <b> {{ $resp->policia }} </b><br/>
                City : <b>   {{ $address->city }}</b><br/>
                State: <b>   {{ $address->state }}</b><br/>
                Post/Zipe code : <b>{{ $address->zipcode }}</b><br/>
                Country:
                <b>
                    @if($address->country == 'usa')
                        USA
                    @elseif($address->country == 'sa')
                        Saudi Arabia
                    @else
                        United Arab Emirates
                    @endif
                </b><br/>
                Mobile: <b>{{ $address->mobile }}</b><br/>
            </p>
          </td>
        </tr>
        <br/>
        @endforeach
        @endif
        <tr style="height: 13px;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 13px;">
            <br/>
            <p>Thank you for giving us the opportunity to serve you and we hope to always earn your trust.</p>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; height: 48px;"><span style="font-size: 16px; color: #595959; line-height: 24px; font-family: Arial, Helvetica, sans-serif;"><a href="https://collectandship.com/">collectandship.com</a><br>
              <br>
            </span>
          </td>
        </tr>
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; height: 20px;" height="10">---------------------------------------</td>
        </tr>
      </tbody>
    </table>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif;">
        <tr style="font-family: Arial, Helvetica, sans-serif;">
          <td style="font-family: Arial, Helvetica, sans-serif;">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                    All rights reserved for Express Delivery Foundation for Trading 2020-2022</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Bldg.
                                    8794</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Jeddah
                                    22442-3382</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                    Saudi Arabia</td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                    <table class="devicewidth" style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                            <table class="devicewidth" style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);">
                                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Phone:
                                                    966126217229</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                    <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                      <tbody style="font-family: Arial, Helvetica, sans-serif;">
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                          <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                            <table class="devicewidth" style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; border-collapse: collapse; direction: ltr !important;" width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                              <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">Customer
                                      Support Email:<span>&nbsp;</span><a target="_blank" href="#" rel="noopener">support@collectandship.com</a></span></td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="font-family: Arial, Helvetica, sans-serif;">
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 14px; color: #fff; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;"></span><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                  <td style="font-family: Arial, Helvetica, sans-serif;" height="10"><br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <p></p>


</body></html>
