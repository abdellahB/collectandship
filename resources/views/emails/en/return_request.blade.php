<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="generator" content="openElement (1.57.9)">
    <link id="openElement" rel="stylesheet" type="text/css" href="WEFiles/Css/v02/openElement.css?v=50491134000">
    <link id="OETemplate1" rel="stylesheet" type="text/css" href="Templates/Base.css?v=50491134000">
    <link id="OEBase" rel="stylesheet" type="text/css" href="index.css?v=50491134000">
    <!--[if lte IE 7]>
  <link rel="stylesheet" type="text/css" href="WEFiles/Css/ie7.css?v=50491134000" />  <![endif]-->
    <script type="text/javascript">
   var WEInfoPage = {"PHPVersion":"phpOK","OEVersion":"1-57-9","PagePath":"index","Culture":"DEFAULT","LanguageCode":"AR-AE","RelativePath":"","RenderMode":"Export","PageAssociatePath":"index","EditorTexts":null};
  </script>
    <script type="text/javascript" src="WEFiles/Client/jQuery/1.10.2.js?v=50491134000"></script>
    <script type="text/javascript" src="WEFiles/Client/jQuery/migrate.js?v=50491134000"></script>
    <script type="text/javascript" src="WEFiles/Client/Common/oe.min.js?v=50491134000"></script>
  </head>
  <body class="" data-gl="{&quot;KeywordsHomeNotInherits&quot;:false}">
    <form id="XForm" method="post" action="#" style="height: 5px;"></form>
    <div id="XBody" class="BaseDiv RBoth OEPageXbody OESK_XBody_Default" style="z-index:1000">
      <div class="OESZ OESZ_DivContent OESZG_XBody">
        <div class="OESZ OESZ_XBodyContent OESZG_XBody OECT OECT_Content OECTAbs"></div>
        <div class="OESZ OESZ_XBodyFooter OESZG_XBody OECT OECT_Footer OECTAbs"></div>
      </div>
    </div>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: ltr !important"
      width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif">
        <tr style="font-family: Arial, Helvetica, sans-serif">
          <td style="font-family: Arial, Helvetica, sans-serif">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: ltr !important"
              width="648" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif">
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" align="center"><img
                      src="http://collectandship.com/assets/front/images/site-logo.png"
                      style="width: 201px; height: 78px;"></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="4"
                    bgcolor="#183e77"> <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="20">
                    <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="font-size: 14px;color: #595959;font-family: Arial, Helvetica, sans-serif">
                      Dear {{ $fullname }} </span></td>
                </tr>

                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif">
                        <br/>
                        Your package return request is pending for the shipping charges payment.
                        Please log in to your account to pay the amount due                    </span>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="15">
                    <br/>
                    <a href="https://collectandship.com" target="_blank"> https://collectandship.com </a>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif">
                    <br/>
                    After confirming the payment process, we will ship as soon as possible and provide you with the tracking number, if any, according to the shipping method you have chosen.
                    </span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="15">
                    <br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
                    Thank you for dealing with us and we hope to always earn your trust.
                    </span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif"><span style="font-size: 14px;color: #595959;line-height: 24px;font-family: Arial, Helvetica, sans-serif"><a
                        href="https://collectandship.com">collectandship.com</a></span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="10">
                    <br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: ltr !important"
      width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
      <tbody style="font-family: Arial, Helvetica, sans-serif">
        <tr style="font-family: Arial, Helvetica, sans-serif">
          <td style="font-family: Arial, Helvetica, sans-serif">
            <table class="devicewidth" style="font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;direction: ltr !important"
              width="648" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
              align="center">
              <tbody style="font-family: Arial, Helvetica, sans-serif">
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">
                      All rights reserverd for Express Delivery Foundation for Trading 2020</span></td>
                </tr>
                <tr style="font-family: Arial,  Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                    Bldg
                    8794</td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px">
                    Jeddah 22442 </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif; font-size: 14px">
                    Saudi Arabia </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif">
                    <span style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">phone:
                      966126217229 </span> </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #595959;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif">
                      E-mail: <a target="_blank" href="support@collectandship.com">support@collectandship.com</a></span></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="text-align: center;font-family: Arial, Helvetica, sans-serif"><span
                      style="font-size: 14px;color: #fff;line-height: 25px;text-align: center;font-family: Arial, Helvetica, sans-serif"></span><br>
                  </td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif">
                  <td style="font-family: Arial, Helvetica, sans-serif" height="10">
                    <br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
