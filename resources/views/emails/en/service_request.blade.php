<!DOCTYPE html>
<html dir="ltr" lang="ar">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>
    </head>
    <body>
        <table role="presentation" style="max-width: 600px; margin: auto; border: 1px solid #eee;" class="email-container" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
                <tr>
                    <td><br /></td>
                </tr>
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                    <td style="font-family: Arial, Helvetica, sans-serif; height: 78px;" align="center">
                        <img src="http://collectandship.com/assets/front/images/site-logo.png" style="width: 201px; height: 78px;" />
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #000066;height30px"><br /></td>
                </tr>
                <tr>
                    <td style="direction: ltr; text-align: left; padding: 20px 20px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666;" bgcolor="#ffffff">
                        <b>Dear <span style="color: #183e77;"> {{ $fullname }}،</span></b>
                    </td>
                </tr>
                <tr>
                    <td class="txtContent" style="direction: ltr; padding: 5px 20px 10px; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666;" bgcolor="#ffffff">
                        The service you requested&nbsp; was completed. Below are the service details:
                    </td>
                </tr>
                <tr>
                    <td
                        style="border: 1px solid #eee; direction: ltr; padding: 20px; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #183e77;"
                        bgcolor="#F2F3F4"
                    >
                        <h3 style="margin: 0; padding-bottom: 0px; font-size: 18px;">
                            Service Request Details
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td
                        style="direction: ltr; text-align: left; padding: 10px 20px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; border-bottom: 1px solid #eee;"
                        bgcolor="#ffffff"
                    >
                        <table role="presentation" style="margin: 0; width: 100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr class="order-list__item">
                                    <td
                                        class="order-list__item__cell"
                                        style="direction: ltr; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; padding-top: 10px;"
                                    >
                                        <table style="margin: 0; width: 100%; table-layout: initial !important;">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        Service#:
                                                    </td>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        <span class="order-list__item-description" style="font-weight: normal; font-size: 14px;"> Service #{{ $body->code_service }} - {{ $body->getPservice->service_name_en }} </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        Service Type:
                                                    </td>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        {{ $body->getPservice->service_name_en }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        For the package of:
                                                    </td>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        <span class="order-list__item-description" style="font-weight: normal; font-size: 14px;">
                                                            VARIOUS ITEMS {{ $body->getPship->codeparcel }} {{ $body->getPship->date_arrival }} {{ $body->getPship->tracking }}
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        Service Cost:
                                                    </td>
                                                    <td
                                                        class="order-list__product-description-cell"
                                                        style="
                                                            direction: ltr;
                                                            text-align: left;
                                                            font-family: Arial, Helvetica, sans-serif;
                                                            font-size: 14px;
                                                            mso-height-rule: exactly;
                                                            line-height: 20px;
                                                            color: #666666;
                                                            padding-top: 5px;
                                                            padding-bottom: 5px;
                                                        "
                                                    >
                                                        <span class="order-list__item-description" style="font-weight: normal; font-size: 14px;"> ${{ $body->getPservice->service_price }} </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="txtContent" style="direction: ltr; padding: 10px 20px; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 30px; color: #666666;" bgcolor="#ffffff">
                        We have added new photos which you can view at the product details in your account.
                    </td>
                </tr>
                <tr>
                    <td
                        class="txtContent"
                        style="direction: ltr; padding: 10px 20px; text-align: justify; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 30px; color: #666666;"
                        bgcolor="#ffffff"
                    >
                        Thank you for giving us the opportunity to serve you and we hope to always have your trust.<br />
                        <span style="color: #0000ee;">collectandship.com</span>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td
                                        style="text-align: center; direction: ltr; padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #666666; background: #f4f4f4;"
                                    >
                                        &nbsp; If you have any questions, please feel free to contact us at our email: <a target="_blank" href="mailto:support@collectandship.com" style="text-decoration: none;">support@collectandship.com</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="backgroundTable" style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;" width="600px" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                <tr style="font-family: Arial, Helvetica, sans-serif;">
                    <td style="font-family: Arial, Helvetica, sans-serif;">
                        <table
                            class="devicewidth"
                            style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; direction: ltr !important;"
                            width="648"
                            cellspacing="0"
                            cellpadding="0"
                            border="0"
                            bgcolor="#ffffff"
                            align="center"
                        >
                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr>
                                                                    <td><br /></td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;font-size: 14px;color: #595959;">
                                                                        All rights reserved for Express Delivery Foundation for Trading 2020-2022
                                                                    </td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                                                        <table
                                                                            class="devicewidth"
                                                                            style="
                                                                                color: rgb(0, 0, 0);
                                                                                font-family: Arial, Helvetica, sans-serif;
                                                                                font-size: medium;
                                                                                font-style: normal;
                                                                                font-variant-ligatures: normal;
                                                                                font-variant-caps: normal;
                                                                                font-weight: 400;
                                                                                letter-spacing: normal;
                                                                                orphans: 2;
                                                                                text-align: start;
                                                                                text-indent: 0px;
                                                                                text-transform: none;
                                                                                white-space: normal;
                                                                                widows: 2;
                                                                                word-spacing: 0px;
                                                                                -webkit-text-stroke-width: 0px;
                                                                                text-decoration-style: initial;
                                                                                text-decoration-color: initial;
                                                                                border-collapse: collapse;
                                                                                direction: ltr !important;
                                                                            "
                                                                            width="648"
                                                                            cellspacing="0"
                                                                            cellpadding="0"
                                                                            border="0"
                                                                            bgcolor="#ffffff"
                                                                            align="center"
                                                                        >
                                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);">
                                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Bldg. 8794</td>
                                                                                </tr>
                                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);"></tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Jeddah 22442-3382</td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: #595959;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Saudi Arabia</td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                                        <table
                                                                            class="devicewidth"
                                                                            style="
                                                                                color: rgb(0, 0, 0);
                                                                                font-family: Arial, Helvetica, sans-serif;
                                                                                font-size: medium;
                                                                                font-style: normal;
                                                                                font-variant-ligatures: normal;
                                                                                font-variant-caps: normal;
                                                                                font-weight: 400;
                                                                                letter-spacing: normal;
                                                                                orphans: 2;
                                                                                text-align: start;
                                                                                text-indent: 0px;
                                                                                text-transform: none;
                                                                                white-space: normal;
                                                                                widows: 2;
                                                                                word-spacing: 0px;
                                                                                -webkit-text-stroke-width: 0px;
                                                                                text-decoration-style: initial;
                                                                                text-decoration-color: initial;
                                                                                border-collapse: collapse;
                                                                                direction: ltr !important;
                                                                            "
                                                                            width="648"
                                                                            cellspacing="0"
                                                                            cellpadding="0"
                                                                            border="0"
                                                                            bgcolor="#ffffff"
                                                                            align="center"
                                                                        >
                                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                                <tr style="font-family: Arial, Helvetica, sans-serif; color: rgb(89, 89, 89);">
                                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Phone: 966126217229</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                        <table
                                            class="devicewidth"
                                            style="
                                                color: #000000;
                                                font-family: Arial, Helvetica, sans-serif;
                                                font-size: medium;
                                                font-style: normal;
                                                font-variant-ligatures: normal;
                                                font-variant-caps: normal;
                                                font-weight: 400;
                                                letter-spacing: normal;
                                                orphans: 2;
                                                text-align: start;
                                                text-indent: 0px;
                                                text-transform: none;
                                                white-space: normal;
                                                widows: 2;
                                                word-spacing: 0px;
                                                -webkit-text-stroke-width: 0px;
                                                text-decoration-style: initial;
                                                text-decoration-color: initial;
                                                border-collapse: collapse;
                                                direction: ltr !important;
                                            "
                                            width="648"
                                            cellspacing="0"
                                            cellpadding="0"
                                            border="0"
                                            bgcolor="#ffffff"
                                            align="center"
                                        >
                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                        <table
                                                            class="devicewidth"
                                                            style="
                                                                color: #000000;
                                                                font-family: Arial, Helvetica, sans-serif;
                                                                font-size: medium;
                                                                font-style: normal;
                                                                font-variant-ligatures: normal;
                                                                font-variant-caps: normal;
                                                                font-weight: 400;
                                                                letter-spacing: normal;
                                                                orphans: 2;
                                                                text-align: start;
                                                                text-indent: 0px;
                                                                text-transform: none;
                                                                white-space: normal;
                                                                widows: 2;
                                                                word-spacing: 0px;
                                                                -webkit-text-stroke-width: 0px;
                                                                text-decoration-style: initial;
                                                                text-decoration-color: initial;
                                                                border-collapse: collapse;
                                                                direction: ltr !important;
                                                            "
                                                            width="648"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            border="0"
                                                            bgcolor="#ffffff"
                                                            align="center"
                                                        >
                                                            <tbody style="font-family: Arial, Helvetica, sans-serif;">
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                                        <span
                                                                            style="
                                                                                color: rgb(89, 89, 89);
                                                                                font-family: Arial, Helvetica, sans-serif;
                                                                                font-size: 14px;
                                                                                font-style: normal;
                                                                                font-variant-ligatures: normal;
                                                                                font-variant-caps: normal;
                                                                                font-weight: 400;
                                                                                letter-spacing: normal;
                                                                                orphans: 2;
                                                                                text-align: center;
                                                                                text-indent: 0px;
                                                                                text-transform: none;
                                                                                white-space: normal;
                                                                                widows: 2;
                                                                                word-spacing: 0px;
                                                                                -webkit-text-stroke-width: 0px;
                                                                                background-color: rgb(255, 255, 255);
                                                                                text-decoration-style: initial;
                                                                                text-decoration-color: initial;
                                                                                display: inline !important;
                                                                                float: none;
                                                                            "
                                                                        >
                                                                            Customer Support Email:
                                                                        </span>
                                                                        <span style="font-size: 14px; color: #595959; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                                            <span> </span><a target="_blank" href="http://aljassarem.com/email/support@collectandship.com" rel="noopener">support@collectandship.com</a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="font-family: Arial, Helvetica, sans-serif;"></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                    <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                        <span style="font-size: 14px; color: #fff; line-height: 25px; text-align: center; font-family: Arial, Helvetica, sans-serif;"></span><br />
                                    </td>
                                </tr>
                                <tr style="font-family: Arial, Helvetica, sans-serif;">
                                    <td style="font-family: Arial, Helvetica, sans-serif;" height="10"><br /></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <p></p>
        <p></p>
    </body>
</html>
