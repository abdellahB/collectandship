@extends('layouts.app')

@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.pricing_title') }}
            </h1>
        </div>
    </div>
    <!--Section: Contact v.1-->
<section class="section pb-5">
    <div class="container-lg py-3">
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-5 mb-4">
              <!--Form with header-->
              <div class="card">
                <div class="card-body">
                    <div class="row form-group">
                        <label class="col-md-4 col-sm-4 col-4">{{ __('home.pricing_1') }}</label>
                        <div class="col-md-8 col-sm-8 col-8">
                          <label class="radio-inline">
                            <input type="radio" name="weight_measure" id="lbs_measure" value="lbs"> {{ __('home.pricing_2') }}
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="weight_measure" id="kg_measure" checked value="kg"> {{ __('home.pricing_3') }}
                          </label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4 col-sm-4 col-4">
                            <input type="number" name="length" min="0" placeholder="{{ __('home.pricing_4') }}" class="form-control length allow_decimal">
                        </div>
                        <div class="col-md-4 col-sm-4 col-4">
                            <input type="number" name="width" min="0" placeholder="{{ __('home.pricing_5') }}" class="form-control width allow_decimal">
                        </div>
                        <div class="col-md-4 col-sm-4 col-4">
                            <input type="number" name="height" min="0" placeholder="{{ __('home.pricing_6') }}" class="form-control height allow_decimal">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-12 col-12">
                            <select name="ship_country" class="form-control ship_country">
                                <option value=""> {{ __('home.source_country') }} </option>
                                @if(count($address)>0)
                                    @foreach ( $address as $address )
                                        <option value="{{ $address->country }}">
                                            @if($address->country == 'usa')
                                                {{ __('account.usa') }}
                                            @elseif($address->country == 'sa')
                                                {{ __('account.sa') }}
                                            @else
                                                {{ __('account.uae') }}
                                            @endif
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12 col-12">
                            <select name="countrie_id" class="form-control countrie_id">
                                <option> {{ __('home.pricing_7') }} </option>
                                @if(count($countries)>0)
                                    @foreach ($countries as $countrie)
                                        <option value="{{$countrie->id}}">
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $countrie->countriename_ar }}
                                            @else
                                                {{ $countrie->countriename }}
                                            @endif
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 col-sm-12 col-12">
                            <input type="number" name="weight" min="0" placeholder="{{ __('home.pricing_8') }}" class="form-control weight allow_decimal">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-8 m-auto">
                            <a href="javascript:void()" class="btn btn-secondary btn-block rounded-lg calculer" href="#">{{ __('home.pricing_9') }} </a>
                        </div>
                    </div>
                </div>
              </div>
              <!--Form with header-->
            </div>
            <div class="col-lg-7">
                <div class="information">
                    <div class="form-header blue accent-1">
                        <h3>{{ __('home.pricing_10') }}</h3>
                    </div>
                    <ul>
                        <li>
                            <h4>{{ __('home.pricing_11') }}</h4>
                        </li>
                        <li>
                            <h4>{{ __('home.pricing_12') }}</h4>
                        </li>
                        <li>
                            <h4>{{ __('home.pricing_13') }}</h4>
                        </li>
                        <li>
                            <h4>{{ __('home.pricing_14') }}</h4>
                        </li>
                        <li>
                            <h4>{{ __('home.pricing_15') }}</h4>
                        </li>
                    </ul>
                </div>
            </div>
          </div><!--end -row-->
          <div class="row showTable" style="display:none">
            <div class="container-lg py-3">
                <h1 class="secondary-head text-shadow text-center">
                </h1>
            </div>
            <div class="col-lg-6 mb-4">
                <h3> {{ __('home.airfreight')}} </h3>
                <table class="table table-hover table-striped mytables">
                    <tbody class="table_weight">

                    </tbody>
                </table>
            </div>
            <div class="col-lg-6 mb-4">
                <table class="table table-hover table-striped mynewtable">
                    <thead>
                        <th scope="col"> {{ __('home.pricing_16') }} </th>
                        <th scope="col"> {{ __('home.pricing_20') }}</th>
                        <th scope="col"> {{ __('home.pricing_21') }} </th>
                    </thead>
                    <tbody class="result_resp">

                    </tbody>
                </table>
            </div>
          </div><!--end -row-->
    </div>
</section>
  <!--Section: Contact v.1-->
</div>
@endsection
