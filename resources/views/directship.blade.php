@extends('layouts.app')
@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.directship') }}
            </h1>
        </div>
    </div>
    <!--Section: Contact v.1-->
<section class="section pb-5">
    <div class="container-lg py-3">
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-12 mb-4">
          <!--Form with header-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 firststep">
                            <div class="row form-group">
                                <label class="col-md-4 col-sm-4 col-4"> {{ __('home.pricing_1') }}</label>
                                <div class="col-md-8 col-sm-8 col-8">
                                  <label class="radio-inline">
                                    <input type="radio" name="weight_measure" id="lbs_measure" value="lbs">{{ __('home.pricing_2') }}
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="weight_measure" id="kg_measure" checked value="kg"> {{ __('home.pricing_3') }}
                                  </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <select name="ship_country" class="form-control ship_country" required="true">
                                        <option value=""> {{ __('home.source_country') }} </option>
                                            @if(count($address)>0)
                                                @foreach ( $address as $address )
                                                    <option value="{{ $address->country }}">
                                                        @if($address->country == 'usa')
                                                            {{ __('account.usa') }}
                                                        @elseif($address->country == 'sa')
                                                            {{ __('account.sa') }}
                                                        @else
                                                            {{ __('account.uae') }}
                                                        @endif
                                                    </option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <select name="countrie_id" class="form-control countrie_id" required="true">
                                        <option value="">{{ __('home.pricing_7') }} </option>
                                        @if(count($countries)>0)
                                            @foreach ($countries as $countrie)
                                                <option data-id="{{$countrie->countriename}}" value="{{$countrie->id}}"> {{$countrie->countriename}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="field">
                                <div class="row rows 1">
                                    <div class="col-md-3 form-group">
                                        <input type="number" name="weight" class="form-control weight" placeholder="{{ __('home.pricing_8') }}" required="true">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="number" name="length" class="form-control length" placeholder="{{ __('home.pricing_4') }}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="number" name="width" class="form-control width" placeholder="{{ __('home.pricing_5') }}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="number" name="height" class="form-control height" placeholder="{{ __('home.pricing_6') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <input type="hidden" id="count" value="1" name="count"/>
                                </div>
                                <div class="col-md-3 form-group">
                                    <button type="button" value="1" class="btn btn-success btn-block rounded-lg getcompany">
                                        {{ __('home.pricing_9') }}
                                   </button>
                                </div>
                                <div class="col-md-3 form-group">
                                    <button type="button" class="btn btn-info btn-block rounded-lg addmore">
                                     {{ __('home.pricing_9_1') }}
                                   </button>
                                </div>
                            </div>
                        </div>
                    </div> <!--end row-->
                    <form class="secondstep" style="display:none">
                    <div class="row">
                        <h4 class="col-xs-12 col-md-12 titles"> <b>{{ __('home.pricing_9')}} </b> </h4>
                        <div class="col-md-12">
                            <div class="row form-group">
                                <table class="table table-bordered">
                                    <thead class="panel-heading">
                                        <tr>
                                            <th></th>
                                            <th scope="col">{{ __('home.pricing_16')}} </th>
                                            <th scope="col">{{ __('home.pricing_20')}} </th>
                                            <th scope="col">{{ __('home.pricing_21')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody class="getCompany_to_direct">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> <!--end row-->
                    <div class="row">
                        <h4 class="col-xs-12 col-md-12 titles"> <b> {{ __('home.pack_content')}}</b> </h4>
                        <span class="col-md-12 show_content"></span>
                    </div>
                    <div class="row">
                        <h4 class="col-xs-12 col-md-12 titles"> <b> {{ __('home.pack_place')}} </b> </h4>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label> {{ __('home.firstname')}}</label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.firstname')}}" name="firstname_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.address')}} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.address')}}" name="address_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('account.address_state')}}</label>
                                <input type="text" class="form-control" placeholder="{{ __('account.address_state')}}" name="state_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('account.countrie') }} </label>
                                <input type="text" class="form-control countrie" name="countrie" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label> {{ __('home.lastname') }} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.lastname') }}" name="lastname_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.city') }} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.city') }}" name="city_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.zipcode')}} </label>
                                <input type="text" class="form-control" required="true" placeholder=" {{ __('home.zipcode')}}" name="zipcode_collect"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.phone') }}</label>
                                <input type="number" class="form-control" required="true" placeholder="{{ __('home.phone') }}" name="phone_collect"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h4 class="col-xs-12 col-md-12 titles"> <b>{{ __('home.pack_delevery') }} </b> </h4>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label> {{ __('home.firstname')}} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.firstname')}}" name="firstname"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.address')}} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.address')}}" name="address"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('account.address_state')}}</label>
                                <input type="text" class="form-control" placeholder="{{ __('account.address_state')}}" name="state"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.email') }} </label>
                                <input type="text" class="form-control" placeholder="{{ __('home.email') }}" name="address_email"/>
                            </div>

                            <div class="form-group">
                                <label> {{ __('account.countrie') }} </label>
                                <input type="text" class="form-control country_collect" name="country_collect" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label> {{ __('home.lastname') }} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.lastname') }}" name="lastname"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.city') }} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.city') }}" name="city"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.zipcode')}} </label>
                                <input type="text" class="form-control" required="true" placeholder="{{ __('home.zipcode')}}" name="zipcode"/>
                            </div>
                            <div class="form-group">
                                <label> {{ __('home.phone') }} </label>
                                <input type="number" class="form-control" required="true" placeholder="{{ __('home.phone') }}" name="phone"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <input id="terms_prd" name="terms_prd" type="radio" class="with-font" value="1" />
                                <label> <b>  {{ __('home.pack_terms')}}  </b>  </label>

                                    <button type="button" name="submit" class="btn btn-secondary float-left submit_order_prd directshow"> {{ __('home.pack_submit') }}  </button>

                                    <button type="button" name="submit" data-toggle="modal" data-target="#loginmodal" class="btn btn-secondary float-left directhide"> {{ __('home.pack_submit') }}  </button>

                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div><!--end-->
      </div>
    </div>
</section>
</div>
@endsection
