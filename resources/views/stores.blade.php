@extends('layouts.app')

@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.allstore') }}
            </h1>
        </div>
    </div>
    <section class="section pb-5">
        <div class="container">
            @foreach ($categories as $categorie)
            <h3 class="py-3" style="font-weight:bold;">
                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                    {{$categorie->catname }}
                @else
                    {{$categorie->catname_en }}
                @endif
             </h3>
                <div class="row">
                    @foreach ($categorie->getStore as $store )
                        <div class="card col-3">
                            <a href="{{ $store->store_url }}" target="_blank">
                                <img src="{{asset('upload/stores/'.$store->store_logo)}}" style="padding-top:10px;width:100%;height: 60px;">
                            </a>
                            <div class="card-body">
                                <p class="card-text">
                                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                        {{ $store->store_desc }}
                                    @else
                                        {{ $store->store_desc_en }}
                                    @endif
                                </p>
                                <a href="{{ $store->store_url }}" target="_blank" class="btn btn-link">
                                    {{ __('home.shopnow') }}
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
</div>
@endsection
