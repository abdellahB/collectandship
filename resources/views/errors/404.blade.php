@extends('layouts.app')
@section('content')
<style>
    .section::after {
        border-right: none;
        border-left: none;
    }
</style>
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            
        </div>
    </div>
    <section class="section pb-5">
        <div class="container-lg py-3">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <span class="display-1 d-block">404</span>
                    <div class="mb-4 lead">{{ __('account.404_title')}}</div>
                   <!-- <a href="https://collectandship.com/" class="btn btn-info">{{ __('account.404_btn')}}</a>-->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
