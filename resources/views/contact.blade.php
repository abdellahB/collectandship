@extends('layouts.app')

@section('content')
<div id="body">
    <div class="section">
        <div class="container-lg py-3">
            <h1 class="secondary-head text-shadow text-center">
                {{ __('home.contactus') }}
            </h1>
        </div>
    </div>
    <!--Section: Contact v.1-->
<section class="section pb-5">
    <div class="container-lg py-3">
         <!--Section description-->
    <p class="section-description pb-4 text-center">
        {{ __('home.contact_title') }}
    </p>
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-5 mb-4">
          <!--Form with header-->
          <div class="card">
            <div class="card-body contactsuccess text-center" style="color:green;font-weight:bold;display:none">
                {{ __('home.emailsent') }}
            </div>
            <form class="card-body sendemail">
              <!--Header-->
              <div class="form-header blue accent-1">
                <h3><i class="fas fa-envelope"></i>  {{ __('home.contact_question') }} </h3>
              </div>
              <br>
              <div class="contactrerreur" style="color:red"></div>
              <!--Body-->
              <div class="md-form">
                <input type="text" name="fullname" placeholder="{{ __('home.contact_fullname') }}" required="true" id="form-name" class="form-control">
                <label for="form-name"></label>
              </div>
              <div class="md-form">
                <input type="text" name="email" placeholder="{{ __('home.contact_email') }}" required="true" id="form-email" class="form-control">
                <label for="form-email"></label>
              </div>
              <div class="md-form">
                <input type="text" name="subject" placeholder="{{ __('home.contact_subject') }}" required="true" id="form-Subject" class="form-control">
                <label for="form-Subject"></label>
              </div>
              <div class="md-form">
                {!! csrf_field() !!}
                <textarea id="form-text" name="message" placeholder="{{ __('home.contact_message') }}" required="true" class="form-control md-textarea" rows="4"></textarea>
              </div>
                <div class="md-form text-center mt-4">
                    {!! app('captcha')->display() !!}
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                    @endif
                </div>
              <div class="text-center mt-4">
                <button type="submit" class="btn btn-secondary btn-block rounded-lg sendemail">{{ __('home.contact_btn') }}</button>
              </div>
            </form>
          </div>
          <!--Form with header-->
        </div>
        <!--Grid column-->
        <!--Grid column-->
        <div class="col-lg-7">
          <!--Google map-->
          <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 450px">
            <iframe src="{{ $setting->maps_iframe }}"
              frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <br>
          <!--Buttons-->
          <div class="row text-center">
            <div class="col-md-4">
              <a class="btn-floating blue accent-1"><i class="fas fa-map-marker-alt"></i></a>
              <p>@if( LaravelLocalization::getCurrentLocale() == 'ar') {{ $setting->address }} @else {{ $setting->address_en }} @endif</p>
              <p> @if( LaravelLocalization::getCurrentLocale() == 'ar') {{ $setting->countrie }} @else {{ $setting->countrie_en }} @endif</p>
            </div>

            <div class="col-md-4">
              <a class="btn-floating blue accent-1"><i class="fas fa-phone"></i></a>
              <p>{{ $setting->phone }}</p>
              <p> @if( LaravelLocalization::getCurrentLocale() == 'ar') {{ $setting->working }} @else {{ $setting->working_en }} @endif</p>
            </div>

            <div class="col-md-4">
              <a class="btn-floating blue accent-1"><i class="fas fa-envelope"></i></a>
              <p>{{ $setting->contact_email }}</p>
            </div>
          </div>
        </div>
        <!--Grid column-->
      </div>
    </div>
</section>
  <!--Section: Contact v.1-->
</div>
@endsection
