@extends('layouts.account')

@section('content')
<style>
    @media
     only screen and (max-width: 760px),
     (min-device-width: 768px) and (max-device-width: 1024px)  {
     table {
       border: 0;
     }

     table caption {
       font-size: 1.3em;
     }

     table thead {
       border: none;
       clip: rect(0 0 0 0);
       height: 1px;
       margin: -1px;
       overflow: hidden;
       padding: 0;
       position: absolute;
       width: 1px;
     }

     table tr {
       border-bottom: 3px solid #ddd;
       display: block;
       margin-bottom: .625em;
     }

     table td {
       border-bottom: 1px solid #ddd;
       display: block;
       font-size: .8em;
       text-align: left;
     }

     table td::before {
       /*
       * aria-label has no advantage, it won't be read inside a table
       content: attr(aria-label);
      */
       content: attr(data-label);
       float: right;
       font-weight: bold;
       text-transform: uppercase;
     }

     table td:last-child {
       border-bottom: 0;
     }
   }
 </style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home')}} / </span> {{ __('account.direct_1') }} </span>
        </div>
        @if(count($orders)>0)
        <div class="row box-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ __('account.direct_2') }}</th>
                        <th>{{ __('account.direct_3') }}</th>
                        <th>{{ __('account.direct_4') }}</th>
                        <!--<th>{{ __('account.direct_5') }}</th>-->
                        <th> {{ __('account.direct_6') }} </th>
                        <th>{{ __('account.direct_7') }}</th>
                        <th> {{ __('account.direct_8') }} </th>
                        <th> {{ __('account.direct_9') }} </th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td data-label="{{ __('account.direct_2') }}"> {{ $order->code_order }} </td>
                        <td data-label="{{ __('account.direct_3') }}"> {{ $order->getchild->count() }} </td>
                        <td> {{ $order->created_at->format('Y-m-d') }} </td>
                        <td data-label="{{ __('account.direct_4') }}">
                            {{ $order->getcollect->firstname }}  {{ $order->getcollect->lastname }} <br/>
                            {{ $order->getcollect->province }} {{ $order->getcollect->city }} {{ $order->getcollect->zipcode }} <br/>
                            {{ $order->getcollect->address }} <br/>
                            {{ $order->getcollect->phone }} <br/>
                            {{ $order->getcollect->ship_country }} <br/>
                        </td>

                        <td data-label="{{ __('account.direct_6') }}">
                            @if($order->company_url)
                                <a href="{{ $order->company_url }}" target="_blank"> URL </a> <br/>
                                {{ $order->tracking_number }}
                            @else
                                -
                            @endif
                        </td>
                        <td data-label="{{ __('account.direct_7') }}">
                            @if(Session::get('currency') == "$")
                                {{ $order->pricetopay }}{{ __('account.dollar')}}
                            @else
                                {{ number_format($order->pricetopay * 3.75,2,',','') }} {{ __('account.sar') }}
                            @endif
                        </td>
                        <td data-label="{{ __('account.direct_8') }}">
                            @if($order->status == 0)
                                <span class='badge badge-warning'> {{ __('account.direct_11') }} </span>
                            @elseif($order->status == 1)
                                <span class='badge badge-info'> {{ __('account.direct_12') }} </span>
                            @elseif($order->status == 2)
                                <span class='badge badge-success'>{{ __('account.direct_13') }} </span>
                            @elseif($order->status == 3)
                                <span class='badge badge-success'> {{ __('account.direct_14') }} </span>
                            @elseif($order->status == 10)
                                <span class='badge badge-danger'> {{ __('account.direct_15') }} </span>
                            @endif
                        </td>
                        <td data-label="{{ __('account.direct_9') }}">
                            <button type="button" id="{{$order->id}}" class="btn btn-secondary btn-sm  col-sm-10 bts detail_ship">{{ __('account.packing_details')}}</button> <br/>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!--end row -->
        <div class="row">
            {{ $orders->links() }}
        </div>
        @else
            <div class="col-md-12 box-body pad-0">
            <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                    <h1>
                    <span>{{ __('account.packing_empty') }}</span>
                    <span>

                    </span>
                    </h1>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
