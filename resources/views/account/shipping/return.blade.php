@extends('layouts.account')

@section('content')
<style>
    @media
     only screen and (max-width: 760px),
     (min-device-width: 768px) and (max-device-width: 1024px)  {
     table {
       border: 0;
     }

     table caption {
       font-size: 1.3em;
     }

     table thead {
       border: none;
       clip: rect(0 0 0 0);
       height: 1px;
       margin: -1px;
       overflow: hidden;
       padding: 0;
       position: absolute;
       width: 1px;
     }

     table tr {
       border-bottom: 3px solid #ddd;
       display: block;
       margin-bottom: .625em;
     }

     table td {
       border-bottom: 1px solid #ddd;
       display: block;
       font-size: .8em;
       text-align: left;
     }

     table td::before {
       /*
       * aria-label has no advantage, it won't be read inside a table
       content: attr(aria-label);
      */
       content: attr(data-label);
       float: right;
       font-weight: bold;
       text-transform: uppercase;
     }

     table td:last-child {
       border-bottom: 0;
     }
     .tds{
         width: 100% !important;
     }
   }
 </style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span>{{ __('home.home') }}  / </span> {{ __('account.menu_11') }} </span>
        </div>
        @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
        @if(count($parcels)>0)
        <div class="row box-body">
            <h4 class="row">
                <div class="col-md-12">
                    <b> {{ __('account.menu_11') }}  </b>
                </div>
            </h4>
            <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                <div class="row">
                    <table class="table table-bordered shipping">
                        <thead class="panel-heading">
                            <tr>
                                <th></th>
                                <th scope="col">{{ __('account.parcel_img') }} </th>
                                <th scope="col">{{ __('account.parcel') }}</th>
                                <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                <th scope="col">{{ __('account.parcel_info') }}</th>
                            </tr>
                        </thead>
                        <tbody class="resp_product">
                            @foreach ($parcels as $parcel)
                            <tr class="{{$parcel->id}}">
                                <td  data-label="-" style="vertical-align: middle;" scope="row">
                                    <div class="customcontrol customcheckbox">
                                        <input type="radio" name="parcel_return" id="{{$parcel->moreimage}}" class="customcontrolinput" value="{{$parcel->id}}">
                                    </div>
                                </td>
                                <td data-label="{{ __('account.parcel_img') }}">
                                    <span class="icon-with-child m-r">
                                        @foreach ($parcel->GetImage as $image)
                                            <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                                                <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                            </a>
                                            @break
                                        @endforeach
                                    </span>
                                    <script>
                                        $(document).ready(function() {
                                            $("#fancybox-manual-{{$parcel->id}}").click(function() {
                                                $.fancybox.open([
                                                    @foreach ($parcel->GetImage as $image)
                                                    {
                                                        href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                        type : 'iframe',
                                                    },
                                                    @endforeach
                                                ]
                                                );
                                            });
                                        });
                                    </script>
                                </td>
                                <td data-label="{{ __('account.parcel') }}">
                                    <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                    <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
                                    <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                    <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                </td>
                                <td data-label="{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}">
                                    <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{__('home.kg')}} <br/>
                                    <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{__('home.cm')}} <br/>
                                    <b>{{ __('account.parcel_company') }} </b>   : {{ $parcel->company}} <br/>
                                    <b>{{ __('account.parcel_track') }}</b>      : {{ $parcel->tracking}} <br/>
                                </td>
                                <td data-label="{{ __('account.parcel_info') }}" class="tds" style="width: 20%;">
                                    <b> {{ $parcel->sellername }}</b> <br/>
                                    {!! $parcel->description !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    {{ $parcels->links() }}
                </div>
            </div>
        </div> <!--end row-->
        <div class="row box-body">
            <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                <div class="row py-2">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="seller_address"> {{ __('account.return_seller_address') }} </label>
                            <input type="text" class="form-control" required="true" name="seller_address" id="seller_address" placeholder="{{ __('account.return_seller_address') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="return_info"> {{ __('account.return_problem')}}</label>
                            <textarea class="form-control" rows="5" required="true" name="return_info" id="return_info" placeholder="{{ __('account.return_problem')}}"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <input id="terms_return" name="terms_return" type="radio" class="with-font" value="1" />
                            <label> <b>{{ __('account.packing_terms_prd')}} </b>  </label>
                            <button type="button" name="submit" class="btn btn-secondary float-left submit_order_return">{{ __('account.packing_submit_order') }}  </button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        @else
            <div class="col-md-12 box-body pad-0">
                <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                    <h1>
                    <span>{{ __('account.packing_empty') }}</span>
                    <span>

                    </span>
                    </h1>
                </div>
            </div>
        @endif
        @if($info_bottom)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_bottom->message !!}
                        @else
                            {!! $info_bottom->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
