@extends('layouts.account')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span>{{ __('home.home') }}  / </span> {{  __('account.menu_5') }} </span>
            </div>
            @if(count($packings)>0)
            <div class="row box-body">
                <h4>  {{ __('account.menu_5') }} </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col"> {{ __('account.parcel_img') }}  </th>
                                    <th scope="col">{{ __('account.parcel_code') }}  </th>
                                    <th scope="col">  {{ __('account.packing_type') }}  </th>
                                    <th scope="col"> {{ __('account.ship_address') }} </th>
                                    <th scope="col"> {{ __('account.Ship_company')}} </th>
                                    <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                    <th scope="col"> {{ __('account.ship_moreservice') }} </th>
                                    <th scope="col">{{ __('account.shipping_amount') }} </th>
                                    <th scope="col"> {{ __('account.purchase_8') }} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($packings AS $packing)
                                <tr class="{{ $packing->id }}">
                                    <td>
                                        <span class="icon-with-child m-r">
                                            @foreach ($packing->imagePacking as $image)
                                                <a href="javascript:voide()" id="fancybox-manual-{{$packing->id}}">
                                                    <img  width="90" height="90" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                                </a>
                                                @break
                                            @endforeach
                                        </span>
                                        <script>
                                            $(document).ready(function() {
                                                $("#fancybox-manual-{{$packing->id}}").click(function() {
                                                    $.fancybox.open([
                                                        @foreach ($packing->imagePacking as $image)
                                                        {
                                                            href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                            title : 'My title'
                                                        },
                                                        @endforeach
                                                    ]
                                                    );
                                                });
                                            });
                                        </script>
                                    </td>
                                    <td><a href="javascript:void()" id="{{ $packing->id}}" class="getlist">{{ $packing->code_parcel}}</a></td>
                                    <td>
                                        @if($packing->modepacking =="withpack")
                                           {{ __('account.packing_using') }}
                                        @else
                                            {{ __('account.remove_packing') }}
                                        @endif
                                    </td>
                                    <td style="text-align: left;">
                                        {{ $packing->getAddress->adress }} <br/>
                                        {{ $packing->getAddress->province }} |  {{ $packing->getAddress->city }}<br/>
                                        {{ $packing->getAddress->phone }} <br/>
                                        {{ $packing->getAddress->getcountry->countriename }} <br/>
                                    </td>
                                    <td>
                                        <a href="{{ $packing->getTracking->company_url }}" target="_blank">
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $packing->getCompany->companiename}}
                                            @else
                                                {{ $packing->getCompany->companiename_en}}
                                            @endif
                                        </a> <br/>
                                        {{ $packing->getTracking->tracking_info }}
                                    </td>
                                    <td>
                                        {{ $packing->weight}} {{ __('home.kg') }}<br/>
                                        {{ $packing->length}} * {{ $packing->width}} * {{ $packing->height}} {{ __('home.cm') }}<br/>
                                    </td>
                                    <td>
                                        @if(count($packing->getchild)>0)
                                            @foreach ($packing->getchild as $child)
                                                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                    {{ $child->getService->service_name }} <br/>
                                                @else
                                                    {{ $child->getService->service_name_en }} <br/>
                                                @endif
                                                @if(Session::get('currency') == "$")
                                                    {{ $child->getService->service_price }}{{ __('account.dollar')}} <br/>
                                                @else
                                                    {{ number_format($child->getService->service_price * 3.75,2,',','') }} {{ __('account.sar') }} <br/>
                                                @endif
                                            @endforeach
                                        @else
                                            <b> {{ __('account.notexist') }} </b>
                                        @endif
                                    </td>
                                    <td>
                                        @if(Session::get('currency') == "$")
                                            {{ $packing->pricetotal }}{{ __('account.dollar')}}
                                        @else
                                            {{ number_format($packing->pricetotal * 3.75,2,',','') }} {{ __('account.sar') }}
                                        @endif
                                    </td>
                                    <td>
                                        <span class="badge badge-success">
                                            {{ __('account.dateship')}} <br/><br/> {{ $packing->updated_at->format('Y-m-d') }}
                                        </span><br/>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        {{ $packings->links() }}
                    </div>
                </div>
                @else
                <div class="col-md-12 box-body pad-0">
                    <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                        <h1>
                        <span>{{ __('account.packing_empty') }}</span>
                        <span>

                        </span>
                        </h1>
                    </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
