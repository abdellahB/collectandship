@extends('layouts.account')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home')}}  / </span>{{ __('account.menu_3')}} </span>
            </div>
            @if($info_top)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_top->message !!}
                            @else
                                {!! $info_top->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
            @if(count($packings)>0)
            <div class="row box-body">
                <h4>{{ __('account.menu_3') }} </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col"> {{ __('account.parcel_code') }}  </th>
                                    <th scope="col"> {{ __('account.packing_number_prd') }} </th>
                                    <th scope="col"> {{ __('account.packing_type')}} </th>
                                    <th scope="col"> {{ __('account.packing_date_order')}} </th>
                                    <th scope="col"> {{ __('account.ship_address')}} </th>
                                    <th scope="col"> {{ __('account.Ship_company')}} </th>
                                    <th scope="col">{{ __('account.ship_moreservice')}} </th>
                                    <th scope="col"> {{ __('account.purchase_8')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($packings AS $packing)
                                <tr class="{{ $packing->id }}">
                                    <td><a href="javascript:void()" id="{{ $packing->id}}" class="getlist">{{ $packing->code_parcel}}</a></td>
                                    <td>{{ $packing->Nbpackage}}</td>
                                    <td>
                                        @if($packing->modepacking =="withpack")
                                           {{ __('account.packing_using') }}
                                        @else
                                             {{ __('account.remove_packing') }}
                                        @endif
                                    </td>
                                    <td>{{ $packing->created_at->format('Y-m-d') }}</td>
                                    <td style="text-align: left;">
                                        {{ $packing->getAddress->adress }} <br/>
                                        {{ $packing->getAddress->province }} |  {{ $packing->getAddress->city }}<br/>
                                        {{ $packing->getAddress->phone }} <br/>
                                        {{ $packing->getAddress->getcountry->countriename }} <br/>
                                    </td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                            {{ $packing->getCompany->companiename}}
                                        @else
                                            {{ $packing->getCompany->companiename_en}}
                                        @endif
                                    </td>
                                    <td>
                                        @if(count($packing->getchild)>0)
                                            @foreach ($packing->getchild as $child)
                                                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                    {{ $child->getService->service_name }}
                                                @else
                                                    {{ $child->getService->service_name_en }}
                                                @endif
                                                @if(Session::get('currency') == "$")
                                                   <b class="flt"> {{ $child->getService->service_price }}{{ __('account.dollar')}}</b> <br/>
                                                @else
                                                    <b class="flt">{{ number_format($child->getService->service_price * 3.75,2,',','') }} {{ __('account.sar') }}</b> <br/>
                                                @endif
                                            @endforeach
                                        @else
                                            <b> {{ __('account.notexist') }} </b>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="badge badge-warning"> {{ __('account.packing_proccess')}} </span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        {{ $packings->links() }}
                    </div>
                </div>
                @else
                    <div class="col-md-12 box-body pad-0">
                    <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                            <h1>
                            <span>{{ __('account.packing_empty') }}</span>
                            <span>

                            </span>
                            </h1>
                        </div>
                    </div>
                @endif
            </div>
            @if($info_bottom)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_bottom->message !!}
                            @else
                                {!! $info_bottom->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
