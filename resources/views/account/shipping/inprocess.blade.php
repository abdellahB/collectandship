@extends('layouts.account')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span>{{ __('home.home')}}  / </span> {{ __('account.menu_8')}}</span>
            </div>
            @if($info_top)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_top->message !!}
                            @else
                                {!! $info_top->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
            @if(count($services)>0)
            <div class="row box-body">
                <h4> <b> {{ __('account.menu_8') }} </b> </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col"> {{ __('account.service_order_code')}} </th>
                                    <th scope="col"> {{ __('account.parcel_code') }} </th>
                                    <th scope="col"> {{ __('account.packing_date_order')}} </th>
                                    <th scope="col"> {{ __('account.packing_service_2')}} </th>
                                    <th scope="col"> {{ __('account.packing_service_3')}} </th>
                                    <th scope="col"> {{ __('account.purchase_8')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                        <tr>
                                            <td> {{ $service->code_service}}</td>
                                            <td><a href="javascript:void()" id="{{ $service->shipping_id }}" class="getlist_prd">{{ $service->getPship->codeparcel}} </a></td>
                                            <td> {{ $service->created_at->format('Y-m-d')}}</td>
                                            <td>
                                                @if(Session::get('currency') == "$")
                                                    {{ $service->getPservice->service_price }}{{ __('account.dollar')}}
                                                @else
                                                    {{ number_format($service->getPservice->service_price * 3.75,2,'.','') }} {{ __('account.sar') }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ $service->information}} <br/>
                                                <b style="color:red">
                                                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                        {{ $service->getPservice->service_name }}
                                                    @else
                                                        {{ $service->getPservice->service_name_en }}
                                                    @endif
                                                </b>
                                            </td>
                                            <td>
                                                @if($service->status==0)
                                                    <span class="badge badge-warning"> {{ __('account.service_order_1')}} </span>
                                                @endif
                                            </td>
                                        </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end row-->
            @else
                <div class="col-md-12 box-body pad-0">
                    <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                        <h1>
                         <span>{{ __('account.packing_empty') }}</span>
                        <span>

                        </span>
                        </h1>
                    </div>
                </div>
            @endif
            @if($info_bottom)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_bottom->message !!}
                            @else
                                {!! $info_bottom->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
