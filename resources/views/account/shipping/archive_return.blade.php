@extends('layouts.account')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home')}}  / </span> {{ __('account.menu_13') }} </span>
        </div>
        @if(count($parcels)>0)
        <div class="row box-body">
            <h4 class="row">
                <div class="col-md-12">
                    <b>   {{ __('account.menu_13') }}  </b>
                </div>
            </h4>
            <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                <div class="row">
                    <table class="table table-bordered shipping">
                        <thead class="panel-heading">
                            <tr>
                                <th scope="col">{{ __('account.service_order_code') }}</th>
                                <th scope="col">{{ __('account.parcel') }}</th>
                                <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                <th scope="col"> {{ __('account.Ship_company')}} </th>
                                <th scope="col">{{ __('account.return_seller_address')}}</th>
                                <th scope="col">{{ __('account.return_problem')}}</th>
                                <th scope="col"> {{ __('account.packing_service_2')}} </th>
                                <th scope="col">{{ __('account.purchase_8')}}</th>
                            </tr>
                        </thead>
                        <tbody class="resp_product">
                            @foreach ($parcels as $parcel)
                            <tr class="{{$parcel->id}}">

                                <td>{{$parcel->code_return}}</td>
                                <td>
                                    <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                    <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
                                    <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                    <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                </td>
                                <td>
                                    <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{ __('home.kg')}} <br/>
                                    <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
                                </td>
                                <td>
                                    @if($parcel->status==3)
                                        <b> {{ __('account.parcel_track') }} : {{$parcel->tracking_code}} </b> <br>
                                        <b> {{ __('account.direct_10')}}  : <a href="{{$parcel->tracking_url}}" target="_blank">  {{ __('account.purchase_10') }} </a> <br/>
                                    @endif
                                </td>
                                <td>
                                    {{ $parcel->seller_address }}
                                </td>
                                <td> {{ $parcel->return_info }}</td>
                                <td>
                                    @if($parcel->status!=0)
                                        @if(Session::get('currency') == "$")
                                            {{ $parcel->pricetopay }}{{ __('account.dollar')}}
                                        @else
                                            {{ number_format($parcel->pricetopay * 3.75,2,'.','') }} {{ __('account.sar') }}
                                        @endif
                                    @else
                                        <b>{{ __('account.return_noselected')}} </b>
                                    @endif
                                </td>
                                <td>
                                    <span class="badge badge-success"> {{ __('account.parcel_shipped')}} </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    {{ $parcels->links() }}
                </div>
            </div>
        </div> <!--end row-->
        @else
            <div class="col-md-12 box-body pad-0">
                <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                    <h1>
                    <span>{{ __('account.packing_empty') }}</span>
                    <span>

                    </span>
                    </h1>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
