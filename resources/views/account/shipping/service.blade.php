@extends('layouts.account')

@section('content')
<style>
    @media
     only screen and (max-width: 760px),
     (min-device-width: 768px) and (max-device-width: 1024px)  {
     table {
       border: 0;
     }

     table caption {
       font-size: 1.3em;
     }

     table thead {
       border: none;
       clip: rect(0 0 0 0);
       height: 1px;
       margin: -1px;
       overflow: hidden;
       padding: 0;
       position: absolute;
       width: 1px;
     }

     table tr {
       border-bottom: 3px solid #ddd;
       display: block;
       margin-bottom: .625em;
     }

     table td {
       border-bottom: 1px solid #ddd;
       display: block;
       font-size: .8em;
       text-align: left;
     }

     table td::before {
       /*
       * aria-label has no advantage, it won't be read inside a table
       content: attr(aria-label);
      */
       content: attr(data-label);
       float: right;
       font-weight: bold;
       text-transform: uppercase;
     }

     table td:last-child {
       border-bottom: 0;
     }
     .tds{
         width: 100% !important;
     }
   }
 </style>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home')}}  / </span> {{ __('account.menu_6') }} </span>
            </div>
            @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
            @endif
            <div class="row box-body ship_process">
                <h4 class="row">
                    <div class="col-md-12">
                        <b>  {{ __('account.menu_6') }} </b>
                    </div>
                </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col">
                                    </th>
                                    <th scope="col">{{ __('account.parcel_img') }} </th>
                                    <th scope="col">{{ __('account.parcel') }}</th>
                                    <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                    <th scope="col">{{ __('account.parcel_info') }}</th>
                                </tr>
                            </thead>
                            <tbody class="resp_product">
                                @foreach ($parcels as $parcel)
                                <tr class="{{$parcel->id}}">
                                    <td  data-label="-" style="vertical-align: middle;" scope="row">
                                        <div class="customcontrol customcheckbox">
                                            <input type="radio" name="parcelID[]" id="{{$parcel->moreimage}}" class="customcontrolinput" value="{{$parcel->id}}">
                                        </div>
                                    </td>
                                    <td data-label="{{ __('account.parcel_img') }}">
                                        <span class="icon-with-child m-r">
                                            @if($parcel->moreimage == 1)
                                                <img  width="80" height="80" src="{{url('assets/user/img/processing.png')}}" alt="">
                                            @else
                                                @foreach ($parcel->GetImage as $image)
                                                    <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                                                        <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                                    </a>
                                                    @break
                                                @endforeach
                                            @endif
                                        </span>
                                        <script>
                                            $(document).ready(function() {
                                                $("#fancybox-manual-{{$parcel->id}}").click(function() {
                                                    $.fancybox.open([
                                                        @foreach ($parcel->GetImage as $image)
                                                        {
                                                            href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                                title : 'My title'
                                                            },
                                                            @endforeach
                                                    ]
                                                    );
                                                });
                                            });
                                        </script>
                                    </td>
                                   <td data-label="{{ __('account.parcel') }}">
                                    <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                    <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
                                    <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                    <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                    </td>
                                    <td data-label="{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}">
                                        <b>{{ __('account.parcel_weight') }}</b>      : {{ $parcel->weight}} {{ __('home.kg') }} <br/>
                                        <b>{{ __('account.parcel_dimension') }}</b>     : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
                                        <b>{{ __('account.parcel_company') }} </b>: {{ $parcel->company}} <br/>
                                        <b>{{ __('account.parcel_track') }}</b> : {{ $parcel->tracking}} <br/>
                                    </td>
                                    <td data-label="{{ __('account.parcel_info') }}" class="tds" style="width: 20%;">
                                       <b> {{ $parcel->sellername }}</b> <br/>
                                        {!! $parcel->description !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        {{ $parcels->links() }}
                    </div>
                </div>
            </div><!--end row -->
            <div class="row box-body">
                <h4> <b>{{ __('account.packing_service_purchase') }}</b> </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th></th>
                                   <th scope="col">{{ __('account.packing_service_1')}} </th>
                                    <th scope="col">{{ __('account.packing_service_2')}} </th>
                                    <th scope="col">{{ __('account.packing_service_3')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($prd_services as $service)
                                    <tr>
                                        <td>
                                            <div class="customcontrol customcheckbox">
                                                <input type="checkbox" name="service_id[]" class="customcontrolinput_service" value="{{ $service->id }}">
                                            </div>
                                        </td>
                                        <td>
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $service->service_name }}
                                            @else
                                                {{ $service->service_name_en }}
                                            @endif
                                        </td>
                                        <td>
                                            @if(Session::get('currency') == "$")
                                                {{ $service->service_price }}{{ __('account.dollar')}}
                                            @else
                                                {{ number_format($service->service_price * 3.75,2,'.','') }} {{ __('account.sar') }}
                                            @endif
                                        </td>
                                        <td>
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $service->service_des }}
                                            @else
                                                {{ $service->service_des_en }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row box-body">
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row py-2">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('account.packing_information_prd') }} </label>
                                <textarea class="form-control information_prd" rows="5" placeholder="{{ __('account.packing_information_prd') }}" name="information_prd"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <input id="terms_prd" name="terms_prd" type="radio" class="with-font" value="1" />
                                <label> <b> {{ __('account.packing_terms_prd') }} </b>  </label>
                                <button type="button" name="submit" class="btn btn-secondary float-left submit_orderprd">
                                    {{ __('account.packing_submit_order') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            @if($info_bottom)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_bottom->message !!}
                            @else
                                {!! $info_bottom->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
