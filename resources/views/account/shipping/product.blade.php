@extends('layouts.account')

@section('content')
<style>
    @media
     only screen and (max-width: 760px),
     (min-device-width: 768px) and (max-device-width: 1024px)  {
     table {
       border: 0;
     }

     table caption {
       font-size: 1.3em;
     }

     table thead {
       border: none;
       clip: rect(0 0 0 0);
       height: 1px;
       margin: -1px;
       overflow: hidden;
       padding: 0;
       position: absolute;
       width: 1px;
     }

     table tr {
       border-bottom: 3px solid #ddd;
       display: block;
       margin-bottom: .625em;
     }

     table td {
       border-bottom: 1px solid #ddd;
       display: block;
       font-size: .8em;
       text-align: left;
     }

     table td::before {
       /*
       * aria-label has no advantage, it won't be read inside a table
       content: attr(aria-label);
      */
       content: attr(data-label);
       float: right;
       font-weight: bold;
       text-transform: uppercase;
     }

     table td:last-child {
       border-bottom: 0;
     }
     .tds{
         width: 100% !important;
     }
   }
 </style>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home') }}  / </span>  {{ __('account.menu_2') }} </span>
            </div>
            @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
            @endif
            @if(count($parcel_usa)>0 || count($parcel_sa)>0 || count($parcel_uae)>0)
            <div class="row box-body ship_process">
                <ul class="nav nav-tabs navtabs col-md-12">
                    @if(count($parcel_usa)>0)
                        <li class="col-md-4" ><a class="@if(count($parcel_usa)>0) active @endif  btn-block warhouse" id="usa" data-toggle="tab" href="#home">{{ __('account.warehouse_usa')}} </a></li>
                    @endif
                    @if(count($parcel_sa)>0)
                        <li class="col-md-4"><a class="@if(count($parcel_usa)<=0 && count($parcel_sa)>0) active @endif btn-block warhouse"         id="sa"  data-toggle="tab" href="#menu1">{{ __('account.warehouse_sa')}} </a></li>
                    @endif
                    @if(count($parcel_uae)>0)
                        <li class="col-md-4"><a class="@if(count($parcel_usa)<=0 && count($parcel_sa)<=0 && count($parcel_uae)>0) active @endif btn-block warhouse"         id="uae" data-toggle="tab" href="#menu2">{{ __('account.warehouse_uae')}} </a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade @if(count($parcel_usa)>0) in active show @endif">
                        <table class="table table-bordered shipping_prd">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col">
                                        <div class="customcontrol customcheckbox">
                                            <input type="checkbox" class="customcontrolinput checkAll_usa" value>
                                        </div>
                                    </th>
                                    <th scope="col">{{ __('account.parcel_img') }} </th>
                                    <th scope="col">{{ __('account.parcel') }}</th>
                                    <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                    <th scope="col">{{ __('account.parcel_info') }}</th>
                                    <th scope="col">{{ __('account.parcel_price') }} </th>
                                </tr>
                            </thead>
                            <tbody class="respproduct">
                                @foreach ($parcel_usa as $parcel)
                                    <tr class="{{$parcel->id}}">
                                        <td  data-label="-" style="vertical-align: middle;">
                                            <div class="customcontrol customcheckbox">
                                                <input type="checkbox" name="parcelID[]" id="{{$parcel->moreimage}}" class="customcontrolinput_usa" value="{{$parcel->id}}">
                                            </div>
                                        </td>
                                        <td data-label="{{ __('account.parcel_img') }}">
                                            <span class="icon-with-child m-r">
                                                @if($parcel->moreimage == 1)
                                                    <img  width="80" height="80" src="{{url('assets/user/img/processing.png')}}" alt="">
                                                @else
                                                    @foreach ($parcel->GetImage as $image)
                                                        <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                                                            <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                                        </a>
                                                        @break
                                                    @endforeach
                                                @endif
                                            </span>
                                            <script>
                                                $(document).ready(function() {
                                                    $("#fancybox-manual-{{$parcel->id}}").click(function() {
                                                        $.fancybox.open([
                                                            @foreach ($parcel->GetImage as $image)
                                                                {
                                                                    href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                                    title : 'My title'
                                                                },
                                                            @endforeach
                                                        ]
                                                        );
                                                    });
                                                });
                                            </script>
                                        </td>
                                        <td data-label="{{ __('account.parcel') }}">
                                            <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                            <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
                                            <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                            <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                        </td>
                                        <td data-label="{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}">
                                            <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{ __('home.kg') }} <br/>
                                            <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
                                            <b>{{ __('account.parcel_company') }} </b>   : {{ $parcel->company}} <br/>
                                            <b>{{ __('account.parcel_track') }}</b>      : {{ $parcel->tracking}} <br/>
                                        </td>
                                        <td data-label="{{ __('account.parcel_info') }}">
                                        <b> {{ $parcel->sellername }}</b> <br/>
                                            {!! $parcel->description !!}
                                        </td>
                                        <td data-label="{{ __('account.parcel_price') }}" class="tds" style="vertical-align: middle;width:20%;">
                                            <input type="number" class="form-control product_price" placeholder=" {{ __('account.parcel_price') }} @if(Session::get('currency') == "$") {{__('account.dollar')}} @else {{__('account.sar')}} @endif"/>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            {{ $parcel_usa->links() }}
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade @if(count($parcel_usa)<=0 && count($parcel_sa)>0) in active show @endif">
                        <table class="table table-bordered shipping_prd">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col">
                                        <div class="customcontrol customcheckbox">
                                            <input type="checkbox" class="customcontrolinput checkAll_sa" value>
                                        </div>
                                    </th>
                                    <th scope="col">{{ __('account.parcel_img') }} </th>
                                    <th scope="col">{{ __('account.parcel') }}</th>
                                    <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                    <th scope="col">{{ __('account.parcel_info') }}</th>
                                    <th scope="col">{{ __('account.parcel_price') }} </th>
                                </tr>
                            </thead>
                            <tbody class="respproduct">
                                @foreach ($parcel_sa as $parcel)
                                    <tr class="{{$parcel->id}}">
                                        <td  data-label="-" style="vertical-align: middle;" scope="row">
                                            <div class="customcontrol customcheckbox">
                                                <input type="checkbox" name="parcelID[]" id="{{$parcel->moreimage}}" class="customcontrolinput_sa" value="{{$parcel->id}}">
                                            </div>
                                        </td>
                                        <td data-label="{{ __('account.parcel_img') }}">
                                            <span class="icon-with-child m-r">
                                                @if($parcel->moreimage == 1)
                                                    <img  width="80" height="80" src="{{url('assets/user/img/processing.png')}}" alt="">
                                                @else
                                                    @foreach ($parcel->GetImage as $image)
                                                        <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                                                            <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                                        </a>
                                                        @break
                                                    @endforeach
                                                @endif
                                            </span>
                                            <script>
                                                $(document).ready(function() {
                                                    $("#fancybox-manual-{{$parcel->id}}").click(function() {
                                                        $.fancybox.open([
                                                            @foreach ($parcel->GetImage as $image)
                                                            {
                                                                href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                                    title : 'My title'
                                                                },
                                                                @endforeach
                                                        ]
                                                        );
                                                    });
                                                });
                                            </script>
                                        </td>
                                        <td data-label="{{ __('account.parcel') }}">
                                            <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                            <b> {{ __('account.parcel_date') }} </b>    :{{ $parcel->date_arrival}} <br/>
                                            <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                            <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                        </td>
                                        <td data-label="{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}">
                                            <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{ __('home.kg') }} <br/>
                                            <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
                                            <b>{{ __('account.parcel_company') }} </b>   : {{ $parcel->company}} <br/>
                                            <b>{{ __('account.parcel_track') }}</b>      : {{ $parcel->tracking}} <br/>
                                        </td>
                                        <td data-label="{{ __('account.parcel_info') }}">
                                        <b> {{ $parcel->sellername }}</b> <br/>
                                            {{ $parcel->description }}
                                        </td>
                                        <td data-label="{{ __('account.parcel_price') }}"  class="tds" style="vertical-align: middle;width:20%;">
                                            <input type="number" class="form-control product_price" placeholder=" {{ __('account.parcel_price') }} @if(Session::get('currency') == "$") {{__('account.dollar')}} @else {{__('account.sar')}} @endif"/>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            {{ $parcel_sa->links() }}
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <table class="table table-bordered shipping_prd">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col">
                                        <div class="customcontrol customcheckbox">
                                            <input type="checkbox" class="customcontrolinput checkAll_uae" value>
                                        </div>
                                    </th>
                                    <th scope="col">{{ __('account.parcel_img') }} </th>
                                    <th scope="col">{{ __('account.parcel') }}</th>
                                    <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                                    <th scope="col">{{ __('account.parcel_info') }}</th>
                                    <th scope="col">{{ __('account.parcel_price') }} </th>
                                </tr>
                            </thead>
                            <tbody class="respproduct">
                                @foreach ($parcel_uae as $parcel)
                                    <tr class="{{$parcel->id}}">
                                        <td  data-label="-" style="vertical-align: middle;" scope="row">
                                            <div class="customcontrol customcheckbox">
                                                <input type="checkbox" name="parcelID[]" id="{{$parcel->moreimage}}" class="customcontrolinput_uae" value="{{$parcel->id}}">
                                            </div>
                                        </td>
                                        <td data-label="{{ __('account.parcel_img') }}">
                                            <span class="icon-with-child m-r">
                                                @if($parcel->moreimage == 1)
                                                    <img  width="80" height="80" src="{{url('assets/user/img/processing.png')}}" alt="">
                                                @else
                                                    @foreach ($parcel->GetImage as $image)
                                                        <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                                                            <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                                                        </a>
                                                        @break
                                                    @endforeach
                                                @endif
                                            </span>
                                            <script>
                                                $(document).ready(function() {
                                                    $("#fancybox-manual-{{$parcel->id}}").click(function() {
                                                        $.fancybox.open([
                                                            @foreach ($parcel->GetImage as $image)
                                                            {
                                                                href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                                                    title : 'My title'
                                                                },
                                                                @endforeach
                                                        ]
                                                        );
                                                    });
                                                });
                                            </script>
                                        </td>
                                        <td data-label="{{ __('account.parcel') }}">
                                            <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
                                            <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
                                            <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
                                            <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
                                        </td>
                                        <td data-label="{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}">
                                            <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{ __('home.kg') }} <br/>
                                            <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
                                            <b>{{ __('account.parcel_company') }} </b>   : {{ $parcel->company}} <br/>
                                            <b>{{ __('account.parcel_track') }}</b>      : {{ $parcel->tracking}} <br/>
                                        </td>
                                        <td data-label="{{ __('account.parcel_info') }}">
                                        <b> {{ $parcel->sellername }}</b> <br/>
                                            {{ $parcel->description }}
                                        </td>
                                        <td data-label="{{ __('account.parcel_price') }}"  class="tds" style="vertical-align: middle; width:20%;">
                                            <input type="number" class="form-control product_price" placeholder=" {{ __('account.parcel_price') }} @if(Session::get('currency') == "$") {{__('account.dollar')}} @else {{__('account.sar')}} @endif"/>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            {{ $parcel_uae->links() }}
                        </div>
                    </div>
                </div>
            </div><!--end row -->
            <div class="col-md-12 box-body ship_select ship_process">
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row py-2">
                        <div class="col-md-3">
                            <label class="country-label">
                                <div data-toggle="tooltip" title="بإمكانك تعديل عناوينك أو إضافة عناوين جديدة من دفتر عناويني" class="mytooltip pt-2">
                                   {{ __('account.packng_address') }}
                                </div>
                            </label>
                        </div>
                        <div class="col-md-6">
                            @if(count($newaddress)>0)
                                <select id="address_country" data ="@if(count($parcel_usa)>0)usa @elseif(count($parcel_usa)<=0 && count($parcel_sa)>0)sa @else uae @endif" name="address_country" class="form-control input-lg address_country">
                                    <option value=""> {{ __('account.packing_myaddress') }} </option>
                                    @foreach ($newaddress as $newaddress)
                                        <option value="{{ $newaddress->id }}">
                                            {{ $newaddress->firstname }}  {{ $newaddress->lastname }} |
                                            {{ $newaddress->adress }} |
                                            {{ $newaddress->city }} |
                                            {{ $newaddress->phone }} |
                                            {{ $newaddress->getcountry->countriename }}
                                        </option>
                                    @endforeach
                                </select>
                            @else
                                <a href="javascript:void()" data-toggle="modal" data-target="#myModalNewAddress" class="btn btn-secondary rounded-lg btn-block">
                                    {{ __('account.add_new_address') }}
                                </a>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:void()" class="btn btn-secondary btn-block rounded-lg shipping_btn">
                                {{ __('account.packing_modeship') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            @else
            <div class="col-md-12 box-body pad-0">
            <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                    <h1>
                    <span>{{ __('account.packing_empty') }}</span>
                    <span>

                    </span>
                    </h1>
                </div>
            </div>
            @endif
            <form class="forms">
            <div class="row box-body">
                <h4> <b> {{ __('account.modeshipping')}} </b> </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th></th>
                                    <th scope="col">{{ __('account.shipping_mode') }} </th>
                                    <th scope="col">{{ __('account.shipping_time') }} </th>
                                </tr>
                            </thead>
                            <tbody class="show_company">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row box-body">
                <h4> <b>{{ __('account.packing_service')}}</b> </h4>
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th></th>
                                    <th scope="col">{{ __('account.packing_service_1')}} </th>
                                    <th scope="col">{{ __('account.packing_service_2')}} </th>
                                    <th scope="col">{{ __('account.packing_service_3')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                    <tr>
                                        <td>
                                            <div class="customcontrol customcheckbox">
                                                <input type="checkbox" name="serviceid" class="customcontrolinputservice" value="{{ $service->id }}">
                                            </div>
                                        </td>
                                        <th>
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $service->service_name }}
                                            @else
                                                {{ $service->service_name_en }}
                                            @endif
                                        </th>
                                        <th>
                                            @if(Session::get('currency') == "$")
                                                {{ $service->service_price }}$
                                            @else
                                                {{ number_format($service->service_price * 3.75,2,'.','') }}{{ __('account.sar') }}
                                            @endif
                                        </th>
                                        <th>
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $service->service_des }}
                                            @else
                                                {{ $service->service_des_en }}
                                            @endif
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row box-body">
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="row py-2">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <ul class="list-group" style="color:red">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                </ul>
                            </div>
                            <div class="form-group">
                                <div class="plan-data">
                                    <input id="modpacking" name="modpacking" type="radio" class="with-font" value="withpack" />
                                    <label> {{ __('account.packing_modpacking_1') }}</label>
                                </div>
                                <div class="plan-data">
                                    <input id="modpacking" name="modpacking" type="radio" class="with-font" value="withoutpack" />
                                    <label> {{ __('account.packing_modpacking_2') }} </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>{{ __('account.packing_total_purchase')}}</label>
                                <input class="form-control total_purchase" type="number" name="total_purchase" readonly/>
                            </div>
                            <div class="form-group">
                                <label>{{ __('account.packing_information') }}</label>
                                <textarea class="form-control information" placeholder="{{ __('account.packing_information') }}" name="information"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <input id="terms" name="terms" type="radio" class="with-font" value="1" />
                                <label> <b> {{ __('account.packing_terms') }} </b>  </label>
                                <button type="button" name="submit" class="btn btn-secondary float-left submit_order">
                                    {{ __('account.packing_submit') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->
            </form>
            @if($info_bottom)
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="usability">
                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                {!! $info_bottom->message !!}
                            @else
                                {!! $info_bottom->message_en !!}
                            @endif
                        </blockquote>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
