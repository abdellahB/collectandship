@extends('layouts.account')

@section('content')
<style>
   @media
	only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
    table {
      border: 0;
    }

    table caption {
      font-size: 1.3em;
    }

    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: .625em;
    }

    table td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: .8em;
      text-align: left;
    }

    table td::before {
      /*
      * aria-label has no advantage, it won't be read inside a table
      content: attr(aria-label);
     */
      content: attr(data-label);
      float: right;
      font-weight: bold;
      text-transform: uppercase;
    }

    table td:last-child {
      border-bottom: 0;
    }
  }
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home')}} / </span>{{ __('account.menu_24_4') }} </span>
        </div>
        @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
        <div class="row box-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ __('account.transacton') }}</th>
                        <th>{{ __('account.transacton_1') }}</th>
                        <th>{{ __('account.transacton_2') }}	</th>
                        <th> {{ __('account.transacton_r') }} </th>
                        <th>{{ __('account.transacton_3') }}	</th>
                        <th>{{ __('account.transacton_4') }}	</th>
                        <th>{{ __('account.transacton_5') }}</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaction as  $transact )
                        <tr>
                            <td data-label="{{ __('account.transacton') }}">{{ $transact->id }}</td>
                            <td data-label="{{ __('account.transacton_1') }}">{{ $transact->transaction_code }}</td>
                            <td data-label="{{ __('account.transacton_2') }}">
                                {{ $transact->transaction_amount }}{{ $transact->currency }}
                            </td>
                            <td data-label="{{ __('account.transacton_r') }}">
                                {{ $transact->transaction_refund }}{{ $transact->currency }}
                            </td>
                            <td data-label="{{ __('account.transacton_3') }}">{{ $transact->created_at->format('Y-m-d') }}</td>
                            <td data-label="{{ __('account.transacton_4') }}">{{ $transact->transaction_orders }}</td>
                            <td data-label="{{ __('account.transacton_5') }}">{{ $transact->transaction_payment }}</td>
                            <td data-label="-">
                                <a href="#" class="btn btn-info showOrders " id="{{ $transact->id }}"> {{ __('account.packing_details') }} </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!--end row -->
        <div class="row">
            {{ $transaction->links() }}
        </div>
        @if($info_bottom)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_bottom->message !!}
                        @else
                            {!! $info_bottom->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
