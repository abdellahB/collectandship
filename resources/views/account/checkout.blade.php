@extends('layouts.account')
@section('content')
<style>
    .showriyal{
        display: none;
    }
</style>
<div class="wrapper wrapper-content">
    <div class="row row py-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if($message = Session::get('pay_success'))
                <script>$(function() { $(".wrapper-content").hide(); $('#myModalSuccessPay').modal('show');  });</script>
                {{ Cart::instance('collectandship')->destroy() }}
                {{ Session::forget('pay_success') }}
            @endif
            @if($message = Session::get('pay_error'))
                <div class="badge badge-danger btn-block">
                    <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {!! $message !!}
                </div>
                {{ Session::forget('pay_error') }}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <h4 class="textbg"> {{ __('account.cart_pay') }} </h4>
            <form method="POST" action="{{ LaravelLocalization::localizeUrl('/paytabs_payment') }}">
                <div class="widget py-3 shadow">
                    <h5 class="box-title" style="padding-bottom: 10px;color: #17a2b8;"> {{ __('account.billing_address')}} </h5>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label> {{ __('home.firstname') }} </label>
                                <input class="form-control" type="text" name="firstname" value="{{ Auth::user()->firstname }}">
                            </div>
                            @csrf
                            <div class="form-group">
                                <label> {{ __('home.lastname') }} </label>
                                <input class="form-control" type="text" name="lastname" value="{{ Auth::user()->lastname }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('home.email') }}</label>
                                <input class="form-control" type="text" name="email"  readonly value="{{ Auth::user()->email }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('home.phone') }} </label>
                                <input class="form-control" type="text" name="phone"  value="{{ Auth::user()->phone }}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label>{{ __('account.countrie') }} </label>
                                <select type="text" name="countrie_id" class="form-control">
                                    <option>{{ __('home.countrie') }} </option>
                                    @if(count($countries)>0)
                                        @foreach ($countries as $countrie)
                                            <option value="{{$countrie->id}}" @if(Auth::user()->countrie_id == $countrie->id ) selected @endif >
                                                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                    {{ $countrie->countriename_ar }}
                                                @else
                                                    {{ $countrie->countriename }}
                                                @endif
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ __('home.city') }}</label>
                                <input class="form-control" type="text" name="city" value="{{ Auth::user()->city }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('home.address') }}</label>
                                <input class="form-control" type="text" name="address" value="{{ Auth::user()->address }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('home.zipcode') }}</label>
                                <input class="form-control" type="text" name="zipcode" value="{{ Auth::user()->zipcode }}">
                                <input type="hidden" name="currency" value="{{ Session::get('currency') }}"/>
                            </div>
                        </div>
                    </div>
                    <h5 class="box-title"> {{ __('account.checkout_1') }} </h5>
                    <div class="plan-selection pt-4">
                        <div class="plan-data">
                            <input id="modepay" name="modepay" type="radio" class="with-font" value="visa" />
                            <label> {{ __('account.checkout_2') }}  </label>
                        </div>
                        <div class="plan-data">
                            <input id="modepay" name="modepay" type="radio" class="with-font" checked value="paypal" />
                            <label> {{ __('account.checkout_3') }}  </label>
                        </div>
                        {{ csrf_field() }}
                    </div>
                </div>
                <div class="widget py-3 shadow">
                    <button type="submit" name="submit" class="btn btn-secondary btn-block"> {{ __('account.checkout_4') }} </button>
                </div>
            </form>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <h4 class="textcolor">{{ __('account.cart_list') }}</h4>
            <div class="shadow">
                <ul class="list-group widget">
                    <?php $priceTotal = 0; $priceTotalsar=0;?>
                    @foreach(Cart::instance('collectandship')->content() as $item)
                    @if(($item->options->order_type != "shipping") && ($item->options->order_type != "ship_direct"))
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                      <div>
                        <h6 class="my-0"> {{ $item->name }}</h6>
                        <small class="text-muted">
                            @if($item->options->order_type == "purchase")
                               {{ __('account.checkout_5') }}
                            @elseif($item->options->order_type == "shipping")
                                 {{ __('account.checkout_6') }}
                            @elseif($item->options->order_type == "prd_service")
                               {{ __('account.checkout_7') }}
                            @elseif($item->options->order_type == "return_prd")
                                {{ __('account.checkout_8') }}
                            @elseif($item->options->order_type == "ship_direct")
                                {{ __('home.directship') }}
                            @endif
                        </small>
                      </div>
                        <span class="text-muted">
                            @if(Session::get('currency') == "$")
                                @if($item->options->currency !='$')
                                    {{ __('account.dollar') }}{{ number_format($item->price * 0.266667,2,'.','') }}
                                    <?php $price = $item->price * 0.266667; ?>
                                @else
                                {{ __('account.dollar') }}{{ number_format($item->price,2,'.','') }}
                                    <?php $price = $item->price; ?>
                                @endif
                                <?php $priceTotal += $price;?>
                            @else
                                @if($item->options->currency =='$')
                                {{ number_format($item->price * 3.75,2,'.','') }} {{ __('account.sar') }}
                                    <?php $price_sar = $item->price * 3.75; ?>
                                @else
                                {{ number_format($item->price,2,'.','') }} {{ __('account.sar') }}
                                    <?php $price_sar = $item->price; ?>
                                @endif
                                <?php $priceTotal += $price_sar;?>
                            @endif
                        </span>
                        <small class="text-muted">
                            @if($item->options->order_type != "prd_service")
                                <a href="{{ url('destroy',$item->rowId)}}"><i class="far fa-trash-alt" style="font-size: 20px; color: red;"></i></a>
                            @else
                            <a href="{{ url('archive')}}"><i class="fas fa-info fa-2x"></i></a>
                            @endif
                        </small>
                    </li>
                    @else
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                          <h6 class="my-0"> {{ $item->name }}</h6>
                          <small class="text-muted">
                            @if($item->options->order_type == "shipping")
                                    {{ __('account.checkout_6') }}
                            @elseif($item->options->order_type == "prd_service")
                                {{ __('account.checkout_7') }}
                            @endif
                          </small>
                        </div>
                        <small class="text-muted">
                            <a href="{{ url('destroy',$item->rowId)}}">
                                <i class="far fa-trash-alt" style="font-size: 20px; color: red;"></i>
                            </a>
                        </small>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                          <small class="text-muted">
                            {{ __('account.shipping_amount') }}
                          </small>
                        </div>
                        <span class="text-muted">
                            @if(Session::get('currency') == "$")
                                @if($item->options->currency !='$')
                                    {{ __('account.dollar') }}{{ number_format($item->price * 0.266667,2,'.','') }}
                                    <?php $price = ($item->price + $item->options->servicecost) * 0.266667; ?>
                                @else
                                {{ __('account.dollar') }}{{ number_format($item->price,2,'.','') }}
                                    <?php $price = $item->price + $item->options->servicecost; ?>
                                @endif
                                <?php $priceTotal += $price;?>
                            @else
                                @if($item->options->currency =='$')
                                {{ number_format($item->price * 3.75,2,'.','') }} {{ __('account.sar') }}
                                    <?php $price_sar = ($item->price + $item->options->servicecost) * 3.75; ?>
                                @else
                                {{ number_format($item->price,2,'.','') }} {{ __('account.sar') }}
                                    <?php $price_sar = $item->price + $item->options->servicecost; ?>
                                @endif
                                <?php $priceTotal += $price_sar;?>
                            @endif
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                          <small class="text-muted">
                            {{ __('account.ship_moreservice')}}
                          </small>
                        </div>
                        <span class="text-muted">
                            @if(Session::get('currency') == "$")
                                {{ $item->options->servicecost }}{{ __('account.dollar')}}
                            @else
                                {{ number_format($item->options->servicecost * 3.75,2,'.','') }} {{ __('account.sar') }}
                            @endif
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div style="width: 80%;">
                            @if($item->options->product_price >= 100)
                                <?php
                                    if($item->options->product_price * 0.02 < 20 ){
                                        $assurance = 20 ;
                                    }else{
                                        $assurance = $item->options->product_price * 0.02 ;
                                    }
                                ?>
                            @else
                                <?php $assurance = 0;?>
                            @endif
                            <small style="white-space: pre;font-size: 70%;"> {{ __('account.assurance_desc')}} </small>
                            <small class="text-muted">
                                <input type="checkbox" @if($item->options->assurance == "true") checked @endif class="assurance" data="{{ $item->rowId }}" name="assurance" value="@if(Session::get('currency') == "$")<?=$assurance?>@else{{ number_format($assurance * 3.75,2,',','') }}@endif"/>  {{ __('account.assurance_title')}}
                            </small>
                        </div>
                        <span class="text-muted" style="padding-top:6%;">
                            @if(Session::get('currency') == "$")
                                <?=$assurance?>{{ Session::get('currency') }}
                            @else
                                {{ number_format($assurance * 3.75,2,'.','') }}{{ Session::get('currency') }}
                            @endif
                        </span>
                        <?php $tot = 0; ?>
                        @if($item->options->assurance == "true")
                            <?php
                                $tot += $assurance;
                                $priceTotal = $priceTotal + $tot;
                            ?>
                        @endif
                    </li>
                    @endif
                    @endforeach
                    <li class="list-group-item d-flex justify-content-between">
                      <span>  {{ __('account.checkout_9') }}  </span>
                      <input type="hidden" class="inputtotal" value="<?=number_format($priceTotal,2,'.','')?>"/>
                      <strong> <span class="ptotal"><?=number_format($priceTotal,2,'.','')?></span>{{ Session::get('currency') }}</strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->
<div id="myModalSuccessPay" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="fas fa-check"></i>
				</div>
				<h4 class="modal-title"> {{ __('account.checkout_12') }}  </h4>
			</div>
			<div class="modal-body">
				<p class="text-center">{{ __('account.checkout_13') }}</p>
			</div>
			<div class="modal-footer">
                <a href="{{url('account')}}" class="btn btn-success btn-block">OK</a>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.modal-confirm {
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px;
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}
	.modal-confirm .icon-box {
		color: #fff;
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		background: #82ce34;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm .icon-box i {
		font-size: 58px;
		position: relative;
		top: 3px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #82ce34;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
	.modal-confirm .btn:hover, .modal-confirm .btn:focus {
		background: #6fb32b;
		outline: none;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>
@endsection
