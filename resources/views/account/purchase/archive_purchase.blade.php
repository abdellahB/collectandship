@extends('layouts.account')

@section('content')
<style>
    .pagination{
        padding-top: 15px;
    }
    @media
     only screen and (max-width: 760px),
     (min-device-width: 768px) and (max-device-width: 1024px)  {
     table {
       border: 0;
     }

     table caption {
       font-size: 1.3em;
     }

     table thead {
       border: none;
       clip: rect(0 0 0 0);
       height: 1px;
       margin: -1px;
       overflow: hidden;
       padding: 0;
       position: absolute;
       width: 1px;
     }

     table tr {
       border-bottom: 3px solid #ddd;
       display: block;
       margin-bottom: .625em;
     }

     table td {
       border-bottom: 1px solid #ddd;
       display: block;
       font-size: .8em;
       text-align: left;
     }

     table td::before {
       /*
       * aria-label has no advantage, it won't be read inside a table
       content: attr(aria-label);
      */
       content: attr(data-label);
       float: right;
       font-weight: bold;
       text-transform: uppercase;
     }

     table td:last-child {
       border-bottom: 0;
     }
     .tds{
         width: 100% !important;
     }
   }
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home') }} / </span> {{ __('account.purchase_archive') }} </span>
        </div>
        @if(count($waitings)>0)
        @foreach ($waitings as $waiting)
        <div class="row box-body">
            <h4 class="pu_title">
                <span class="col-md-4"> {{ __('account.purchase_1') }} : {{$waiting->code_order}} </span>
                <span class="col-md-4">{{ __('account.purchase_2') }}   : {{$waiting->created_at->format('Y-m-d')}} </span>
            </h4>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mbw">
                        <table class="table tablers">
                            <thead>
                                <tr>
                                    <th style="width:20%"> {{ __('account.purchase_3') }}  </th>
                                    <th> {{ __('account.purchase_4') }} </th>
									<th> {{ __('account.purchase_5') }} </th>
                                    <th> {{ __('account.purchase_6') }} </th>
                                    <th>{{__('account.purchase_tax')}}</th>
                                    <th>{{__('account.purchase_ship')}}</th>
                                    <th> {{ __('account.commission')}}</th>
                                    <th style="width:20%"> {{ __('account.purchase_7') }} </th>
                                    <th> {{ __('account.purchase_8') }} </th>
                                    <th> {{ __('account.purchase_9') }}  </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($waiting->getOrders as $order )
                                    <tr>
                                        <td data-label="{{ __('account.purchase_3') }}">{{ $order->product_name}}</td>
                                        <td data-label="{{ __('account.purchase_4') }}"><a href="{{$order->product_url}}" target="_blank"> {{ __('account.purchase_10') }} </a></td>
                                        <td data-label="{{ __('account.purchase_5') }}">{{ $order->product_qty}}</td>
                                        <td data-label="{{ __('account.purchase_6') }}">
                                            @if($order->currency == '$' && Session::get('currency') == "$")
                                                {{ $order->product_price }}{{ __('account.dollar') }}
                                            @elseif($order->currency == '$' && Session::get('currency') != "$")
                                                {{ number_format($order->product_price * 3.75,2,',','') }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') == "ريال")
                                                {{ $order->product_price }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') != "ريال")
                                                {{ number_format($order->product_price * 0.266667,2,',','') }}{{ __('account.dollar') }}
                                            @endif
                                        </td>
                                        <td data-label="{{ __('account.purchase_tax') }}">
                                            @if($order->currency == '$' && Session::get('currency') == "$")
                                                {{ $order->product_tax }}{{ __('account.dollar') }}
                                            @elseif($order->currency == '$' && Session::get('currency') != "$")
                                                {{ number_format($order->product_tax * 3.75,2,',','') }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') == "ريال")
                                                {{ $order->product_tax }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') != "ريال")
                                                {{ number_format($order->product_tax * 0.266667,2,',','') }}{{ __('account.dollar') }}
                                            @endif
                                        </td>
                                        <td data-label="{{ __('account.purchase_ship') }}">
                                            @if($order->currency == '$' && Session::get('currency') == "$")
                                                {{ $order->product_shipping }}{{ __('account.dollar') }}
                                            @elseif($order->currency == '$' && Session::get('currency') != "$")
                                                {{ number_format($order->product_shipping * 3.75,2,',','') }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') == "ريال")
                                                {{ $order->product_shipping }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') != "ريال")
                                                {{ number_format($order->product_shipping * 0.266667,2,',','') }}{{ __('account.dollar') }}
                                            @endif
                                        </td>
                                        <td data-label="{{ __('account.commission') }}">
                                            @if($order->currency == '$' && Session::get('currency') == "$")
                                                {{ $order->product_taux }}{{ __('account.dollar') }}
                                            @elseif($order->currency == '$' && Session::get('currency') != "$")
                                                {{ number_format($order->product_taux * 3.75,2,',','') }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') == "ريال")
                                                {{ $order->product_taux }} {{ __('account.sar') }}
                                            @elseif($order->currency == 'ريال' && Session::get('currency') != "ريال")
                                                {{ number_format($order->product_taux * 0.266667,2,',','') }}{{ __('account.dollar') }}
                                            @endif
                                        </td>
                                        <td data-label="{{ __('account.purchase_7') }}">{{ $order->product_des}}</td>
                                        <td data-label="{{ __('account.purchase_8') }}">
                                            @if($order->status == 3)
                                                <span class="badge badge-info"> {{ __('account.purchase_11') }}  </span>
                                            @elseif($order->status == 4)
                                                <span class="badge badge-success"> {{ __('account.purchase_12') }} </span>
                                            @elseif($order->status == 10)
                                                <span class='badge badge-danger'> {{ __('account.direct_15') }} </span>
                                            @endif
                                        </td>
                                        <td data-label="{{ __('account.purchase_9') }}">
                                            @if($order->status != 10)
                                                <a href="javascript:void()" id="{{ $order->id}}" class="btn-link showInfo">
                                                    {{ __('account.purchase_10') }}
                                                </a>
                                            @else
                                                <span class='badge badge-danger'> {{ __('account.direct_15') }} </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $currency= $order->currency; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer col-md-12 col-sm-12 col-xs-12 ">
                    	<hr>
                    	<div class="float-right">
                            <label>  {{ __('account.purchase_13') }}  : </label>
                            <span class="price_usd_val color-c">
                                @if($currency == '$' && Session::get('currency') == "$")
                                    {{ $waiting->price_total }}{{ __('account.dollar') }}
                                @elseif($currency == '$' && Session::get('currency') != "$")
                                    {{ number_format($waiting->price_total * 3.75,2,',','') }} {{ __('account.sar') }}
                                @elseif($currency == 'ريال' && Session::get('currency') == "ريال")
                                    {{ $waiting->price_total }} {{ __('account.sar') }}
                                @elseif($currency == 'ريال' && Session::get('currency') != "ريال")
                                    {{ number_format($waiting->price_total * 0.266667,2,',','') }}{{ __('account.dollar') }}
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end row -->
        @endforeach
        @else
        <div class="col-md-12 box-body pad-0">
        <div class="no-packagebox"><img src="{{ url('assets/user/img/dropbox.png')}}">
                <h1>
                <span> {{ __('account.purchase_14') }}</span>
                <span>

                </span>
                </h1>
            </div>
        </div>
        @endif
        <div class="row box-body">
            {{ $waitings->links() }}
        </div>
    </div>
</div>
@endsection
