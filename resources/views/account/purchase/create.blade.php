@extends('layouts.account')

@section('content')
<!--<style>
	@media
	only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {

		/* Force table to not be like tables anymore */
		table, thead, tbody, th, td, tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}

		tr { border-bottom: 1px solid #ccc; }

		td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-right: 50% !important;
		}

		td:after {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			top: 6px;
			right: 6px;
			width: 45%;
			padding-right: 10px;
			white-space: nowrap;
		}

		/*
		Label the data
		*/
		td:nth-of-type(1):after { content: "الشراء من"; }
		td:nth-of-type(2):after { content: "إسم المنتج و الرابط"; }
		td:nth-of-type(3):after { content: "الكمية"; }
		td:nth-of-type(4):after { content: "الوزن"; }
		td:nth-of-type(5):after { content: "السعر"; }
		td:nth-of-type(6):after { content: "مسح"; }
    }
</style>-->
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home') }} / </span> {{ __('account.purchase_create') }} </span>
        </div>
        @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
        <div class="row box-body">
            <h4>{{ __('account.purchase_basket') }}  </h4>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row py-3 ">
                    <div class="col-md-12">
                        <form method="POST" id="fastbuys" action="" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="default prepend-icon filled"> {{ __('account.purchase_product') }} </label>
                                        <input  placeholder="{{ __('account.purchase_product') }}" id="product" class="form-control" type="text" name="product" required="true">
                                    </div>
                                    <div class="form-group">
                                        <label class="default prepend-icon filled"> {{ __('account.purchase_product_url') }} </label>
                                        <input class="form-control" id="url" type="text" placeholder="{{ __('account.purchase_product_url') }}" name="url" required="true">
                                    </div>

                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="default prepend-icon filled"> {{ __('account.purchase_product_price') }} @if(Session::get('currency') == "$") {{ __('account.dollar')}} @else {{ __('account.sar')}} @endif </label>
                                        <input class="form-control allow_decimal" type="text" name="price" placeholder="{{ __('account.purchase_product_price') }} @if(Session::get('currency') == "$") {{ __('account.dollar')}} @else  {{ __('account.sar')}} @endif" id="price"  required="true">
                                    </div>
                                    <div class="form-group">
                                        <label class="default prepend-icon filled">{{ __('account.purchase_product_qty') }}</label>
                                        <input id="count" type="text" placeholder="{{ __('account.purchase_product_qty') }}" class="form-control allow_numeric" name="count" required="true"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label class="default prepend-icon filled">{{ __('account.purchase_product_info') }} </label>
                                        <textarea name="description" rows="4" id="description" class="form-control" placeholder="{{ __('account.purchase_product_info') }}"></textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="float-left">
                                        <div class="form-group">
                                            <button type="button" id="btnfastbuy" class="btn btn-secondary rounded-lg" name="submit">
                                               {{ __('account.purchase_product_add') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row box-body">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mbw">
                        <table class="table createwafirhali tablers shopping-section">
                            <thead>
                                <tr>
                                    <th style="width:20%"> {{ __('account.purchase_3')}} </th>
                                    <th style="width:15%">{{ __('account.purchase_4')}}</th>
                                    <th style="width:15%">{{ __('account.purchase_5')}}</th>
                                    <th style="width:15%">{{ __('account.purchase_6')}}</th>
                                    <th style="width:20%">{{ __('account.purchase_7')}}</th>
                                    <th style="width:10%">{{ __('account.purchase_15')}}</th>
                                </tr>
                            </thead>
                            <tbody class="product-list">
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer col-md-12 col-sm-12 col-xs-12 ">
                        <hr>
                        <div class="float-right">
                            <p class="">{{ __('account.purchase_13') }} :
                                <span class="price_usd_val color-c"> 0 </span>
                                <small class="color-c" style="font-size: 14px;">
                                    @if(Session::get('currency') == "$")
                                    {{ __('account.dollar') }}
                                    @else
                                    {{ __('account.sar') }}
                                    @endif
                                </small>
                            </p>
                        </div>
                        <div class="float-left">
                            <div class="form-group">
                                <button type="button" style="display:none" class="btn btn-secondary rounded-lg btn_submit_order" name="submit">
                                   {{ __('account.purchase_product_submit') }}
                                </button>
                                <input type="hidden" id="countprodact" value="0"/></div>
                                <input type="hidden" class="price_r" id="price_r" value="0">
                                <input type="hidden" class="currency" id="currency" value="{{Session::get('currency')}}">
                                <!--<input type="hidden" class="taux" id="taux" value="{{ $setting->taux_purchase/100 }}">-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end row -->
        <!--<div class="row box-body">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12 pt-4">
                        <div class="float-right">
                            <label>  السعر الأجمالي : </label>
                            <span class="price_usd_val color-c">0</span>
                            <small class="color-c" style="font-size: 14px;"> دولار </small>
                        </div>
                        <div class="float-left">
                            <div class="form-group">
                                <button type="button" class="btn btn-secondary rounded-lg btn_submit_order" name="submit">
                                    تقديم الطلب
                                </button>
                                <input type="hidden" id="countprodact" value="0"/></div>
                                <input type="hidden" class="price_r" id="price_r" value="0">
                                <input type="hidden" class="taux" id="taux" value="{{ $setting->taux_purchase/100 }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        @if($info_bottom)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_bottom->message !!}
                        @else
                            {!! $info_bottom->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
