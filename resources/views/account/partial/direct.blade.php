<?php $i=1;?>
@foreach ($companies as $companie)
    <tr class="{{ $companie->id }}">
        <td>
            <div class="customcontrol customcheckbox">
                <input type="radio" name="companyid" class="customcontrolinputs" value="{{ $companie->id }}">
            </div>
        </td>
        <td>
            <span>
                <img class="" style="width:80px;height:30px;" src="{{ url('upload/company')}}/{{$companie->companylogo}}"/>
            <span>
            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                {{$companie->companiename}}
            @else
                {{$companie->companiename_en}}
            @endif
        </td>
        <td>
            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                {{$companie->datedelevier}}
            @else
                {{$companie->datedelevier_en}}
            @endif
        </td>
        <td>
            {{$companie->pricecompany + 10 }}$
            <input type="hidden" name="pricetopay" class="pricetopay" value="{{ $companie->pricecompany + 10 }}">
        </td>
    </tr>
    <?php $i++ ; ?>
@endforeach
