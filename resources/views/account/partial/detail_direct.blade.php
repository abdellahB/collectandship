<?php $i=1;?>
@foreach ($orders as $order)
    @foreach ($order->getchild as $child)
        <tr class="{{ $order->id }}">
            <td data-label="#"> <?=$i?></td>
            <td data-label="{{ __('account.direct_orderN') }}"> {{ $order->code_order }} </td>
            <td data-label="{{ __('account.direct_orderp') }}"> {{ $child->weight }} {{ __('home.kg')}}</td>
            <td data-label="{{ __('account.direct_dimension') }}"> {{ $child->width }} * {{ $child->lenght }} * {{ $child->height }} {{ __('home.cm')}}</td>
            <td data-label="{{ __('account.direct_content') }}">{{ $child->content }}</td>
            <td data-label="{{ __('account.direct_value') }}">
                @if(Session::get('currency') == "$")
                    {{ $child->value }}$
                @else
                    {{ number_format($child->value * 3.75,2,'.','') }}{{ __('account.sar') }}
                @endif
            </td>
        </tr>
        <?php $i++ ; ?>
    @endforeach
@endforeach
