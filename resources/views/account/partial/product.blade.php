@foreach ($parcels as $parcel)
<tr class="{{$parcel->id}}">
    <td  style="vertical-align: middle;" scope="row">
        <div class="customcontrol customcheckbox">
            <input type="checkbox" name="parcelID[]" id="{{$parcel->moreimage}}" class="customcontrolinput" value="{{$parcel->id}}">
        </div>
    </td>
    <td>
        <span class="icon-with-child m-r">
            @if($parcel->moreimage == 1)
                <img  width="80" height="80" src="{{url('assets/user/img/processing.png')}}" alt="">
            @else
                @foreach ($parcel->GetImage as $image)
                    <a href="javascript:voide()" id="fancybox-manual-{{$parcel->id}}">
                        <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                    </a>
                    @break
                @endforeach
            @endif
        </span>
        <script>
            $(document).ready(function() {
                $("#fancybox-manual-{{$parcel->id}}").click(function() {
                    $.fancybox.open([
                        @foreach ($parcel->GetImage as $image)
                        {
                            href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                title : 'My title'
                            },
                            @endforeach
                    ]
                    );
                });
            });
        </script>
    </td>
    <td>
        <b> {{ __('account.parcel_code') }}  </b>   :{{ $parcel->codeparcel}} <br/>
        <b> {{ __('account.parcel_date') }} </b>  :{{ $parcel->date_arrival}} <br/>
        <b> {{ __('account.parcel_stock') }}      </b>  :{{ $parcel->stockcode }} <br/>
        <b> {{ __('account.parcel_country') }}</b>   :@if($parcel->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($parcel->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
    </td>
    <td>
        <b>{{ __('account.parcel_weight') }}</b>     : {{ $parcel->weight}} {{ __('home.kg') }} <br/>
        <b>{{ __('account.parcel_dimension') }}</b>  : {{ $parcel->lenght }} * {{ $parcel->width }} * {{ $parcel->height }} {{ __('home.cm') }} <br/>
        <b>{{ __('account.parcel_company') }} </b>   : {{ $parcel->company}} <br/>
        <b>{{ __('account.parcel_track') }}</b>      : {{ $parcel->tracking}} <br/>
    </td>
    <td style="width: 20%;">
       <b> {{ $parcel->sellername }}</b> <br/>
        {!! $parcel->description !!}
    </td>
    <td style="vertical-align: middle;width: 20%;">
        <input type="number" class="form-control product_price" placeholder=" {{ __('account.parcel_price') }} @if(Session::get('currency') == "$") دولار @else ريال @endif"/>
    </td>
</tr>
@endforeach
