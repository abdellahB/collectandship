<?php $i=1;?>
@foreach ($companies as $companie)
    <tr>
        <td>
            <span>
                <img style="width: 100%; height: 100%; border: 0; max-width: 80px;max-height:60px;" src="{{ url('upload/company')}}/{{$companie->companylogo}}"/>
                @if( LaravelLocalization::getCurrentLocale() == 'ar')
                    {{$companie->companiename}}
                @else
                    {{$companie->companiename_en}}
                @endif
            <span>
        </td>
        <!--<td>{{$weight}}kg</td>
        <td>{{$dimenssion}} kg </td>
        <td>{{$companie->weightcompany}}kg</td>-->
        <td>
            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                {{$companie->datedelevier}}
            @else
                {{$companie->datedelevier_en}}
            @endif
        </td>
        <td>
            @if(Session::get('currency') == "$")
                {{ $companie->pricecompany }}{{ __('account.dollar')}}
            @else
                {{ number_format($companie->pricecompany * 3.75,2,'.','') }} {{ __('account.sar') }}
            @endif
        </td>
    </tr>
    <?php $i++ ; ?>
@endforeach
