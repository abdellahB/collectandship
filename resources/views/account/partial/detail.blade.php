@foreach ($transaction as $transact )
    @foreach ($transact->getDetails as $detail)
        <tr id="{{ $transact->id }}">
            <td data-label="{{ __('account.modal_1') }}" class="maw-320">
                @if($detail->service_name == "purchase")
                   {{ __('account.menu_14') }}
                @elseif($detail->service_name == "shipping")
                   {{ __('account.menu_1') }}
                @elseif($detail->service_name == "prd_service")
                   {{ __('account.menu_6') }}
                @elseif($detail->service_name == "return_prd")
                  {{ __('account.menu_10') }}
                @elseif($detail->service_name == "ship_direct")
                   {{ __('account.menu_20') }}
                @endif
            </td>
            <td data-label="{{ __('account.modal_2') }}" class="maw-320">
                {{ $detail->code_order }}
            </td>
            <td data-label="{{ __('account.modal_3') }}" class="maw-320">
                {{ $detail->order_price }}{{ $detail->currency }}
            </td>
            <td data-label="{{ __('account.modal_4') }}" class="maw-320">
                {{ $transact->transaction_code }}
            </td>
            <td {{ __('account.modal_5') }}" class="maw-320">
                {{ $transact->created_at->format('Y-m-d') }}
            </td>
            <td data-label="-" class="maw-320">
                @if($detail->status == 1)
                    <span class='label label-success'> {{ __('account.payed') }} </span>
                @endif
            </td>
        </tr>
    @endforeach
@endforeach
