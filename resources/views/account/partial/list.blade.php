@foreach ($packings as $packing)
    @foreach ($packing->child_packing as $child)
        <tr class="{{$child->Shipping->id}}">
            <td>
                <span class="icon-with-child m-r">
                    @foreach ($child->Shipping->GetImage as $image)
                        <a href="javascript:voide()" id="fancybox-manual-{{$child->Shipping->id}}">
                            <img  width="80" height="80" src="{{url('upload/ship')}}/{{ $image->imagename}}" alt="">
                        </a>
                        @break
                    @endforeach
                </span>
                <script>
                    $(document).ready(function() {
                        $("#fancybox-manual-{{$child->Shipping->id}}").click(function() {
                            $.fancybox.open([
                                @foreach ($child->Shipping->GetImage as $image)
                                    {
                                        href : '{{url('upload/ship')}}/{{ $image->imagename}}',
                                        title : 'My title'
                                    },
                                 @endforeach
                            ]
                            );
                        });
                    });
                </script>
            </td>
            <td> {{ $packing->code_parcel }}</td>
            <td> {{ $child->Shipping->codeparcel }}</td>
            <td>
                <b>{{ __('account.parcel_date') }} </b>  : {{ $child->Shipping->date_arrival}} <br/>
                <b> {{ __('account.parcel_stock') }} </b>  :{{ $child->Shipping->stockcode }} <br/>
                <b> {{ __('account.parcel_country') }} </b>   : @if($child->Shipping->shipcountry=="usa") <b class="ware">{{ __('account.usa') }}</b> @elseif($child->Shipping->shipcountry=="sa") <b class="ware">{{ __('account.sa') }}</b> @else  <b class="ware">{{ __('account.uae') }}</b> @endif
            </td>
            <td>
                <b>{{ __('account.parcel_weight') }}</b>      : {{ $child->Shipping->weight}} {{ __('home.kg') }} <br/>
                <b>{{ __('account.parcel_dimension') }}</b>     : {{ $child->Shipping->lenght }} * {{ $child->Shipping->width }} * {{ $child->Shipping->height }} {{ __('home.cm') }} <br/>
                <b>{{ __('account.parcel_track') }}</b> : {{ $child->Shipping->tracking}} <br/>
            </td>
            <td>
                <b> {{ $child->Shipping->sellername }}</b> <br/>
                {!! $child->Shipping->description !!}
            </td>
        </tr>
    @endforeach
@endforeach
