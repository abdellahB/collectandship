<div class="modal fade" id="myModalNewAddress" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center" style="display: block;">
               {{ __('account.new_address') }}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                <!-- Begin # Login Form -->
                <form class="form-bg gradient-bg p-4 new_address" _lpchecked="1">
                    <div class="regestererreur"></div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="firstname" required="true" placeholder="{{ __('home.firstname') }}" class="form-control shadow-control">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="lastname" required="true" placeholder="{{ __('home.lastname') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="text" name="address" required="true" placeholder="{{ __('home.address') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="text" name="province" required="true" placeholder="المحافطة" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <select name="countrie_id" class="form-control shadow-control">
                                <option value="">{{ __('home.countrie') }}</option>
                                @if(count($countries)>0)
                                    @foreach ($countries as $countrie)
                                        <option value="{{ $countrie->id }}">
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $countrie->countriename_ar }}
                                            @else
                                                {{ $countrie->countriename }}
                                            @endif
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="city" required="true" placeholder="{{ __('home.city') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="zip_code" required="true" placeholder="{{ __('home.zipcode') }}" class="form-control shadow-control">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="phone" required="true" placeholder="{{ __('home.phone') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-8 m-auto">
                            <a class="btn btn-secondary btn-lg btn-block rounded-lg newaddress" href="javascript:void()">
                                 {{ __('account.add_new_address') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>
<div id="myModalShowProduct" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> {{ __('account.parcel') }} </th>
                        <th scope="col">{{ __('account.parcel_code') }} </th>
                        <th scope="col">{{ __('account.parcel_prd') }}</th>
                        <th scope="col">{{ __('account.parcel') }}</th>
                        <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}</th>
                        <th scope="col">{{ __('account.parcel_info') }}</th>
                    </tr>
                </thead>
                <tbody class="getlist_s">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<div id="myModalShowProduct_2" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> {{ __('account.parcel_img') }}	 </th>
                        <th scope="col">{{ __('account.parcel') }}</th>
                        <th scope="col">{{ __('account.parcel_weight') }} / {{ __('account.parcel_dimension') }}	</th>
                        <th scope="col">{{ __('account.parcel_info') }}</th>
                    </tr>
                </thead>
                <tbody class="getlist_prods">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div class="modal fade" id="myModalChangeInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    {{ __('account.shipping_change') }}
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body more_changeInfo">

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalMoreInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                      {{ __('account.shipping_moreinfo') }}
                </h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body more_Info_detail">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalPackingReturn" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="trackings" role="form">
                    <div class="form-group">
                        <label for="seller_address"> عنوان البائع كامل </label>
                        <input type="hidden" id="product_id" name="product_id" value="">
                        <input type="text" class="form-control" required="true" name="seller_address" id="seller_address" placeholder="عنوان البائع كامل"/>
                    </div>
                    <div class="form-group">
                        <label for="return_info"> سبب ارجاع المنتج</label>
                        <textarea class="form-control" required="true" name="return_info" id="return_info" placeholder="سبب ارجاع المنتج"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_return">
                  ارسال طلب ارجاع المنتج
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProductMore" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <form class="trackings" role="form">
                    <div class="form-group">
                        <table class="table table-bordered shipping">
                            <thead class="panel-heading">
                                <tr>
                                    <th scope="col"> نوع الخدمة </th>
                                    <th scope="col"> التكلفة </th>
                                    <th scope="col"> وصف الخدمة </th>
                                </tr>
                            </thead>
                            <tbody class="get_service">

                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="return_info"> ملاحظات عن الخدمة </label>
                        <textarea class="form-control" required="true" name="return_info" id="return_info" placeholder="ملاحظات عن الخدمة"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_order">
                 تقديم الطلب
                </button>
            </div>
        </div>
    </div>
</div>

<div id="myModalShowDirectShip" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>

                        <th scope="col"> # </th>
                        <th scope="col"> {{ __('account.direct_orderN') }} </th>
                        <th scope="col">{{ __('account.direct_orderp') }} </th>
                        <th scope="col">{{ __('account.direct_dimension') }} </th>
                        <th scope="col">{{ __('account.direct_content') }}</th>
                        <th scope="col">{{ __('account.direct_value') }}</th>
                    </tr>
                </thead>
                <tbody class="getlist_directShip">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<div id="myModalShowDetail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body getlist">
            <table class="table table-bordered shipping">
                <thead class="panel-heading">
                    <tr>
                        <th scope="col"> {{ __('account.modal_1') }}  </th>
                        <th scope="col"> {{ __('account.modal_2') }}  </th>
                        <th scope="col"> {{ __('account.modal_3') }} </th>
                        <th scope="col"> {{ __('account.modal_4') }}</th>
                        <th scope="col"> {{ __('account.modal_4') }} </th>
                        <th scope="col"> {{ __('account.modal_5') }}</th>
                    </tr>
                </thead>
                <tbody class="getlist_details">

                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="myModalNotification" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Body -->
            <div class="modal-body text-center">
                <h4 class="loginerreur error_price" style="diplay:none">
                    {{ __('account.error_price') }}
                </h4>
                <h4 class="loginerreur error_product" style="diplay:none">
                    {{ __('account.error_product') }}
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalNotification_packing" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Body -->
            <div class="modal-body text-center">
                <h4 class="loginerreur error_company" style="diplay:none">
                    {{ __('account.error_company')}}
                </h4>
                <h4 class="loginerreur error_packing" style="diplay:none">
                    {{ __('account.error_packing')}}
                </h4>
                <h4 class="loginerreur error_service" style="diplay:none">
                    {{ __('account.error_service')}}
                </h4>
                <h4 class="loginerreur error_terms" style="diplay:none">
                    {{ __('account.error_terms')}}
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalNotification_service" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Body -->
            <div class="modal-body text-center">
                <h4 class="loginerreur error_prd_service" style="diplay:none">
                    {{ __('account.error_prd_service')}}
                </h4>
                <h4 class="loginerreur error_prd_select" style="diplay:none">
                    {{ __('account.error_prd_select')}}
                </h4>
                <h4 class="loginerreur error_prd_return" style="diplay:none">
                    {{ __('account.error_prd_return')}}
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="myModalShowTracking" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body showData">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
        </div>
      </div>
    </div>
</div>
