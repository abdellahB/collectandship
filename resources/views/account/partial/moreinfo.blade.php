<div class="form-group">
    <label for="ship_weight">{{ __('account.parcel_weight') }}  </label>
    <input type="text" class="form-control" name="ship_weight" required="true" value="{{ $packing->weight}} {{__('home.kg')}}" readonly/>
</div>
<div class="form-group">
    <label for="ship_dimension"> {{ __('account.parcel_dimension') }} </label>
    <input type="text" class="form-control" name="ship_dimension" required="true" value="{{ $packing->length}} * {{ $packing->width}} * {{ $packing->height}} {{__('home.cm')}}" readonly/>
</div>
<div class="form-group">
    <label for="ship_price"> {{ __('account.shipping_amount') }} </label>
    <input type="text" class="form-control" name="ship_price" required="true" value="{{ $packing->pricetotal}}$" readonly />
</div>
