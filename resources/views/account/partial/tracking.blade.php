<div class="form-group">
    <label for="companyurl"> {{ __('account.direct_10')}} </label>
    <input type="text" class="form-control" name="companyurl" readonly id="companyurl" value="{{ $tracking->companyurl}}"/>
</div>
<div class="form-group">
    <label for="trackinginfo"> {{ __('account.parcel_track')}}  </label>
    <input type="text" class="form-control" name="trackinginfo" readonly id="trackinginfo" value="{{ $tracking->trackinginfo }}"/>
</div>
