<form class="formPack_update">
    <div class="form-group">
        <label for="length"> {{ __('account.shipping_address') }}  </label>
        <input type="text" class="form-control" name="shi_address" readonly value="{{ $packing->getAddress->adress}} | {{ $packing->getAddress->getcountry->countriename}} "/>
    </div>
    <div class="form-group">
        <label for="width"> {{ __('account.shipping_mode') }}  </label>
        <input type="hidden" value="{{ $packing->id }}" name="packing_id" class="packing_id">
        <select class="form-control mode_shipping" name="mode_shipping" required="true">
            @foreach ($companies as $companie)
                <option value="{{ $companie->id }}" @if($companie->id == $packing->companie_id) selected @endif>
                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                        {{ $companie->companiename }}
                    @else
                        {{ $companie->companiename_en }}
                    @endif
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="height"> {{ __('account.shipping_time') }}</label>
        <input type="text" class="form-control ship_time" name="ship_time" readonly value=" @if( LaravelLocalization::getCurrentLocale() == 'ar') {{ $packing->getCompany->datedelevier}} @else {{ $packing->getCompany->datedelevier_en}} @endif" />
    </div>
    <div class="form-group">
        <?php $servicecost = 0; ?>
        @foreach ($packing->getchild as $child)
        <?php $servicecost = $servicecost + $child->getService->service_price;?>
        @endforeach
        <label for="height"> {{ __('account.shipping_amount') }}   </label>
        <input type="text" class="form-control ship_price" readonly name="ship_price" value="@if(Session::get('currency') == "$"){{ $packing->pricetotal - $servicecost }}{{ __('account.dollar')}}@else{{ number_format($packing->pricetotal - $servicecost * 3.75,2,',','') }}{{ __('account.sar') }}@endif" />
    </div>
    @csrf
    <div class="form-group">
        <button type="button" name="submit" class="btn btn-secondary btn-block update_pack_price">
          {{ __('account.shipping_change') }}
        </button>
    </div>
</form>
