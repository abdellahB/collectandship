@extends('layouts.account')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home') }} / </span> {{ __('account.menu_23') }}  </span>
            </div>
            <div class="box-body row">
                <h4>{{ __('account.menu_23_1') }}</h4>
                  <div class="col-md-12 col-sm-12 col-xs-12 py-2">
                    @if ($message = Session::get('success'))
                        <div class="alert-success success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($messages = Session::get('errors'))
                        <div class="alert-danger danger">
                            <p>{{ $messages }}</p>
                        </div>
                    @endif
                    <form method="POST" action="{{ LaravelLocalization::localizeUrl('/profile') }}" enctype="multipart/form-data">
                        <div class="row py-2">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label> {{ __('home.firstname') }} </label>
                                    <input class="form-control" type="text" name="firstname" value="{{ Auth::user()->firstname }}">
                                </div>
                                @csrf
                                <div class="form-group">
                                    <label> {{ __('home.lastname') }} </label>
                                    <input class="form-control" type="text" name="lastname" value="{{ Auth::user()->lastname }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.email') }}</label>
                                    <input class="form-control" type="text" name="email" value="{{ Auth::user()->email }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.phone') }} </label>
                                    <input class="form-control" type="text" name="phone"  value="{{ Auth::user()->phone }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.password') }} </label>
                                    <input class="form-control" type="password" placeholder="{{ __('home.password') }}" name="password">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.comfirmpassword') }} </label>
                                    <input class="form-control" type="password" placeholder="{{ __('home.comfirmpassword') }}" name="password_confirmation">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="newsletter" value="1" />
                                   <input class="formcontrol" type="checkbox" value="0" @if(Auth::user()->newsletter == 0) checked @endif name="newsletter"> {{ __('account.newsletter')}}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>{{ __('account.countrie') }} </label>
                                    <select type="text" name="countrie_id" class="form-control">
                                        <option>{{ __('home.countrie') }} </option>
                                        @if(count($countries)>0)
                                            @foreach ($countries as $countrie)
                                                <option value="{{$countrie->id}}" @if(Auth::user()->countrie_id == $countrie->id ) selected @endif >
                                                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                        {{ $countrie->countriename_ar }}
                                                    @else
                                                        {{ $countrie->countriename }}
                                                    @endif
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.city') }}</label>
                                    <input class="form-control" type="text" placeholder="{{ __('home.city') }}" name="city" value="{{ Auth::user()->city }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.address') }}</label>
                                    <input class="form-control" type="text" placeholder="{{ __('home.address') }}" name="address" value="{{ Auth::user()->address }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('home.zipcode') }}</label>
                                    <input class="form-control" type="text" placeholder="{{ __('home.zipcode') }}" name="zipcode" value="{{ Auth::user()->zipcode }}">
                                </div>
                                <div class="form-group">
                                    <label> {{ __('account.notify_lang') }}</label>
                                    <select type="text" name="lang" class="form-control">
                                        <option value="ar" @if(Auth::user()->lang == "ar") selected @endif> العربية</option>
                                        <option value="en" @if(Auth::user()->lang == "en") selected @endif> English</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label> {{ __('account.photo') }} </label>
                                    <input class="form-control" type="file" name="thumbnail">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
								<div class="pull-left">
									<div class="form-group">
										<button type="submit" class="btn btn-secondary btn-block rounded-lg" name="submit">
                                            {{ __('account.editdate') }}
                                        </button>
									</div>
								</div>
							</div>
                        </div>
                    </form>
                  </div>
              </div>
        </div>
    </div>
@endsection
