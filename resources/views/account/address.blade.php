@extends('layouts.account')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <span class="sitmap"><i class="fas fa-home"></i> <span> {{ __('home.home') }} / </span> {{ __('account.bookaddress') }} </span>
        </div>
        @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
        <div class="row box-body">
            <h4> {{ __('account.bookaddress') }} </h4>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row py-3 ">
                    <div class="col-md-12">
                        <div class="float-left">
                            <a href="javascript:void()" data-toggle="modal" data-target="#myModalNewAddress" class="btn btn-secondary">
                                {{ __('account.add_new_address') }}
                            </a>
                        </div>
                    </div>
                </div> <!--end row -->
                <div class="row py-3 ">
                    @if(count($newaddress) > 0)
                    @foreach($newaddress AS $newaddress)
                    <div class="col-md-4 address myaddress py-3">
                        <div class="shadow py-2 col_bg">
                            <ul class="list-group">
                                <li>
                                    <span class="control-field">
                                       {{ __('account.fullname') }} :
                                        <b>{{ $newaddress->firstname }} {{ $newaddress->lastname}}</b>
                                    </span>
                                </li>
                                <li>
                                    <span class="control-field">
                                    {{ __('home.address') }} :
                                    <b>{{$newaddress->adress}}</b>
                                    </span>
                                </li>
                                <li>
                                    <span class="control-field">
                                        {{ __('account.address_state') }} :
                                    <b>{{ $newaddress->province}}</b>
                                    </span>
                                </li>
                                <li>
                                    <span class="control-field">
                                        {{ __('home.city') }} :
                                    <b>{{$newaddress->city}}</b>
                                    </span>
                                </li>
                                    <li>
                                    <span class="control-field"> {{ __('account.countrie') }} :
                                    <b> {{$newaddress->getcountry->countriename}}</b>
                                    </span>
                                </li>
                                <li>
                                    <span class="control-field">{{ __('home.zipcode') }}:
                                    <b>{{$newaddress->zip_code}}</b>
                                    </span>
                                </li>
                                <li>
                                    <span class="control-field">
                                   {{ __('home.phone') }} :
                                    <b> {{$newaddress->phone}} </b>
                                    </span>
                                </li>
                                <li class="text-center py-3">
                                    <a href="{{url('newaddress',$newaddress->id)}}" class="btn btn-danger btnblock rounded-lg">
                                         {{ __('account.address_remove') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
        @if($info_bottom)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_bottom->message !!}
                        @else
                            {!! $info_bottom->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
