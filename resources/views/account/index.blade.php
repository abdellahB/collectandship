@extends('layouts.account')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <span class="sitmap"><i class="fas fa-home"></i> <span> / </span> {{ __('home.home') }} </span>
            </div>
            @if($info_top)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_top->message !!}
                        @else
                            {!! $info_top->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
            @endif
            <div class="box-body row">
                <div class="col-md-12 col-sm-12 col-xs-12 profiles">
                    <div class="profile-green">
                    <img class="imagprof" src="@if(Auth::user()->thumbnail) {{ url('upload/users')}}/{{ Auth::user()->thumbnail }}@else {{ url('assets/user/img/avatar.png')}} @endif">
                        <p class="name">{{ __('account.mydata') }}</p>
                    </div>
                    <div class="col-md-12">
                        <ul>
                            <li>
                                <p class="cda"> {{ __('account.fullname') }}</p>
                                <span>{{ Auth::user()->firstname }} {{ Auth::user()->lastname}}</span>
                            </li>
                            <li>
                                <p class="cda">{{ __('account.accounrcode') }} </p>
                                <span>{{Auth::user()->policia}}</span>
                            </li>
                            <li>
                                <p class="cda">{{ __('account.memberdate') }} </p>
                                <span>{{Auth::user()->created_at->format('Y-m-d')}}</span>
                            </li>
                            <li>
                                <p class="cda"> {{ __('account.countrie') }} </p>
                                <span id="earnadvence" class="text-center">
                                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                        {{Auth::user()->getcountry->countriename_ar }}
                                    @else
                                        {{Auth::user()->getcountry->countriename}}
                                    @endif
                                </span>
                            </li>
                            <li class="float-left py-3">
                                <a href="{{ url('profile') }}" class="btn btn-secondary"> {{ __('account.editdate') }}  </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row box-body">
                <h4>{{ __('account.addresshipping') }}</h4>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row py-3 address">
                        @foreach($address as $oneaddre)
                        <div class="col-md-4">
                            <h4>
                                @if($oneaddre->country == "sa")
                                    {{ __('account.address_sa') }}
                                @elseif($oneaddre->country == "usa")
                                    {{ __('account.address_usa') }}
                                @else
                                    {{ __('account.address_uae') }}
                                @endif
                            </h4>
                            <ul class="list-group">
                                <li>
                                    <span class="control-field">
                                        Full name:
                                        <b>{{ Auth::user()->firstname }} {{ Auth::user()->lastname}}</b>
                                    </span>
                                </li>
                                <li>
                                <span class="control-field">
                                Adress line 1 :
                                    <b>{{ $oneaddre->Adressline }}</b>
                                </span>
                                </li>
                                <li>
                                    <span class="control-field">
                                    Line 2 :
                                    <b>{{Auth::user()->policia}} </b>
                                </span>
                                    </li>
                                    <li>
                                        <span class="control-field">
                                City :
                                <b>{{ $oneaddre->city }}</b>
                                </span>
                                    </li>
                                    <li>
                                        <span class="control-field">State :
                                <b>{{ $oneaddre->state }}</b>
                                </span>
                                    </li>
                                    <li>
                                        <span class="control-field">Post/Zipe code :
                                <b>{{ $oneaddre->zipcode }}</b>
                                </span>
                                    </li>
                                    <li>
                                        <span class="control-field">
                                Country : <b>

                                    @if($oneaddre->country == 'usa')
                                        United States
                                    @elseif($oneaddre->country == 'sa')
                                        Saudi Arabia
                                    @else
                                        United Arab Emirates
                                    @endif
                                </b>
                                </span>
                                    </li>
                                    <li>
                                        <span class="control-field">
                                        Mobile :
                                        <b> {{ $oneaddre->mobile }}</b>
                                        </span>
                                    </li>
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="box-body row">
                <h4> {{ __('home.pricing_title') }} </h4>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row py-3">
                        <div class="col-md-12">
                            <div class="row form-group">
                                <label class="col-md-4 col-sm-4 col-4">{{ __('home.pricing_1') }}</label>
                                <div class="col-md-8 col-sm-8 col-8">
                                  <label class="radio-inline">
                                    <input type="radio" name="weight_measure" id="lbs_measure" value="lbs"> {{ __('home.pricing_2') }}
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="weight_measure" checked id="kg_measure" value="kg"> {{ __('home.pricing_3') }}
                                  </label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-4 col-4">
                                    <input type="number" name="length" placeholder="{{ __('home.pricing_4') }}" class="form-control length allow_decimal">
                                </div>
                                <div class="col-md-4 col-sm-4 col-4">
                                    <input type="number" name="width" placeholder="{{ __('home.pricing_5') }}" class="form-control width allow_decimal">
                                </div>
                                <div class="col-md-4 col-sm-4 col-4">
                                    <input type="number" name="height" placeholder="{{ __('home.pricing_6') }}" class="form-control height allow_decimal">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6 col-sm-12 col-12 form-group">
                                    <select name="ship_country" class="form-control ship_country">
                                        <option value=""> {{ __('home.source_country') }} </option>
                                        @if(count($address)>0)
                                            @foreach ( $address as $address )
                                                <option value="{{ $address->country }}">
                                                    @if($address->country == 'usa')
                                                        {{ __('account.usa') }}
                                                    @elseif($address->country == 'sa')
                                                        {{ __('account.sa') }}
                                                    @else
                                                        {{ __('account.uae') }}
                                                    @endif
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12 col-12 form-group">
                                    <select type="text" name="countrie_id" class="form-control countrie_id">
                                        <option> {{ __('home.pricing_7') }} </option>
                                        @if(count($countries)>0)
                                            @foreach ($countries as $countrie)
                                                <option value="{{$countrie->id}}">
                                                    @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                        {{ $countrie->countriename_ar }}
                                                    @else
                                                        {{ $countrie->countriename }}
                                                    @endif
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12 col-sm-12 col-12">
                                    <input type="number" name="weight" placeholder="{{ __('home.pricing_8') }}" class="form-control weight allow_decimal">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-8 m-auto">
                                    <a href="javascript:void()" class="btn btn-secondary btn-block rounded-lg account_calculer" href="#">{{ __('home.pricing_9') }} </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row showTable" style="display:none">
                        <div class="container-lg py-3">
                            <h1 class="secondary-head text-shadow text-center">
                            </h1>
                        </div>
                        <div class="col-lg-6 mb-4">
                            <h3> {{ __('home.airfreight')}} </h3>
                            <table class="table table-hover table-striped mytables">
                                <tbody class="table_weight">

                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6 mb-4">
                            <table class="table table-hover table-striped mynewtable">
                                <thead>
                                    <th scope="col"> {{ __('home.pricing_16') }} </th>
                                    <th scope="col"> {{ __('home.pricing_20') }}</th>
                                    <th scope="col"> {{ __('home.pricing_21') }} </th>
                                </thead>
                                <tbody class="result_resp">

                                </tbody>
                            </table>
                        </div>
                    </div><!--end -row-->
                </div>
            </div>
            @if($info_bottom)
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="usability">
                        @if( LaravelLocalization::getCurrentLocale() == 'ar')
                            {!! $info_bottom->message !!}
                        @else
                            {!! $info_bottom->message_en !!}
                        @endif
                    </blockquote>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection
