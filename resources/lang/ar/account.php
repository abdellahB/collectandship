<?php
    return [
        'menu_1' => 'خدمات التجميع والشحن',
        'menu_2' => 'شحنات جديده',
        'menu_3' => 'شحنات قيد التجهيز',
        'menu_4' => 'جاهزة للدفع و الشحن',
        'menu_5' => 'أرشيف الشحنات',

        'menu_6' => 'خدمات اضافية',
        'menu_7' => 'طلب خدمة',
        'menu_8' => 'خدمات قيد التنفيد',
        'menu_9' => 'أرشيف الخدمات',

        'menu_10' => 'طلبات ارجاع الى التاجر',
        'menu_11' => 'طلب ارجاع المنتج',
        'menu_12' => ' قيد التجهيز و الدفع',
        'menu_13' => 'أرشيف طلبات الارجاع',

        'menu_14' => 'المتسوق الشخصي',
        'menu_15' => 'طلب شراء جديد',
        'menu_16' => 'طلبات قيد المراجعة',
        'menu_17' => 'جاهز للدفع و التنفيد',
        'menu_18' => 'طلبات تم تنفيدها',
        'menu_19' => 'أرشيف طلبات الشراء',

        'menu_20' => 'طلبات الشحن المباشر',
        'menu_21' => 'طلبات تحت التنفيد',
        'menu_22' => 'ارشيف الشحنات',
        'menu_23' => 'ادارة الحساب',
        'menu_23_1' => 'تحديث المعلومات الشخصية',
        'menu_24' => ' دفتر عناويني',
        'menu_24_4' => 'الدفعات المالية',
        'photo' => 'صورة شخصية',

        //
        'currency' => 'العملة',
        'dollar'   => '$',
        'sar'      => 'ريال',

        //account
        'mydata' => 'بياناتي',
        'fullname' => 'الاسم الكامل',
        'accounrcode' => 'رقم الحساب',
        'memberdate' => 'عضو مند',
        'countrie' => 'الدولة',
        'editdate' => 'تعديل',

        //
        'addresshipping' => 'عناوين الشحن',
        'address_sa' => ' العنوان السعودي',
        'address_usa' => ' العنوان الامريكي',
        'address_uae' => '  العنوان الاماراتي',

        //
        'usa' => 'امريكا',
        'sa' => 'السعودية',
        'uae' => 'الامارات',


        'shipping_address' => 'عنوان الشحن',
        'shipping_mode' => 'وسيلة الشحن',
        'shipping_time' => 'مدة الشحن',
        'shipping_amount' => 'تكلفة الشحن',
        'shipping_change' => ' تغيير الشركة',
        'shipping_total'   => 'الاجمالـي',
        'shipping_moreinfo' => ' تفاصيل الشحنة',

        'parcel'       => 'الشحنة',
        'parcel_code' => 'كود الشحنة',
        'parcel_prd' => 'كود المنتج',
        'parcel_date' => 'ناريخ الوصول',
        'parcel_stock' => 'المخزن',
        'parcel_country' => 'دولة الشحن',
        'parcel_weight' => 'الوزن',
        'parcel_dimension' => 'الأبعاد',
        'parcel_company' => ' شركة الشحن',
        'parcel_track' => 'رقم التتبع',
        'parcel_price' => 'سعر المنتج',
        'parcel_info'   => 'الوصف',
        'parcel_img'    =>  'صور الشحنة ',

        'new_address' => ' عنوان شحن جديد',
        'add_new_address' => 'اضافة عنوان الشحن',
        'bookaddress' => 'دفتر عناوين الشحن الخاصة بي',
        'address_state' => 'المحافطة',
        'address_remove'    => 'حدف العنوان ',



        //checkout
        'checkout_1'  => 'اختر وسيلة الدفع المناسبة',
        'checkout_2'  => 'البطاقة الائتمانية',
        'checkout_3'  => 'حساب بايبال',
        'checkout_4'  => ' الدفع الان',
        'checkout_5'  => 'خدمة الشراء',
        'checkout_6'  => 'خدمة الشحن',
        'checkout_7'  => ' خدمة مشتريات',
        'checkout_8'  => 'خدمة ارجاع المنتج',
        'checkout_9'  => ' المبلغ المطلوب دفعه',
        'checkout_10'  => 'كوبون التخفيض',
        'checkout_11'  => 'تطبيق',
        'checkout_12'  => 'شكرا لك',
        'checkout_13'  => 'تم عملية الدفع بنجاح سيتم التعامل مع طلبك في اسرع وقت ممكن .',

        //purchase
        'purchase_archive'  => 'أرشيف طلبات الشراء',
        'purchase_1'  => ' رقم الطلب',
        'purchase_2'  => 'تاريخ الطلب',

        'purchase_3'  => 'المنتج',
        'purchase_4'  => 'الرابط',
        'purchase_5'  => 'الكمية',
        'purchase_6'  => 'سعر المنتج',
        'purchase_7'  => 'تعليمات',
        'purchase_8'  => 'الحالة',
        'purchase_9'  => 'التتبع',
        'purchase_10'  => 'مشاهدة',
        'purchase_11'  => 'تم التنفيد',
        'purchase_12'  => ' طلب مكتمل',
        'purchase_13'  => 'السعر الأجمالي',
        'purchase_14'  => ' ليس لديك اي طلبات في الوقت الحالي',
        'purchase_15'   => 'ازالة ',

        'purchase_completed'  => 'تم التنفيد',
        'purchase_create'  => 'طلب شراء ',
        'purchase_basket'  => 'سلة المشتريات',
        'purchase_product'  => ' إسم المنتج',
        'purchase_product_url'  => ' رابط المنتج',
        'purchase_product_price'  => 'سعر المنتج ب ',
        'purchase_product_qty'  => 'الكمية المطلوبة',
        'purchase_product_info'  => 'تعليمات أو ملاحظات عن المنتج',
        'purchase_product_add'  => 'إضافة المنتج',
        'purchase_product_submit'  => ' تقديم الطلب',
        'purchase_payment'  => 'جاهز للدفع و التنفيد',
        'purchase_waitpay'  => 'انتظار الدفع',
        'purchase_payed'  => 'تم الدفع',
        'purchase_addtocart'  => ' أضف للسلة',
        'purchase_reviews'  => 'انتظار المراجعة ',
        'purchase_waitreview'  => 'قيد المراجعة',
        'purchase_waitpayment'  => 'انتظار الدفع',

        //packing
        'packing_1'  => 'لرؤية الشحنات يرجي اختيار الدولة التي بها الشحنات اولا',
        'packng_address'  => ' اختر عنوان الشحن',
        'packing_myaddress'  => 'عناوين الشحن الخاصة بي',
        'packing_modeship'  => ' تحديد وسيلة الشحن ',
        'packing_empty'  => 'ليس لديك اي مشتريات في الوقت الحالي',
        'modeshipping'  => 'خيارات الشحن',
        'packing_service'  => 'خدمات التجهيز',
        'packing_service_purchase'  => 'خدمات التجهيز',
        'packing_service_1'  => 'نوع الخدمة',
        'packing_service_2'  => 'التكلفة',
        'packing_service_3'  => 'وصف الخدمة',
        'packing_modpacking_1'  => ' احتفط بالتغليف المصنعي للمنتج',
        'packing_modpacking_2'  => ' تخلص من التغليف المصنعي للمنتج لتقليض حجم الشحنة',
        'packing_total_purchase'  => 'القيمة الاجمالية للمشتربات',
        'packing_information'  => 'ملاحظات خاصة بتغليف المشتريات',
        'packing_terms'  => 'أوافق على الشروط و الأحكام و ارغب بتغليف مشترياتي',
        'packing_submit'  => 'تقديم طلب التجهيز',

        'packing_information_prd'  => 'ملاحظات خاصة بخدمات المشتريات',
        'packing_terms_prd'  => 'أوافق على الشروط و الأحكام الخاصة بهد الخدمة ',
        'packing_submit_order'  => 'تقديم الطلب ',



        //new information
        'parcel_archive'  => ' أرشيف الشحنات',
        'packing_type'  => 'نوع التغليف',
        'ship_address'  => ' عنوان الشحن',
        'Ship_company'  => 'شركة الشحن',
        'ship_moreservice'  => ' خدمات اضافية',
        'packing_using'  => ' بتغليف المصنع',
        'remove_packing'  => ' ازالة التغليف',
        'parcel_shipped'  => 'تم الشحن ',
        'packing_number_prd'  => 'عدد المنتجات',
        'packing_date_order'  => 'تاريخ الطلب',
        'packing_proccess'  => 'قيد التجهيز',
        'packing_ready'  => ' جاهزة للدفع',
        'packing_waitshipping'  => ' انتظار الشحن',
        'packing_details'  => 'تفاصيل',
        'packing_change_company'  => 'تغيير وسيلة الشحن',
        'packing_cancels'  => 'فك التجهيز',
        'service_order_code'  => ' رقم الطلب',
        'service_order_1'  => ' طلب مستلم',
        //
        'return_seller_address'  => ' عنوان البائع',
        'return_problem'  => 'سبب ارجاع المنتج ',
        'return_noselected'  => 'غير محدد ',

        'cart_pay'  => 'سلة التسوق',
        'cart_list'  => 'ملخص الطلب',

        //
        'direct_1'  => 'أرشيف طلبات الشحن المباشر',
        'direct_1_1'  => ' طلبات الشحن المباشر',
        'direct_2'  => 'كود الطلب',
        'direct_3'  => 'عدد الشحنات',
        'direct_4'  => 'تاريخ الطلب',
        'direct_5'  => 'مكان الاستلام',
        'direct_6'  => 'مكان التوصيل ',
        'direct_7'  => 'شركة الشحن',
        'direct_8'  => 'السعر',
        'direct_9'  => 'الحالة',
        'direct_10'  => 'رابط التتبع',
        'direct_11'  => 'انتظار الدفع',
        'direct_12'  => 'انتظار الاستلام',
        'direct_13'  => 'تم التسليم ',
        'direct_14'  => 'وصلت الشحنة ',
        'direct_15'  => 'طلب ملغي',
        'direct_cancel'  => 'الغاء الطلب',


        'transacton'  => 'رقم الطلب',
        'transacton_1'  => 'رقم المعاملة',
        'transacton_2'  => 'مبلغ المعاملة',
        'transacton_r'  => 'مبلغ معاد',
        'transacton_3'  => 'تاريخ المعاملة',
        'transacton_4'  => 'عدد الطلبات',
        'transacton_5'  => 'وسيلة الدفع',


        'modal_1'  => 'اسم الخدمة ',
        'modal_2'  => 'رقم الطلب',
        'modal_3'  => 'سعر الطلب',
        'modal_4'  => 'كود المعاملة',
        'modal_5'  => 'التاريخ',
        'modal_6'  => 'الحالة',
        'payed'  => 'مدفوع',

        'error_price'  => 'يرجي ادخال اسعار المنتجات  التي تم اختيارها',
        'error_product'  => ' يرجي اختيار المنتجات المطلوب تجميعها',

        'warehouse_usa'  => 'المستودع الامريكي',
        'warehouse_sa'  => 'المستودع السعودي',
        'warehouse_uae'  => 'المستودع الاماراتي',


        'error_company'  => ' يرجي اخيتار شركة الشحن المطلوبة من خيارات الشحن المتوفرة',
        'error_packing'  => ' يرجي اختيار نوع التغليف المطلوب لهده الشحنه',
        'error_service'  => 'يرجي اختيار احدى خدمات من خيارات خدمات التجهيز',
        'error_terms'  => ' لتقديم الطلب يجب اولا الموافقة على الشروط و الاحكام',


        'notify_lang'  => 'لغة الاشعارات ',

        //new
        'direct_orderN' => 'رقم الطلب',
        'direct_orderp' => 'وزن الشحنة',
        'direct_dimension' => 'ابعاد الشحنة',
        'direct_content' => 'محتوى الشحنة',
        'direct_value' => 'قيمة الشحنة',

        'error_prd_service' => ' يرجي اخيتار الخدمة المطلوبة لهدا المنتج من الخدمات المقترحة',
        'error_prd_select' => '  يرجي اختيار المنتج المطلوب اضافة الخدمة له',
        'error_prd_return' => ' يرجي اختيار المنتج المطلوب ارجاعه للبائع',

        'updated' => 'تم تحديث الملف الشخصي',
        'pay_error' => 'يرجي اكمال معلومات الملف الشخصي لاتمام عملية الدفع',
        'pay_failed' => 'فشل في عملية الدفع ',
        'notexist' => 'لا توجد ',


        'purchase_tax' => 'الضريبة',
        'purchase_ship'  => 'تكلفة الشحن',
        'commission' => 'العمولة',
        'errorupdate' => 'عفوا لايمكنك استخدام هذا الايميل لانه محجوز مسبقا',

        '404_title' => 'لم يتم العثور على الصفحة التي تبحث عنها.',
        '404_btn'  => 'العودة الى الصفحة الرئسية',

        'dateship' => 'تم الشحن في',

        'assurance_title' => 'تأمين على  الشحنة',
        'assurance_desc' => 'بامكانك إختياريا التأمين على شحنتك ليتم تعويضك في حالة الفقد او التلف',
        'billing_address'   => 'عنوان الفاتورة',
        'newsletter' => ' الاشتراك في القائمة البريدية',
    ]
?>
