<?php

return [
    'home' => 'Home',
    'whyus' => 'Why us',
    'warehouse'       => 'Get your warehouse',
    'calculator'     => 'Price calculator',
    'contact'     => 'Contact us',
    'login'     => 'Sign In',
    'register'  => 'Create a New Account',
    'account'   => 'My Account ',
    'logout'    => 'Logout',
    'cart'    => 'Cart',
    'logincaptcha' => 'The g-recaptcha-response field is required.',
    'shopnow'  => 'Shop Now',
    'directory' => 'Shopping Directory',

    //new
    'forgetpassword'    => 'Forget password ?',
    'rememberme'    => 'Remember me',
    'signup_title'  => 'Welcome back. Please Enter your email and Password to login',
    'signup_title_1'  => ' Fill the form fields below to create a new account. It is Free!',
    'resetpassword' => 'Forgot your password?',
    'signup_response' => ' Please confirm sending activation information to your e-mail',
    'signup_resp_1' => 'The activation email has been sent to your email address',
    'signup_resp_2' => 'Please check your email inbox. Please note that you may find the message in your junk mail box.',
    'verify' => ' Your account has been successfully activated',
    'close' => 'Close',
    'signup_resp_3' => 'Your account was successfully created and you can now start using your account by clicking on the Sign-in link.',

    //
    'msg_password' => 'The password must not be less than 8 digits',
    'loginerror'    => 'you have entered wrong credentials, or you do not have an account with us',
    'rest_title'    => 'Enter your registered email address and we will send you a letter with instructions to recover your password',
    'rest_msg'      => 'If you have a registered account with us, you will receive a message to retrieve your password',

    //
    'modepay'   => 'Supported payment methods',
    'contactus' =>'Contact us',
    'faq' =>'FAQs',
    'questions' =>'Do you have any questions?',
    'privacy' =>'Privacy policy',
    'terms' =>'Terms of use',
    'shiptopya' =>'Supported Shipping and Payment Methods',
    'directship' =>'Direct Shipping Service',
    'copyright'  => 'All Rights Reserved 2020',

    //index
    'title_sous' => 'Shop online',
    'title' => 'from your favorite stores!',

    'title_btn' => 'Get your Free addresses in Saudi Arabia',
    'service' => 'Our Services',
    'service_sous' => 'We provide you with a free Saudi Arabian addresses that you can use to shop easily from your favorite online stores and then ship your purchases to your own local address via various shipping methods',

    'warehouse_how' => 'How Collect and Ship works?',
    'warehouse_step' => 'Step',
        'warehouse_step1' => 'Signup',
        'warehouse_step2' => 'and get your Saudi Arbian Addresses',
        'warehouse_step3' => 'Shop at any online store',
        'warehouse_step4' => 'Ship your products to your Saudi Arabian address',
        'warehouse_step5' => 'Consolidate your products in one package and ship it to your own address',
        'warehouse_step6' => 'and save upto 80% in shipping cost',

    'signup_warehouse' => 'Get your Address Now',
    'firstname' => 'Fistname',
    'lastname' => 'Lastname',
    'email' => 'Email',
    'password' => 'Password',
    'comfirmpassword' => 'Repeat Password',
    'address' => 'Address',
    'countrie' => 'Select country',
    'city' => 'City',
    'zipcode' => 'Zip Code',
    'phone' => 'Phone',
    'terms&condition' => 'I agree to collectandship.com Terms of Service and Privacy Policy',
    'signup' => 'Sign Up',

    'title_buy' => 'Shop',
    'title_buy1' => 'at all Saudi Arabian Online Stores',


    //pricing ...
    'pricing_title' => 'Shipping Rate Calculator',
    'pricing_1' => 'Unit',
    'pricing_2' => 'pound/inch',
    'pricing_3' => 'kg/cent',

    'pricing_4' => 'Length',
    'pricing_5' => 'Width',
    'pricing_6' => 'Height',

    'pricing_7'   => 'Ship To',
    'pricing_8'   => 'Package Weight',
    'pricing_9'   => 'Get Shipping Rates',
    'pricing_9_1' => 'More than One Package?',

    'pricing_10' => 'Our shipping rate includes the following services:',
    'pricing_11' => 'FREE storage up to 90 days!',
    'pricing_12' => 'FREE photos of your purchases once we receive them at our warehouse!',
    'pricing_13' => 'FREE packages consolidation! and package repacking, which helps you reduce the shipping costs',
    'pricing_14' => 'Shipping insurance on your packages during international shipping',
    'pricing_15' => 'High quality packaging to ensure the safety of your items!',

    'pricing_16' => 'Shipping Method',
    'pricing_17' => 'Actual Weight',
    'pricing_18' => 'Volumetric Weight',
    'pricing_19' => 'Calculated Weight',
    'pricing_20' => 'Delivery Time',
    'pricing_21' => 'Shipping Rate',


    //contact us
    'contact_title' => 'Don\'t hesitate to contact us!.. Our goal is to offer you the best shipping services that saves you money and delivers your packages safely',
    'contact_question' => 'Do you have any questions?',
    'contact_fullname' => 'Fullname',
    'contact_email' => 'Email',
    'contact_subject' => 'Subject',
    'contact_message' => 'message',
    'contact_btn' => 'Send',

    'rest_success' => 'Your account password has been successfully updated',
    'kg' => 'kg',
    'msg_kg'    => 'Maximum shipping charge is',
    'source_country' => 'Ship From ',
    'allstore'  => 'Browse all websites',
    'emailsent' => 'The message has been sent. You will be answered as soon as possible',
    'airfreight'    => 'Air freight options',
    'changepassword'    => 'Change Password',
    'changepassword_title'    => 'Change the account password',
    'direct_msg'    => 'Sorry, the direct shipping service is not available between the countries you have selected.',


    'pack_content'  => 'Content of shipments ',
    'pack_place'    => 'Place of receipt',
    'pack_delevery' => 'Place of delivery',
    'pack_terms'    => 'I agree to the terms and conditions and I want to submit the order',
    'pack_submit'   => 'Submit order',
    'cm'   => 'cm',
    'msg_addtocart' => 'ًWas added to the shopping cart, please go to the cart to complete the order'


];
