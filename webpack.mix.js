const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/js/app.js', 'public/js')
   //.sass('resources/sass/app.scss', 'public/css');

/**** FRONT CSS/JS ******/
mix.styles([
    'public/assets/front/css/bootstrap-rtl.min.css',
    'public/assets/front/css/style.css',
    'public/assets/front/css/sweetalert.min.css'
],'public/css/rtl.css');

//ltr
mix.styles([
    'public/assets/front/css/bootstrap.min.css',
    'public/assets/front/css/ltr.css',
    'public/assets/front/css/sweetalert.min.css'
],'public/css/ltr.css');


mix.scripts([
    'public/assets/front/js/jquery.min.js',
    'public/assets/front/js/popper.min.js',
    'public/assets/front/js/bootstrap.min.js',
    'public/assets/front/js/sweetalert.min.js',
    'public/assets/front/js/script.js'
], 'public/js/front.js');
/************* End  ****************/
/*********** USER CSS/JS ***********/
mix.styles([
    'public/assets/front/css/bootstrap-rtl.min.css',
    'public/assets/front/css/style.css',
    'public/assets/user/css/rtl.css',
    'public/assets/front/css/sweetalert.min.css',
    'public/assets/user/css/jquery.fancybox.css'
],'public/css/user/rtl.css');

//ltr
mix.styles([
    'public/assets/front/css/bootstrap.min.css',
    'public/assets/front/css/ltr.css',
    'public/assets/user/css/ltr.css',
    'public/assets/front/css/sweetalert.min.css',
    'public/assets/user/css/jquery.fancybox.css'
],'public/css/user/ltr.css');


mix.scripts([
    'public/assets/front/js/jquery.min.js',
    'public/assets/front/js/popper.min.js',
    'public/assets/front/js/bootstrap.min.js',
    'public/assets/front/js/sweetalert.min.js',
    'public/assets/user/js/jquery.fancybox.js',
    'public/assets/front/js/script.js',
    'public/assets/user/js/script.js'
  ], 'public/js/user/front.js');
/*************** END ******************/
