<div class="modal fade" id="myModalSign" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center" style="display: block;">
                {{ __('home.signup_title_1') }}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                <!-- Begin # Login Form -->
                <form class="form-bg gradient-bg p-4 signup" _lpchecked="1">
                    <div class="row">
                        <div id="reg_errors" class="text-white text-center"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 col-sm-12 col-12">
                            <input type="text" name="firstname" required="true" placeholder=" {{ __('home.firstname') }}" class="form-control shadow-control" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 col-sm-12 col-12">
                            <input type="text" name="lastname" required="true" placeholder="{{ __('home.lastname') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="email" name="email" required="true" placeholder="{{ __('home.email') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="password" name="password" required="true" placeholder="{{ __('home.password') }}" class="form-control shadow-control">
                            <small class="text-white"> {{ __('home.msg_password') }} </small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="password" name="password_confirmation" required="true" placeholder="{{ __('home.comfirmpassword') }}" class="form-control shadow-control">
                        </div>
                    </div>
                    <!--<div class="row form-group">
                        <div class="col-12">
                            <input type="text" name="address" placeholder="{{ __('home.address') }}" class="form-control shadow-control">
                        </div>
                    </div>-->
                    <div class="row form-group">
                        <div class="col-md-12 col-sm-12 col-12">
                            <select name="countrie_id" required="true" class="form-control shadow-control">
                                <option value="">{{ __('home.countrie') }}</option>
                                @if(count($countries)>0)
                                    @foreach ($countries as $countrie)
                                        <option value="{{ $countrie->id }}">
                                            @if( LaravelLocalization::getCurrentLocale() == 'ar')
                                                {{ $countrie->countriename_ar }}
                                            @else
                                                {{ $countrie->countriename }}
                                            @endif
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!--<div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="city"  placeholder="{{ __('home.city') }}" class="form-control shadow-control">
                        </div>-->
                    </div>
                    <!--<div class="row form-group">
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="zipcode"  placeholder="{{ __('home.zipcode') }}" class="form-control shadow-control">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <input type="text" name="phone" placeholder="{{ __('home.phone') }}" class="form-control shadow-control">
                        </div>
                    </div>-->

                    <div class="row form-group">
                        <div class="col-12">
                            <div class="custom-control custom-radio">
                                <input type="checkbox" checked class="custom-control-input" id="defaultUnchecked" name="terms&condition">
                                <label class="custom-control-label" for="defaultUnchecked">
                                    {{ __('home.terms&condition') }}
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="custom-control custom-radio">
                                <input type="checkbox" class="custom-control-input" value="0" id="newsletters" name="newsletter">
                                <label class="custom-control-label" for="newsletters">
                                    {{ __('account.newsletter')}}
                                </label>
                                <input type="hidden" name="newsletter" value="1" />
                            </div>
                        </div>
                    </div>
                    <style>
                        .g-recaptcha div {
                            width: auto !important;
                        }
                    </style>
                    <div class="row form-group">
                        <div class="col-12 text-center">
                            {!! app('captcha')->display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 m-auto">
                            <a class="btn btn-secondary btn-lg btn-block rounded-lg signUp" href="javascript:void()">{{ __('home.register') }} </a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>


<div class="modal fade" id="ModalVerify" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-body text-center">
            <h2>
                {{ __('home.verify') }}
            </h2>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalNotification" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Body -->
            <div class="modal-body text-center">
                <h4 class="loginerreur maxweight" style="diplay:none">
                    {{ __('home.msg_kg')}} <b class="n_weight"> </b> {{ __('home.kg')}}
                </h4>
                <h4 class="loginerreur directship" style="diplay:none">
                    {{ __('home.direct_msg') }}
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-info"> {{ __('home.close') }}</button>
            </div>
        </div>
    </div>
</div>
