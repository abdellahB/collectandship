$(document).ready(function(){


    //
    $('#codeclient').autocomplete({
        source: function(request,response){
              $.ajax({
                url : 'import',
                dataType: "json",
                    type:"POST",
                    data: {
                        codeclt: request.term,
                        type: 'codeclient'
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function( data ) {
                        response( $.map( data,function(item){
                            return {
                                label: item,
                                value: item
                            }
                        }));
                    }
              });

            },
            autoFocus: true,
            minLength: 0
    });

    $('#codeclient').blur(function(){
        var $import_policier = $('#codeclient').val();
        var data ="policia="+$import_policier;
        $.ajax({
            type : "POST",
            url  : "signleUser",
            data : data,
            cache:false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $("#fullname").val(response[0].lastname +' '+ response[0].firstname);
                $("#country").val(response[0].countriename);
                $("#user_id").val(response[0].id);
            }
        });
    });

    //
    $('.country').on('change',function(){
        var country      =  $(this).val();
        var ship_country = $(".ship_country").val();
        var data ="country="+country+"&ship_country="+ship_country;
        $.ajax({
            type : "POST",
            url  : "listcompany",
            data:data,
            cache:false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".show_result").html(" ");
                $(".show_result").append(response.html);
            }
        });
    });

    $(".table").on("click"," .updateprices",function(){
        var idprices = parseInt($(this).val());
        var prices  = $("."+idprices+" td > .getprice").val();
        var data = "idprices="+idprices+"&prices="+prices;
        //alert(data);
        swal({
            title: "Are your Sure ?",
            text: "You want Update this prices ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes I am Sure",
            closeOnConfirm: true
            },
        function(){
            $.ajax({
                type :"POST",
                url : "../../updateprices",
                data:data,
                cache:false,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        $("."+idprices).remove();
                    }else{
                        alert("No")
                    }
                }
            });
        });
    });

    //saveTitle
    $(".saveQuestion").on("click",function(){
        var questions_en = $(".questions_en").val();
        var answer_en    = $(".answer_en").val();
        var questions_ar = $(".questions_ar").val();
        var answer_ar    = $(".answer_ar").val();
        var data = "questions_en="+questions_en+"&answer_en="+answer_en+"&questions_ar="+questions_ar+"&answer_ar="+answer_ar;
        //alert(data);
        $.ajax({
            type : "POST",
            url  : "addquestion",
            data : data,
            cache:false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
					swal({
                        title: "تمت العملية",
                        text: "تمت اضافة السؤال المطلوب بنجاج",
                        type: "success"
                    }, function() {
                        location.reload();
                    });
                }else{
                    alert("No")
                }
            }
        });
    });

    //
    $(".addcoupons").on("click",function(){
        var couponccode = $(".couponccode").val();
        var couponprice = $(".couponprice").val();
        var coupontype = $(".coupontype").val();
        var couponutilisation = $(".couponutilisation").val();
        var datetostart = $(".datetostart").val();
        var datetoend   = $(".datetoend").val();
        var data="couponccode="+couponccode+"&couponprice="+couponprice+"&coupontype="+coupontype+"&datetostart="+datetostart+"&datetoend="+datetoend+"&couponutilisation="+couponutilisation;
        if(couponccode && couponprice && coupontype && couponutilisation &&  datetostart && datetoend){
            //if(datetostart >= datetoend){
                $.ajax({
                    type :"POST",
                    url : "addcoupon",
                    data:data,
                    cache:false,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(response){
                        if(response.success == true){
                            location.reload();
                        }
                    }
                });
            //}else{
                //sweetAlert("Error", "End date not valid ...", "error");
            //}
        }else{
            sweetAlert("Error", "All field required", "error");
        }
    });

    //
    $(".table").on('click',' .operDiscount',function(){
        var couponid = $(this).attr("id");
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        var data = "couponid="+couponid+"&status="+status;
        $.ajax({
            type :"POST",
            url : "operationcoupon",
            data:data,
            cache:false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    location.reload();
                }
            }
        });
    });


    //addusers
    $(".addusers").on("submit",function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url :'register',
            data:$(".addusers").serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(response){
                if(response.success == true){
                    location.reload();
                }
            }
        });
    });

    //send email
    $(".table").on("click",".sendEmail",function(){
        var userId = $(this).attr("id");
        $(".user_id").html(" ");
        $(".user_id").val(userId);
        $("#myModalEmail").modal();
    });
    $(".sendemailtouser").on("submit",function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url :'sendemail',
            data:$(this).serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(response){
                if(response.success == true){
                    swal({
                        title: "تمت الارسال",
                        text: "تم ارسال الرسالة للعميل المطلوب ",
                        type: "success"
                    }, function() {
                        location.reload();
                    });
                }
            }
        });
    });

    $(".sendnewletter").on("submit",function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url :'newsletter',
            data:$(this).serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(response){
                if(response.success == true){
                    swal({
                        title: "تمت الارسال",
                        text: "تم ارسال الرسالة  المطلوبة ",
                        type: "success"
                    }, function() {
                        location.reload();
                    });
                }
            }
        });
    });
    //operaccount
    $(".table").on("click",".operaccount",function(){
            var idusers=$(this).attr("id");
            var status = parseInt($(this).attr('class').replace(/\D/g,''));
            var data="idusers="+idusers+"&status="+status;
            //alert(data);
            swal({
                title: "هل أنت متأكد ؟",
                text: "تريد تنفيد هده العملية لهدا الحساب ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "نعم تنفيد ",
                cancelButtonText: "لا, إلغاء!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
        function(isConfirm){
            if (isConfirm){
                    $.ajax({
                    type :"POST",
                    url : "operaccount",
                    data:data,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    cache:false,
                    success: function(response){
                            if(response.success == true){
                                swal({
                                    title: "تمت العملية",
                                    text: "تم تنفيد العلمية المطلوية بنجاح",
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }else{
                    sweetAlert("تم الالغاء", "تم الغاء العملية", "error");
                }
        });
    });

    //
    $(".table").on("click",".fastbuyetat_ok",function(){
		var fastbuyetat_prd = $(this).attr("id");
		var data = "fastbuyetat_prd="+fastbuyetat_prd;
		$.ajax({
			type : "POST",
			url  : "getprd",
            data : data,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			cache  : false,
			success: function(response){
				$(".show_data_wafirha_prd").html(" ");
				$(".show_data_wafirha_prd").append(response.html);
				$("#classModalOrderWafirha").modal();
			}
		});
    });

    $(".table").on("click"," .Edit_product",function(){ //
		var edit_fastbuy_product = $(this).attr("id");
		var data ="edit_fastbuy_product="+edit_fastbuy_product;
		$.ajax({
			url : 'editprd',
			data: data,
            type: 'POST',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(response){
				//if(val == 1){
				$(".getproduct_info").html(" ");
				$(".getproduct_info").append(response.html);
				$("#myModalEditProduct").modal();
				//}
			}
		});
    });
    $('#classModalOrderWafirha').on('hidden.bs.modal', function () {
        location.reload();
    });
    //
    $(".update_fastbuyf").on("click",function(e){
        var product_url = $("#product_url").val();
        var product_shipping = $("#product_shipping").val();
        var product_qty = $("#product_qty").val();
        var product_name = $("#product_name").val();
        var product_tax = $("#product_tax").val();
        var product_price = $("#product_price").val();
        var currency      = $("#currency").val();
        var product_taux  = $("#product_taux").val();
        var product_id = $("#product_id").val();
        var order_id   = $("#order_id").val();
        var product_des = $("#product_des").val();
        var data ="product_url="+product_url+"&product_shipping="+product_shipping+"&product_qty="+product_qty+"&product_name="+product_name+
        "&product_tax="+product_tax+"&product_price="+product_price+"&product_id="+product_id+"&product_des="+product_des+"&order_id="+order_id+
        "&currency="+currency+"&product_taux="+product_taux;
        $.ajax({
			url : 'updateprd',
			data: data,
            type: 'POST',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(response){
				if(response.success == true){
                    loadOrder_product(order_id,1);
                }
			}
		});
    });

    //
    function loadOrder_product(id,s){
        var data ="fastbuyetat_prd="+id;
        $.ajax({
            type :"POST",
            url : "getprd",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".show_data_wafirha_prd").html(" ");
                $(".show_data_wafirha_prd").append(response.html);
                if(s==1){
                    $("#myModalEditProduct").modal("toggle");
                }else if(s==2){
                    $("#myModalTracking").modal("toggle");
                }
            }
        });
    }

    //accept order
    $(".table").on("click",".acceptorders",function(){
        var id = $(this).attr("id");
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        var data="orderid="+id+"&status="+status;
        swal({
          title: "هل أنت متأكد ؟",
          text: "تريد القيام بهده العمليةلهدا الطلب  ",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "نعم,موافق",
          cancelButtonText: "لا, إلغاء!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm){
                $.ajax({
                type :"POST",
                url : "new",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                        if(response.success == true){
                            swal("شكرا", "تم العملية المطلوبة بنجاح", "success");
                            location.reload();
                        }
                    }
                });
            }else{
                swal("ألغي", "تم التراجع عن تنفيد العملية", "error");
            }
        });
    });

    $(".table").on('click'," .addtracking",function(){
        var product = $(this).attr("id");
        var orderId = parseInt($(this).attr('class').replace(/\D/g,''));
        $("#productid").val(product);
        $("#orderId").val(orderId);
        $("#myModalTracking").modal();
    });

    $(".submittrackinginfo").on("click",function(){
        var chilorder_id = $("#productid").val();
        var orderId      = $("#orderId").val();
        var companyurl   = $("#companyurl").val();
        var trackinginfo = $("#trackinginfo").val();
        var data ="chilorder_id="+chilorder_id+"&companyurl="+companyurl+"&trackinginfo="+trackinginfo+"&orderId="+orderId;
        $.ajax({
            type :"POST",
            url : "addtracking",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    loadOrder_product(orderId,2);
                }
            }
        });
    });


    //updatetrackinginfo
    $(".updatetrackinginfo").on("click",function(){
        var companyurl   = $("#companyurl_update").val();
        var trackinginfo = $("#trackinginfo_update").val();
        var trackingid = $("#trackingid").val();
        var data ="companyurl="+companyurl+"&trackinginfo="+trackinginfo+"&trackingid="+trackingid;
        $.ajax({
            type :"POST",
            url : "updatetracking",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    $(".result_resp").html(" ");
                    $(".result_resp").append("<b> تم تحديث معلومات الشحن </b>")
                    //loadOrder_product(orderId,2);
                }
            }
        });
    });
    //
    $(".table").on("click"," .show_tracking",function(){
        $(".result_resp").html(" ");
        product_id = $(this).attr("id");
        var data = "product_id="+product_id;
        $.ajax({
            type :"POST",
            url : "showtracking",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".showData").html(" ");
                $(".showData").append(response.html);
                $("#myModalShowTracking").modal();
            }
        });
    });

    //

    $(".table").on("click"," .prd_arrive",function(){
        var product_id = $(this).attr("id");
        var orderId = parseInt($(this).attr('class').replace(/\D/g,''));
        var data   = "product_id="+product_id+"&orderId"+orderId;
        $.ajax({
            type :"POST",
            url : "prd_arrive",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    loadOrder_product(orderId,3);
                }
            }
        });
    });

    //
    $(".table").on("click",'.getlist',function(){
        var pack = $(this).attr('id');
        var data ="packing_id="+pack;
        $.ajax({
            type : "POST",
            url  : "new",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".getlist_s").html(" ");
                $(".getlist_s").append(response.html);
                $("#myModalShowProduct").modal();
            }
        });
    });

    //
    $(".table").on('click',' .packed' ,function(){
        var pack = $(this).attr('id');
        $("#packign_id").val(pack);
        $("#myModalPacking").modal();
    });

    $(".packFinish").on("click",function(e){
        var proceed = true;
		$(".formPack input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
        });
        if(proceed) //everything looks good! proceed...
        {
            $(this).hide();
            $(".loading_icons").css('display','block');

            var m_data = new FormData();
            m_data.append( 'packign_id', $('input[name=packign_id]').val());
            m_data.append( 'weight', $('input[name=weight]').val());
            m_data.append( 'length', $('input[name=length]').val());
			m_data.append( 'width', $('input[name=width]').val());
            m_data.append( 'height', $('input[name=height]').val());
            m_data.append( 'ship_unity', $('select[name=ship_unity]').val());
            var ins = document.getElementById('fileToUpload').files.length;
            for (var x = 0; x < ins; x++) {
                //m_data.append( 'images', $('input[name=images]').files[x]);
                m_data.append("images[]", document.getElementById('fileToUpload').files[x]);
            }

            $.ajax({
                type:'POST',
                url :'packing',
                data: m_data,
                processData: false,
                contentType: false,
                dataType:'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    if(response.success == true){
                        $(".packFinish").show();
                        $(".loading_icons").css('display','none');
                        swal({
                            title: "تمت العملية",
                            text: "تم عمليه تجميع الشحنة بنجاح وتم اشغار العميل ",
                            type: "success"
                        }, function() {
                            $("#myModalPacking").modal('toggle');
                            location.reload();
                        });
                        //location.reload();
                    }else{
                        alert("الوزن المدخل أو الوزن الججمي اكبر من الموجود في ملف الاسعار ");
                        $(".packFinish").show();
                        $(".loading_icons").css('display','none');
                    }
                }
            });
        }
    });

    //

    //
    $(".submit_tracking_pack").on("click",function(e){
        var proceed = true;
		$(".trackings input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
        });
        if(proceed) //everything looks good! proceed...
        {
            var packing_id    = $("#packing_id").val();
            var mode_shipping = $(".mode_shipping").val();
            var tracking_info = $(".tracking_info").val();
            var data = "packing_id="+packing_id+"&tracking_info="+tracking_info+"&mode_shipping="+mode_shipping;

            $.ajax({
                type : "POST",
                url  : "tracking_pack",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تمت اضافة معلومات التتيع للشحنة",
                            type: "success"
                        }, function() {
                            $("#myModalPackingTracking").modal('toggle');
                            location.reload();
                        });
                    }
                }
            });

        }
    });


    $(".table").on("click"," .addimages",function(){
        var product_id = $(this).attr("id");
        var service_id = parseInt($(this).attr('class').replace(/\D/g,''));
        $("#product_id").val(product_id);
        $("#service_id").val(service_id);
        $("#myModalProductImage").modal();
    });
    $(".photo").hide();
    $(".video").hide();
    $(".service_s").on("change",function(){
        var service_s = $(this).val();
        if(service_s == 2){
            $(".photo").show();
            $(".video").hide();
        }else if(service_s == 3){
            $(".video").show();
            $(".photo").hide();
        }else{
            $(".photo").hide();
            $(".video").hide();
        }
    });
    $(".submit_product_img").on("click",function(){
        var proceed = true;
		$(".images input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
        });
        if(proceed){

            var m_data = new FormData();
            m_data.append('product_id', $('input[name=product_id]').val());
            m_data.append('service_s', $('select[name=service_s]').val());
            m_data.append('service_id', $('input[name=service_id]').val());
            var ins = document.getElementById('more_images').files.length;
            var ins2 = document.getElementById('more_video').files.length;
            if(ins2){
                m_data.append('more_video', $('input[name=more_video]')[0].files[0]);
            }else{
                for (var x = 0; x < ins; x++) {
                    m_data.append("images[]", document.getElementById('more_images').files[x]);
                }
            }

            $.ajax({
                type:'POST',
                url :'moreimage',
                data: m_data,
                processData: false,
                contentType: false,
                dataType:'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تم عمليه تجميع الشحنة بنجاح وتم اشغار العميل ",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });

        }
    });

    $(".table").on("click",' .removeparcel',function(){
        var product_id = $(this).attr("id");
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        var data ="product_id="+product_id+"&status="+status;
        if(product_id){
            $.ajax({
                type : "POST",
                url  : "rm_product",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تم تنفيد العلمية المطلوبة على هده الشحنة",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    $(".table").on("click",'.ship_status',function(){
        var retutn_id = $(this).attr("id");
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        $(".return_id").val(retutn_id);
        if(status == 1){
            $("#myModalProductReturn_price").modal();
        }else{
            $("#myModalProductReturn_Track").modal();
        }
    });

    $(".submit_product_return").on("click",function(){
        var reutnid = $("#return_id").val();
        var price_topay = $("#price_topay").val();
        var data = "return_id="+reutnid+"&price_topay="+price_topay;
        if(reutnid && price_topay){
            $.ajax({
                type : "POST",
                url  : "returnprice",
                data : data,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تمت اضافة شحن الشحن للطلب المطلوب بنجاح",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    $(".submit_track_return").on("click",function(){
        var returnid = $("#return_id").val();
        var company_urls = $("#company_urls").val();
        var company_track = $("#company_track").val();
        if(returnid && company_urls && company_track){
            var datas ="returnid="+returnid+"&company_urls="+company_urls+"&company_track="+company_track;
            $.ajax({
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url  : "return/returntrack",
                dataType: "JSON",
                type : "POST",
                data : datas,
                success: function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تمت اضافة رثم التتيع للشحنة المطلوب اعادتها",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });

        }
    });

    //
    $(".table").on("click",'.activateaddress',function(){
        var addressid = $(this).attr('id');
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        var data ="addressid="+addressid+"&status="+status;
        swal({
            title: "هل أنت متأكد ؟",
            text: "تريد القيام بهده العملية لهدا العنوان   ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "نعم,موافق",
            cancelButtonText: "لا, إلغاء!",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm){
            if (isConfirm){
                  $.ajax({
                  type :"POST",
                  url : "address/action",
                  data:data,
                  cache:false,
                  headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  success: function(response){
                          if(response.success == true){
                              swal("شكرا", "تم العملية المطلوبة بنجاح", "success");
                              location.reload();
                          }
                      }
                  });
              }else{
                  swal("ألغي", "تم التراجع عن تنفيد العملية", "error");
              }
          });
    });
    //
    $(".table").on("click",'.inknow_parcel',function(){
        var unknow = $(this).attr('id');
        var status = parseInt($(this).attr('class').replace(/\D/g,''));
        if(status === 2){
            var data ="unknow="+unknow+"&status="+status;
            $.ajax({
                type :"POST",
                url : "unknow",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                        if(response.success == true){
                            swal("شكرا", "تم العملية المطلوبة بنجاح", "success");
                            location.reload();
                        }
                    }
                });
        }else{
            $(".shippingid").val(unknow);
            $("#myModalUserPolicia").modal();
        }
    });
    $(".update_shipping").on("click",function(){
        var userpolicia = $(".userpolicia").val();
        var shipping = $(".shippingid").val();
        var data ="userpolicia="+userpolicia+"&shipping="+shipping;
        $.ajax({
            type :"POST",
            url : "unknow_update",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                    if(response.success == true){
                        swal("ممتاز", "تم العملية المطلوبة بنجاح", "success");
                        location.reload();
                    }else{
                        $(".resp_error").html("");
                        $(".resp_error").append("<b>رقم العميل المدخل غير صحيح </b>")
                    }
                }
        });
    });
    //
    $("#myform").on("submit", function(){
        $(".loading").fadeIn();
    }); //submit

    $(".identityvalue").on("change",function(){
        var value = $(this).val();
       if(value == "-2"){
            $(".tohide").hide();
            $(".toshow").show();
            $(".codeclient").attr("readonly",true);
       }else{
            $(".tohide").show();
            $(".toshow").hide();
            $(".codeclient").attr("readonly",false);
       }
    });

    var next = 0;
    $(".addmores").click(function(e){
        e.preventDefault();
        next = next + 1;
        var newIns = '<div class="'+next+'"> <div class="form-group col-lg-4"> <label class="default prepend-icon" > اسم السلعة  </label> <input type="text" name="product_name[]" placeholder="اسم السلعة" class="form-control"> </div> <div class="form-group col-lg-4"> <label class="default prepend-icon" > الكمية  </label> <input type="number" name="product_qty[]" placeholder="الكمية" class="form-control"> </div> <div class="form-group col-lg-3"> <label class="default prepend-icon" > السعر  </label> <input type="number" name="product_price[]" placeholder="السعر" class="form-control"> </div> <div class="form-group col-md-1"> <label class="default prepend-icon" style="color:#fff;"> السعر  </label> <a href="javasript:void()" id="'+next+'" class="btn btn-danger btn-sm removeme"> <i class="icon icon-remove"></i> </a> </div> </div>';
        $(".field").append(newIns);
    });

     //delete
    $('.field').on("click"," .removeme",function(e){
        var id = $(this).attr('id');
        var count = $("#count").val();
        var cc = count - 1;
        $("."+id).remove();
        $("#count").val(cc);
    });

    //showOrders
    $(".table").on("click",'.showOrders',function(){
        var transid = $(this).attr("id");
        var data = "transid="+transid;
        if(transid){
            $.ajax({
                type :"POST",
                url : "showOrders",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getlist_details").html("");
                    $(".getlist_details").append(response.html);
                    $("#myModalShowDetail").modal();
                }
            });
        }
    });
    $(".table").on("click",'.showOrder',function(){
        var transid = $(this).attr("id");
        var data = "transid="+transid;
        if(transid){
            $.ajax({
                type :"POST",
                url : "../showOrders",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getlist_details").html("");
                    $(".getlist_details").append(response.html);
                    $("#myModalShowDetail").modal();
                }
            });
        }
    });
    //search ...
    $(".searchclient").on("keyup",function(){
        var search = $(this).val();
        var data = "search="+search;
        if(search){
            $.ajax({
                type :"POST",
                url : "search",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".resp_results").html("");
                    $(".resp_results").append(response.html);
                }
            });
        }else{
            location.reload();
        }
    });


    //
    $(".table").on("click"," .detail_ship",function(){
        var id_ship = $(this).attr("id");
        var data = "id_ship="+id_ship;
        $.ajax({
            type :"POST",
            url : "detail_ship",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".getlist_directShip").html("");
                $(".getlist_directShip").append(response.html);
                $("#myModalShowDirectShip").modal();
            }
        });
    });


    //
    $(".table").on("click"," .ship_recerved",function(){
        var id_ship = $(this).attr("id");
        var data = "id_ship="+id_ship;
        $.ajax({
            type :"POST",
            url : "ship_recerved",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    swal({
                        title: "تمت العملية",
                        text: "تم تاكيد وصول شحنة العميل الى مستودع الشركة",
                        type: "success"
                    }, function() {
                        location.reload();
                    });
                }
            }
        });
    });

    //
    $(".table").on("click",".direct_track",function(){
        var id_ship = $(this).attr("id");
        //var company_name = $("."+id_ship+" .companyname").text();
        $("#directship_id").val(id_ship);
        //$("#company_name_direct").val(company_name);
        $("#myModalDirectShipTracking").modal();
    });

    $(".submit_tracking_direct").on("click",function(e){
        var proceed = true;
		$(".tracking_direct input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
        });
        if(proceed) //everything looks good! proceed...
        {
            var directship_id = $("#directship_id").val();
            var company_url = $("#company_url").val();
            var tracking_number_direct = $("#tracking_number_direct").val();
            var data = "directship_id="+directship_id+"&tracking_number_direct="+tracking_number_direct+"&company_url="+company_url;
            $.ajax({
                type : "POST",
                url  : "tracking_direct",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        swal({
                            title: "تمت العملية",
                            text: "تمت اضافة معلومات التتيع للشحنة",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });

        }
    });


    //
    $(".addcategory").on("click",function(){
        var catname = $(".catname").val();
        var catname_en = $(".catname_en").val();
        var data = "catname="+catname+"&catname_en="+catname_en;
        $.ajax({
            type : "POST",
            url  : "addcategory",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    $(".catname").val(" ");
                    $(".catname_en").val(" ");
                    $(".resp_msg").html("");
                    $(".resp_msg").append('<span class="label label-success btn-block"> تمت اضافة التصنيف ينجاج </span>')
                }
            }
        });
    });


    //
    $(".table").on("click",'.refundtoclient',function(){
        var transact_id = $(this).attr("id");
        $(".transact_id").val(transact_id);
        $("#myModalRefund").modal();
    });

    $(".done_refund").on("click",function(){
        var transact_id = $(".transact_id").val();
        var refunded    = $(".refunded").val();
        var data = "transact_id="+transact_id+"&refunded="+refunded;
        if(refunded && transact_id){
            $.ajax({
                type : "POST",
                url  : "refund",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        $("#myModalRefund").modal('toggle');
                        swal({
                            title: "تمت العملية",
                            text: "تمت تسجيل العملية المطلوبة في حساب العميل ",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });


    $(".table").on("click"," .pageaction",function(){
        var info_id   = $(this).attr("id");
        var status    = parseInt($(this).attr('class').replace(/\D/g,''));
        var data = "info_id="+info_id+"&status="+status;
        if(info_id && status){
            $.ajax({
                type : "POST",
                url  : "usability/delete",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        swal({
                            title: "تمت العملية",
                            text: "تم تنفيد العملية المطلوبة لهده الصفحة ",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    //
    function getcompany(country , ship_country){
        var data ="country="+country+"&ship_country="+ship_country;
        $.ajax({
            type : "POST",
            url  : "listcompany",
            data:data,
            cache:false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".show_result").html(" ");
                $(".show_result").append(response.html);
            }
        });
    }
    //
    $(".table").on("click"," .companyAction",function(){
        var company_id    = $(this).attr("id");
        var countrie_id   = $(this).attr("data");
        var ship_country  = $(this).attr("data_id");
        var status        = parseInt($(this).attr('class').replace(/\D/g,''));
        var data          = "company_id="+company_id+"&status="+status;
        if(company_id && status){
            $.ajax({
                type : "POST",
                url  : "company/delete",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        swal({
                            title: "تمت العملية",
                            text: "تم تنفيد العملية المطلوبة لهده الشركة ",
                            type: "success"
                        }, function() {
                            getcompany(countrie_id,ship_country);
                        });
                    }
                }
            });
        }
    });


    $(".table").on("click"," .actionusers",function(){
        //alert("ok");
        var admin_id  = $(this).attr("id");
        var status    = parseInt($(this).attr('class').replace(/\D/g,''));
        var data      = "admin_id="+admin_id+"&status="+status;
        if(admin_id && status){
            $.ajax({
                type : "POST",
                url  : "users",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        swal({
                            title: "تمت العملية",
                            text: "تم تنفيد العملية المطلوبة لهده المستخدم ",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });

    $(".table").on("click"," .actionService",function(){
        var service_id  = $(this).attr("id");
        var status    = parseInt($(this).attr('class').replace(/\D/g,''));
        var data      = "service_id="+service_id+"&status="+status;
        if(service_id && status){
            $.ajax({
                type : "POST",
                url  : "services",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        swal({
                            title: "تمت العملية",
                            text: "تم تنفيد العملية المطلوبة   ",
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                }
            });
        }
    });


    //
    $(".table").on("click"," .shipdetail",function(){
        var shipment_id  = $(this).attr("id");
        var data         = "shipment_id="+shipment_id;
        if(shipment_id){
            $.ajax({
                type : "POST",
                url  : "shipdetail",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getlist_shipment").html("");
                    $(".getlist_shipment").append(response.html);
                    $("#myModalShowShipment").modal();
                }
            });
        }
    });

    $(".table").on("click"," .viewService",function(){
        var service_id  = $(this).attr("id");
        var data        = "service_id="+service_id;
        if(service_id){
            $.ajax({
                type : "POST",
                url  : "serivcedetail",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getlist_service").html("");
                    $(".getlist_service").append(response.html);
                    $("#myModalShowService").modal();
                }
            });
        }
    });


    $(".table").on("click"," .packShipped",function(){
        var packing_id = $(this).attr("id");
        var data   = "packing_id="+packing_id;
        if(service_id){
            $.ajax({
                type : "POST",
                url  : "../changeInfo",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getforms").html();
                    $(".getforms").append(response.html);
                    $("#myModalPackingTracking").modal();
                }
            });
        }
    });

});
