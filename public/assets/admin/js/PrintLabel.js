//----------------------------------------------------------------------------
//
//  $Id: PrintLabel.js 38773 2015-09-17 11:45:41Z nmikalko $ 
//
// Project -------------------------------------------------------------------
//
//  DYMO Label Framework
//
// Content -------------------------------------------------------------------
//
//  DYMO Label Framework JavaScript Library Samples: Print label
//
//----------------------------------------------------------------------------
//
//  Copyright (c), 2010, Sanford, L.P. All Rights Reserved.
//
//----------------------------------------------------------------------------


(function()
{
    // called when the document completly loaded
    function onload()
    {
        /*var clientname = document.getElementById('clientname');
        var codeclient = document.getElementById('codeclient');
        var datearrival = document.getElementById('datearrival');
        var newweight = document.getElementById('newweight');
        var newdimension = document.getElementById('newdimension');
        var newprices = document.getElementById('newprices');
        var codeparcel = document.getElementById('codeparcel');*/

        //alert(clientname);


        var printButton = document.getElementById('printButton');

        // prints the label
        printButton.onclick = function()
        {
            var clientname = $(".clientname").val();
            var codeclient = $(".codeclient").val();
            var datearrival = $(".datearrival").val();
            var newweight = $(".newweight").val();
            var newdimension = $(".newdimension").val();
            var newprices = $(".newprices").val();
            var codeparcel = $(".codeparcel").val();

            try
            {
                // open label
                var labelXml = '<?xml version="1.0" encoding="utf-8"?>\
                    <DieCutLabel Version="8.0" Units="twips">\
                        <PaperOrientation>Landscape</PaperOrientation>\
                        <Id>Address</Id>\
                        <PaperName>30252 Address</PaperName>\
                        <DrawCommands/>\
                        <ObjectInfo>\
                            <TextObject>\
                                <Name>Text</Name>\
                                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                <LinkedObjectName></LinkedObjectName>\
                                <Rotation>Rotation0</Rotation>\
                                <IsMirrored>False</IsMirrored>\
                                <IsVariable>True</IsVariable>\
                                <HorizontalAlignment>Left</HorizontalAlignment>\
                                <VerticalAlignment>Middle</VerticalAlignment>\
                                <TextFitMode>ShrinkToFit</TextFitMode>\
                                <UseFullFontHeight>True</UseFullFontHeight>\
                                <Verticalized>False</Verticalized>\
                                <StyledText/>\
                            </TextObject>\
                            <Bounds X="332" Y="150" Width="4455" Height="1260" />\
                        </ObjectInfo>\
                        <ObjectInfo>\
                            <BarcodeObject>\
                                <Name>BARCODE</Name>\
                                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                <LinkedObjectName></LinkedObjectName>\
                                <Rotation>Rotation0</Rotation>\
                                <IsMirrored>False</IsMirrored>\
                                <IsVariable>True</IsVariable>\
                                <Text></Text>\
                                <Type>Code39</Type>\
                                <Size>Medium</Size>\
                                <TextPosition>Bottom</TextPosition>\
                                <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" />\
                                <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" />\
                                <TextEmbedding>None</TextEmbedding>\
                                <ECLevel>0</ECLevel>\
                                <HorizontalAlignment>Center</HorizontalAlignment>\
                                <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" />\
                            </BarcodeObject>\
                            <Bounds X="331" Y="178" Width="4260" Height="420" />\
                        </ObjectInfo>\
                    </DieCutLabel>';
                var label = dymo.label.framework.openLabelXml(labelXml);

                // alert(clientname);

                // set label text
                label.setObjectText("Text", clientname);
                label.setObjectText("BARCODE", codeparcel);

                
                // select printer to print on
                // for simplicity sake just use the first LabelWriter printer
                var printers = dymo.label.framework.getPrinters();
                if (printers.length == 0)
                    throw "No DYMO printers are installed. Install DYMO printers.";

                var printerName = "";
                for (var i = 0; i < printers.length; ++i)
                {
                    var printer = printers[i];
                    if (printer.printerType == "LabelWriterPrinter")
                    {
                        printerName = printer.name;
                        break;
                    }
                }
                
                if (printerName == "")
                    throw "No LabelWriter printers found. Install LabelWriter printer";

                // finally print the label
                label.print(printerName);
            }
            catch(e)
            {
                alert(e.message || e);
            }
        }
    };

   function initTests()
	{
		if(dymo.label.framework.init)
		{
			//dymo.label.framework.trace = true;
			dymo.label.framework.init(onload);
		} else {
			onload();
		}
	}

	// register onload event
	if (window.addEventListener)
		window.addEventListener("load", initTests, false);
	else if (window.attachEvent)
		window.attachEvent("onload", initTests);
	else
		window.onload = initTests;

} ());