$(document).ready(function(){//start js

    var $formLogin = $('#login-form');
    var $formLost = $('#lost-form');
	var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;

	$('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister) });
    $('#register_login_btn').click( function () {
        modalAnimate($formRegister, $formLogin);
    });
    $('#login_lost_btn').click( function () {
        modalAnimate($formLogin, $formLost);
        $(".signup_title").hide();
        $(".rest_title").show();
        $(".resetmsg").hide();
        $(".tohide").show();
    });
    $('#lost_login_btn').click( function () {
        modalAnimate($formLost, $formLogin);
        $(".signup_title").show();
        $(".rest_title").hide();
    });
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });

    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
	}


    function msgFade ($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }

    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
  		}, $msgShowTime);
    }

    //
    $(".restpassword").on('click',function(){
        var proceed = true;
		$("#lost-form input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
            var url = $('meta[name="url"]').attr('content');
            var email = $("#lost_email").val();
            var data = "email="+email;
            $.ajax({
                type:'POST',
                url : url+'/reset',
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    if(resp.success == true){
                        $(".resetmsg").show();
                        $(".tohide").hide();
                        setTimeout(function() {
                            // location.reload();
                        }, 1000);
                    }
                }
            });
        }
    });
    //
    $(".signUp").on("click",function(){
        var proceed = true;
		$(".signup input[required=true] , .signup select[name='countrie_id']").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){

            var url = $('meta[name="url"]').attr('content');

            $.ajax({
                type:'POST',
                url : url+'/register',
                data:$(".signup").serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $(".regestererreur").html(" ");
                    if(resp.success == true){
                        $('#loginmodal').modal('hide');
                        $('#myModalSign').modal('hide');
                        $('#myModalSignSuccess').modal();
                    }else if(resp.success == false){
                        $(".regestererreur").append("<span class='label label-warning col-md-12'>Ø­Ø¯Ø« Ø®Ø·Ø§ Ø­Ø§ÙˆÙ„ Ø§Ù„ØªØ­Ù‚Ù‚ Ù…Ù† Ø§Ù„ÙƒØ§Ø¨ØªØ´Ø§</span>");
                    }else{
                        $(".regestererreur").text(val);
                    }
                },
                error: function (data) {
                    $("#reg_errors").html(" ");
                    var response = JSON.parse(data.responseText);
                    var errorString = '<ul>';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>';
                    $("#reg_errors").append(errorString);
                    console.log(errorString);
                }
            });
        }
    });


    $(".resetmypassword").on("click",function(){
        var proceed = true;
		$(".resetmypasswords input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){

            var url = $('meta[name="url"]').attr('content');

            $.ajax({
                type:'POST',
                url : url+'/changepassword',
                data:$(".resetmypasswords").serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(data){
                    $("#reset_errors").html(" ");
                    $("#reset_success").show();
                    $(".tohide").hide();
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                },
                error: function (data) {
                    $("#reset_errors").html(" ");
                    $("#reset_success").hide();
                    var response = JSON.parse(data.responseText);
                    var errorString = '<ul>';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>';
                    $("#reset_errors").append(errorString);
                }
            });
        }
    });

    //login
    $(".signin").on("submit",function(e){
        e.preventDefault();
        var lg_username = $('.login_username').val();
        var lg_password = $('.login_password').val();
        var lg_position = $(".position").val();
        var url = $('meta[name="url"]').attr('content');
        if(lg_username && lg_password){
            var data = "lg_username="+lg_username+"&lg_password="+lg_password;
            $.ajax({
                type:'POST',
                url : url+'/login',
                data:$(".signin").serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $(".loginerreur").hide();
                    $(".logincaptcha").hide();
                    if(lg_position == 1){
                        if(resp.success == true){
                            $("#loginmodal").modal('toggle');
                            $(".directhide").hide();
                            $(".directshow").show();
                            $(".submit_order_prd").trigger('click');
                        }else if(resp.error == true){
                            $(".logincaptcha").show();
                            //grecaptcha.reset();
                        }else{
                            $(".loginerreur").show();
                        }
                    }else{
                        if(resp.success == true){
                            window.location.href = "account";
                        }else if(resp.error == true){
                            $(".logincaptcha").show();
                        }else{
                            $(".loginerreur").show();
                        }
                    }
                }
            });
        }
    });


    $(".registerform").on("click",function(e){
        e.preventDefault();
        var proceed = true;
		$(".registerforms input[required=true] , .registerforms select[name='countrie_id']").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
            var url = $('meta[name="url"]').attr('content');
            $.ajax({
                type:'POST',
                url : url+'/register',
                data:$(".registerforms").serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $("#reg_errors").html(" ");
                    $('#myModalSignSuccess').modal();
                },
                error:function(resp){
                    $("#reg_errors").html(" ");
                    var response = JSON.parse(resp.responseText);
                    var errorString = '<ul>';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>';
                    $("#reg_errors").append(errorString);
                    console.log(errorString);
                }
            });
        }
    });

    $(".calculer").on("click",function(){
        var measure      =   $("input[name='weight_measure']:checked").val();
        var country      =   $('.countrie_id').val();
        var ship_country =   $('.ship_country').val();
        var weight       =   $('.weight').val();
        var width        =   $('.width').val();
        var lenght       =   $('.length').val();
        var height       =   $('.height').val();
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');

        if(country && weight>0 && ship_country && country){
            var data="country="+country+"&weight="+weight+"&width="+width+"&lenght="+lenght+"&height="+height+"&measure="+measure+"&ship_country="+ship_country;
            $.ajax({
                type:"POST",
                url:  url+"/calculer",
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    if(response.success === false){
                        //alert(response.maxweight)
                        $(".maxweight").show();
                        $(".directship").hide();
                        $(".n_weight").text(response.maxweight);
                        $("#myModalNotification").modal();
                    }else if(response.error === true){
                        if(baseurl === 'ar'){
                            alert('عفوا، نحن لانشحن الى الدولة التي اخترتها');
                        }else{
                            alert("Sorry, we do not ship to the country you selected.");
                        }
                    }else{
                        $(".showTable").show();
                        $(".result_resp").html(" ");
                        $(".table_weight").html(" ");
                        $(".result_resp").append(response.html);
                        $(".table_weight").append(response.html2);

                    }
                }
            });
        }else{
            $(".weight").css('border-color','red');
        }
    });

    //
    $(".account_calculer").on("click",function(){
        var measure =   $("input[name='weight_measure']:checked").val();
        var country =   $('.countrie_id').val();
        var ship_country =   $('.ship_country').val();
        var weight  =   $('.weight').val();
        var width   =   $('.width').val();
        var lenght  =   $('.length').val();
        var height  =   $('.height').val();
        var url     = $('meta[name="url"]').attr('content');

        var data="country="+country+"&weight="+weight+"&width="+width+"&lenght="+lenght+"&height="+height+"&measure="+measure+"&ship_country="+ship_country;
        //alert(data)
        if(ship_country && country && weight && country){
            $.ajax({
                type:"POST",
                url:  url+"/acc_calculer",
                data:data,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
                    if(response.success === false){
                        //alert(response.maxweight)
                        $(".maxweight").show();
                        $(".n_weight").text(response.maxweight);
                        $("#myModalNotification").modal();
                    }else{
                        $(".showTable").show();
                        $(".result_resp").html(" ");
                        $(".table_weight").html(" ");
                        $(".result_resp").append(response.html);
                        $(".table_weight").append(response.html2);
                    }
                }
            });
        }
    });


    //new address
    $(".newaddress").on("click",function(){
        var proceed = true;
		$(".new_address input[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
        if(proceed){
            var url = $('meta[name="url"]').attr('content');

            $.ajax({
                type:'POST',
                url : url+'/newaddress',
                data:$(".new_address").serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(resp){
                    $(".regestererreur").html(" ");
                    if(resp.success === true){
                        $(".regestererreur").append("<span class='label label-success col-md-12'تمت اضافة عنوان الشحن بنجاح</span>");
                        setTimeout(function() {
                            location.reload();
                            /*swal({
                                title: "تمت العملية",
                                text: "تم تنفيد العلمية المطلوية بنجاح",
                                type: "success"
                            }, function() {
                                location.reload();
                            });*/
                        }, 1000);
                    }
                }
            });
        }
    });


    //
    var next = 1;
    $(".addmore").click(function(e){
        e.preventDefault();
        var baseurl = $('meta[name="baseurl"]').attr('content');
        //alert(baseurl);
        if(baseurl === 'ar'){
            weight ="وزن الشحنة";
            length = "الطول";
            width  = "العرض";
            height = "الارتفاع";
        }else{

            weight = "Package Weight";
            length = "Length";
            width  = "Width";
            height = "Height";

        }
        next = next + 1;
        var newIns = '<div class="row rows  '+next+'"> <div class="col-md-3 form-group"> <input type="number" name="weight" class="form-control weight" placeholder="'+weight+'" required="true"> </div> <div class="col-md-3 form-group"> <input type="number" name="length" class="form-control length" placeholder="'+length+'" required> </div> <div class="col-md-3 form-group"> <input type="number" name="width" class="form-control width" placeholder="'+width+'" required> </div> <div class="col-md-2 form-group"> <input type="number" class="form-control height" name="height" placeholder="'+height+'" required> </div> <div class="col-md-1 form-group"> <a href="javasript:void()" id="'+next+'" class="btn btn-danger btn-sm removeme"> <i class="fas fa-trash-alt"></i> </a> </div> </div>';
        $(".field").append(newIns);
        $("#count").val(next);
    });

     //delete
    $('.field').on("click"," .removeme",function(e){
        var id = $(this).attr('id');
        var count = $("#count").val();
        var cc = count - 1;
        $("."+id).remove();
        $("#count").val(cc);
    });


    $(".getcompany").on("click",function(){
            var count = $("#count").val();
            var ship_country = $(".ship_country").val();
            var countrie_id = $(".countrie_id").val();
            var countrie_name = $(".countrie_id").find(':selected').attr('data-id');
            var url = $('meta[name="url"]').attr('content');

            var weight = 0 ; var length = 0 ; var width = 0 ; var height=0;
            $('.rows').each(function(index,element){
                id = index + 1;
                weights = $("." + id + " input[name='weight']").val();
                if(weights){
                    weight = weight + parseFloat($("." + id + " input[name='weight']").val());
                    length = length + parseFloat($("." + id + " input[name='length']").val());
                    width  = width  + parseFloat($("." + id + " input[name='width']").val());
                    height = height + parseFloat($("." + id + " input[name='height']").val());
                }
            });
            if(!length && !width && !height){
                height = 1;
                width = 1;
                length = 1;
            }
            var measure  = $("input[name='weight_measure']:checked").val();
            var baseurl = $('meta[name="baseurl"]').attr('content');
            if(baseurl === 'ar'){
                content = "محتوى الشحنة";
                value= "قيمة الشحنة";
            }else{
                content = "Shipment content";
                value   = "Shipment vlaue";
            }
            //var data = "ship_country="+ship_country+"&countrie_id="+countrie_id+"&weight="+weight+"&length="+length+"&width="+width+"&height="+height+"&measure="+measure;
            if(count>=1){
                $(".show_content").html(" ");
                for(i=0; i<count; i++){
                    ls = i + 1;
                    var lp = '<div class="row col-md-12 rows '+ls+'"> <div class="col-xs-12 col-md-6 form-group"> <label> '+content+' '+ls+' </label> <input type="text" class="form-control" placeholder="'+content+' '+ls+' " name="content"/> </div> <div class="col-xs-12 col-md-6 form-group"> <label> '+value+' '+ls+' </label> <input type="number" class="form-control" placeholder="'+value+' '+ls+'" name="value"/> </div> </div>';
                    $(".show_content").append(lp);
                }
            }
            if(ship_country && countrie_id && weight && count){
                $(".firststep").hide();
                $(".secondstep").show();
                $(".countrie").val(ship_country);
                //$(".country_collect").val(countrie_name);
                /*$.ajax({
                    type:'POST',
                    url : url+'/getdata',
                    data:data,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(resp){
                        if(resp.success === false){
                            $(".maxweight").hide();
                            $(".directship").show();
                            $("#myModalNotification").modal();
                        }else{
                            $(".countrie").val("");
                            $(".country_collect").val("");
                            $(".countrie").val(ship_country);
                            $(".country_collect").val(countrie_name);
                            $(".getCompany_to_direct").html(" ");
                            $(".getCompany_to_direct").append(resp.html);
                            $(".firststep").hide();
                            $(".secondstep").show();
                        }
                    }
                });*/
            }else{
                $(".firststep input[required=true] , select[required=true]").each(function(){
                    $(this).css('border-color','');
                    if(!$.trim($(this).val())){ //if this field is empty
                        $(this).css('border-color','red'); //change border color to red
                    }
                });
            }
    });

    //

    $(".submit_order_prd").on("click",function(){
        var terms  = $("input[name='terms_prd']:checked").val();
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var someJSON = [];
        process = true;
        if(terms){
            $('.rows').each(function(index,element){
                id = index + 1;
                weight = $("." + id + " input[name='weight']").val();
                if(weight){
                    var l = {
                        "weight" : $("." + id + " input[name='weight']").val(),
                        "length" : $("." + id + " input[name='length']").val(),
                        "width" : $("." + id + " input[name='width']").val(),
                        "height" : $("." + id + " input[name='height']").val(),
                        "content" : $("." + id + " input[name='content']").val(),
                        "value" : $("." + id + " input[name='value']").val(),
                    }
                    someJSON.push(l);
                }
            });

            //var companyid  = $("input[name='companyid']:checked").val();
            //var pricetopay = $("."+companyid+" td > .pricetopay").val();
            /*if(!companyid){
                if(baseurl === 'ar'){
                    alert("يرجي اخيتار شركة الشحن المطلوبة من خيارات الشحن المتوفرة ");
                }else{
                    alert("Please choose the required shipping company from the available shipping options");
                }
                return
            }*/

            $(".secondstep input[required=true]").each(function(){
                $(this).css('border-color','');
                if(!$.trim($(this).val())){ //if this field is empty
                    $(this).css('border-color','red'); //change border color to red
                    process = false;
                }
            });
            if(process){
                $(this).attr('disabled',true);
                var firstname_collect = $("input[name='firstname_collect']").val();
                var address_collect = $("input[name='address_collect']").val();
                var state_collect = $("input[name='state_collect']").val();
                var lastname_collect = $("input[name='lastname_collect']").val();
                var city_collect = $("input[name='city_collect']").val();
                var zipcode_collect = $("input[name='zipcode_collect']").val();
                var phone_collect = $("input[name='phone_collect']").val();
                var ship_country = $(".ship_country").val();

                /*var firstname = $("input[name='firstname']").val();
                var address = $("input[name='address']").val();
                var state = $("input[name='state']").val();
                var lastname = $("input[name='lastname']").val();
                var city = $("input[name='city']").val();
                var zipcode = $("input[name='zipcode']").val();
                var phone = $("input[name='phone']").val();
                var countrie_id = $(".countrie_id").val();
                var address_email = $("input[name='address_email']").val();*/

                var data = {
                    "firstname_collect":firstname_collect,
                    "address_collect":address_collect,
                    "state_collect":state_collect,
                    "lastname_collect":lastname_collect,
                    "city_collect":city_collect,
                    "zipcode_collect":zipcode_collect,
                    "phone_collect":phone_collect,
                    "ship_country":ship_country,
                    /*"firstname":firstname,
                    "address":address,
                    "state":state,
                    "lastname":lastname,
                    "city":city,
                    "zipcode":zipcode,
                    "phone":phone,
                    "address_email":address_email,
                    "countrie_id":countrie_id,
                    "companyid":companyid,
                    "pricetopay":pricetopay*/
                }
                $.ajax({
                    type :"POST",
                    url : url+"/add_order",
                    data:{data_array: JSON.stringify(someJSON),data_array2:JSON.stringify(data)},
                    cache:false,
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(response){
                        if(response.success == true){
                            if(baseurl === 'ar'){
                                swal({
                                    title: "شكرا لك",
                                    text: "سيتم تحويلك لصفحة الدفع لاكمال عملية الطلب ",
                                    type: "success",
                                    confirmButtonText: "موافق",
                                }, function() {
                                    window.location = "shipement"
                                });
                            }else{
                                swal({
                                    title: "Thank You",
                                    text: "You will be redirect to a payment page to complete the order",
                                    type: "success",
                                    confirmButtonText: "OK",
                                }, function() {
                                    window.location = "shipement"
                                });
                            }
                        }
                    }
                });

            }
        }else{
            if(baseurl === 'ar'){
                alert("لتقديم الطلب يجب اولا الموافقة على الشروط و الاحكام");
            }else{
                alert("To submit order, you must first agree to the terms and conditions");
            }
        }
    });

    //contact

    $(".sendemail").on("submit",function(e){
        e.preventDefault();
        var url = $('meta[name="url"]').attr('content');
        $.ajax({
            type:'POST',
            url : url+'/contact',
            data:$(".sendemail").serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success:function(resp){
                $(".contactsuccess").show();
                $(".contactrerreur").hide();
            },
            error:function(resp){
                $(".contactrerreur").html("");
                var response = JSON.parse(resp.responseText);
                var errorString = '<ul>';
                $.each( response.errors, function( key, value) {
                    errorString += '<li>' + value + '</li>';
                });
                errorString += '</ul>';
                $(".contactrerreur").append(errorString);
            }
        });
    });
});
