$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $("#btnfastbuy").on("click", function() {
        var a = !0;
        var baseurl = $('meta[name="baseurl"]').attr('content');
        if ($("#fastbuys input[required=true]").each(function() {
                $(this).css("border-color", ""), $.trim($(this).val()) || ($(this).css("border-color", "red"), a = !1)
            }), a)
		{
            var c = $("#product").val(),
                d = $("#url").val(),
                e = parseInt($("#count").val()),
                f = parseFloat(($("#price").val()).trim()),
                g = $("#description").val(),
                x = $("#currency").val(),

                h = parseInt($("#countprodact").val());
                taux = 0;
                s = parseFloat($(".price_r").val()),
                nb = parseInt($(".product-list tr").last().attr("class")), nb ? nb += 1 : nb = 1,
                ns = parseFloat(f * e),
                nt = ns * (1 + taux),
                t = parseFloat(parseFloat(nt) + s).toFixed(1);
                if(x === "$"){
                    if(baseurl == 'ar'){
                        xc = "دولار";
                    }else{
                        xc = "$";
                    }
                }else{
                    if(baseurl == 'ar'){
                        xc = "ريال";
                    } else {
                        xc = "SAR";
                    }
                }
                if(baseurl == 'ar'){ view = "مشاهدة" ;}else{ view = "View"; }
            n = "<tr  class=" + nb + "><td class='product'>"+c+"</td><td class='url'><a href='" + d + "' id='" + d + "' target='_blank' class='url_product'>"+view+"</a></td><td class='count'>"+e+"<input type='hidden' class='description' value='"+g+"'/> </td><td class='price'>" + f * e + xc +"<input type='hidden' class='currency' value='" + x + "'/><input type='hidden' class='price_product' value='" + f * e + "'/><input type='hidden' class='priceproduct' value='" + f + "'/></td><td class='infor'>"+g+"</td><td><a href='#' id=" + nb + " class='discard-btn pointer'><i class='fas fa-trash-alt'></i></a></td></tr>",
			$(".shopping-section tbody").append(n),

			$("#product").val(""),
			$("#url").val(""),
			$("#count").val(""),
			$("#price").val(""),
            $("#description").val(""),

            $(".price_r").val(t), //dollar
            $("#price_usd").val(t), //dollar
            $(".price_usd_val").text(t),//dollar
			$("#countprodact").val(nb)
            $(".btn_submit_order").show();
		}
    });

    $(".shopping-section").on("click", ".discard-btn", function() {
        var a = $(this).attr("id"),
            b = $("#price_r").val(),
            h = parseInt($("#countprodact").val()),
            c = $(".shopping-section tbody tr." + a + " .price_product").val(),
            d = parseFloat(parseFloat(b) - parseFloat(c)).toFixed(1);
            nb = h - 1;
            //alert(d),
			$("#price_usd").val(d),
            $(".price_r").val(d),
            $(".price_usd_val").text(d),
            $(".shopping-section tbody tr." + a).remove(),
            $("#countprodact").val(nb);
            if(nb<1){
                $(".btn_submit_order").hide();
            }
    });

    //
    $(".btn_submit_order").on("click",function(e){
        e.preventDefault();
        var	c = parseInt($("#countprodact").val());
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var someJSON = [];
        if(c>0){
            //$(this).attr("disabled",true);
            $(".loading").show();
            for(i = 1; i <= c; i++){
                var f = $(".shopping-section tbody tr." + i + " .product").text();
                var o = $(".shopping-section tbody tr." + i + " .url_product").attr("id");
                var p = $(".shopping-section tbody tr." + i + " .count").text();
                var q = parseFloat($(".shopping-section tbody tr." + i + " .priceproduct").val());
                var u = $(".shopping-section tbody tr." + i + " .description").val();
                var ux = $(".shopping-section tbody tr." + i + " .currency").val();
                var l = {
                    "myproduct" : f ,
                    "myurl_product" : o ,
                    "mycount" : p ,
                    "myprice_product" : q ,
                    "currency": ux,
                    "mydescription" : u
                };
                someJSON.push(l);
            }
            $.ajax({
                type: "POST",
                url: url+"/purchase/create",
                data: {data_array: JSON.stringify(someJSON)},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(resp) {
                    if(resp.success == true){
                        $(".loading").hide();
                        if(baseurl === "ar"){
                            setTimeout(function() {
                                swal({
                                    title: "شكرا لك",
                                    text: "تم استقبال طلبك بنجاح وستتم مراجعته خلال ٤٨ ساعه",
                                    type: "success",
                                    confirmButtonText: "موافق",
                                }, function() {
                                    window.location = "waiting"
                                });
                            });
                        }else{
                            setTimeout(function() {
                                swal({
                                    title: "Thank You",
                                    text: "Your order has been successfully received and will be reviewed within 48 hours",
                                    type: "success",
                                    confirmButtonText: "OK",
                                }, function() {
                                    window.location = "waiting"
                                });
                            });
                        }

                    }
                }
            });
        }
    });

    $(".table").on("click"," .showInfo",function(){
        var product_id = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var data = "product_id="+product_id;
        $.ajax({
            type :"POST",
            url : url+"/showtracking",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".showData").html(" ");
                $(".showData").append(response.html);
                $("#myModalShowTracking").modal();
            }
        });
    });
    //$(".ship_select").hide();
    /*$(".shipcountry").on("change",function(){
        var shipcountry = $(this).val();
        var url = $('meta[name="url"]').attr('content');
        var data = "shipcountry="+shipcountry;
        $.ajax({
            type :"POST",
            url : url+"/getproduct",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".resp_product").html(" ");
                $(".resp_product").append(response.html);
                $(".ship_select").show();
            }
        });
    });*/

    $(".checkAll_usa").change(function () {
        $(".customcontrolinput_usa").prop('checked', $(this).prop("checked"));
        if($(this).prop("checked")){
            $(this).val("usa");
        }else{
            $(this).val("");
        }
        $(".customcontrolinput_sa").prop('checked', false);
        $(".checkAll_sa").prop('checked',false);
        $(".customcontrolinput_uae").prop('checked', false);
        $(".checkAll_uae").prop('checked',false);
    });
    $(".checkAll_sa").change(function () {
        $(".customcontrolinput_sa").prop('checked', $(this).prop("checked"));
        if($(this).prop("checked")){
            $(this).val("sa");
        }else{
            $(this).val("");
        }
        $(".customcontrolinput_usa").prop('checked', false);
        $(".checkAll_usa").prop('checked',false);
        $(".customcontrolinput_uae").prop('checked', false);
        $(".checkAll_uae").prop('checked',false);
    });
    $(".checkAll_uae").change(function () {
        $(".customcontrolinput_uae").prop('checked', $(this).prop("checked"));
        if($(this).prop("checked")){
            $(this).val("uae");
        }else{
            $(this).val("");
        }
        $(".customcontrolinput_sa").prop('checked', false);
        $(".checkAll_sa").prop('checked',false);
        $(".customcontrolinput_usa").prop('checked', false);
        $(".checkAll_usa").prop('checked',false);
    });
    $(".warhouse").on("click",function(){
        var warhouse = $(this).attr('id');
        $(".address_country").attr('data',warhouse);
        $(".address_country").val(" ");
        $(".shipping_btn").hide();
    });
    $(".shipping_btn").hide();
    $(".address_country").on("change",function(){
        var address_country = $(this).val();
        //var shipcountry     = $(".shipcountry").val();
        var warhouse =   ($(this).attr('data')).trim();
        //alert(warhouse)
        var url = $('meta[name="url"]').attr('content');
        if(address_country && warhouse){
            if($('.customcontrolinput_'+warhouse+':checked').val()){
                var total = 0;
                var proceed = true;
                $('.customcontrolinput_'+warhouse+':checked').each(function(index,element){
                    if(element.value !='all_'+warhouse+''){
                        price = $("."+element.value+" .product_price").val();
                        total = total + parseFloat(price);
                        if(!price){
                            $("."+element.value+" .product_price").css('border-color','red');
                            proceed = false;
                        }
                    }
                });
                if(proceed){
                    var data = "address_country="+address_country+"&shipcountry="+warhouse;
                    $.ajax({
                        type :"POST",
                        url : url+"/getcompany",
                        data:data,
                        cache:false,
                        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function(response){
                            $(".show_company").html(" ");
                            $(".show_company").append(response.html);
                            $(".total_purchase").val(total);
                            $(".shipping_btn").show();
                        }
                    });
                }else{
                    $(".address_country").val(" ");
                    $(".error_price").show();
                    $(".error_product").hide();
                    $("#myModalNotification").modal();
                }
            }else{
                $(".address_country").val(" ");
                $(".error_price").hide();
                $(".error_product").show();
                $("#myModalNotification").modal();
            }
        }
    });
    $(".forms").hide();
    $(".shipping_btn").on("click",function(){
        $(".forms").show();
        $(".ship_process").hide();
    });

    $(".submit_order").on("click",function(){
        var terms = $("input[name='terms']:checked").val();
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var someJSON = [];
        var newJSON = [];
        if(terms){

            var information    = $(".information").val();
            var total_purchase = $(".total_purchase").val();
            var modpacking     = $("input[name='modpacking']:checked").val();
            var serviceid      = $("input[name='serviceid']:checked").val();
            var companyid      = $("input[name='companyid']:checked").val();

            var address_country = $(".address_country").val();

            var shipcountry  = ($(".address_country").attr('data')).trim();
            //alert(shipcountry);
            if(!shipcountry){
                alert("No");
                return
            }
            if(!companyid){
                $(".error_service").hide();
                $(".error_packing").hide();
                $(".error_terms").hide();
                $(".error_company").show();
                $("#myModalNotification_packing").modal();
                return
            }

            if(!modpacking){
                $(".error_service").hide();
                $(".error_packing").show();
                $(".error_terms").hide();
                $(".error_company").hide();
                $("#myModalNotification_packing").modal();
                return
            }

            /*if(!serviceid){
                $(".error_service").show();
                $(".error_packing").hide();
                $(".error_terms").hide();
                $(".error_company").hide();
                $("#myModalNotification_packing").modal();
                return
            }*/
                $(this).attr("disabled",true);
                $(".loading").show();
                data = {
                    "information":information,
                    "total_purchase":total_purchase,
                    "modpacking":modpacking,
                    "companyid":companyid,
                    "address_country":address_country,
                    "shipcountry":shipcountry
                }
                $('.customcontrolinput_'+shipcountry+':checked').each(function(index,element){
                    if(element.value !='all_'+shipcountry+''){
                        productprice = $("."+element.value+" .product_price").val();
                        productID    = element.value;
                        var l = {
                            "productid" : productID,
                            "product_price" : productprice
                        }
                        someJSON.push(l);
                    }
                });

                $("input[name='serviceid']:checked").each(function(index,element){
                    var x = {
                        "serviceid" : element.value,
                    }
                    newJSON.push(x);
                });

                $.ajax({
                    type :"POST",
                    url : url+"/save_order",
                    data:{data_array: JSON.stringify(someJSON),data_array2:JSON.stringify(data),data_array3:JSON.stringify(newJSON)},
                    cache:false,
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(response){
                        if(response.success == true){
                            $(".loading").hide();
                            if(baseurl === "ar"){
                                setTimeout(function() {
                                    swal({
                                        title: "شكرا لك",
                                        text: "تم استقبال طلبك بنجاح وستتم مراجعته خلال ٤٨ ساعه",
                                        type: "success",
                                        confirmButtonText: "موافق",
                                    }, function() {
                                        window.location = "processing"
                                    });
                                });
                            }else{

                                setTimeout(function() {
                                    swal({
                                        title: "Thank you",
                                        text: "Your order has been successfully received and will be reviewed within 48 hours",
                                        type: "success",
                                        confirmButtonText: "OK",
                                    }, function() {
                                        window.location = "processing"
                                    });
                                });

                            }

                        }
                    }
                });
        }else{
            $(".error_service").hide();
            $(".error_packing").hide();
            $(".error_terms").show();
            $(".error_company").hide();
            $("#myModalNotification_packing").modal();
        }
    });

    //
    $(".table").on("click",'.getlist',function(){
        var pack = $(this).attr('id');
        var url = $('meta[name="url"]').attr('content');
        var data ="packing_id="+pack;
        $.ajax({
            type : "POST",
            url  : url+"/processing",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".getlist_s").html(" ");
                $(".getlist_s").append(response.html);
                $("#myModalShowProduct").modal();
            }
        });
    });

    //getlist_prd
    $(".table").on("click",'.getlist_prd',function(){
        var pack = $(this).attr('id');
        var url = $('meta[name="url"]').attr('content');
        var data ="shipping_id="+pack;
        $.ajax({
            type : "POST",
            url  : url+"/getlist_prd",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".getlist_prods").html(" ");
                $(".getlist_prods").append(response.html);
                $("#myModalShowProduct_2").modal();
            }
        });
    });
    //


    //
    $(".table").on("click"," .Unpacking", function(){
        var packing_id = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var data ="packing_id="+packing_id;
        $.ajax({
            type:"POST",
            url : url+"/unpacking",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    if(baseurl === "ar"){

                        setTimeout(function() {
                            swal({
                                title: "تمت العملية",
                                text: "تم فك تجهيز الشحنة بنجاح و اعادتها للمستودع",
                                type: "success",
                                confirmButtonText: "موافق",
                            }, function() {
                                window.location = "product"
                            });
                        });

                    }else{

                        setTimeout(function() {
                            swal({
                                title: "Done",
                                text: "The consignment was successfully dismantled and returned to the warehouse",
                                type: "success",
                                confirmButtonText: "OK",
                            }, function() {
                                window.location = "product"
                            });
                        });

                    }
                }
            }
        });
    });

    //
    $(".table").on("click"," .moreInfo", function(){
        var packing_id = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var data ="packing_id="+packing_id;
        if(packing_id){
            $.ajax({
                type : "POST",
                url  : url+"/moreinfo",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".more_Info_detail").html("");
                    $(".more_Info_detail").append(response.html);
                    $("#myModalMoreInfo").modal();
                }
            });
        }
    });

    //
    $(".submit_orderprd").on("click",function(){
        var terms    = $("input[name='terms_prd']:checked").val();
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        //var someJSON = [];
        if(terms){
            if($('.customcontrolinput:checked').val()){
                if($('.customcontrolinput_service:checked').val()){

                    var information    = $(".information_prd").val();
                    //var service_select = $('.customcontrolinput_service:checked').serialize();
                    var product_id     = $('.customcontrolinput:checked').val();
                    //var data = 'information='+information+'&product_id='+product_id;
                    $.ajax({
                        type : "POST",
                        url  : url+"/moreimage",
                        data:$('.customcontrolinput_service:checked').serialize()+'&information='+information+'&product_id='+product_id,
                        cache:false,
                        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function(response){
                            if(response.success == true){
                                if(baseurl === "ar"){
                                    setTimeout(function() {
                                        swal({
                                            title: "شكرا لك",
                                            text: "تم استقبال طلبك بنجاح وستتم مراجعته خلال ٤٨ ساعه",
                                            type: "success",
                                            confirmButtonText: "موافق",
                                        }, function() {
                                            window.location= "inprocess"
                                        });
                                    });
                                }else{
                                    setTimeout(function() {
                                        swal({
                                            title: "Thank You",
                                            text: "Your order has been successfully received and will be reviewed within 48 hours",
                                            type: "success",
                                            confirmButtonText: "OK",
                                        }, function() {
                                            window.location= "inprocess"
                                        });
                                    });
                                }
                            }
                        }
                    });
                }else{
                    $(".error_prd_select").hide();
                    $(".error_prd_service").show();
                    $(".error_prd_return").hide();
                    $("#myModalNotification_service").modal();
                }
            }else{
                $(".error_prd_select").show();
                $(".error_prd_service").hide();
                $(".error_prd_return").hide();
                $("#myModalNotification_service").modal();
            }
        }else{
            $(".error_service").hide();
            $(".error_packing").hide();
            $(".error_terms").show();
            $(".error_company").hide();
            $("#myModalNotification_packing").modal();
        }
    });


    //
    $(".submit_order_return").on("click",function(){
        var terms = $("input[name='terms_return']:checked").val();
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        if(terms){
            var product_id = $('.customcontrolinput:checked').val();
            var seller_address    = $("#seller_address").val();
            var return_info       = $("#return_info").val();

            if(!product_id){
                $(".error_prd_select").hide();
                $(".error_prd_service").hide();
                $(".error_prd_return").show();
                $("#myModalNotification_service").modal();
                return
            }
            if(!seller_address){
                $("#seller_address").css('border-color','red');
                return
            }else{
                $("#seller_address").css('border-color','green');
            }
            if(!return_info){
                $("#return_info").css('border-color','red');
                return
            }else{
                $("#return_info").css('border-color','green');
            }
            var data = "product_id="+product_id+"&seller_address="+seller_address+"&return_info="+return_info;
            $(this).attr("disabled",true);
            $(".loading").show();

            $.ajax({
                type :"POST",
                url : url+"/submit_return",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success == true){
                        $(".loading").hide();
                        if(baseurl === "ar"){
                            setTimeout(function() {
                                swal({
                                    title: "شكرا لك",
                                    text: "تم استقبال طلبك بنجاح وستتم مراجعته خلال ٤٨ ساعه",
                                    type: "success",
                                    confirmButtonText: "موافق",
                                }, function() {
                                    window.location = "waitreturn"
                                });
                            });
                        }else{
                            setTimeout(function() {
                                swal({
                                    title: "Thank You",
                                    text: "Your order has been successfully received and will be reviewed within 48 hours",
                                    type: "success",
                                    confirmButtonText: "OK",
                                }, function() {
                                    window.location = "waitreturn"
                                });
                            });

                        }
                    }
                }
            });

        }else{
            $(".error_service").hide();
            $(".error_packing").hide();
            $(".error_terms").show();
            $(".error_company").hide();
            $("#myModalNotification_packing").modal();
        }
    });

    $("input[name='modepay']").on("change",function(){
        var modepay = $(this).val();
        if(modepay === "visa"){
            $(".showriyal").show();
            $(".showdollar").hide();
        }else{
            $(".showdollar").show();
            $(".showriyal").hide();
        }
    });


    $(".table").on("click"," .detail_ship",function(){
        var id_ship = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var data = "id_ship="+id_ship;
        $.ajax({
            type :"POST",
            url : url+"/detail_ship",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                $(".getlist_directShip").html("");
                $(".getlist_directShip").append(response.html);
                $("#myModalShowDirectShip").modal();
            }
        });
    });

    $(".table").on("click"," .cancel_ship",function(){
        var id_ship = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var data = "id_ship="+id_ship;
        $.ajax({
            type :"POST",
            url : url+"/cancel_ship",
            data:data,
            cache:false,
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(response){
                if(response.success == true){
                    if(baseurl === "ar"){
                        setTimeout(function() {
                            swal({
                                title: "تمت العملية ",
                                text: "تمت ازلة طلب الشحن المباشر من حسابك ",
                                type: "success",
                                confirmButtonText: "اوكي",
                            }, function() {
                                window.location.reload();
                            });
                        });
                    }else{

                        setTimeout(function() {
                            swal({
                                title: "Done",
                                text: "The direct shipping request has been removed from your account",
                                type: "success",
                                confirmButtonText: "OK",
                            }, function() {
                                window.location.reload();
                            });
                        });

                    }

                }
            }
        });
    });

    $(".table").on("click",'.showOrders',function(){
        var transid = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var data = "transid="+transid;
        if(transid){
            $.ajax({
                type :"POST",
                url : url+"/showOrders",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $(".getlist_details").html("");
                    $(".getlist_details").append(response.html);
                    $("#myModalShowDetail").modal();
                }
            });
        }
    });

    //
    $(".table").on("click"," .changeInfo", function(){
        var packing_id = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var data ="packing_id="+packing_id;
        if(packing_id){
            $.ajax({
                type : "POST",
                url  : url+"/changeInfo",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){

                    $(".more_changeInfo").html("");
                    $(".more_changeInfo").append(response.html);
                    $("#myModalChangeInfo").modal();

                    $(".mode_shipping").on("change",function(){
                        var companyid  = $(this).val();
                        var packing_id = $(".packing_id").val();
                        var url = $('meta[name="url"]').attr('content');
                        var data = "companyid="+companyid+"&packing_id="+packing_id;
                        if(companyid && packing_id){
                            $.ajax({
                                type :"POST",
                                url : url+"/mode_shipping",
                                data:data,
                                cache:false,
                                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                success: function(response){
                                    if(response.success === true){
                                        $(".ship_time").html();
                                        $(".ship_time").val(response.datedelever);
                                        $(".ship_price").html();
                                        $(".ship_price").val(response.priceTotal);
                                    }
                                }
                            });
                        }
                    });

                    $(".update_pack_price").on("click",function(){
                        var companyid  = $(".mode_shipping").val();
                        var packing_id = $(".packing_id").val();
                        var url = $('meta[name="url"]').attr('content');
                        var baseurl = $('meta[name="baseurl"]').attr('content');
                        var data = "companyid="+companyid+"&packing_id="+packing_id;
                        //alert(data);
                        if(companyid && packing_id){
                            $.ajax({
                                type :"POST",
                                url : url+"/update_packprice",
                                data:data,
                                cache:false,
                                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                success: function(response){
                                    if(response.success === true){
                                        $("#myModalChangeInfo").modal('toggle');
                                        if(baseurl === "ar"){
                                            setTimeout(function() {
                                                swal({
                                                    title: "تمت العملية ",
                                                    text: "تم تحديث شركة الشحن لهده الشحنة ",
                                                    type: "success",
                                                    confirmButtonText: "اوكي",
                                                }, function() {
                                                    window.location.reload();
                                                });
                                            });
                                        }else{

                                            setTimeout(function() {
                                                swal({
                                                    title: "Done",
                                                    text: "The shipping company has been updated for this shipment",
                                                    type: "success",
                                                    confirmButtonText: "OK",
                                                }, function() {
                                                    window.location.reload();
                                                });
                                            });

                                        }
                                    }
                                }
                            });
                        }
                    });



                }
            });
        }
    });


    //cancel_purchase
    $(".cancel_purchase").on("click",function(){
        var purchase_id = $(this).attr("id");
        var url = $('meta[name="url"]').attr('content');
        var baseurl = $('meta[name="baseurl"]').attr('content');
        var data ="purchase_id="+purchase_id;
        if(purchase_id){
            $.ajax({
                type :"POST",
                url : url+"/cancel_purchase",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        if(baseurl === "ar"){
                            setTimeout(function() {
                                swal({
                                    title: "تمت العملية ",
                                    text: "تم الغاء الطلب المطلوب ",
                                    type: "success",
                                    confirmButtonText: "اوكي",
                                }, function() {
                                    window.location.reload();
                                });
                            });
                        }else{

                            setTimeout(function() {
                                swal({
                                    title: "Done",
                                    text: "The order request has been canceled",
                                    type: "success",
                                    confirmButtonText: "OK",
                                }, function() {
                                    window.location.reload();
                                });
                            });

                        }
                    }
                }
            });
        }
    });

    $(".allow_numeric").on("input", function(evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57))
        {
          evt.preventDefault();
        }
    });

    $(".allow_decimal").on("input", function(evt) {
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
        {
          evt.preventDefault();
        }
    });


    $(".assurance").on("click",function(){
        var rowid = $(this).attr("data");
        var url = $('meta[name="url"]').attr('content');
        var assurance = parseFloat($(this).val());
        if(this.checked) {
            //var p_total   = parseFloat($(".input_total").val());
            //$(".p_total").html(" ");
            //$(".p_total").text(assurance + p_total);
            var assu_status = 1;
        }else{
            //var p_total   = parseFloat($(".input_total").val());
            //$(".p_total").html(" ");
            //$(".p_total").text(p_total - assurance);
            var assu_status = 2;
        }
        if(assu_status){
            var data  = "assu_status="+assu_status+"&rowid="+rowid;
            $.ajax({
                type :"POST",
                url : url+"/assurance",
                data:data,
                cache:false,
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    if(response.success === true){
                        location.reload();
                    }
                }
            });
        }
    });


});
