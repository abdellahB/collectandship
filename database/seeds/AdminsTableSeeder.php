<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'abdellahB',
            'email' => 'starfilmy@gmail.com',
            'firstname'=>'abdellah',
            'lastname'=>'abdellah',
            'role'=> 'admin',
            'image'=> '-',
            'password' => bcrypt('123456'),
        ]);

        /*DB::table('pages')->insert([
            'title_en' => 'about us',
            'title_ar' => 'معلومات عنا ',
            'slug'     => 'about-us',
            'content_en'=>'text english ',
            'content_ar'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        //
        DB::table('pages')->insert([
            'title_en' => 'Terms & condition',
            'title_ar' => 'الشروط و لاحكام ',
            'slug'     => 'terms-and-conditions',
            'content_en'=>'text english ',
            'content_ar'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        //
        DB::table('pages')->insert([
            'title_en' => 'Privacy Policy',
            'title_ar' => 'سياسة الخصوصية',
            'slug'     => 'privacy-and-policy',
            'content_en'=>'text english ',
            'content_ar'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        DB::table('pages')->insert([
            'title_en' => 'Shipping and payment methods',
            'title_ar' => 'وسائل الشحن و الدفع المعتمدة',
            'slug'     => 'ship-pay-method',
            'content_en'=>'text english ',
            'content_ar'=>'المحتوى العربي',
            'position'=> 'fixed',
        ]);

        DB::table('settings')->insert([
            'sitename'   => 'collectandship',
            'facebook'   => 'facebook.com',
            'instagram'  => 'instagram.com',
            'phone'      => 'xxx',
            'sitelogo'   => 'xxx',
            'twitter'    => 'twitter.com',
            'address'    => 'xxx',
            'taux_directship'      => '10',
            'countrie'      => 'السعودية',
            'countrie_en'      => 'Arabic Saudi',
            'working'      => 'الاثنين -  الجمعة ',
            'working_en'      => 'Mon - Fri, 8:00-22:00',
            'description' => 'xxx',
            'sitename_en'  => 'collectandship',
            'address_en'   => 'xxx',
            'description_en'  => 'XXXXX',
            'contact_email' => 'xxx',
            'maps_iframe'   => 'https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed',
            'paypal_client_id' => 'xxx',
            'paypal_secret'  => 'xxxxx',
            'paypal_mode'   => 'xxx',
            'paytabs_merchant_email'  => 'XXXXX',
            'paytabs_secret_key' => 'xxx',
        ]);*/
    }
}
