<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->string('codeparcel')->unique();
            $table->string('stockcode');
            $table->string('company');
            $table->float('weight');
            $table->float('width');
            $table->float('height');
            $table->string('tracking');
            $table->date('date_arrival');
            $table->float('lenght');
            $table->string('shipcountry');
            $table->string('sellername')->nullable();
            $table->string('fullname')->nullable();
            $table->text('description')->nullable();
            $table->integer('more')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
    }
}
