<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_prds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('shipping_id')->unsigned();
            $table->foreign('shipping_id')->references('id')->on('shippings');
            $table->string('code_return')->unique();
            $table->string('seller_address');
            $table->string('return_info');
            $table->float('pricetopay')->default(0);
            $table->string('tracking_url')->nullable();
            $table->string('tracking_code')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returns');
    }
}
