<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipamountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipamounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('companie_id')->index();
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->float('weightcompany');
            $table->float('pricecompany');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipamounts');
    }
}
