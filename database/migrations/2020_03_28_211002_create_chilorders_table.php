<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChilordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chilorders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('product_name');
            $table->string('product_url');
            $table->integer('product_qty');
            $table->float('product_price');
            $table->float('total_price');
            $table->string('currency',7)->default('$');
            $table->float('product_tax')->default(0);
            $table->float('product_taux')->default(0);
            $table->float('product_shipping')->default(0);
            $table->text('product_des');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chilorders');
    }
}
