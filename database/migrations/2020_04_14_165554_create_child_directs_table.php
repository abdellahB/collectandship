<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildDirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_directs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('directshipping_id')->index();
            $table->foreign('directshipping_id')->references('id')->on('directshippings');
            $table->float('weight');
            $table->float('width')->default(0);
            $table->float('lenght')->default(0);
            $table->float('height')->default(0);
            $table->string('content')->nullable();
            $table->float('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_directs');
    }
}
