<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_imgs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('ship_service_id')->index();
            $table->foreign('ship_service_id')->references('id')->on('ship_services');
            $table->string('imagename');
            $table->string('type')->default('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_imgs');
    }
}
