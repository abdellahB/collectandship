<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectshippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directshippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('code_order')->unique();
            //$table->unsignedBigInteger('companie_id')->index();
            //$table->foreign('companie_id')->references('id')->on('companies');
            $table->float('pricetopay');
            $table->string('unity')->nullable();
            $table->string('company_url')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('assurance')->default('false');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directshippings');
    }
}
