<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('countrie_id')->index();
            $table->foreign('countrie_id')->references('id')->on('countries');
            $table->string('ship_country',10);
            $table->string('companiename');
            $table->string('company_url');
            $table->string('companiename_en');
            $table->string('companylogo');
            $table->string('datedelevier');
            $table->string('datedelevier_en');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
