<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_collects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('directshipping_id')->index();
            $table->foreign('directshipping_id')->references('id')->on('directshippings');
            $table->string('ship_country');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('city');
            $table->string('province');
            $table->string('zipcode');
            $table->string('address');
            $table->integer('phone');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_collects');
    }
}
