<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('useradresse_id')->index();
            $table->foreign('useradresse_id')->references('id')->on('useradresses');
            $table->unsignedBigInteger('companie_id')->index();
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->string('code_parcel')->unique();
            $table->string('modepacking');
            $table->string('ship_country',5);
            $table->integer('Nbpackage')->default(1);
            $table->float('weight')->default(0);
            $table->string('length')->default(0);
            $table->string('width')->default(0);
            $table->string('height')->default(0);
            $table->float('pricetotal')->default(0);
            $table->string('assurance')->default('false');
            $table->text('information')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packings');
    }
}
