<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pack_trackings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('packing_id')->unsigned();
            $table->foreign('packing_id')->references('id')->on('packings');
            $table->string("company_url");
            $table->string("tracking_info");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pack_trackings');
    }
}
