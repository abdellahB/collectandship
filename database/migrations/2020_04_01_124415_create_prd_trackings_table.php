<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrdTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prd_trackings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('chilorder_id')->unsigned();
            $table->foreign('chilorder_id')->references('id')->on('chilorders');
            $table->string("companyurl");
            $table->string("trackinginfo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prd_trackings');
    }
}
