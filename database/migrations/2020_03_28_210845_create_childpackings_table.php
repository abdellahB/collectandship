<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildpackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('childpackings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('packing_id')->unsigned();
            $table->foreign('packing_id')->references('id')->on('packings');
            $table->unsignedBigInteger('shipping_id')->unsigned();
            $table->foreign('shipping_id')->references('id')->on('shippings');
            $table->string('product_name')->nullable();
            $table->float('product_price');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('childpackings');
    }
}
