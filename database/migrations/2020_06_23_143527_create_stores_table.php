<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('categorie_id')->index();
            $table->foreign('categorie_id')->references('id')->on('categories');
            $table->string('storename');
            $table->string('storename_en');
            $table->string('store_url');
            $table->string('store_logo');
            $table->text('store_desc');
            $table->text('store_desc_en');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
