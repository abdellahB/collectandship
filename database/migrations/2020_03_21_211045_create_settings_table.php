<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sitename');
            $table->string('sitename_en');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('phone');
            $table->string('sitelogo');
            $table->string('twitter');
            $table->string('address');
            $table->string('address_en');
            $table->string('countrie');
            $table->string('countrie_en');
            $table->string('working');
            $table->string('working_en');
            $table->float('taux_directship');
            $table->text('description');
            $table->text('description_en');
            $table->string('contact_email');
            $table->text('maps_iframe');
            //paypal
            $table->string('paypal_client_id');
            $table->text('paypal_secret');
            $table->string('paypal_mode');
            //paytabs
            $table->string('paytabs_merchant_email');
            $table->text('paytabs_secret_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
