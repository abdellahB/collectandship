<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
//use App\Mail\Sendmail;
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {

        Route::post('/register', 'Auth\RegisterController@register')->name('register');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        Route::post('/login', 'Auth\LoginController@login')->name('login');
        Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('verify');
        Route::post('/reset', 'Auth\RegisterController@reset')->name('reset');
        Route::get('/resetpassword/{token}', 'Auth\RegisterController@resetpassword')->name('resetpassword');
        Route::post('/changepassword', 'Auth\RegisterController@changepassword')->name('changepassword');

        /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
        Route::get('/', 'HomeController@index')->name('index');
        Route::get('/about', 'HomeController@about')->name('about');
        Route::get('/terms', 'HomeController@terms')->name('terms');
        Route::get('/privacy', 'HomeController@privacy')->name('privacy');
        Route::get('/ship-pay-method', 'HomeController@shippaymethod')->name('shippaymethod');
        Route::get('/stores','HomeController@stores')->name('stores');

        Route::get('/directship', 'HomeController@directship')->name('directship');
        Route::post("/getdata","HomeController@getdata");

        Route::get('/pricing', 'HomeController@pricing')->name('pricing');
        Route::POST('/calculer', 'HomeController@calculer')->name('calculer');

        Route::get('/contact', 'HomeController@contact')->name('contact');
        Route::post('/contact', 'HomeController@sendemail')->name('contact');
        Route::get('/faq', 'HomeController@faq')->name('faq');
        Route::get('/warehouse', 'HomeController@warehouse')->name('warehouse');
        Route::post('/add_order', 'HomeController@add_order');

        //account
        Route::get('/account','AccountController@index')->name('account');
        Route::get('/address','AccountController@address')->name('address');
        Route::POST('/newaddress','AccountController@newaddress')->name('newaddress');
        Route::get('/newaddress/{id}','AccountController@delete');

        Route::get('/profile','AccountController@profile')->name('profile');
        Route::POST('/profile','AccountController@Updateprofile')->name('Updateprofile');
        Route::POST('/acc_calculer','AccountController@acc_calculer')->name('acc_calculer');

        Route::get('/product','AccountController@product')->name('shipping.product');
        Route::get('/prod_service','AccountController@prod_service')->name('shipping.prod_service');
        Route::get('/inprocess','AccountController@inprocess')->name('shipping.inprocess');

        Route::get('/processing','AccountController@processing')->name('account.processing');

        Route::post('/processing','AccountController@getlist');
        Route::post('/getlist_prd','AccountController@getlist_prd');
        Route::post('/moreimage','AccountController@moreimage');

        Route::get('/payment','AccountController@payment')->name('account.payment');
        Route::get('/archive','AccountController@archive')->name('account.archive');
        Route::get('/archive_prod','AccountController@archive_prod')->name('account.archive_prod');
        Route::get('/archive_return','AccountController@archive_return')->name('account.archive_return');
        Route::get('/archive_purchase','AccountController@archive_purchase')->name('account.archive_purchase');

        Route::get('return','AccountController@return')->name('account.shipping.return');
        Route::get('waitreturn','AccountController@waitreturn')->name('account.shipping.waitreturn');
        Route::post('submit_return','AccountController@submit_return');
        //purchase
        Route::get('/purchase/create','AccountController@create')->name('account.purchase.create');
        Route::POST('/purchase/create','AccountController@store');

        Route::get('/purchase/waiting','AccountController@waiting')->name('account.purchase.waiting');
        Route::get('/purchase/payment','AccountController@pay')->name('account.purchase.payment');
        Route::get('/purchase/completed','AccountController@completed')->name('account.purchase.completed');
        Route::POST("/showtracking","AccountController@showtracking");

        //
        Route::POST("cancel_purchase","AccountController@cancel_purchase");

        //shipement
        Route::get('/shipement','AccountController@shipement');
        Route::get('/ship_archive','AccountController@ship_archive');
        Route::post('/detail_ship','AccountController@detail_ship');
        Route::post('/cancel_ship','AccountController@cancel_ship');

        Route::post('/getproduct','AccountController@getproduct');
        Route::post('/getcompany','AccountController@getcompany');
        Route::post('/save_order','AccountController@save_order');
        Route::post('/unpacking','AccountController@unpacking');
        Route::post('/moreinfo','AccountController@moreinfo');
        Route::post('/changeInfo','AccountController@changeInfo');
        Route::post('/mode_shipping','AccountController@mode_shipping');
        Route::post('/update_packprice','AccountController@update_packprice');

        //currecny
        Route::get('/currency/{code}','AccountController@currency');

        //cart
        Route::POST('/add','CartController@add');
        Route::get('/destroy/{id}','CartController@destroy');
        Route::post('/assurance','CartController@assurance');

        Route::get('/checkout','CartController@checkout');

        //payment
        Route::get('/transaction','PayController@transaction');
        Route::post('/showOrders','PayController@showOrders');

        Route::post('/paytabs_payment','PayController@paytabs_payment');
        Route::post('/paytabs_response','PayController@paytabs_response');

         Route::get('/paywithpaypal', array('as' => 'paywithpaypal','uses' => 'CartController@checkout'));
        // route for post request
         Route::post('/paypal', array('as' => 'paypal','uses' => 'PayController@postPaymentWithpaypal'));
         // route for check status responce
         Route::get('/paypal', array('as' => 'status','uses' => 'PayController@getPaymentStatus'));

});


//control
Route::prefix('control')->group(function() {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('control.auth.login');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('control.auth.logout');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('control.auth.login.submit');

    Route::post('/register', 'Auth\AdminLoginController@register')->name('control.auth.register.submit');

    Route::get('/', 'AdminController@index')->name('control.index');
    Route::get('/profile', 'AdminController@profile')->name('control.profile');

    Route::get('/address','AdminController@list')->name('control.adress.index');
    Route::get('/address/create','AdminController@create')->name('control.address.create');
    Route::get('/address/edit/{id}','AdminController@editaddress')->name('control.address.edit');
    Route::POST('/address/create','AdminController@store');
    Route::POST('/address/edit','AdminController@updateaddress');
    Route::POST('/address/action','AdminController@action');

    //clients
    Route::get('/clients', 'AdminController@clients')->name('control.clients');
    Route::get('/clients/{code}', 'AdminController@clients')->name('control.clients');
    Route::POST('/operaccount', 'AdminController@operaccount');
    Route::POST('/search', 'AdminController@search');


    //stores
    Route::get('/stores', 'AdminController@stores')->name('control.stores.index');
    Route::post('/addcategory', 'AdminController@addcategory')->name('control.addcategory');
    Route::get('/stores/create', 'AdminController@newstore');
    Route::post('/stores/create', 'AdminController@addstore');


    //transaction
    Route::get('/transaction', 'AdminController@transaction')->name('control.transaction');
    Route::get('/clienttransact/{id}', 'AdminController@clienttransact')->name('control.client_transact');
    Route::post('/showOrders','AdminController@showOrders');
    Route::post('/refund','AdminController@refund');

    Route::get('/users', 'AdminController@users')->name('control.users');
    Route::get('/users/edit/{id}', 'AdminController@edituser')->name('control.users.edituser');
    Route::post('/users/edit/{id}', 'AdminController@editsave');
    Route::post('/users', 'AdminController@update');
    Route::get('/general', 'AdminController@general')->name('control.general');
    Route::POST('/general', 'AdminController@saveSettings')->name('control.saveSettings');

    //services
    Route::get('/services', 'AdminController@services')->name('control.service.index');
    Route::post('/services', 'AdminController@updat_service');
    Route::get('/services/create', 'AdminController@addservice')->name('control.service.create');
    Route::post('/services/create', 'AdminController@Storeservice');
    Route::get('/services/edit/{id}', 'AdminController@editservice')->name('control.service.edit');

    //
    Route::get('/company', 'CompanyController@index')->name('control.company');
    Route::get('/company/create', 'CompanyController@create')->name('control.create');
    Route::get('/company/show/{id}', 'CompanyController@show')->name('control.show');
    Route::get('/company/edit/{id}', 'CompanyController@edit')->name('control.edit');
    Route::post('/company/store', 'CompanyController@store')->name('control.store');
    Route::post('/company/edit', 'CompanyController@update')->name('control.update');
    Route::post('/listcompany', 'CompanyController@listcompany')->name('control.listcompany');
    Route::post('/updateprices', 'CompanyController@updateprices')->name('control.updateprices');
    Route::post('/company/delete', 'CompanyController@delete');

    //shipping
    Route::get('/import', 'ShipController@imports')->name('control.shipping.import');
    Route::post('/import', 'ShipController@GetallUser');
    Route::post('/signleUser', 'ShipController@signleUser');
    Route::post('/add_import', 'ShipController@add_import');


    Route::get('/unknow', 'ShipController@unknow')->name('control.shipping.unknow');
    Route::post('/unknow', 'ShipController@Supunknow');
    Route::post('/unknow_update', 'ShipController@unknow_update');

    Route::get('/all', 'ShipController@all')->name('control.shipping.all');

    Route::get('/moreimage', 'ShipController@moreimage')->name('control.shipping.moreimage');
    Route::post('/moreimage', 'ShipController@addimages');
    Route::post('/rm_product', 'ShipController@rm_product');
    Route::post('/shipdetail', 'ShipController@shipdetail');
    Route::post('/serivcedetail', 'ShipController@serivcedetail');
    Route::post('/changeInfo', 'ShipController@changeInfo');

    Route::get('/return', 'ShipController@return')->name('control.shipping.return');
    Route::POST('/returnprice','ShipController@returnprice');
    Route::POST('/return/returntrack','AdminController@returntrack');

    Route::get('/shipping/new', 'ShipController@new')->name('control.shipping.orders.new');
    Route::post('/shipping/new', 'ShipController@getlist');
    Route::post('/shipping/packing', 'ShipController@packing');
    Route::post('/shipping/tracking_pack', 'ShipController@tracking_pack');

    Route::get('/shipping/payment', 'ShipController@payment')->name('control.shipping.orders.payment');
    Route::get('/shipping/waiting', 'ShipController@waiting')->name('control.shipping.orders.waiting');
    Route::get('/shipping/shipped', 'ShipController@shipped')->name('control.shipping.orders.shipped');

    //
    Route::get('/shipping/direct/new', 'ShipController@neworder')->name('control.shipping.direct.new');
    Route::get('/shipping/direct/recerved', 'ShipController@recerved')->name('control.shipping.direct.recerved');
    Route::get('/shipping/direct/shipped', 'ShipController@ship')->name('control.shipping.direct.shipped');

    Route::post('/shipping/direct/detail_ship','ShipController@detail_ship');
    Route::post('/shipping/direct/ship_recerved','ShipController@ship_recerved');
    Route::post('/shipping/direct/tracking_direct','ShipController@tracking_direct');

    //pages
    Route::get('/terms', 'PageController@terms')->name('control.terms');
    Route::get('/privacy', 'PageController@privacy')->name('control.privacy');
    Route::get('/about', 'PageController@about')->name('control.about');
    Route::get('/methode', 'PageController@methode')->name('control.methode');

    Route::POST('/terms', 'PageController@saveterms')->name('control.saveterms');
    Route::POST('/privacy', 'PageController@saveprivacy')->name('control.saveprivacy');
    Route::POST('/about', 'PageController@saveabout')->name('control.saveabout');
    Route::POST('/methode', 'PageController@savemethode')->name('control.savemethode');

    //faq
    Route::get('/faq', 'FaqController@faq')->name('control.faq');
    Route::get('/faq/{id}', 'FaqController@delete')->name('control.delete');
    Route::POST('/addquestion', 'FaqController@addquestion')->name('control.addquestion');
    //Route::POST('/about', 'PageController@saveabout')->name('control.saveabout');

    //coupon
    Route::get('/coupon','CouponController@coupons')->name('control.settings.coupon');
    Route::POST('/addcoupon','CouponController@addcoupon')->name('control.settings.addcoupon');
    Route::POST('/operationcoupon','CouponController@operationcoupon')->name('control.settings.operationcoupon');

    //modepay
    Route::get('/modepay', 'AdminController@modepay')->name('control.setting.modepay');
    Route::post('/modepay', 'AdminController@savemodepay');

    //
    Route::post('/sendemail', 'AdminController@sendemail');
    Route::post('/newsletter', 'AdminController@newsletter');


    //purchase
    Route::get('/purchase/new','PurchaseController@index')->name('control.purchase.new');
    Route::POST('/purchase/new','PurchaseController@accept');
    Route::POST('/purchase/addtracking','PurchaseController@addtracking');
    Route::POST('/purchase/showtracking','PurchaseController@showtracking');
    Route::POST('/purchase/prd_arrive','PurchaseController@prd_arrive');

    Route::get('/purchase/waiting','PurchaseController@waiting')->name('control.purchase.waiting');
    Route::get('/purchase/proccessing','PurchaseController@proccessing')->name('control.purchase.proccessing');
    Route::get('/purchase/purchase','PurchaseController@purchase')->name('control.purchase.purchase');
    Route::get('/purchase/completed','PurchaseController@completed')->name('control.purchase.completed');

    Route::post('/purchase/getprd','PurchaseController@getpoduct');
    Route::post('/purchase/editprd','PurchaseController@editproduct');
    Route::post('/purchase/updateprd','PurchaseController@updateprd');

    Route::post('/purchase/updatetracking','PurchaseController@updatetracking');

    //usability
    Route::get('/usability','UsabilityController@index')->name('control.usability.index');
    Route::get('/usability/create','UsabilityController@create')->name('control.usability.create');
    Route::get('/usability/edit/{id}','UsabilityController@edit')->name('control.usability.edit');
    Route::post('/usability/create','UsabilityController@store');
    Route::post('/usability/edit/{id}','UsabilityController@update');
    Route::post('/usability/delete','UsabilityController@delete');

});
