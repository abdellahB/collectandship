<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship_image extends Model
{
    //

    protected $fillable = [
        'imagename' , 'shipping_id' , 'type'
    ];
}
