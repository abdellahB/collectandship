<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipService extends Model
{
    //
    protected $fillable = [
        'code_service' ,'user_id','shipping_id' , 'service_id' , 'information' , 'status'
    ];

    public function getPservice(){
        return $this->belongsTo('App\Service','service_id');
    }

    public function getPship(){
        return $this->belongsTo('App\Shipping','shipping_id');
    }
    public function getUser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function GetData(){
        return $this->HasMany('App\Service_img','ship_service_id');
    }
}
