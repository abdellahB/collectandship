<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrdTracking extends Model
{
    //
        protected $fillable =[
            'chilorder_id', 'companyurl' , 'trackinginfo'
        ];
}
