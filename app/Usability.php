<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usability extends Model
{
    //

    protected $fillable = [
        'page_name' , 'position' , 'message' ,'message_en', 'status'
    ];
}
