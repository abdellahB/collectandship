<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $fillable = [
        'user_id','code_order','count_product','price_total','currency','status'
    ];

    public function getOrders(){
        return $this->HasMany('App\Chilorder');
    }

    public function getUser(){
        return $this->belongsTo('App\User','user_id');
    }
}
