<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directshipping extends Model
{
    //

    protected $fillable = [
        'user_id' , 'code_order' ,'status','unity','pricetopay','assurance','tracking_number','company_url'
    ];


    public function getUser(){
        return $this->belongsTo('App\User','user_id');
    }

    //collect
    public function getcollect(){
        return $this->hasOne('App\AddressCollect');
    }


    //delivery
    public function getdelivery(){
        return $this->hasOne('App\AddressDelivery');
    }

    //
    public function getcompany(){
        return $this->belongsTo('App\Companie','companie_id');
    }


    public function getchild(){
        return $this->hasMany('App\ChildDirect');
    }
}
