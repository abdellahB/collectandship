<?php

namespace App\Export;

use App\Order;
use Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class QueryExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        $trackingcode = $this->data['trackingcode'];
        /*$order_destcountry = $this->data['order_destcountry'];
        $transport_id = $this->data['transport_id'];
        $orderstatus = $this->data['orderstatus'];
        $recipientname = $this->data['recipientname'];
        $orderdate = $this->data['orderdate'];
        $searchDate = $this->data['searchDate'];*/
        //if($trackingcode){
        $orders = Order::where('user_id',Auth::id())->where('order_providernumber',$trackingcode)->get();
        // }
        return view('account.exports.excel',compact('orders'));
    }
}
