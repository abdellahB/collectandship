<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqQuestion extends Model
{
    //

    protected $fillable = [
        'questions_en' , 'questions_ar' , 'answer_en' , 'answer_ar'
    ];
}
