<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packing extends Model
{
    //

    protected $fillable = [
        'user_id' , 'useradresse_id' , 'companie_id' , 'code_parcel' , 'modepacking',
        'Nbpackage' , 'weight' , 'length' ,'width','height', 'pricetotal' ,'assurance', 'information' , 'status',
        'ship_country'
    ];

    public function child_packing(){
        return $this->hasMany('App\Childpacking','packing_id');
    }

    public function getUser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getCompany(){
        return $this->belongsTo('App\Companie','companie_id');
    }

    public function getchild(){
        return $this->hasMany('App\packing_service');
    }

    public function getAddress(){
        return $this->belongsTo('App\Useradress','useradresse_id');
    }

    public function imagePacking(){
        return $this->hasMany('App\Imgpacking');
    }

    public function getTracking(){
        return $this->Hasone('App\PackTracking');
    }
}
