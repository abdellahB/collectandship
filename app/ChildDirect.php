<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildDirect extends Model
{
    //
    protected $fillable = [
        'directshipping_id' , 'weight' ,'width' ,'lenght','height','content','value'
    ];
}
