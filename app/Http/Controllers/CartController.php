<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use Response;
class CartController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }

    public function checkout(){
        //dd(Cart::instance('collectandship')->content());
        if(count(Cart::instance('collectandship')->content())<=0){
            return redirect()->back();
        }else{
            return view('account.checkout');
        }
    }

    public function add(Request $request){
        //dd($request->all());
        if(!$request->servicecost){ $request->servicecost = 0;}
        if(!$request->product_price){ $request->product_price = 0;}
        $cartItem = Cart::instance('collectandship')->add(
            $request->order_id,$request->code_order,1,$request->price_total,0,
            [
                "order_type"    => $request->order_type,
                "currency"      => $request->currency,
                "servicecost"   => $request->servicecost,
                "product_price" => $request->product_price,
                'assurance'     => 'false'
            ]
        );
        flashy()->success( __('home.msg_addtocart') );
        return redirect()->back()->with('success_code',1);
    }
    public function assurance(Request $request){
        $rowid = $request->rowid;
        $assu_status = $request->assu_status;
        if($rowid && $assu_status){
            $item =  Cart::instance('collectandship')->get($rowid);
            if($assu_status == 1){
                Cart::instance('collectandship')->update($rowid,
                    [
                        'options'  => [
                            "order_type"    => $item->options->order_type,
                            "currency"      => $item->options->currency,
                            "servicecost"   => $item->options->servicecost,
                            "product_price" => $item->options->product_price,
                            'assurance'     => 'true'
                        ]
                    ]
                );
                //dd(Cart::instance('collectandship')->content());
            }else{
                Cart::instance('collectandship')->update($rowid,
                    [
                        'options'  => [
                            "order_type"    => $item->options->order_type,
                            "currency"      => $item->options->currency,
                            "servicecost"   => $item->options->servicecost,
                            "product_price" => $item->options->product_price,
                            'assurance'     => 'false'
                        ]
                    ]
                );
            }
            return Response::json(array('success' => true), 200);
        }
    }
    public function destroy($id){
       Cart::instance('collectandship')->remove($id);
       if(count(Cart::instance('collectandship')->content())<1){
            return redirect('account')->withSuccessMessage('Item has been removed!');
       }else{
            return redirect('checkout')->withSuccessMessage('Item has been removed!');
       }
    }
}
