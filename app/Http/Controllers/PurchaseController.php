<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Chilorder;
use Response;
use App\Setting;
use App\PrdTracking;
use App\Mail\Sendmail;
use Mail;
class PurchaseController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $Orders = Order::where('status',0)->get();
        return view('control.purchase.new',compact('Orders'));
    }

    public function waiting(){
        $Orders = Order::where('status',1)->get();
        return view('control.purchase.waiting',compact('Orders'));
    }

    public function proccessing(){
        $Orders = Order::where('status',2)->get();
        return view('control.purchase.processing',compact('Orders'));
    }

    public function purchase(){
        $Orders = Order::where('status',3)->get();
        return view('control.purchase.purchase',compact('Orders'));
    }

    public function completed(){
        $Orders = Order::whereIN('status',[4,10])->get();
        return view('control.purchase.completed',compact('Orders'));
    }

    //
    public function getpoduct(Request $request){
        $order = Order::where('id',$request->fastbuyetat_prd)->first();
        $viewRendered = view('control.partial.product',compact('order'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function editproduct(Request $request){
        $product = Chilorder::where('id',$request->edit_fastbuy_product)->first();
        $viewRendered = view('control.partial.editprd',compact('product'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function updateprd(Request $request){
        //$setting   = Setting::first();
        $price = (floatval($request->product_price) * floatval($request->product_qty));
        $total_price = $price + floatval($request->product_tax) + floatval($request->product_shipping) + floatval($request->product_taux) ;
        if($request->product_des){
            $product_des = $request->product_des;
        }else{
            $product_des = "-";
        }
        DB::table('chilorders')->where('id',$request->product_id)->update(
            array(
                'product_name'  => $request->product_name,
                'product_url'   => $request->product_url,
                'product_qty'   => $request->product_qty,
                'product_price' => $request->product_price,
                'total_price'   => $total_price,//
                'currency'      => $request->currency,
                'product_tax'   => $request->product_tax,
                'product_taux'   => $request->product_taux,
                'product_shipping' => $request->product_shipping,
                'product_des'      => $product_des
            )
        );

        $pricetotal = DB::table('chilorders')->where('order_id',$request->order_id)->sum('total_price');

        DB::table('orders')->where('id',$request->order_id)->update(
            array(
                'price_total'  => $pricetotal
            )
        );

        return Response::json(array('success' => true), 200);
    }


    //
    public function accept(Request $request){
        $orderid = $request->orderid;
        $status = $request->status;
        if($orderid && $status){

            $order = Order::where('id',$orderid)->first();

            DB::table('orders')->where('id',$orderid)->update(
                array(
                    'status'  => $status,
                    'updated_at' => Now()
                )
            );

            DB::table('chilorders')->where('order_id',$orderid)->update(
                array(
                    'status'  => $status,
                    'updated_at' => Now()
                )
            );

            if($status != 10){

                $to_name   = $order->getUser->firstname;
                $to_email  = $order->getUser->email;
                $content   = array('fullname' => $order->getUser->lastname.' '.$order->getUser->firstname,'body' => $order);

                if($order->getUser->lang == "en"){
                    $subject   = "Your personal shopper request is ready for payment ".$order->code_order;
                }else{
                    $subject   = "طلب المتسوق الشخصي جاهز للدفع ".$order->code_order;
                }

                Mail::send('emails.'.$order->getUser->lang.'.purchase_request', $content, function($message) use ($to_name,$to_email,$subject) {
                    $message->to($to_email, $to_name)->subject($subject);
                    $message->from("support@collectandship.com","collectandship");
                });

            }

            return Response::json(array('success' => true), 200);
        }
    }

    public function addtracking (Request $request){
        $data = $request->all();
        $tracking = PrdTracking::create([
            'chilorder_id' => $data['chilorder_id'],
            'companyurl'   => $data['companyurl'],
            'trackinginfo' => $data['trackinginfo']
        ]);
        DB::table('chilorders')->where('id',$data['chilorder_id'])->update(
            array(
                'status'  => 3,
                'updated_at' => Now()
            )
        );
        $child = Chilorder::where('order_id',$data['orderId'])->where('status',2)->count();
        if($child <1){
            DB::table('orders')->where('id',$data['orderId'])->update(
                array(
                    'status'  => 3,
                    'updated_at' => Now()
                )
            );
        }
        return Response::json(array('success' => true), 200);
    }
    public function updatetracking (Request $request){
        DB::table('prd_trackings')->where('id',$request->trackingid)->update(
            array(
                'companyurl'  => $request->companyurl,
                'trackinginfo' => $request->trackinginfo
            )
        );
        return Response::json(array('success' => true), 200);
    }
    public function prd_arrive (Request $request){

        $childorder = Chilorder::where('id',$request->product_id)->first();
        DB::table('chilorders')->where('id',$request->product_id)->update(
            array(
                'status'  => 4,
                'updated_at' => Now()
            )
        );
        $child = Chilorder::where('order_id',$childorder->order_id)->where('status',3)->count();
        if($child < 1){
            DB::table('orders')->where('id',$childorder->order_id)->update(
                array(
                    'status'  => 4,
                    'updated_at' => Now()
                )
            );
        }

        return Response::json(array('success' => true), 200);

    }
    public function showtracking(Request $request){
        $tracking = PrdTracking::where('chilorder_id',$request->product_id)->first();
        $viewRendered = view('control.partial.tracking',compact('tracking'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

}
