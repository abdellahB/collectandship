<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\helpers;
use Illuminate\Support\Facades\Validator;
use \Mcamara\LaravelLocalization\Facades\LaravelLocalization;

use Auth;
use App;
use Response;
use App\Page;
use App\FaqQuestion;
use App\Directshipping;
use App\ChildDirect;
use App\AddressCollect;
use App\AddressDelivery;
use App\Address;
use App\Usability;
//use App\Mail\Sendmail;
use Mail;
use App\categorie;

use Cart;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        return view ('index');
    }

    public function about(){
        $about = Page::where('id','=',1)->first();
        return view ('about',compact('about'));
    }

    public function terms(){
        $terms = Page::where('id','=',2)->first();
        return view ('terms',compact('terms'));
    }

    public function privacy(){
        $privacy = Page::where('id','=',3)->first();
        return view ('privacy',compact('privacy'));
    }
    public function shippaymethod(){
        $methode = Page::where('id','=',4)->first();
        return view('methodes',compact('methode'));
    }
    public function pricing(){
        $address   = Address::where('status',0)->get();
        return view ('pricing',compact('address'));
    }
    public function warehouse(){
        return view('warehouse');
    }
    public function contact(){
        return view ('contact');
    }
    public function sendemail(Request $request){

        $data = $request->all();

        $validator = Validator::make($data, [
            'fullname' => 'required',
            'email'    => 'required|email',
            'subject'  => 'required',
            'message'  => 'required',
            'g-recaptcha-response' => 'required',
        ]);

        if ($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toArray()
            ),400); // 400 being the HTTP code for an invalid request.
        }

        $to_name   = $data['fullname'];
        $to_email  = "support@collectandship.com";
        $content   = array('fullname' => $data['fullname'], "body" => $data['message']);

        $subject   = $data['subject'];
        $rep_email = $data['email'];
        $rep_name  = $data['fullname'];

        Mail::send('emails.'.LaravelLocalization::getCurrentLocale().'.contact', $content, function($message) use ($to_name, $to_email , $subject , $rep_email , $rep_name) {
            $message->to($to_email, $to_name)->subject($subject);
            $message->from("admin@collectandship.com","collectandship")
                    ->replyTo($rep_email,$rep_name);
        });

        return Response::json(array('success' => true), 200);

    }
    public function faq(){
        $questions = FaqQuestion::get();
        return view ('faq',compact('questions'));
    }

    public function stores(){
        $categories  = categorie::get();
        return view('stores',compact('categories'));
    }

    public function calculer(Request $request){

        $country = $request->country;
        $measure = $request->measure;
        $ship_country = $request->ship_country;

        $weight  = $request->weight;
        $width   = $request->width;
        $lenght  = $request->lenght;
        $height  = $request->height;

        if($country && $weight>0 && $ship_country){

            if($measure != "kg"){
                $weight = number_format(str_replace(',', '.',$weight) * 0.453592,1);
                $lenght = number_format(str_replace(',', '.',$lenght) * 2.54,1);
                $width  = number_format(str_replace(',', '.',$width) * 2.54,1);
                $height = number_format(str_replace(',', '.',$height) * 2.54,1);
            }

            $dimenssion =  number_format(($width * $lenght * $height)/5000,1); //الوزن الحجمي
            if($dimenssion <= 0.5){
                $dimenssion = 0.5;
            }
            if($dimenssion > $weight){
                $weight_to_use = flaotNnumber($dimenssion);
            }elseif($dimenssion <= $weight ){
                $weight_to_use = flaotNnumber($weight);
            }

            $companies  =  DB::table('companies')
            ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
            ->where('companies.countrie_id','=',$country)
            ->where('companies.ship_country',$ship_country)
            ->where('shipamounts.weightcompany','=',$weight_to_use)
            ->get();

            if(count($companies)>0){

                $viewRendered = view('account.partial.calculer',compact('companies','dimenssion','weight'))->render();
                $viewRendered2 = view('account.partial.calcul',compact('weight_to_use','dimenssion','weight'))->render();
                return Response::json(['html'=>$viewRendered ,'html2'=>$viewRendered2]);

            }else{

                $maxweight  =  DB::table('companies')
                ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
                ->where('companies.countrie_id','=',$country)
                ->where('companies.ship_country',$ship_country)
                ->max('shipamounts.weightcompany');

                if($maxweight < $weight_to_use && $maxweight!=0){
                    return Response::json(array('success' => false,'maxweight'=>$maxweight), 200);
                }else{
                    return Response::json(array('error' => true,'maxweight'=>$maxweight), 200);
                }

            }
        }
    }

    public function directship(){
        $address       = Address::where('status',0)->get();
        $info_top      = Usability::where('status',0)->where('page_name','directship')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','directship')->where('position','bottom')->first();
        return view('account.directship',compact('address','info_top','info_bottom'));
    }

    public function getdata(Request $request){
        $ship_country = $request->ship_country;
        $countrie_id  = $request->countrie_id;
        $measure      = $request->measure;

        $weight  = $request->weight;
        $lenght  = $request->lenght;
        $width   = $request->width;
        $height  = $request->height;

        if($countrie_id && $weight && $ship_country){

            if($measure != "kg"){
                $weight = number_format(str_replace(',', '.',$weight) * 0.453592,1);
                $lenght = number_format(str_replace(',', '.',$lenght) * 2.54,1);
                $width  = number_format(str_replace(',', '.',$width) * 2.54,1);
                $height = number_format(str_replace(',', '.',$height) * 2.54,1);
            }

            $dimenssion =  number_format(($width * $lenght * $height)/5000,1); //الوزن الحجمي

            if($dimenssion > $weight){
                $weight_to_use = flaotNnumber($dimenssion);
            }elseif($dimenssion < $weight ){
                $weight_to_use = flaotNnumber($weight);
            }

            $companies  =  DB::table('companies')
            ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
            ->where('companies.countrie_id','=',$countrie_id)
            ->where('companies.ship_country',$ship_country)
            ->where('shipamounts.weightcompany','=',$weight_to_use)
            ->select('companies.*','shipamounts.weightcompany','shipamounts.pricecompany')
            ->get();

            if(count($companies)>0){
                $viewRendered = view('account.partial.direct',compact('companies'))->render();
                return Response::json(['html'=>$viewRendered]);
            }else{
                return Response::json(array('success' => false), 200);
            }

        }
    }

    protected function makeOrderShip(){
        $code = DB::table('directshippings')->select('code_order')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->code_order,3);
            $int=(int)$int+1;
            return  $code="SH-".$int;
        }else{
           return $code="SH-1000";
        }
    }

    public function add_order(Request $request){

        $data  = json_decode($request->data_array,true);
        $data2 = json_decode($request->data_array2,true);

        $ship = Directshipping::create([
            'user_id'     => Auth::user()->id,
            'code_order'  => $this->makeOrderShip(),
            'pricetopay'  => 10,
            'unity'       => "-",
        ]);

        foreach($data AS $value){
            if(Session::get('currency') == "$"){
                $product_price = $value['value'];
            }else{
                $product_price = number_format($value['value'] * 3.75,2,'.','');
            }
            $child = ChildDirect::create([
                'directshipping_id' => $ship->id,
                'weight'  => $value['weight'],
                'width'   => $value['width']  ? $value['width'] : 0,
                'lenght'  => $value['length'] ? $value['length'] : 0,
                'height'  => $value['height'] ? $value['height']: 0,
                'content' => $value['content'],
                'value'   => $product_price,
            ]);
        }

        $collect = AddressCollect::create([
            'directshipping_id' => $ship->id,
            'ship_country'      => $data2['ship_country'],
            'firstname'         => $data2['firstname_collect'],
            'lastname'          => $data2['lastname_collect'],
            'city'              => $data2['city_collect'],
            'province'          => $data2['state_collect'],
            'zipcode'           => $data2['zipcode_collect'],
            'address'           => $data2['address_collect'],
            'phone'             => $data2['phone_collect']
        ]);

        /*$delivery = AddressDelivery::create([
            'directshipping_id' => $ship->id,
            'countrie_id'       => $data2['countrie_id'],
            'firstname'        =>  $data2['firstname'],
            'lastname'         =>  $data2['lastname'],
            'city'             =>  $data2['city'],
            'province'         =>  $data2['state'],
            'zipcode'          =>  $data2['zipcode'],
            'address'          =>  $data2['address'],
            'phone'            =>  $data2['phone'],
            'address_email'    =>  $data2['address_email'],
        ]);*/


        /*$cartItem = Cart::instance('collectandship')->add(
            $ship->id,$ship->code_order,1,$ship->pricetopay,
            [
                "order_type" => 'ship_direct',
                "currency"   => '$'
            ]
        );*/

        return Response::json(array('success' => true), 200);

    }

}
