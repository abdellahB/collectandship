<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\helpers;
use Illuminate\Support\Facades\DB;
use Response;
use App\Companie;
use App\Countrie;
use Auth;
use App\Discount;
use App\Address;
use App\Useradress;
use App\Shipping;
use App\Setting;
use App\Order;
use App\Chilorder;
use App\PrdTracking;
use App\Packing;
use App\Childpacking;
use App\ShipService;
use Cart;
use App\Return_prd;
use App\Directshipping;
use App\packing_service;
use App\User;
use App\Usability;
use LaravelLocalization;
use Session;
class AccountController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(){
        $info_top      = Usability::where('status',0)->where('page_name','account')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','account')->where('position','bottom')->first();
        return view('account.index',compact('info_top','info_bottom'));
    }
    public function address(){
        $newaddress   = Useradress::Where('user_id',Auth::user()->id)->where('status',0)->get();
        $info_top      = Usability::where('status',0)->where('page_name','address')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','address')->where('position','bottom')->first();
        return view('account.address',compact('newaddress','info_top','info_bottom'));
    }
    public function profile(){
        return view('account.profile');
    }
    public function Updateprofile(Request $request){

        $data = $request->all();
        //dd($data);
        if($request->hasFile('thumbnail')){
            $imagep = $request->file('thumbnail');
            $extension = $imagep->getClientOriginalExtension();
            $thumbnail = time().".{$extension}";
            $destinationPath = public_path('upload/users');
            $imagep->move($destinationPath, $thumbnail);
        }else{
            $thumbnail = Auth::user()->thumbnail;
        }

        if($data['password']){
            if(strlen($data['password']) != strlen($data['password_confirmation'])){
                return back()->with('errors', __('validation.confirmed'));
            }else{
                $password = bcrypt($data['password']);
            }

        }else{
            $password = Auth::user()->password;
        }

        $user  = User::where('email',$data['email'])->first();
        if(Auth::user()->email == $data['email']){

            Auth::user()->firstname     = $data['firstname'];
            Auth::user()->lastname      = $data['lastname'];
            Auth::user()->phone         = $data['phone'];
            Auth::user()->email         = $data['email'];
            Auth::user()->countrie_id   = $data['countrie_id'];
            Auth::user()->city          = $data['city'];
            Auth::user()->address       = $data['address'];
            Auth::user()->zipcode       = $data['zipcode'];
            Auth::user()->thumbnail     = $thumbnail;
            Auth::user()->password      = $password;
            Auth::user()->lang          = $data['lang'];
            Auth::user()->newsletter    = $data['newsletter'];
            Auth::user()->save();

            return back()->with('success',__('account.updated'));

        }else if(Auth::user()->email != $data['email']){
            if(!$user){

                Auth::user()->firstname     = $data['firstname'];
                Auth::user()->lastname      = $data['lastname'];
                Auth::user()->phone         = $data['phone'];
                Auth::user()->email         = $data['email'];
                Auth::user()->countrie_id   = $data['countrie_id'];
                Auth::user()->city          = $data['city'];
                Auth::user()->address       = $data['address'];
                Auth::user()->zipcode       = $data['zipcode'];
                Auth::user()->thumbnail     = $thumbnail;
                Auth::user()->password      = $password;
                Auth::user()->lang          = $data['lang'];
                Auth::user()->newsletter    = $data['newsletter'];
                Auth::user()->save();

                return back()->with('success',__('account.updated'));

            }else{
                return back()->with('success',__('account.errorupdate'));
            }
        }
    }

    //purchase
    protected function makeOrderCode(){
        $code = DB::table('orders')->select('code_order')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->code_order,3);
            $int=(int)$int+1;
            return  $code="WB-".$int;
        }else{
           return $code="WB-1000";
        }
    }
    public function create(){
        $info_top      = Usability::where('status',0)->where('page_name','purchasecreate')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','purchasecreate')->where('position','bottom')->first();
        return view('account.purchase.create',compact('info_top','info_bottom'));
    }
    public function store(Request $request){
        $setting   = Setting::first();
        $taux_purchase = $setting->taux_purchase/100;
        $data = json_decode($request->data_array,true);

        $order = Order::create([
            'user_id' => Auth::user()->id,
            'code_order' => $this->makeOrderCode(),
            'count_product' => 1,
            'price_total' => 0,
            'status' => 0
        ]);

        foreach($data as $key => $value) {
           //
           $price = (floatval($value['myprice_product']) * floatval($value['mycount']));
           $total_price = $price ;
           $child = Chilorder::create([
                'order_id'      => $order->id,
                'product_name'  => preg_replace('/[^A-Za-z0-9 ]/', '', $value['myproduct']),
                'product_url'   => $value['myurl_product'],
                'product_qty'   => $value['mycount'],
                'product_price' => $value['myprice_product'],
                'total_price'   => $total_price,
                'currency'      => $value['currency'],
                'product_tax'   => 0,
                'product_shipping' => 0,
                'product_des' => $value['mydescription'],
                'status' => 0
            ]);
        }

        $pricetotal     = DB::table('chilorders')->where('order_id', $order->id)->sum('total_price');
        $count          = DB::table('chilorders')->where('order_id', $order->id)->count();
        $currency       = DB::table('chilorders')->where('order_id', $order->id)->first();
        if($count>=1){
            DB::table('orders')->where('id',$order->id)->update(
                array(
                    'price_total'  => $pricetotal,
                    'currency'     => $currency->currency,
                    'count_product' => $count
                )
            );
            return Response::json(array('success' => true), 200);
        }else{
            DB::table('orders')->where('id',$order->id)->delete();
            return Response::json(array('success' => false), 200);
        }

    }
    public function waiting(){
        $info_top      = Usability::where('status',0)->where('page_name','purchasewaiting')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','purchasewaiting')->where('position','bottom')->first();
        $waitings = Order::where('user_id',Auth::user()->id)->where('status',0)->paginate(20);
        return view('account.purchase.waiting',compact('waitings','info_top','info_bottom'));
    }
    public function pay(){
        $waitings = Order::where('user_id',Auth::user()->id)->whereIn('status',[1,2])->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','purchasepayment')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','purchasepayment')->where('position','bottom')->first();
        return view('account.purchase.payment',compact('waitings','info_top','info_bottom'));
    }
    public function completed(){
        $waitings      = Order::where('user_id',Auth::user()->id)->whereIn('status',[3])->paginate(15);
        $info_top      = Usability::where('status',0)->where('page_name','purchasecompleted')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','purchasecompleted')->where('position','bottom')->first();
        return view('account.purchase.completed',compact('waitings','info_top','info_bottom'));
    }
    public function archive_purchase(){
        $waitings = Order::where('user_id',Auth::user()->id)->whereIn('status',[4,10])->paginate(15);
        return view('account.purchase.archive_purchase',compact('waitings'));
    }
    //
    public function acc_calculer (Request $request){

        $country = $request->country;
        $measure = $request->measure;
        $ship_country = $request->ship_country;

        $weight  = $request->weight;
        $width   = $request->width;
        $lenght  = $request->lenght;
        $height  = $request->height;

        if($country && $weight && $ship_country){

            if($measure != "kg"){
                $weight = number_format(str_replace(',', '.',$weight) * 0.453592,1);

                $lenght = number_format(str_replace(',', '.',$lenght) * 2.54,1);
                $width  = number_format(str_replace(',', '.',$width) * 2.54,1);
                $height = number_format(str_replace(',', '.',$height) * 2.54,1);
            }

            $dimenssion =  number_format(($width * $lenght * $height)/5000,1); //الوزن الحجمي
            if($dimenssion <= 0.5){
                $dimenssion = 0.5;
            }
            if($dimenssion > $weight){
                $weight_to_use = flaotNnumber($dimenssion);
            }elseif($dimenssion <= $weight ){
                $weight_to_use = flaotNnumber($weight);
            }

            $companies  =  DB::table('companies')
            ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
            ->where('companies.countrie_id','=',$country)
            ->where('companies.ship_country',$ship_country)
            ->where('shipamounts.weightcompany','=',$weight_to_use)
            ->get();

            if(count($companies)>0){
                $viewRendered = view('account.partial.calculer',compact('companies','dimenssion','weight'))->render();
                $viewRendered2 = view('account.partial.calcul',compact('weight_to_use','dimenssion','weight'))->render();
                return Response::json(['html'=>$viewRendered ,'html2'=>$viewRendered2]);
            }else{
                $maxweight  =  DB::table('companies')
                ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
                ->where('companies.countrie_id','=',$country)
                ->where('companies.ship_country',$ship_country)
                ->max('shipamounts.weightcompany');
                if($maxweight < $weight_to_use && $maxweight!=0){
                    return Response::json(array('success' => false,'maxweight'=>$maxweight), 200);
                }else{
                    return Response::json(array('error' => true,'maxweight'=>$maxweight), 200);
                }
            }
        }
    }

    //
    public function newaddress(Request $request){
        $data = $request->all();

        $new = Useradress::create([
            'user_id' => Auth::user()->id,
            'countrie_id' => $data['countrie_id'],
            'firstname' => $data['firstname'],
            'lastname'  => $data['lastname'],
            'city'      => $data['city'],
            'province'  => $data['province'],
            'zip_code'  => $data['zip_code'],
            'adress'    => $data['address'],
            'phone'     => $data['phone'],
        ]);

        return Response::json(array('success' => true), 200);
    }

    public function delete($id){
        DB::table('useradresses')
        ->where('user_id', Auth::user()->id)
        ->where('id', $id)
        ->update(
            array(
                'status' => 1
            )
        );
        return back();
    }

    public function currency(Request $request){
        $request->session()->put('currency', $request->code);
        return back();
    }

    //shipement
    public function shipement(){
        $info_top      = Usability::where('status',0)->where('page_name','shipement')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','shipement')->where('position','bottom')->first();
        $orders = Directshipping::whereIn('status',[0,1,2])->where('user_id',Auth::user()->id)->paginate(15);
        return view('account.shipement',compact('orders','info_top','info_bottom'));
    }
    public function ship_archive(){
        $orders = Directshipping::whereIn('status',[3,10])->where('user_id',Auth::user()->id)->paginate(15);
        return view('account.archive',compact('orders'));
    }
    public function detail_ship(Request $request){
        $orders = Directshipping::where('id',$request->id_ship)->where('user_id',Auth::user()->id)->get();
        $viewRendered = view('account.partial.detail_direct',compact('orders'))->render();
        return Response::json(['html'=>$viewRendered]);
    }
    public function cancel_ship(Request $request){

        DB::table('directshippings')
        ->where('user_id', Auth::user()->id)
        ->where('id', $request->id_ship)
        ->update(
            array(
                'status' => 10
            )
        );

        Cart::instance('collectandship')->destroy();

        return Response::json(array('success' => true), 200);

    }
    //
    public function showtracking (Request $request){
        //dd($request->product_id);
        $tracking = PrdTracking::where('chilorder_id',$request->product_id)->first();
        $viewRendered = view('account.partial.tracking',compact('tracking'))->render();
        return Response::json(['html'=>$viewRendered]);
    }


    //shipping
    public function prod_service() {
        $parcels       = Shipping::where('user_id',Auth::user()->id)->where('status',0)->paginate(15);
        $info_top      = Usability::where('status',0)->where('page_name','prod_service')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','prod_service')->where('position','bottom')->first();
        return view('account.shipping.service',compact('parcels','info_top','info_bottom'));
    }
    public function product(){
        $parcel_usa = Shipping::where('user_id',Auth::user()->id)->where('status',0)->where('shipcountry','usa')->paginate(20);
        $parcel_sa = Shipping::where('user_id',Auth::user()->id)->where('status',0)->where('shipcountry','sa')->paginate(20);
        $parcel_uae = Shipping::where('user_id',Auth::user()->id)->where('status',0)->where('shipcountry','uae')->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','product')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','product')->where('position','bottom')->first();
        return view('account.shipping.product',compact('parcel_usa','parcel_sa','parcel_uae','info_top','info_bottom'));
    }
    public function processing(){
        $packings      = Packing::where('status',0)->where('user_id',Auth::user()->id)->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','processing')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','processing')->where('position','bottom')->first();
        return view('account.shipping.processing',compact('packings','info_top','info_bottom'));
    }
    public function getlist(Request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packings = Packing::where('id',$packing_id)->where('user_id',Auth::user()->id)->get();
            $viewRendered = view('account.partial.list',compact('packings'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }
    public function getlist_prd(Request $request){
        $shipping_id = $request->shipping_id;
        if($shipping_id){
            $parcels = Shipping::where('id',$shipping_id)->where('user_id',Auth::user()->id)->get();
            $viewRendered = view('account.partial.list_prd',compact('parcels'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }
    public function payment(){
        $info_top      = Usability::where('status',0)->where('page_name','payment')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','payment')->where('position','bottom')->first();
        $packings = Packing::whereIn('status',[1,2])->where('user_id',Auth::user()->id)->paginate(20);
        return view('account.shipping.payment',compact('packings','info_top','info_bottom'));
    }
    public function archive_prod(){
        $packings = Packing::whereIn('status',[3,4])->where('user_id',Auth::user()->id)->paginate(20);
        return view('account.shipping.archive_prod',compact('packings'));
    }

    public function getproduct(Request $request){
        if($request->shipcountry !="all"){

            $parcels = Shipping::where('user_id',Auth::user()->id)
                ->where('shipcountry',$request->shipcountry)
                ->where('status',0)->paginate(15);

        }else{

            $parcels = Shipping::where('user_id',Auth::user()->id)
                ->where('status',0)->paginate(15);

        }
        $viewRendered = view('account.partial.product',compact('parcels'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function getcompany(Request $request){
        $address_country = $request->address_country;
        $newaddress   = Useradress::Where('user_id',Auth::user()->id)->where('id',$address_country)->first();
        $shipcountry     = $request->shipcountry;
        if($address_country && $shipcountry){

            $companies  =  DB::table('companies')
            ->where('countrie_id','=',$newaddress->countrie_id)
            ->where('ship_country',$shipcountry)
            ->get();

            $viewRendered = view('account.partial.company',compact('companies'))->render();

            return Response::json(['html'=>$viewRendered]);
        }
    }
    protected function makeOrderParcel(){
        $code = DB::table('packings')->select('code_parcel')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->code_parcel,3);
            $int=(int)$int+1;
            return  $code="PA-".$int;
        }else{
           return $code="PA-1000";
        }
    }
    public function save_order(Request $request){

        $data  = json_decode($request->data_array,true);
        $data2 = json_decode($request->data_array2,true);
        $data3 = json_decode($request->data_array3,true);


        $packing = Packing::create([
            'user_id'         => Auth::user()->id,
            'useradresse_id' =>  $data2['address_country'],
            'companie_id'     => $data2['companyid'],
            //'service_id'      => $data2['serviceid'],
            'code_parcel'     => $this->makeOrderParcel(),
            'modepacking'     => $data2['modpacking'],
            'Nbpackage'       => 1,
            'ship_country'    => $data2['shipcountry'],
            //'weight'        => '',
            //'dimenssion'    => '',
            //'pricetotal'    => '',
            'information'     => $data2['information'],
            'status'          => 0,
        ]);

        foreach($data AS $value){
            if(Session::get('currency') == "$"){
                $product_price = $value['product_price'];
            }else{
                $product_price = number_format($value['product_price'] * 3.75,2,'.','');
            }
            $child = Childpacking::create([
                'packing_id'    => $packing->id,
                'shipping_id'   => $value['productid'],
                'product_name'  => '-',
                'product_price' => $product_price,
                'status'        => 0,
            ]);

            DB::table('shippings')->where('id',$value['productid'])->update(array('status'  => 1));

        }

        if(count($data3)>0){
            foreach($data3 AS $value){
                $service  = packing_service::create([
                    'packing_id' => $packing->id,
                    'service_id' => $value['serviceid']
                ]);
            }
        }


        $count = DB::table('childpackings')->where('packing_id', $packing->id)->count();
        DB::table('packings')->where('id',$packing->id)->update(array('Nbpackage'  => $count));

        //send email
        return Response::json(array('success' => true), 200);

    }


    public function unpacking(Request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packings = Childpacking::where('packing_id',$packing_id)->get();
            foreach($packings AS $packing){
                DB::table('shippings')->where('id',$packing->shipping_id)->update(array('status'  => 0));
            }
            DB::table('packings')->where('id',$packing_id)->update(array('status'  => 5));
            return Response::json(array('success' => true), 200);
        }
    }

    public function moreinfo(Request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packing  =  Packing::where('user_id',Auth::user()->id)->where('id',$packing_id)->first();
            $viewRendered = view('account.partial.moreinfo',compact('packing'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

    public function changeInfo(Request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packing  =  Packing::where('user_id',Auth::user()->id)->where('id',$packing_id)->first();
            $companies  =  DB::table('companies')
            ->where('companies.countrie_id','=',$packing->getAddress->countrie_id)
            ->where('companies.ship_country',$packing->ship_country)->get();
            $viewRendered = view('account.partial.changeinfo',compact('packing','companies'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }
    public function mode_shipping(Request $request){
        $companyid = $request->companyid;
        $packing_id = $request->packing_id;
        if($companyid && $packing_id){

            $packing = Packing::where('id',$packing_id)->first();

            $service_p=0;
            foreach ($packing->getchild as $child){
                $service_p += $child->getService->service_price;
            }

            $weight     =  number_format($packing->weight,1,'.','');
            $dimenssion =  number_format(($packing->length * $packing->width * $packing->height)/5000,1,'.','');

            if($dimenssion > $weight){
                $weight_to_use = flaotNnumber($dimenssion);
            }elseif($dimenssion < $weight ){
                $weight_to_use = flaotNnumber($weight);
            }

            $companies  =  DB::table('companies')
            ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
            ->where('companies.id','=',$companyid)
            ->where('shipamounts.weightcompany','=',$weight_to_use)
            ->first();

            if($companies){

                $priceTotal  = $companies->pricecompany;
                if( LaravelLocalization::getCurrentLocale() == 'ar'){
                    $datedelever = $companies->datedelevier;
                }else{
                    $datedelever = $companies->datedelevier_en;
                }
                if(Session::get('currency') == "$"){
                    $priceTotal = $priceTotal;
                }else{
                    $priceTotal = number_format($priceTotal * 3.75,2,',','');
                }
                // $datedelever = $companies->datedelevier_en;
                return Response::json(
                    array(
                        'success' => true ,
                        'priceTotal'  => $priceTotal.Session::get('currency'),
                        'datedelever'  => $datedelever,
                    )
                , 200);

            }
            //dd($companies);

        }
    }
    public function update_packprice(Request $request){

        $companyid = $request->companyid;
        $packing_id = $request->packing_id;
        if($companyid && $packing_id){

            $packing = Packing::where('id',$packing_id)->first();

            $service_p=0;
            foreach ($packing->getchild as $child){
                $service_p += $child->getService->service_price;
            }

            $weight     =  number_format($packing->weight,1,'.','');
            $dimenssion =  number_format(($packing->length * $packing->width * $packing->height)/5000,1,'.','');

            if($dimenssion > $weight){
                $weight_to_use = flaotNnumber($dimenssion);
            }elseif($dimenssion < $weight ){
                $weight_to_use = flaotNnumber($weight);
            }

            $companies  =  DB::table('companies')
            ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
            ->where('companies.id','=',$companyid)
            ->where('shipamounts.weightcompany','=',$weight_to_use)
            ->first();

            if($companies){

                $priceTotal  = $companies->pricecompany + $service_p;

                Packing::where('id',$packing_id)->update(
                    array(
                        'companie_id' => $companyid,
                        'pricetotal' => $priceTotal,
                    )
                );
                return Response::json(array('success' => true), 200);
            }
        }
    }
    public function return(){
        $parcels       = Shipping::where('user_id',Auth::user()->id)->where('status',0)->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','return')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','return')->where('position','bottom')->first();
        return view('account.shipping.return',compact('parcels','info_top','info_bottom'));
    }
    public function waitreturn(){
        $parcels = Shipping::where('user_id',Auth::user()->id)->Join('return_prds', 'shippings.id', '=', 'return_prds.shipping_id')
        ->whereIn('return_prds.status',[0,1,2])->paginate(20);

        $info_top      = Usability::where('status',0)->where('page_name','waitreturn')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','waitreturn')->where('position','bottom')->first();

        return view('account.shipping.waitreturn',compact('parcels','info_top','info_bottom'));
    }
    public function archive_return(){
        $parcels = Shipping::where('user_id',Auth::user()->id)
        ->Join('return_prds', 'shippings.id', '=', 'return_prds.shipping_id')
        ->where('return_prds.status',3)->paginate(15);
        //dd($parcels);
        return view('account.shipping.archive_return',compact('parcels'));
    }

    protected function makeReturnCode(){
        $code = DB::table('return_prds')->select('code_return')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->code_return,3);
            $int=(int)$int+1;
            return  $code="RT-".$int;
        }else{
           return $code="RT-1000";
        }
    }

    public function submit_return(Request $request){
        $productid= $request->product_id;
        $seller_address= $request->seller_address;
        $return_info= $request->return_info;
        if($productid && $seller_address && $return_info){

            $return = Return_prd::create([
                'code_return' => $this->makeReturnCode(),
                'shipping_id' => $productid,
                'seller_address' => $seller_address,
                'return_info' => $return_info
            ]);

            Shipping::where('id',$productid)->update(array('status'  => 5));

            return Response::json(array('success' => true), 200);

        }
    }

    //moreimage
    protected function makeOrderService(){
        $code = DB::table('ship_services')->select('code_service')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->code_service,3);
            $int=(int)$int+1;
            return  $code="SR-".$int;
        }else{
           return $code="SR-1000";
        }
    }
    public function moreimage(Request $request){
        $data = $request->all();
        foreach($data['service_id'] AS $service_id){
            $service = ShipService::create([
                'code_service' => $this->makeOrderService(),
                'user_id'      =>  Auth::user()->id,
                'shipping_id'  =>  $data['product_id'],
                'service_id'   =>  $service_id,
                'information'  =>  $data['information']
            ]);
        }

        /*Shipping::where('id',$data_array['product_id'])
        ->update(
            array(
                    'more'  => 1,
                    'updated_at' => Now()
                )
            );
        $cartItem = Cart::instance('collectandship')->add(
            $service->id,$service->code_service,1,$service->getPservice->service_price,
            [
                "order_type" => 'prd_service',
                "currency"   => '$'
            ]
        );*/

        return Response::json(array('success' => true), 200);
    }

    public function inprocess(){
        $services = ShipService::where('user_id',Auth::user()->id)->where('status',0)->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','inprocess')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','inprocess')->where('position','bottom')->first();
        return view('account.shipping.inprocess',compact('services','info_top','info_bottom'));
    }

    public function archive(){
        $services = ShipService::where('user_id',Auth::user()->id)->whereIn('status',[1,2,3])->paginate(15);
        return view('account.shipping.archive',compact('services'));
    }

    //cancel_purchase
    public function cancel_purchase(Request $request){
        $orderid = $request->purchase_id;
        if($orderid){

            DB::table('orders')->where('id',$orderid)->update(
                array(
                    'status'  => 10,
                    'updated_at' => Now()
                )
            );

            DB::table('chilorders')->where('order_id',$orderid)->update(
                array(
                    'status'  => 10,
                    'updated_at' => Now()
                )
            );

            Cart::instance('collectandship')->destroy();

            return Response::json(array('success' => true), 200);

        }

    }

}
