<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usability;
use Response;
class UsabilityController extends Controller
{
    //

    public function __construct(){
       $this->middleware('auth:admin');
    }

    public function index(){
        $data = Usability::get();
        return view('control.usability.index',compact('data'));
    }

    public function create(){
        return view('control.usability.create');
    }

    public function edit($id){
        $data = Usability::where('id',$id)->first();
        return view('control.usability.edit',compact('data'));
    }

    //
    public function store(Request $request){
        $data = $request->all();
        $save  = Usability::create($data);
        return back()->with('success','تمت اضافة  لحساب العميل بنجاح');
    }

    public function update($id , Request $request){
        $data = $request->all();
        $info = Usability::findOrFail($id);
        $info->fill($data)->save();
        return back()->with('success','تم تحديث المعلومات المطلوبة بنجاح');
    }

    public function delete(Request $request){
        $info_id = $request->info_id;
        $status  = $request->status;
        if($info_id && $status){
            if($status == 2) { $status = 0; }
            Usability::where('id', $info_id)
                ->update(
                    array('status' => $status)
                );
            return Response::json(array('success' => true), 200);
        }
    }
}
