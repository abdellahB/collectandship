<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Discount;
use Response;
class CouponController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function coupons(){
        $coupons = Discount::get();
        return view('control.settings.coupon',compact('coupons'));
    }

    public function addcoupon (Request $request){
        $couponccode = $request->couponccode;
        $couponprice = $request->couponprice;
        $coupontype  = $request->coupontype;
        $couponutilisation = $request->couponutilisation;
        $datetostart = $request->datetostart;
        $datetoend   = $request->datetoend;
        if($couponccode && $couponprice && $coupontype && $couponutilisation && $datetostart && $datetoend){

            $discount = Discount::create([
                'discountcode'   => $couponccode,
                'amount'         => $couponprice,
                'type'           => $coupontype,
                'numberperson'   => $couponutilisation,
                'numberutiliser' => 0,
                'startdate'      => $datetostart,
                'enddate'        => $datetoend,
                'status'         => 0,
            ]);

            return Response::json(array('success' => true), 200);

        }
    }

    //
    public function operationcoupon (Request $request){
        $couponid = $request->couponid;
        $status = $request->status;
        if($couponid && $status){
            if($status == 1){
                $status = -1;
            }else{
                $status = 0;
            }
            DB::table('discounts')
            ->Where('id', $couponid)
            ->update(array('status' => $status));

            return Response::json(array('success' => true), 200);
        }
    }
}
