<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Companie;
use App\Shipamount;
use App\Countrie;
use Response;
use App\Import\CountrieImport;
use Maatwebsite\Excel\Facades\Excel;
use PragmaRX\Countries\Package\Countries;
class CompanyController extends Controller
{
    //

    public function index(){
        $countries = Countrie::get();
        return view("control.company.index",compact('countries'));
    }

    public function create(){
        $countries = DB::table('countries')->get();
        return view("control.company.create",compact('countries'));
    }

    public function store(Request $request){

        $data_req = $request->all();

        if($file = $request->file('companylogo')){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/company');
            $file->move($destinationPath, $name);
        }

        //get file
        $upload = $request->file('companyprice');
        $filePath = $upload->getRealPath();
        $file   = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader=[];
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        $countries = new Countries();
        if(false != $file){
            while($columns = fgetcsv($file)){
                $countie = $countries->where('cca3',$columns[0])->first();
                if($countie){
                    $count   = Countrie::where('countriename','LIKE',$countie->brk_name)->first();
                    if($count){
                        $company = Companie::where('countrie_id',$count->id)->first();
                        if($company){
                            $companie_id = $company->id;
                        }else{
                            $data_req['companylogo'] = $name;
                            $data_req['countrie_id'] =  $count->id;
                            $companie = Companie::create($data_req);
                            $companie_id = $companie->id;
                        }
                        $shipping = Shipamount::create([
                            'companie_id'   => $companie_id,
                            'weightcompany' => $columns[1],
                            'pricecompany'  => $columns[2],
                        ]);
                    }
                }
            }
        }

        return back()->with('success','Company upload successfully');
    }

    public function listcompany(Request $request){
       $country      = (int) $request->country;
       $ship_country = $request->ship_country;
       if($country && $ship_country){

        $companies  =  DB::table('companies')
        ->where('ship_country',$ship_country)
        ->where('countrie_id',$country)
        ->get();

        $viewRendered = view('control.partial.listCompany',compact('companies'))->render();

        return Response::json(['html'=>$viewRendered]);
       }

    }

    public function show($id){
        $companies = Shipamount::where('companie_id',$id)->get();
        return view("control.company.show",compact('companies'));
    }

    public function updateprices(Request $request){
        $idprices = $request->idprices;
        $prices = $request->prices;
        if($idprices &&  $prices){
            DB::table('shipamounts')->where('id', $idprices)->update(array('pricecompany' => $prices));
            return Response::json(array('success' => true), 200);
        }
    }

    public function edit($id){
        $countries = DB::table('countries')->get();
        $companie = Companie::where('id',$id)->first();
        return view("control.company.edit",compact('countries','companie'));
    }

    public function update (Request $request){

        if($file = $request->file('companylogo')){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/company');
            $file->move($destinationPath, $name);
        }else{
            $name = $request->companie_logo;
        }

        DB::table('companies')->where('id', $request->companie_id)
            ->update(
                array(
                    'countrie_id'     => $request->countrie_id,
                    'ship_country'    => $request->ship_country,
                    'companiename'    => $request->companiename,
                    'company_url'     => $request->company_url,
                    'companiename_en' => $request->companiename_en,
                    'datedelevier'    => $request->datedelevier,
                    'datedelevier_en' => $request->datedelevier_en,
                    'companylogo'     => $name
                )
        );

        $upload = $request->file('companyprice');
        if($upload){
            $filePath = $upload->getRealPath();
            $file     = fopen($filePath, 'r');
            $filePath = $upload->getRealPath();
            $file   = fopen($filePath, 'r');
            $header = fgetcsv($file);
            $escapedHeader=[];
            foreach ($header as $key => $value) {
                $lheader=strtolower($value);
                $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }
            $ship     = Shipamount::where('companie_id',$request->companie_id)->delete();
            if(false != $file){
                while($columns = fgetcsv($file)){
                    //$cols = $columns[0];
                    $shipping = Shipamount::create([
                        'companie_id'   => $request->companie_id,
                        'weightcompany' => $columns[1],
                        'pricecompany'  => $columns[2],
                    ]);
                }
            }
        }

       // dd("OK");
        return back()->with('success','تم تحديث المعلومات بنجاج');

    }

    public function delete(Request $request){
        $company_id = $request->company_id;
        $status = $request->status;
        if($company_id &&  $status){
            if($status == 2 ) { $status = 0; }
            DB::table('companies')->where('id', $company_id)->update(array('status' => $status));
            return Response::json(array('success' => true), 200);
        }
    }

}
