<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FaqQuestion;
use Response;
class FaqController extends Controller
{
    //

    public function __construct() {
       $this->middleware('auth:admin');
    }
    public function faq(){
        $questions = FaqQuestion::paginate(10);
        return view('control.pages.faq',compact('questions'));
    }
    public function delete(Request $request){

        $question = FaqQuestion::where('id',$request->id)->delete();

        return back();
    }
    public function addquestion(Request $request){
        $questions_en = $request->questions_en;
        $questions_ar = $request->questions_ar;
        $answer_en = $request->answer_en;
        $answer_ar = $request->answer_ar;
        if($questions_en && $questions_ar && $answer_en && $answer_ar){

            $question = FaqQuestion::create([
                "questions_en"  => $questions_en,
                "questions_ar"  => $questions_ar,
                "answer_en"     => $answer_en,
                "answer_ar"     => $answer_ar,
            ]);

            return Response::json(array('success' => true), 200);

        }
    }


}
