<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Damas\Paytabs\Paytabs;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
use Auth;
use Validator;
use URL;
use Session;
use Redirect;
use Cart;
use LaravelLocalization;
use App\ShipService;
use App\Transact;
use App\Detail;
use App\Setting;
use App\Usability;
use Response;
use PragmaRX\Countries\Package\Countries;
class PayController extends Controller
{
    //
    private $_api_context;
    public function __construct(){

        $this->middleware('auth');
        /*$this->_api_context = new ApiContext(
            new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret'))
        );
        $this->_api_context->setConfig(config('paypal.settings'));*/
        $general = Setting::first();
        $settings = array(
            'mode' => $general->paypal_mode,
            'http.ConnectionTimeOut' => 100,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path() . '/logs/paypal.log',
            'log.LogLevel' => 'ERROR'
        );
        $this->_api_context = new ApiContext(new OAuthTokenCredential($general->paypal_client_id, $general->paypal_secret));
        $this->_api_context->setConfig($settings);
    }

    public function transaction(){
        $transaction   = Transact::where('user_id',Auth::user()->id)->paginate(20);
        $info_top      = Usability::where('status',0)->where('page_name','transaction')->where('position','top')->first();
        $info_bottom   = Usability::where('status',0)->where('page_name','transaction')->where('position','bottom')->first();
        return view('account.transaction',compact('transaction','info_top','info_bottom'));
    }
    public function showOrders(Request $request){
        $showorder = $request->transid;
        if($showorder){
            $transaction = Transact::Where('id',$showorder)->get();
            $viewRendered = view('account.partial.detail',compact('transaction'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }
    public function paytabs_payment(Request $request){
        $modepay = $request->modepay;
        $currency = $request->currency;
        $priceTotal=0;
        $assu_price = 0;
        //dd(Cart::instance('collectandship')->content());
        foreach(Cart::instance('collectandship')->content() AS $item){
            if($item->options->assurance == "true"){
                if($item->options->product_price >= 100){
                    if($item->options->product_price * 0.02 < 20 ){
                        $assurance = 20 ;
                    }else{
                        $assurance = $item->options->product_price * 0.02 ;
                    }
                }else{
                    $assurance = 0;
                }
                if(Session::get('currency') == "$"){
                    $assu_price += $assurance;
                }else{
                    $assu_price += number_format($assurance * 3.75,2,'.','');
                }
            }elseif($item->options->assurance == "false"){
                $assu_price = 0;
            }
            if(Session::get('currency') == "$"){
                if($item->options->currency !='$'){
                    $price = ($item->price + $item->options->servicecost) * 0.266667;
                }else{
                    $price = $item->price + $item->options->servicecost;
                }
                $priceTotal += $price;
            }else{
                if($item->options->currency =='$'){
                    $price_sar = ($item->price + $item->options->servicecost) * 3.75;
                }else{
                    $price_sar = $item->price + $item->options->servicecost;
                }
                $priceTotal += $price_sar;
            }
        }
        if(Session::get('currency') == "$"){
            $currency_tabs = "USD";
        }else{
            $currency_tabs = "SAR";
        }
        $pr_total = $priceTotal + $assu_price ;
        $price_total = number_format($pr_total,2,'.','');
        //dd($priceTotal + $assu_price);
        if($modepay == "paypal"){

            if(Session::get('currency') == "$"){
                $myamount = $price_total;
            }else{
                $myamount = $price_total * 0.266667;
            }

            //dd($this->_api_context);
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $items = array();
            $index = 0;
            //dd(Cart::instance('collectandship')->content());
            foreach(Cart::instance('collectandship')->content() AS $item){

                if($item->options->currency !='$'){
                    $price = ($item->price + $item->options->servicecost) * 0.266667;
                }else{
                    $price = $item->price + $item->options->servicecost;
                }
                if($item->options->assurance == "true"){
                    if($item->options->product_price >= 100){
                        if($item->options->product_price * 0.02 < 20 ){
                            $assurance = 20 ;
                        }else{
                            $assurance = $item->options->product_price * 0.02 ;
                        }
                    }else{
                        $assurance = 0;
                    }
                    $assu_price = $assurance;
                }elseif($item->options->assurance == "false"){
                    $assu_price = 0;
                }
                $price_o = $price + $assu_price;
                //dd($item->name);
                $items[$index] = new Item();
                $items[$index]->setName("Order N° ".$item->name)
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($price_o);

                $index++;
            }

            $item_list = new ItemList();
            $item_list->setItems($items);

            $details = new Details();
            $details->setShipping(0)
                ->setTax(0)
                ->setSubtotal(0);

            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal(0)
                ->setDetails($details);

            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($myamount);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('الدفع للطلبات في موقع collectandship');

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('status'))
                ->setCancelUrl(URL::route('status'));


            $payment = new Payment();
            $payment->setIntent('sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
            } catch (PayPalConnectionException $ex){
                \Session::put('pay_error','Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
            } catch (Exception $ex) {
                \Session::put('pay_error','Connection timeout');
                return Redirect::route('paywithpaypal');
            }

            foreach($payment->getLinks() as $link) {
                if($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            Session::put('paypal_payment_id', $payment->getId());

            if(isset($redirect_url)) {
                return Redirect::away($redirect_url);
            }

            \Session::put('pay_error','Unknown error occurred');
            return Redirect::route('paywithpaypal');

        }else{
            if(!$request->phone && !$request->address && !$request->city && !$request->zipcode){
                \Session::put('pay_error', __('account.pay_error'));
                return Redirect::route('paywithpaypal');
            }else{

                Auth::user()->firstname     = $request->firstname;
                Auth::user()->lastname      = $request->lastname;
                Auth::user()->phone         = $request->phone;
                Auth::user()->email         = $request->email;
                Auth::user()->countrie_id   = $request->countrie_id;
                Auth::user()->city          = $request->city;
                Auth::user()->address       = $request->address;
                Auth::user()->zipcode       = $request->zipcode;
                Auth::user()->save();

                $general = Setting::first();
                $countries = new Countries();
                $countie = $countries->whereNameCommon(Auth::user()->getcountry->countriename)->first();
                //dd($itemlist);
                $pt = Paytabs::getInstance($general->paytabs_merchant_email, $general->paytabs_secret_key);
                $result = $pt->create_pay_page(array(
                    "merchant_email" => $general->paytabs_merchant_email,
                    'secret_key' => $general->paytabs_secret_key,
                    'title' =>  'Billing for Orders',
                    'cc_first_name' => $request->firstname,
                    'cc_last_name' => $request->lastname,
                    'email' => $request->email,
                    'cc_phone_number' => $countie->dialling['calling_code'][0],
                    'phone_number' => $request->phone,
                    'billing_address' => $request->address,
                    'city' => $request->city,
                    'state' => $request->city,
                    'postal_code' => $request->zipcode,
                    'country' => $countie['cca3'],
                    'address_shipping' => $request->firstname,
                    'city_shipping' => $request->city,
                    'state_shipping' => $request->city,
                    'postal_code_shipping' => $request->zipcode,
                    'country_shipping' => $countie['cca3'],
                    "products_per_title"=> 'Order collectandship°',
                    'currency' => $currency_tabs,
                    "unit_price"=> $price_total,
                    'quantity' => "1",
                    'other_charges' => "0",
                    'amount' => $price_total,
                    'discount'=>"0",
                    "msg_lang" => "english",
                    "reference_no" => str_random(12),
                    "site_url" => "http://collectandship.com",
                    'return_url' => LaravelLocalization::localizeUrl('/paytabs_response'),
                    "cms_with_version" => "API USING PHP"
                ));

                if($result->response_code == 4012){
                    return redirect($result->payment_url);
                }
                return $result->result;
            }
        }
    }


    public function paytabs_response(Request $request){
        $general = Setting::first();
        $pt = Paytabs::getInstance($general->paytabs_merchant_email,$general->paytabs_secret_key);
        $result = $pt->verify_payment($request->payment_reference);
        //dd($result);
        if($result->response_code == 100){
            // Payment Success
            $operation = Transact::create([
                'user_id' => Auth::user()->id,
                'transaction_code'    => $result->transaction_id,
                'transaction_amount'  => $result->amount,
                'currency'     => Session::get('currency'),
                'transaction_orders'  => count(Cart::instance('collectandship')->content()),
                'transaction_payment' => 'Paytabs'
            ]);

            foreach(Cart::instance('collectandship')->content() as $item){

                if($item->options->assurance == "true"){
                    if($item->options->product_price >= 100){
                        if($item->options->product_price * 0.02 < 20 ){
                            $assurance = 20 ;
                        }else{
                            $assurance = $item->options->product_price * 0.02 ;
                        }
                    }else{
                        $assurance = 0;
                    }
                    if(Session::get('currency') == "$"){
                        $assu_price = $assurance;
                    }else{
                        $assu_price = number_format($assurance * 3.75,2,'.','');
                    }
                    $assurance_text = "true";
                }elseif($item->options->assurance == "false"){
                    $assu_price = 0;
                    $assurance_text = "false";
                }else{
                    $assurance_text = "false";
                    $assu_price = 0;
                }
                if(Session::get('currency') == "$"){
                    if($item->options->currency !='$'){
                        $price = ($item->price + $item->options->servicecost) * 0.266667;
                    }else{
                        $price = $item->price + $item->options->servicecost;
                    }
                }else{
                    if($item->options->currency =='$'){
                        $price = ($item->price + $item->options->servicecost) * 3.75;
                    }else{
                        $price = $item->price + $item->options->servicecost;
                    }
                }

                $price_tot = $price + $assu_price;

                $detail = Detail::create([
                    'transact_id'  => $operation->id,
                    'code_order'   => $item->name,
                    'service_name' => $item->options->order_type,
                    'order_price'  => $price_tot,
                    'currency'     => Session::get('currency'),
                    'assurance'    => $item->options->assurance,
                    'status' => 1,
                ]);

                if($item->options->order_type == "purchase"){
                    DB::table('orders')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );
                    DB::table('chilorders')->where('status',1)->where('order_id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );
                }elseif($item->options->order_type == "shipping"){

                    DB::table('packings')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'assurance' => $item->options->assurance,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "prd_service"){

                    DB::table('ship_services')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "return_prd"){

                    DB::table('return_prds')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "ship_direct"){

                    DB::table('directshippings')->where('id',$item->id)->update(
                        array(
                            'status'  => 1,
                            'assurance' => $item->options->assurance,
                            'updated_at' => Now()
                        )
                    );
                }
            }
            \Session::put('pay_success','Payment success');
            return Redirect::route('paywithpaypal');
        }
        //return $result->result;
        \Session::put('pay_error','Payment failed');
		return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus(){
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID ***/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('pay_error','Payment failed');
            return Redirect::route('paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        //print_r($result); /** DEBUG RESULT, remove it later
        if ($result->getState() == 'approved') {
            /** it's all right **/
            $transaction = $result->getTransactions();
            $operation = Transact::create([
                'user_id' => Auth::user()->id,
                'transaction_code'    => $result->getCart(),
                'transaction_amount'  => floatval($transaction[0]->amount->total),
                'currency'            => '$',
                'transaction_orders'  => count(Cart::instance('collectandship')->content()),
                'transaction_payment' => 'Paypal'
            ]);

            //dd(Cart::instance('collectandship')->content());
            foreach(Cart::instance('collectandship')->content() AS $item){

                if($item->options->assurance == "true"){
                    if($item->options->product_price >= 100){
                        if($item->options->product_price * 0.02 < 20 ){
                            $assurance = 20 ;
                        }else{
                            $assurance = $item->options->product_price * 0.02 ;
                        }
                    }else{
                        $assurance = 0;
                    }
                    $assu_price = $assurance;
                    $assurance_text = "true";
                }elseif($item->options->assurance == "false"){
                    $assu_price = 0;
                    $assurance_text = "false";
                }else{
                    $assurance_text = "false";
                    $assu_price = 0;
                }

                if($item->options->currency !='$'){
                    $price = ($item->price + $item->options->servicecost) * 0.266667;
                }else{
                    $price = $item->price + $item->options->servicecost;
                }

                $price_tot = $price + $assu_price;

                $detail = Detail::create([
                    'transact_id'  => $operation->id,
                    'code_order'   => $item->name,
                    'service_name' => $item->options->order_type,
                    'order_price'  => $price_tot,
                    'currency'     => "$",
                    'assurance'    => $assurance_text,
                    'status' => 1,
                ]);

                if($item->options->order_type == "purchase"){
                    DB::table('orders')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );
                    DB::table('chilorders')->where('status',1)->where('order_id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );
                }elseif($item->options->order_type == "shipping"){

                    DB::table('packings')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'assurance' => $item->options->assurance,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "prd_service"){

                    DB::table('ship_services')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "return_prd"){

                    DB::table('return_prds')->where('id',$item->id)->update(
                        array(
                            'status'  => 2,
                            'updated_at' => Now()
                        )
                    );

                }elseif($item->options->order_type == "ship_direct"){

                    DB::table('directshippings')->where('id',$item->id)->update(
                        array(
                            'status'  => 1,
                            'assurance' => $item->options->assurance,
                            'updated_at' => Now()
                        )
                    );
                }
            }

            \Session::put('pay_success','Payment success');
            return Redirect::route('paywithpaypal');
        }
        \Session::put('pay_error', __('account.pay_failed'));
        return Redirect::route('paywithpaypal');
    }
}
