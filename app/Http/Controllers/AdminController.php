<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;
use Response;
use App\Admin;
use App\Setting;
use App\Address;
use App\Service;
use App\Transact;
use App\Detail;
use App\categorie;
use App\store;
use App\Return_prd;
use App\Packing;
use App\Shipping;
use App\ShipService;
use App\Order;
use App\Directshipping;
use Mail;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packings      = Packing::where('status',0)->get();
        $waiting       = Packing::where('status',2)->get();
        $services      = ShipService::whereIn('status',[0])->paginate(15);
        $parcels       = Shipping::where('status',5)->get();
        $orders_new    = Order::where('status',0)->get();
        $order_process = Order::where('status',2)->get();
        $new_direct    = Directshipping::Where('status',1)->get();
        $rec_direct    = Directshipping::Where('status',2)->get();
        $date = \Carbon\Carbon::today()->subDays(7);
        $clients = User::where('created_at', '>=', $date)->get();
        return view('control.index',compact('packings','waiting','services','parcels','orders_new','order_process','new_direct','rec_direct','clients'));
    }

    public function profile(){
        return view('control.profile');
    }

    public function clients (Request  $request){
        if($request->code){
            $clients = User::where('policia',trim($request->code))->get();
        }else{
            $clients = User::paginate(50);
        }
        return view('control.clients',compact("clients"));
    }
    public function operaccount(Request $request){
        $idusers = $request->idusers;
        $status = $request->status;
        if($idusers && $status){
            DB::table('users')->where('id', $idusers)->update(array('status' => $status));
            return Response::json(array('success' => true), 200);
        }
    }

    public function search(Request $request){

        $search = $request->search;
        if($search){

            $clients = User::where('policia', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search. '%')
            ->orWhere('firstname', 'LIKE', '%' . $search. '%')
            ->orWhere('lastname', 'LIKE', '%' . $search. '%')
            ->get();

            $viewRendered = view('control.partial.client',compact('clients'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

    public function users(){
        $admins = Admin::get();
        return view('control.settings.users',compact('admins'));
    }
    public function update(Request $request){
        $admin_id = $request->admin_id;
        $status = $request->status;
        if($admin_id && $status){
            if($status == 2 ){ $status = 0;}
            $admin = Admin::Where('id',$admin_id)->update(['status' => $status]);
            return Response::json(array('success' => true), 200);
        }
    }
    public function edituser($id){
        $admin = Admin::findOrFail($id);
        return view('control.settings.edituser',compact('admin'));
    }
    public function editsave($id , Request $request){
        $admin = Admin::findOrFail($id);
        $data = $request->all();
        if($file = $request->file('image')){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('assets/admin/img');
            $file->move($destinationPath, $name);
        }else{
            $name = $admin->image;
        }
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $admin->password;
        }

        $data['image'] = $name;
        $admin->fill($data)->save();
        return back()->with('success','تم تحديث المعلومات المطلوبة ');
    }
    public function general(){
        $general = Setting::first();
        return view('control.settings.general',compact('general'));
    }

    public function saveSettings(Request $request){

        $sitename = $request->sitename;
        $facebook = $request->facebook;
        $instagram = $request->instagram;
        $phone = $request->phone;
        $maps_iframe = $request->maps_iframe;
        $twitter = $request->twitter;
        $address = $request->address;
        $taux_directship = $request->taux_directship;
        $contact_email = $request->contact_email;
        $description = $request->description;
        $sitename_en = $request->sitename_en;
        $address_en = $request->address_en;
        $description_en = $request->description_en;
        $countrie = $request->countrie;
        $countrie_en = $request->countrie_en;
        $working = $request->working;
        $working_en = $request->working_en;

        if($file = $request->file('sitelogo')){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload');
            $file->move($destinationPath, $name);
        }else{
            $name = $request->logo;
        }

        DB::table('settings')
        ->where('id', $request->id)
        ->update(
            array(
                'sitename' => $sitename,
                'facebook' => $facebook,
                'instagram' => $instagram,
                'phone' => $phone,
                'maps_iframe' => $maps_iframe,
                'working' => $working,
                'working_en' => $working_en,
                'twitter' => $twitter,
                'address' => $address,
                'taux_directship' => $taux_directship,
                'countrie' => $countrie,
                'countrie_en' => $countrie_en,
                'contact_email' => $contact_email,
                'description' => $description,
                'sitename_en' => $sitename_en,
                'address_en' => $address_en,
                'description_en' => $description_en,
                'sitelogo'       => $name
            )
        );

        return back()->with('success','تم تحديث المعلومات المطلوبة ');

    }

    public function transaction(){
        $transaction = Transact::get();
        return view('control.transaction',compact('transaction'));
    }
    public function showOrders(Request $request){
        $showorder = $request->transid;
        if($showorder){

            $transaction = Transact::Where('id',$showorder)->get();

            $viewRendered = view('control.partial.detail',compact('transaction'))->render();
            return Response::json(['html'=>$viewRendered]);

        }
    }

    public function refund(Request $request){
        $transact_id = $request->transact_id;
        $refunded = $request->refunded;
        if($transact_id && $refunded){
            $transaction = Transact::Where('id',$transact_id)->update(['transaction_refund' => $refunded]);
            return Response::json(array('success' => true), 200);
        }
    }

    public function list(){
        $address = Address::get();
        return view('control.address.index',compact('address'));
    }

    public function create(){
        return view('control.address.create');
    }

    public function store(Request $request){
        $data = $request->all();
        $address = Address::create($data);
        return back()->with('success','تمت اضافة عنوان الشحن المطلوب');
    }

    public function services(){
        $services = Service::get();
        return view('control.service.index',compact('services'));
    }
    public function addservice(){
        return view('control.service.create');
    }
    public function Storeservice(Request $request){
        $data = $request->all();
        $service = Service::create($data);
        return back()->with('success','تمت ضافة الخدمة المطلوبة بنجاح');
    }

    public function editaddress(Request $request){
        $address = Address::where('id',$request->id)->first();
        return view('control.address.edit',compact('address'));
    }
    public function updateaddress(Request $request){
        $address = Address::where('id',$request->id)->first();
        $address->fill($request->all())->save();
        return back()->with('success','تم تحديث عنوان الشحن المطلوب ');
    }

    public function action(Request $request){
        $addressid = $request->addressid;
        $status = $request->status;
        if($addressid){

            $address = DB::table('addresses')
            ->where('id', $addressid)
            ->update(
                array(
                    'status' => $status
                )
            );

            return Response::json(array('success' => true), 200);

        }


    }

    //modepay
    public function modepay(Request $request){
        $general = Setting::first();
        return view('control.settings.modepay',compact('general'));
    }

    public function savemodepay(Request $request){

        DB::table('settings')
        ->where('id', $request->id)
        ->update(
            array(
                'paypal_client_id' => $request->paypal_client_id,
                'paypal_secret' => $request->paypal_secret,
                'paypal_mode' => $request->paypal_mode,
                'paytabs_merchant_email' => $request->paytabs_merchant_email,
                'paytabs_secret_key' => $request->paytabs_secret_key
            )
        );

        return back()->with('success','تم تحديث المعلومات المطلوبة ');

    }

    public function stores(){
        $stores = store::get();
        return view('control.stores.index',compact('stores'));
    }
    public function addcategory(Request $request){
        $catname = $request->catname;
        $catname_en = $request->catname_en;
        if($catname && $catname_en){

            categorie::create([
                'catname' => $catname ,
                'catname_en' => $catname_en ,
            ]);

            return Response::json(array('success' => true), 200);

        }
    }

    public function newstore(){
        $categories = categorie::get();
        return view('control.stores.create',compact('categories'));
    }

    public function addstore(Request $request){
        $data = $request->all();
        if($file = $request->file('store_logo')){

            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/stores');
            $file->move($destinationPath, $name);

            $store = store::create([
                'categorie_id'      =>    $data['categorie_id'],
                'storename'         =>    $data['storename'],
                'storename_en'      =>    $data['storename_en'],
                'store_url'         =>    $data['store_url'],
                'store_logo'        =>    $name,
                'store_desc'        =>    $data['store_desc'],
                'store_desc_en'     =>    $data['store_desc_en'],
            ]);

            return back()->with('success','تمت اضافة المتجر المطلوب بنجاج ');

        }
    }


    public function returntrack(Request $request){
        //dd($request->all());
        Return_prd::where('id',$request->returnid)->update(
            array(
                'tracking_url'  => $request->company_urls,
                'tracking_code' => $request->company_track,
                'status'  => 3 ,
                'updated_at'  => Now()
            )
        );

       return Response::json(array('success' => true), 200);
    }


    public function sendemail(Request $request){

        $data = $request->all();

        $client = User::where('id',$data['user_id'])->first();

        $to_name   = $client->firstname;
        $to_email  = $client->email;
        $content   = array('fullname' => $client->firstname , "body" => $data['message']);

        $subject   = $data['subject'];

        Mail::send('emails.contact', $content, function($message) use ($to_name, $to_email , $subject) {
            $message->to($to_email, $to_name)->subject($subject);
            $message->from("support@collectandship.com","collectandship");
        });

        return Response::json(array('success' => true), 200);

    }

    public function newsletter(Request $request){

        $data = $request->all();

        $clients = User::where('lang','LIKE',$data['message_lang'])->where('newsletter',0)->get();
        foreach($clients AS $client){

            $to_name   = $client->firstname;
            $to_email  = $client->email;
            $content   = array('fullname' => $client->firstname , "body" => $data['message_text']);

            $subject   = $data['subject_text'];

            Mail::send('emails.'.$data['message_lang'].'.contact', $content, function($message) use ($to_name, $to_email , $subject) {
                $message->to($to_email, $to_name)->subject($subject);
                $message->from("support@collectandship.com","collectandship");
            });

        }

        return Response::json(array('success' => true), 200);

    }


    public function updat_service(Request $request){
        $service_id = $request->service_id;
        $status = $request->status;
        if($service_id && $status){
            if($status == 2){ $status = 0 ;}
            $service = Service::where('id', $service_id)
            ->update(
                array(
                    'status' => $status
                )
            );
            return Response::json(array('success' => true), 200);
        }
    }

    public function clienttransact($id){
        $transaction = Transact::where('user_id',$id)->get();
        return view('control.client_transact',compact('transaction'));
    }

}
