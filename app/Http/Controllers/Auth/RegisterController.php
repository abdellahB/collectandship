<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use \Mcamara\LaravelLocalization\Facades\LaravelLocalization;

use App\Http\helpers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Response;
use Session;
use Carbon\Carbon;
use App\Mail\Sendmail;
use Mail;
use App\Useradress;
use App\Address;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    //use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function register(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|confirmed|string|min:8|',
            'countrie_id' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($validator->fails()){
            return Response::json(array(
                //'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),422); // 400 being the HTTP code for an invalid request.
        }

        $user = User::create([
            'firstname'   => $data['firstname'],
            'lastname'    => $data['lastname'],
            'email'       => $data['email'],
            'countrie_id' => $data['countrie_id'],
            //'city'        => $data['city'],
            //'address'     => $data['address'],
            'verifyToken' => Str::random(60),
            'lang'        => LaravelLocalization::getCurrentLocale(),
            'password'    => bcrypt($data['password']),
            'policia'     => $this->createPolicia(),
            'status'      => 1
            //'zipcode'     => $data['zipcode'],
            //'phone'       => $data['phone']
        ]);

        /*$new = Useradress::create([
            'user_id' =>  $user->id,
            'countrie_id' => $data['countrie_id'],
            'firstname'  => $data['firstname'],
            'lastname'   => $data['lastname'],
            'city'       => $data['city'],
            'province'   => '-',
            'zip_code'   => $data['zipcode'],
            'adress'     => $data['address'],
            'phone'      => $data['phone']
        ]);*/

        /*$data = [
            'resp'   => $user,
            'status' => 'activate',
            'lang'   => LaravelLocalization::getCurrentLocale()
        ];
        Mail::to($user->email)->send(new Sendmail($data));*/
        $address   = Address::where('status',0)->get();

        $to_name   = $user->firstname;
        $to_email  = $user->email;
        $content   = array('fullname' => $user->lastname.' '.$user->firstname,'address' => $address , 'resp'=> $user);

        if($user->lang == "en"){
            $subject   = "Your warehouse at collectandship.com is ready to use.";
        }else{
            $subject   = "مستودعك في collectandship.com جاهز للإستخدام.";
        }

        Mail::send('emails.'.$user->lang.'.new_account', $content, function($message) use ($to_name,$to_email,$subject) {
            $message->to($to_email, $to_name)->subject($subject);
            $message->from("support@collectandship.com","collectandship");
        });

        return Response::json(array('success' => true), 200);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    /*protected function create(array $data)
    {

    }*/

    public function verify($token){
        $address   = Address::where('status',0)->get();
        $user      = User::where('verifyToken',$token)->first();
        if($user){
            User::where('verifyToken',$token)->update(array('status' => 1));
           // Mail::to($user->email)->send(new Sendmail($data));
        }
        return redirect('/')->with('activated', 1);
    }

    public function reset(Request $request){
        $email  = $request->email;
        if($email){

            $user = User::where('email',$email)->first();

            if($user){

                $to_name   = $user->firstname;
                $to_email  = $user->email;
                $content   = array('fullname' => $user->lastname.' '.$user->firstname,'body' => $user);

                if($user->lang == "en"){
                    $subject   = "Change the account password";
                }else{
                    $subject   = "collectandship.com  لقد طلبت إعادة تعيين كلمة المرور لحسابك على موقع";
                }

                Mail::send('emails.'.$user->lang.'.reset', $content, function($message) use ($to_name,$to_email,$subject) {
                    $message->to($to_email, $to_name)->subject($subject);
                    $message->from("support@collectandship.com","collectandship");
                });

                return Response::json(array('success' => true), 200);

            }

        }
    }

    public function resetpassword($token){
        $user = User::where('verifyToken',$token)->first();
        return redirect('/')->with('resetpassword', $user->id);
    }
    public function changepassword(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'password' => 'required|confirmed|string|min:7|',
            'user_id' => 'required'
        ]);

        if ($validator->fails()){
            return Response::json(array(
                //'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ),422); // 400 being the HTTP code for an invalid request.
        }

        User::where('id',$data['user_id'])->update(
            array(
                'password' => bcrypt($data['password'])
                )
            );

        return Response::json(array('success' => true), 200);

    }
    public function createPolicia()
    {
        $policia = generateRandomString(6);
        $allPlocial = $this->getPolicia($policia);
        while($allPlocial->contains('policia', $policia)){
            $policia = generateRandomString(6);
        }
        return $policia;
    }
    protected function getPolicia($policia)
    {
        return User::select('policia')->where('policia', 'like', $policia.'%')->get();
    }
}
