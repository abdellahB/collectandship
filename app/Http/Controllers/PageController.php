<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Page;
class PageController extends Controller
{
    //
    public function __construct(){
       $this->middleware('auth:admin');
    }

    public function terms(){
        $terms = Page::where('id','=',2)->first();
        return view('control.pages.terms',compact('terms'));
    }
    public function privacy(){
        $privacy = Page::where('id','=',3)->first();
        return view('control.pages.privacy',compact('privacy'));
    }
    public function about(){
        $about = Page::where('id','=',1)->first();
        return view('control.pages.about',compact('about'));
    }

    public function methode(){
        $methode = Page::where('id','=',4)->first();
        return view('control.pages.methode',compact('methode'));
    }

    public function saveterms(Request $request){
        $content_en = $request->content_en;
        $content_ar = $request->content_ar;
        $idterms = $request->idterms;
       if($content_en && $content_ar && $idterms){

            DB::table('pages')->where('id', $idterms)
            ->update(
                array(
                    'content_en' => $content_en,
                    'content_ar' => $content_ar
                )
            );
            return back();

       }
    }

    public function saveprivacy(Request $request){

        $content_en = $request->content_en;
        $content_ar = $request->content_ar;
        $idprivacy = $request->idprivacy;
       if($content_en && $content_ar && $idprivacy){

            DB::table('pages')->where('id', $idprivacy)
            ->update(
                array(
                    'content_en' => $content_en,
                    'content_ar' => $content_ar
                )
            );
            return back();

       }

    }

    public function saveabout(Request $request){
        $content_en = $request->content_en;
        $content_ar = $request->content_ar;
        $idabout = $request->idabout;
       if($content_en && $content_ar && $idabout){

            DB::table('pages')->where('id', $idabout)
            ->update(
                array(
                    'content_en' => $content_en,
                    'content_ar' => $content_ar
                )
            );
            return back();

       }
    }

    //
    public function savemethode(Request $request){
        $content_en = $request->content_en;
        $content_ar = $request->content_ar;
        $idmethode = $request->idmethode;
       if($content_en && $content_ar && $idmethode){

            DB::table('pages')->where('id', $idmethode)
            ->update(
                array(
                    'content_en' => $content_en,
                    'content_ar' => $content_ar
                )
            );
            return back();

       }
    }
}
