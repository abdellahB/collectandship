<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\helpers;

use App\Ship_image;
use App\Shipping;
use App\Packing;
use App\PackTracking;
use Response;
use App\Imgpacking;
use App\ShipService;
use App\Return_prd;
use App\User;
use App\ShipContent;
//use App\Mail\Sendmail;
use Mail;
use App\Directshipping;
use App\Service_img;
class ShipController extends Controller
{
    //

    public function __construct()
    {
       $this->middleware('auth:admin');
    }

    public function imports(){
        return view('control.shipping.import');
    }
    public function GetallUser(Request $request){
        $type = $request->type;
        if($type == "codeclient"){
            $codeclient = $request->codeclt;
            $users  = DB::table('users')->select('policia')
                    ->where('users.policia','like','%'.strtoupper($codeclient).'%')->get();
            $data = array();
            foreach ($users as $user) {
                array_push($data,$user->policia);
            }
            return response()->json($data);
        }
    }
    public function signleUser(Request $request){
        $policia = $request->policia;
        $users  = DB::table('users')
                    ->join('countries', 'countries.id', '=', 'users.countrie_id')
                    ->where('users.policia',$policia)
                    ->select('users.*','countries.countriename')->get();
        return response()->json($users);
    }

    protected function makeOrderCode(){
        $code = DB::table('shippings')->select('codeparcel')->orderBy('id', 'desc')->first();
        if($code){
            $int=substr($code->codeparcel,3);
            $int=(int)$int+1;
            return  $code="WP-".$int;
        }else{
           return $code="WP-1000";
        }
    }

    public function add_import(Request $request){
        $data = $request->all();

        $identity = $request->identity;
        if($identity != -1){
            //$data['user_id'] = '';
        }
        $unity = $request->unity;
        //dd($unity);
        if($unity != 1){
            $data['weight'] = floatval(number_format(str_replace(',', '.',$data['weight']) * 0.453592,1));
            $data['lenght'] = floatval(number_format(str_replace(',', '.',$data['lenght']) * 2.54,1));
            $data['width']  = floatval(number_format(str_replace(',', '.',$data['width']) * 2.54,1));
            $data['height'] = floatval(number_format(str_replace(',', '.',$data['height']) * 2.54,1));
        }
        $data['codeparcel'] = $this->makeOrderCode();
        $data['fullname'] = $data['firstname'] .''. $data['lastname'];
        $import = Shipping::create($data);

        $files = $request->file('image_file');
        foreach($files as $file){
            $name = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('upload/ship');
            $file->move($destinationPath, $name);
            Ship_image::create([
                'imagename'     =>  $name,
                'shipping_id'   => $import->id,
                'type'          => 'photo'
            ]);
        }

        if($identity == -1){

            $to_name   = $import->getuser->firstname;
            $to_email  = $import->getuser->email;
            $content   = array('fullname' => $import->getuser->lastname.' '.$import->getuser->firstname,'body' => $import);

            if($import->getuser->lang == "en"){
                $subject   = "Receive shipment number ".$import->codeparcel;
            }else{
                $subject   = "استلام شحنة رقم ".$import->codeparcel;
            }

            Mail::send('emails.'.$import->getuser->lang.'.new_shipment', $content, function($message) use ($to_name,$to_email,$subject) {
                $message->to($to_email, $to_name)->subject($subject);
                $message->from("support@collectandship.com","collectandship");
            });

        }

        return back()->with('success_import','تمت اضافة المنتج لحساب العميل بنجاح');
    }
    public function unknow(){
        $parcels = Shipping::whereNull('user_id')->where('status',0)->get(); //unknow parcel
        return view('control.shipping.unknow',compact('parcels'));
    }
    public function Supunknow(Request $request){

        $shippings = DB::table('shippings')
            ->where('id', $request->unknow)
            ->update(
                array(
                    'status' => 10
                )
            );

            return Response::json(array('success' => true), 200);
    }
    public function unknow_update(Request $request){
        $user = User::where('policia',trim($request->userpolicia))->first();
        if($user){
            $shippings = DB::table('shippings')
            ->where('id', $request->shipping)
            ->update(
                array(
                    'user_id' => $user->id
                )
            );
            return Response::json(array('success' => true), 200);
        }else{
            return Response::json(array('success' => false), 200);
        }
    }
    public function all(){
        $parcels = Shipping::Where('user_id','!=','')->whereIn('status',[0,5,10])->get();
        return view('control.shipping.all',compact('parcels'));
    }

    //orders
    public function new(){
        $packings = Packing::where('status',0)->get();
        return view('control.shipping.orders.new',compact('packings'));
    }

    //
    public function getlist(request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packings = Packing::where('id',$packing_id)->get();
            $viewRendered = view('account.partial.list',compact('packings'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

    public function packing (Request $request){

        $data = $request->all();
        $packing = Packing::where('id',$data['packign_id'])->first();
        //$country = $packing->getAddress->countrie_id;
        //$ship_country = $packing->ship_country;
        $company = $packing->companie_id;
        $service_p=0;
        foreach ($packing->getchild as $child){
            $service_p += $child->getService->service_price;
        }
        $service_p = floatval($service_p);

        if($data['ship_unity'] !='kg'){
            $data['weight'] = number_format(str_replace(',', '.',$data['weight']) * 0.453592,1);
            $data['length'] = number_format(str_replace(',', '.',$data['length']) * 2.54,1);
            $data['width']  = number_format(str_replace(',', '.',$data['width']) * 2.54,1);
            $data['height'] = number_format(str_replace(',', '.',$data['height']) * 2.54,1);
        }

        $weight     =  number_format($data['weight'],1,'.','');
        $dimenssion =  number_format(($data['length'] * $data['width'] * $data['height'])/5000,1,'.','');

        if($dimenssion > $weight){
            $weight_to_use = flaotNnumber($dimenssion);
        }elseif($dimenssion < $weight ){
            $weight_to_use = flaotNnumber($weight);
        }


        $companies  =  DB::table('companies')
        ->join('shipamounts', 'companies.id', '=', 'shipamounts.companie_id')
        //->where('companies.countrie_id','=',$country)
        //->where('companies.ship_country',$ship_country)
        ->where('companies.id','=',$company)
        ->where('shipamounts.weightcompany','=',$weight_to_use)
        ->first();

        if($companies){
            $priceTotal = $companies->pricecompany + $service_p;
            Packing::where('id',$data['packign_id'])->update(
                array(
                    'weight' =>  $data['weight'],
                    'length' =>  $data['length'],
                    'width'  =>  $data['width'],
                    'height' =>  $data['height'],
                    'pricetotal' => $priceTotal,
                    'status' => 1
                )
            );
            $files = $request->file('images');
            foreach($files as $file){
                $images = time().'-'.$file->getClientOriginalName();
                $destinationPath = public_path('upload/ship');
                $file->move($destinationPath, $images);

                $image = Imgpacking::create([
                    'packing_id' => $data['packign_id'],
                    'imagename'  => $images
                ]);
            }

            $to_name   = $packing->getUser->firstname;
            $to_email  = $packing->getUser->email;
            $content   = array('fullname' => $packing->getUser->lastname.' '.$packing->getUser->firstname,'body' => $packing);

            if($packing->getUser->lang == "en"){
                $subject   = "Your shipments were consolidated ".$packing->code_parcel;
            }else{
                $subject   = "تم تجميع شحنات الطلب رقم  ".$packing->code_parcel;
            }

            Mail::send('emails.'.$packing->getUser->lang.'.package_ready_to_pay', $content, function($message) use ($to_name,$to_email,$subject) {
                $message->to($to_email, $to_name)->subject($subject);
                $message->from("support@collectandship.com","collectandship");
            });

            return Response::json(array('success' => true), 200);

        }else{
            return Response::json(array('success' => false), 200);
        }

    }

    public function payment(){
        $packings = Packing::where('status',1)->get();
        return view('control.shipping.orders.payment',compact('packings'));
    }

    public function waiting(){
        $packings = Packing::where('status',2)->get();
        return view('control.shipping.orders.waiting',compact('packings'));
    }

    public function shipped(){
        $packings = Packing::whereIn('status',[3,4])->get();
        return view('control.shipping.orders.shipped',compact('packings'));
    }

    public function tracking_pack (Request $request){
        $packing_id    = $request->packing_id;
        $mode_shipping = $request->mode_shipping;
        $tracking_info = $request->tracking_info;
        //dd($request->all());
        if($packing_id  && $tracking_info && $mode_shipping){

            Packing::where('id',$packing_id)->update(
                array(
                    'companie_id' => $mode_shipping,
                    'status'  => 3 ,
                    'updated_at'  => NOW() ,
                )
            );

            $packing   = Packing::where('id',$packing_id)->first();

            $companie  =  DB::table('companies')->Where('id',$packing->companie_id)->first();

            $track = PackTracking::create([
                'packing_id' => $packing_id ,
                'company_url' => $companie->company_url ,
                'tracking_info' => $tracking_info
            ]);

            foreach ($packing->child_packing as $child){

                DB::table('ship_services')
                    ->where('status',0)
                    ->Where('shipping_id',$child->shipping_id)
                    ->update(array('status'  => 3));

            }

            $to_name   = $packing->getUser->firstname;
            $to_email  = $packing->getUser->email;
            $content   = array('fullname' => $packing->getUser->lastname.' '.$packing->getUser->firstname,'body' => $packing);

            if($packing->getUser->lang == "en"){
                $subject   = "Order ".$packing->code_parcel." was shipped";
            }else{
                $subject   = "تم شحن الطلب رقم ".$packing->code_parcel;
            }

            Mail::send('emails.'.$packing->getUser->lang.'.package_shipped', $content, function($message) use ($to_name,$to_email,$subject) {
                $message->to($to_email, $to_name)->subject($subject);
                $message->from("support@collectandship.com","collectandship");
            });

            return Response::json(array('success' => true), 200);

        }
    }

    public function return(){
        $parcels = Shipping::where('status',5)->get();
        return view('control.shipping.return',compact('parcels'));
    }
    public function returnprice(Request $request){

        if($request->price_topay == 0){
            $status = 2;
        }else{

            $status = 1;

            $return = Return_prd::where('id',$request->return_id)->first();
            //
            $to_name   = $return->getShip->getuser->firstname;
            $to_email  = $return->getShip->getuser->email;
            $content   = array('fullname' => $return->getShip->getuser->lastname.' '.$return->getShip->getuser->firstname,'body' => $return);

            if($return->getShip->getuser->lang == "en"){
                $subject   = "You package Return request is ready for payment ".$return->code_return;
            }else{
                $subject   = "طلبك لإعادة الشحنة جاهز لدفع رسوم الشحن ".$return->code_return;
            }

            Mail::send('emails.'.$return->getShip->getuser->lang.'.return_request', $content, function($message) use ($to_name,$to_email,$subject) {
                $message->to($to_email, $to_name)->subject($subject);
                $message->from("support@collectandship.com","collectandship");
            });

        }
        Return_prd::where('id',$request->return_id)->update(
            array(
                'pricetopay'  => $request->price_topay,
                'status'      => $status,
                'updated_at'  => Now()
            )
        );

        return Response::json(array('success' => true), 200);

    }

    public function moreimage(){
        $services = ShipService::whereIn('status',[0,1,2,3])->paginate(15);
        return view('control.shipping.images',compact('services'));
    }

    public function addimages(Request $request){
        //$product_id = $request->product_id;
        $service_id = $request->service_id;
        $service_s  = $request->service_s;
        if($service_s != 1){

            if($request->file('more_video')){
                $file = $request->file('more_video');
                $name = time().'-'.$file->getClientOriginalName();
                $destinationPath = public_path('upload/ship');
                $file->move($destinationPath, $name);

                Service_img::create([
                    'imagename'         =>  $name,
                    'ship_service_id'   =>  $service_id,
                    'type'              =>  'video'
                ]);

            }

            if($request->file('images')){
                $files = $request->file('images');
                foreach($files as $file){

                    $name = time().'-'.$file->getClientOriginalName();
                    $destinationPath = public_path('upload/ship');
                    $file->move($destinationPath, $name);

                    Service_img::create([
                        'imagename'         =>  $name,
                        'ship_service_id'   =>  $service_id,
                        'type'              =>  'photo'
                    ]);

                }
            }
        }
        $service = ShipService::where('id',$service_id)->first();
        ShipService::where('id',$service_id)->update(array('status'  => 1));

        $to_name   = $service->getUser->firstname;
        $to_email  = $service->getUser->email;
        $content   = array('fullname' => $service->getUser->lastname.' '.$service->getUser->firstname,'body' => $service);

        if($service->getUser->lang == "en"){
            $subject   = "Your service Request was completed ".$service->code_service;
        }else{
            $subject   = "تم تنفيذ طلب الخدمة  ".$service->code_service;
        }

        Mail::send('emails.'.$service->getUser->lang.'.service_request', $content, function($message) use ($to_name,$to_email,$subject) {
            $message->to($to_email, $to_name)->subject($subject);
            $message->from("support@collectandship.com","collectandship");
        });

       return Response::json(array('success' => true), 200);


    }

    public function rm_product (Request $request){
        $product_id = $request->product_id;
        $status     = $request->status;
        if($product_id){
            Shipping::where('id',$product_id)->update(array('status'  => $status));
            return Response::json(array('success' => true), 200);
        }
    }


    // direct shipping ...
    public function neworder(){
        $orders = Directshipping::Where('status',1)->get();
        return view('control.shipping.direct.new',compact('orders'));
    }
    public function recerved(){
        $orders = Directshipping::Where('status',2)->get();
        return view('control.shipping.direct.recerved',compact('orders'));
    }
    public function ship(){
        $orders = Directshipping::Where('status',3)->get();
        return view('control.shipping.direct.shipped',compact('orders'));
    }

    public function detail_ship(Request $request){
        $orders = Directshipping::where('id',$request->id_ship)->get();
        $viewRendered = view('account.partial.detail_direct',compact('orders'))->render();
        return Response::json(['html'=>$viewRendered]);
    }

    public function ship_recerved(Request $request){

        DB::table('directshippings')
        ->where('id', $request->id_ship)
        ->update(
            array(
                'status' => 3
            )
        );

        return Response::json(array('success' => true), 200);

    }

    public function tracking_direct(Request $request){

        DB::table('directshippings')
        ->where('id', $request->directship_id)
        ->update(
            array(
                'company_url' => $request->company_url,
                'tracking_number' => $request->tracking_number_direct,
                'status' => 2
            )
        );

        return Response::json(array('success' => true), 200);

    }


    public function shipdetail(Request $request){
        $shipment_id = $request->shipment_id;
        if($shipment_id){
            $ship = Shipping::where('id',$shipment_id)->first();
            $viewRendered = view('control.partial.ship',compact('ship'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

    public function serivcedetail(Request $request){
        $service_id = $request->service_id;
        if($service_id){
            $service = ShipService::where('id',$service_id)->first();
            $viewRendered = view('control.partial.image',compact('service'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }


    public function changeInfo(Request $request){
        $packing_id = $request->packing_id;
        if($packing_id){
            $packing    =  Packing::where('id',$packing_id)->first();
            $companies  =  DB::table('companies')
            ->where('companies.countrie_id','=',$packing->getAddress->countrie_id)
            ->where('companies.ship_country',$packing->ship_country)->get();
            $viewRendered = view('control.partial.changeinfo',compact('packing','companies'))->render();
            return Response::json(['html'=>$viewRendered]);
        }
    }

}
