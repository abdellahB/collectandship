<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imgpacking extends Model
{
    //

    protected $fillable = [
        'packing_id' , 'imagename'
    ];
}
