<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Return_prd extends Model
{
    //

    protected $fillable = [
       'code_return' , 'shipping_id' , 'seller_address' , 'return_info' , 'pricetopay' , 'tracking_url' , 'tracking_code' , 'status'
    ];

    public function getShip(){
        return $this->belongsTo('App\Shipping','shipping_id');
    }
}
