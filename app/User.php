<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','email','password','countrie_id','city','address','zipcode',
        'phone','thumbnail','policia','status','verifyToken','lang','newsletter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getcountry(){
        return $this->HasOne('App\Countrie','id','countrie_id');
    }

    public function getAddress(){
        return $this->HasOne('App\Useradress','user_id');
    }
}
