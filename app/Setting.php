<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

    protected $fillable = [
        'sitename' , 'facebook' , 'instagram' , 'phone' , 'sitelogo' , 'twitter' , 'address' , 'taux_directship' ,
        'description' , 'sitename_en' , 'address_en' , 'description_en' , 'client_id' , 'secret' , 'contact_email',
        'countrie','countrie_en','working','working_en','maps_iframe','paypal_client_id','paypal_secret','paypal_mode','paytabs_merchant_email','paytabs_secret_key'
    ];
}
