<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    //

    protected $fillable = [
        'codeparcel','user_id','weight','width','height','tracking','date_arrival','lenght','shipcountry','status'
        ,'sellername','more','stockcode','company','description','fullname'
    ];


    public function getuser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function GetImage(){
        return $this->hasMany('App\Ship_image');
    }

    public function GetService(){
        return $this->hasMany('App\ShipService');
    }

    public function GetReturn(){
        return $this->hasOne('App\Return_prd');
    }

    public function GetContent(){
        return $this->hasMany('App\ShipContent');
    }

    /*public function getcompany(){
        return $this->hasMany('App\Importcompanie');
    }


    public function ch_packing(){
        return $this->belongsTo('App\Child_Packing');
    }

    public function gettracking(){
        return $this->hasone('App\Returninfo');
    }*/
}
