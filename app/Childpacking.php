<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Childpacking extends Model
{
    //
    protected $fillable = [
        'packing_id' , 'shipping_id' , 'product_name' , 'product_price' , 'status'
    ];

    public function Shipping(){
        return $this->hasOne('App\Shipping','id','shipping_id');
    }
}
