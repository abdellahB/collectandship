<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Useradress extends Model
{
    //

    protected $fillable = [
        'user_id' , 'countrie_id' ,'firstname' ,'lastname' ,'city','province','zip_code','adress','phone'
    ];


    public function getcountry(){
        return $this->HasOne('App\Countrie','id','countrie_id');
    }
}
