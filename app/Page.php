<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $fillable = [
        'title_en', 'title_ar', 'slug', 'content_en', 'content_ar', 'position'
    ];
}
