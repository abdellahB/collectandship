<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackTracking extends Model
{
    //

    protected $fillable =[
        'packing_id', 'company_url' , 'tracking_info'
    ];
}
