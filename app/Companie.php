<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    //

    protected $fillable = [
        'countrie_id','ship_country','companiename','companiename_en','companylogo','datedelevier','datedelevier_en','company_url','status'
    ];

    public function getComp_order(){
        //return $this->belongsTo('App\Order');
    }

    public function getprices(){
        return $this->hasMany('App\Shipamount');
    }

    public function getcountry(){
        return $this->hasOne('App\Countrie','id','countrie_id');
    }
}
