<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //

    protected $fillable =[
        'type_service','service_name' , 'service_price','service_des','service_name_en','service_des_en'
    ];
}
