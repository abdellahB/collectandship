<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chilorder extends Model
{
    //

    protected $fillable = [
        'order_id','product_name','product_url','product_qty','product_price','total_price','product_tax','product_taux',
        'product_shipping','product_des','currency','status'
    ];
}
