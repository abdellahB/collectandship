<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressCollect extends Model
{
    //

    protected $fillable = [
        'directshipping_id' , 'ship_country' ,'firstname' ,'lastname' ,'city','province','zipcode','address','phone'
    ];
}
