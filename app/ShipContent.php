<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipContent extends Model
{
    //
    protected $fillable = [
        'shipping_id' , 'product_name' , 'product_qty' , 'product_price'
    ];
}
