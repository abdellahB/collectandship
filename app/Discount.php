<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //

    protected $fillable = [
        'discountcode' , 'amount' , 'type' , 'numberperson' , 'numberutiliser' , 'startdate' , 'enddate' , 'status'
    ];
}
