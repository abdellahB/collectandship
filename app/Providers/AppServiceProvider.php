<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Countrie;
use App\Setting;
use App\Order;
use App\Shipping;
use App\Useradress;
use App\Service;
use App\Packing;
use App\Return_prd;
use App\ShipService;
use App\Directshipping;
use App\Address;
use App\User;
use Session;
use Cart;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        Session::put('currency', '$');

        view()->composer(
            [
                'auth.register','account.purchase.create',
                'auth.login','account.index','account.profile',
                'account.partial.modal','index','warehouse','account.checkout',
                'pricing','layouts.app','layouts.account','contact','account.directship'
            ],
            function($view){
                $countries   = Countrie::get();
                $setting     = Setting::first();
                $view->with(compact('countries','setting'));
            }
        );

        view()->composer(
            [
                'account.index',
                'account.shipping.product',
                'account.shipping.service',
                'account.checkout'
        ],function($view){
            $newaddress      = Useradress::Where('user_id',Auth::user()->id)->Where('status',0)->get();
            $services        = Service::where('status',0)->where('type_service','packing')->get();
            $prd_services    = Service::where('status',0)->where('type_service','product')->get();
            $address         = Address::where('status',0)->get();

            $service_cart = ShipService::where('user_id',Auth::user()->id)->where('status',1)->get();
            foreach ($service_cart AS $service){
                $cartItem = Cart::instance('collectandship')->add(
                    $service->id,$service->code_service,1,$service->getPservice->service_price,0,
                    [
                        "order_type" => 'prd_service',
                        "currency"   => '$',
                        "servicecost"   => 0,
                        "product_price" => 0,
                        'assurance'     => 'false'
                    ]
                );
            }
            $view->with(compact('newaddress','services','prd_services','address'));
        });

        view()->composer(['layouts.admin','control.index'],function($view){

            $item1 = Order::where('status',0)->count();
            $item2 = Order::where('status',1)->count();
            $item3 = Order::where('status',2)->count();
            $item4 = Order::where('status',3)->count();
            $item5 = Order::where('status',4)->count();

            $parcel = Shipping::Where('user_id','!=','')->where('status',0)->count();

            $image  = ShipService::where('status',0)->count();

            $return = Return_prd::whereIn('status',[0,2])->count();

            $packing  =  Packing::where('status',0)->count();
            $packing1 = Packing::where('status',1)->count();
            $packing2 = Packing::where('status',2)->count();
            $packing3 = Packing::whereIn('status',[3,4])->count();


            //direct
            $order1 = Directshipping::where('status',1)->count();
            $order2 = Directshipping::where('status',2)->count();
            $order3 = Directshipping::where('status',3)->count();

            $client = User::where('status',1)->count();

            $view->with(
                compact(
                    'item1','item2','item3','item4','item5',
                    'parcel','image','return',
                    'packing','packing1','packing2','packing3',
                    'order1' , 'order2' , 'order3',
                    'client'
                ));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
