<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class packing_service extends Model
{
    //

    protected $fillable = [
        'packing_id' , 'service_id'
    ];



    public function getService(){
        return $this->belongsTo('App\Service','service_id');
    }
}
