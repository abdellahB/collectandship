<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Sendmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        $address = 'support@collectandship.com';
        $name    = 'collectandship';

        if($this->data['status'] == 'acceptOrder'){

            if($this->data['lang'] == 'en'){

                $subject = "Approval of a purchase order number ".$this->data['resp']->code_order;
                return $this->view('emails.en.purchase.accept')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }else{

                $subject = "الموافقة على طلب شراء رقم ".$this->data['resp']->code_order;
                return $this->view('emails.ar.purchase.accept')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'ship_import'){

            if($this->data['lang'] == 'en'){

                $subject = "Receive shipment number".$this->data['resp']->codeparcel;
                return $this->view('emails.en.new_shipment')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }else{

                $subject = "استلام شحنة رقم ".$this->data['resp']->codeparcel;
                return $this->view('emails.ar.new_shipment')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'ship_packing'){

            if($this->data['lang'] == 'en'){

                $subject = "Shipments have been combined".$this->data['resp']->code_parcel;
                return $this->view('emails.en.package_ready_to_pay')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }else{

                $subject = "تم تجميع شحنات الطلب رقم ".$this->data['resp']->code_parcel;
                return $this->view('emails.ar.package_ready_to_pay')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'ship_shipped'){

            if($this->data['lang'] == 'en'){

                $subject = "Order shipped".$this->data['resp']->code_parcel;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }else{

                $subject = "تم شحن الطلب رقم ".$this->data['resp']->code_parcel;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }


        }elseif($this->data['status'] == 'ship_return_p'){

            if($this->data['lang'] == 'en'){

                $subject = "Return request pending payment".$this->data['resp']->code_return;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }else{

                $subject = "طلب ارجاع في انتظار الدفع رقم ".$this->data['resp']->code_return;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'ship_return_t'){

            if($this->data['lang'] == 'en'){

                $subject = "Request number returned".$this->data['resp']->code_return;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }else{

                $subject = "تمت اعادة الطلب رقم ".$this->data['resp']->code_return;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'ship_service'){

            if($this->data['lang'] == 'en'){

                $subject = "Purchases service was implemented".$this->data['resp']->code_service;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }else{

                $subject = "تم تنفيد خدمة مشتريات رقم ".$this->data['resp']->code_service;
                return $this->view('emails.notify')->from($address, $name)->subject($subject)
                ->with(['test_message' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'reset'){

            if($this->data['lang'] == 'en'){

                $subject = "Change the account password";
                return $this->view('emails.en.reset')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);


            }else{

                $subject = "تغيير كلمة مرور الحساب ";
                return $this->view('emails.ar.reset')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }

        }elseif($this->data['status'] == 'newaccount'){

            if($this->data['lang'] == 'en'){

                $subject = "Your warehouse at collectandship.com is ready to use.";
                return $this->view('emails.en.new_account')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp'],'address'=> $this->data['address']]);

            }else{

                $subject = "مستودعك في collectandship.com جاهز للإستخدام.";
                return $this->view('emails.ar.new_account')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp'],'address'=> $this->data['address']]);

            }


        }elseif ($this->data['status'] == 'sendemail'){

            $subject = $this->data['subject'];
            $address = $this->data['email'];
            $name    = $this->data['fullname'];

            return $this->view('emails.contact')
            ->from("support@collectandship.com", "collectandship")
            ->replyTo($address, $name)
            ->subject($subject)
            ->with(['content' => $this->data['message']]);


        }elseif ($this->data['status'] == 'activate'){

            if($this->data['lang'] == 'en'){

                $subject = "Activate your account";
                return $this->view('emails.en.verify')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }else{

                $subject = "تفعيل حسابك على الموقع";
                return $this->view('emails.ar.verify')->from($address, $name)->subject($subject)
                ->with(['content' => $this->data['resp']]);

            }

        }

    }
}
