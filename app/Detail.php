<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    //

    protected $fillable = [
        'transact_id' , 'code_order' , 'service_name' , 'order_price' , 'currency' ,'assurance', 'status'
    ];
}
