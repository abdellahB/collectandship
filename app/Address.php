<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //

    protected $fillable = [
        'Adressline' , 'city' , 'state' , 'zipcode' , 'country' , 'mobile' , 'status'
        //'country','fulladdress' , 'status'
    ];
}
