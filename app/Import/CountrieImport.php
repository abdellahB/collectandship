<?php

namespace App\Import;

use App\Countrie;
use Maatwebsite\Excel\Concerns\ToModel;

class CountrieImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Countrie([
            'countriename' => $row[0],
            'created_at'   => NOW(),
            'updated_at'   => NOW(),
        ]);
    }
}
