<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class store extends Model
{
    //

    protected $fillable = [
        'categorie_id' , 'storename' , 'storename_en' ,'store_url','store_logo' , 'store_desc' , 'store_desc_en' , 'status'
    ];

    public function getCategory(){
        return $this->belongsTo('App\categorie','categorie_id','id');
    }
}
