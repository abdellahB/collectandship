<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressDelivery extends Model
{
    //
    protected $fillable = [
        'directshipping_id' , 'countrie_id' ,'firstname' ,'lastname' ,'city','province','zipcode','address','phone','address_email'
    ];


    public function getcountry(){
        return $this->HasOne('App\Countrie','id','countrie_id');
    }
}
