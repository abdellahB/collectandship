<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transact extends Model
{
    //

    protected $fillable = [
        'user_id' , 'transaction_code' , 'transaction_amount' ,'transaction_refund', 'currency' , 'transaction_orders' , 'transaction_payment'
    ];


    public function getUser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getDetails(){
        return $this->HasMany('App\Detail');
    }
}
