<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipamount extends Model
{
    //

    protected $fillable = [
        'companie_id' , 'weightcompany' , 'pricecompany'
    ];

    public function getcomp(){
        return $this->belongsTo('App\Companie','companie_id','id');
    }
}
