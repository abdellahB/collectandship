<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_img extends Model
{
    //

    protected $fillable = [
        'imagename' , 'ship_service_id' , 'type'
    ];
}
